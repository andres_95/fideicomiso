/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.administracion.view;

import com.bimodal.app.conf.GlobalConstants;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import vista.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import com.bimodal.app.db.Modelo;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import com.bimodal.app.db.FSOSEG_CONNECT_MODULE;
import com.bimodal.util.ComboValue;
import com.bimodal.util.TextParse;
import controlador.TransactionRegistryController;
import javax.swing.JTextField;

/**
 *
 * @author usuario1
 */

public class FSOSEGTABFRM012 extends javax.swing.JFrame {
         TransactionRegistryController controlador;
         
         
         /**
     * Creates new form Embargo
     */
         
    Connection cc = GlobalConstants.getConnector().getConnection();
    public FSOSEGTABFRM012() throws SQLException {
        initComponents();
        setLocationRelativeTo(null);
        cerrar(); 
        Calendar cal = Calendar.getInstance();
        String hora = cal.get(cal.HOUR_OF_DAY)+":"+cal.get(cal.MINUTE);
        this.jLabel2.setText(date());
        this.jLabel3.setText(hora);
        controlador=new TransactionRegistryController();
        controlador.setProducto(cmbCP);
        controlador.getMonedas(cmbMon);
        controlador.getTipoTransaccion(cmbTipo);
        controlador.getFormaPago(cmbFP);
        controlador.setPersona(txtT_nombre_apellido);
         GlobalConstants.InitAuditoria(this);
       //txtT_fecha_transaccion.setEnabled(true);
    }
    public static String date() {
        Date fecha = new Date();
        SimpleDateFormat formatofecha = new SimpleDateFormat("dd/MM/yyyy");               
        return formatofecha.format(fecha);  
    }
       
    public void cerrar()
    {
        try {
            this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            addWindowListener(new WindowAdapter() {
                public void windowClosing (WindowEvent e){
                    dispose();
                }
            });  this.setVisible(true);
        }catch  (Exception e) {e.printStackTrace();}
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane4 = new javax.swing.JTabbedPane();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnT_insertar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        txtT_codigo_operador_swift = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        txtT_fecha_registro_transaccion = new com.toedter.calendar.JDateChooser();
        jLabel21 = new javax.swing.JLabel();
        txtT_fecha_proceso_transaccion = new com.toedter.calendar.JDateChooser();
        jLabel20 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtT_fecha_utima_actualizacion = new com.toedter.calendar.JDateChooser();
        jLabel23 = new javax.swing.JLabel();
        txtT_nro_transaccion = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cmbTipo = new javax.swing.JComboBox<String>();
        txtT_fecha_transaccion = new com.toedter.calendar.JDateChooser();
        jLabel19 = new javax.swing.JLabel();
        cmbST = new javax.swing.JComboBox<String>();
        jPanel5 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        cmbMon = new javax.swing.JComboBox<String>();
        cmbCP = new javax.swing.JComboBox<String>();
        jPanel9 = new javax.swing.JPanel();
        txtT_nombre_apellido = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtT_ci_rif_beneficiario_transferencia = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        txtT_descrip_comis_trans = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        txtT_indicador_exoneracion_impuesto_deito_banco = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        txtT_monto_transaccion = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        txtT_monto_comis_aplicada = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        txtT_tasa_cambio_trans = new javax.swing.JTextField();
        txtT_monto_impuesto_deito_banco = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        txtT_cuenta_debito_transferencia = new javax.swing.JTextField();
        jLabel47 = new javax.swing.JLabel();
        txtT_NRO_CTA_BCO = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        txtT_mto_moneda_extranjera_trans = new javax.swing.JTextField();
        jLabel50 = new javax.swing.JLabel();
        txtT_nat_trans = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        txtT_cuenta_credito_transferencia = new javax.swing.JTextField();
        cmbFP = new javax.swing.JComboBox<String>();
        jLabel37 = new javax.swing.JLabel();
        txtT_CiRif_Beneficiario = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        btnT_anular = new javax.swing.JButton();
        btnT_modificar = new javax.swing.JButton();
        btnT_consultar = new javax.swing.JButton();
        txtT_buscar = new javax.swing.JTextField();
        btnT_guardar = new javax.swing.JButton();
        btnT_actualizar = new javax.swing.JButton();
        btnT_salir = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1141, 766));
        setSize(new java.awt.Dimension(1141, 766));

        jLabel2.setText("00/00/00");

        jLabel3.setText("00:00");

        jLabel1.setFont(new java.awt.Font("Utsaah", 0, 24)); // NOI18N
        jLabel1.setText("Transacciones ");

        btnT_insertar.setBackground(new java.awt.Color(204, 204, 204));
        btnT_insertar.setFont(new java.awt.Font("Utsaah", 0, 20)); // NOI18N
        btnT_insertar.setText("Insertar");
        btnT_insertar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnT_insertarMouseClicked(evt);
            }
        });
        btnT_insertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnT_insertarActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTabbedPane1.setMinimumSize(new java.awt.Dimension(1141, 766));

        jPanel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel2MouseClicked(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));

        txtT_codigo_operador_swift.setName("Swift"); // NOI18N
        txtT_codigo_operador_swift.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtT_codigo_operador_swiftKeyTyped(evt);
            }
        });

        jLabel22.setText("Código Operador Swift ");

        jLabel43.setText("Indicador Situación de Operación ");

        txtT_fecha_registro_transaccion.setName("Fecha de registro"); // NOI18N

        jLabel21.setText("Fecha Registro de la Transacción");

        txtT_fecha_proceso_transaccion.setName("Fecha de Proceso Transaccion"); // NOI18N

        jLabel20.setText("Fecha Proceso Transacción");

        jLabel11.setText("Código Tipo Transacción");

        txtT_fecha_utima_actualizacion.setName("Ultima Actualizacion"); // NOI18N

        jLabel23.setText("Fecha Utima de la Actualización ");

        txtT_nro_transaccion.setName("Numero de transaccion"); // NOI18N
        txtT_nro_transaccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtT_nro_transaccionKeyTyped(evt);
            }
        });

        jLabel4.setText("Número de Transacción");

        cmbTipo.setName("Tipo de transaccion"); // NOI18N

        txtT_fecha_transaccion.setName("Fecha de transaccion"); // NOI18N

        jLabel19.setText("Fecha de la Transacción");

        cmbST.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "A", "I", "P" }));
        cmbST.setEnabled(false);
        cmbST.setName("Situacion de Operacion"); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                            .addComponent(txtT_nro_transaccion))
                        .addGap(60, 60, 60)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtT_fecha_utima_actualizacion, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel23))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtT_codigo_operador_swift, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtT_fecha_registro_transaccion, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtT_fecha_transaccion, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(60, 60, 60)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel43, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                    .addComponent(txtT_fecha_proceso_transaccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbST, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(399, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel11))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtT_nro_transaccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(jLabel21)
                    .addComponent(jLabel20))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtT_fecha_transaccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtT_fecha_registro_transaccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel43)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cmbST, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel22)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtT_codigo_operador_swift, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel23)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtT_fecha_utima_actualizacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(txtT_fecha_proceso_transaccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));

        jLabel13.setText("Código Producto");

        jLabel9.setText("Código Moneda");

        cmbMon.setName("Moneda"); // NOI18N

        cmbCP.setName("Producto"); // NOI18N
        cmbCP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCPActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                    .addComponent(cmbCP, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(60, 60, 60)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(cmbMon, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel13))
                .addGap(0, 0, 0)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbMon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbCP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26))
        );

        jPanel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));

        txtT_nombre_apellido.setName("NombreApellido"); // NOI18N
        txtT_nombre_apellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtT_nombre_apellidoKeyTyped(evt);
            }
        });

        jLabel5.setText("Nombre y Apellido de la Persona");

        txtT_ci_rif_beneficiario_transferencia.setName("CI-RIF"); // NOI18N
        txtT_ci_rif_beneficiario_transferencia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtT_ci_rif_beneficiario_transferenciaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtT_ci_rif_beneficiario_transferenciaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtT_ci_rif_beneficiario_transferenciaKeyTyped(evt);
            }
        });

        jLabel15.setText("C.I. ó RIF Empresa");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                    .addComponent(txtT_ci_rif_beneficiario_transferencia))
                .addGap(60, 60, 60)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtT_nombre_apellido, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel15))
                .addGap(0, 0, 0)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtT_nombre_apellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtT_ci_rif_beneficiario_transferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jLabel8.setText("Datos de la Persona");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jLabel10.setText("Datos de la Empresa");

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jLabel12.setText("Datos de la Transacción");

        jPanel11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));

        txtT_descrip_comis_trans.setName("Descripcion de Comision"); // NOI18N
        txtT_descrip_comis_trans.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtT_descrip_comis_transKeyTyped(evt);
            }
        });

        jLabel49.setText("Descripción de Comisión de Trans. ");

        txtT_indicador_exoneracion_impuesto_deito_banco.setName("Indicador Exo. Impuesto BCO."); // NOI18N
        txtT_indicador_exoneracion_impuesto_deito_banco.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtT_indicador_exoneracion_impuesto_deito_bancoKeyTyped(evt);
            }
        });

        jLabel44.setText("Indicador Exo. Impuesto BCO. ");

        jLabel26.setText("Código de Forma de Pago ");

        jLabel30.setText("Monto de la Transacción ");

        txtT_monto_transaccion.setName("Monto"); // NOI18N

        jLabel34.setText("Monto de la Comisión Aplicada ");

        txtT_monto_comis_aplicada.setName("Monto de Comision Aplicada"); // NOI18N
        txtT_monto_comis_aplicada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtT_monto_comis_aplicadaActionPerformed(evt);
            }
        });

        jLabel31.setText("Tasa de Cambio de la Trans. ");

        txtT_tasa_cambio_trans.setName("Tasa de Cambio de la transaccion"); // NOI18N

        txtT_monto_impuesto_deito_banco.setName("Monto Impuesto Debito Banco"); // NOI18N

        jLabel45.setText("Monto de Impuesto Debito Banco ");

        txtT_cuenta_debito_transferencia.setName("Cuenta Débito"); // NOI18N
        txtT_cuenta_debito_transferencia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtT_cuenta_debito_transferenciaKeyTyped(evt);
            }
        });

        jLabel47.setText("Cuenta Débito de la Trans. ");

        txtT_NRO_CTA_BCO.setName("Cuenta de BCO"); // NOI18N
        txtT_NRO_CTA_BCO.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtT_NRO_CTA_BCOKeyTyped(evt);
            }
        });

        jLabel35.setText("Número de Cuenta de BCO.");

        jLabel36.setText("Monto de Moneda Ex. Trans.");

        txtT_mto_moneda_extranjera_trans.setName("Monto de Moneda Ex. Trans."); // NOI18N
        txtT_mto_moneda_extranjera_trans.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtT_mto_moneda_extranjera_transActionPerformed(evt);
            }
        });

        jLabel50.setText("Nat. de la Transferencia ");

        txtT_nat_trans.setName("Nat. de la Transferencia"); // NOI18N
        txtT_nat_trans.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtT_nat_transKeyTyped(evt);
            }
        });

        jLabel51.setText("Cuenta Crédito de la Trans. ");

        txtT_cuenta_credito_transferencia.setName("Cuenta Credito de la Transaccion"); // NOI18N
        txtT_cuenta_credito_transferencia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtT_cuenta_credito_transferenciaKeyTyped(evt);
            }
        });

        cmbFP.setName("forma de Pago"); // NOI18N

        jLabel37.setText("C.I. o R.I.F. del beneficiario");

        txtT_CiRif_Beneficiario.setName("Monto de Moneda Ex. Trans."); // NOI18N
        txtT_CiRif_Beneficiario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtT_CiRif_BeneficiarioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtT_indicador_exoneracion_impuesto_deito_banco)
                    .addComponent(jLabel44, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtT_descrip_comis_trans)
                    .addComponent(jLabel49, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                    .addComponent(cmbFP, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(37, 37, 37)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtT_monto_transaccion, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtT_tasa_cambio_trans, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtT_monto_comis_aplicada, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtT_NRO_CTA_BCO, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtT_monto_impuesto_deito_banco, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtT_cuenta_debito_transferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(58, 58, 58)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtT_cuenta_credito_transferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel51, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtT_nat_trans, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel50, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtT_mto_moneda_extranjera_trans, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtT_CiRif_Beneficiario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(jLabel36)
                                .addGap(0, 0, 0)
                                .addComponent(txtT_mto_moneda_extranjera_trans, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15)
                                .addComponent(jLabel50)
                                .addGap(0, 0, 0)
                                .addComponent(txtT_nat_trans, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtT_cuenta_credito_transferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel11Layout.createSequentialGroup()
                                        .addComponent(jLabel51)
                                        .addGap(20, 20, 20))))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(jLabel37)
                                .addGap(0, 0, 0)
                                .addComponent(txtT_CiRif_Beneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel35)
                                    .addComponent(jLabel30))
                                .addGap(0, 0, 0)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtT_NRO_CTA_BCO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtT_monto_transaccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(15, 15, 15)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addComponent(jLabel34)
                                        .addGap(0, 0, 0)
                                        .addComponent(txtT_monto_comis_aplicada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(15, 15, 15)
                                        .addComponent(jLabel31)
                                        .addGap(0, 0, 0)
                                        .addComponent(txtT_tasa_cambio_trans, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel11Layout.createSequentialGroup()
                                        .addComponent(jLabel47)
                                        .addGap(0, 0, 0)
                                        .addComponent(txtT_cuenta_debito_transferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(15, 15, 15)
                                        .addComponent(jLabel45)
                                        .addGap(0, 0, 0)
                                        .addComponent(txtT_monto_impuesto_deito_banco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel26)
                        .addGap(4, 4, 4)
                        .addComponent(cmbFP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel44)
                        .addGap(0, 0, 0)
                        .addComponent(txtT_indicador_exoneracion_impuesto_deito_banco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(jLabel49)
                        .addGap(0, 0, 0)
                        .addComponent(txtT_descrip_comis_trans, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jLabel14.setText("Datos Contables");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(jLabel8)
                            .addComponent(jLabel14))
                        .addGap(0, 979, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addContainerGap(998, Short.MAX_VALUE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addGap(0, 0, 0)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jLabel10)
                .addGap(0, 0, 0)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jLabel12)
                .addGap(0, 0, 0)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jLabel14)
                .addGap(0, 0, 0)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 7)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(102, 102, 102));
        jLabel18.setText("FSOSEGTABFRM012");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 564, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(jLabel18))
        );

        jTabbedPane1.addTab("Datos Administrativos", jPanel3);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1070, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        btnT_anular.setBackground(new java.awt.Color(204, 204, 204));
        btnT_anular.setFont(new java.awt.Font("Utsaah", 0, 20)); // NOI18N
        btnT_anular.setText("Anular");
        btnT_anular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnT_anularActionPerformed(evt);
            }
        });

        btnT_modificar.setBackground(new java.awt.Color(204, 204, 204));
        btnT_modificar.setFont(new java.awt.Font("Utsaah", 0, 20)); // NOI18N
        btnT_modificar.setText("Modificar");

        btnT_consultar.setBackground(new java.awt.Color(204, 204, 204));
        btnT_consultar.setFont(new java.awt.Font("Utsaah", 0, 20)); // NOI18N
        btnT_consultar.setText("Consultar");

        btnT_guardar.setBackground(new java.awt.Color(204, 204, 204));
        btnT_guardar.setFont(new java.awt.Font("Utsaah", 0, 20)); // NOI18N
        btnT_guardar.setText("Guardar");
        btnT_guardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnT_guardarMouseClicked(evt);
            }
        });
        btnT_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnT_guardarActionPerformed(evt);
            }
        });

        btnT_actualizar.setBackground(new java.awt.Color(204, 204, 204));
        btnT_actualizar.setFont(new java.awt.Font("Utsaah", 0, 20)); // NOI18N
        btnT_actualizar.setText("Actualizar");

        btnT_salir.setBackground(new java.awt.Color(204, 204, 204));
        btnT_salir.setFont(new java.awt.Font("Utsaah", 0, 20)); // NOI18N
        btnT_salir.setText("Salir");
        btnT_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnT_salirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnT_insertar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnT_guardar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnT_anular, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnT_modificar)
                                .addGap(10, 10, 10)
                                .addComponent(btnT_actualizar)
                                .addGap(10, 10, 10)
                                .addComponent(txtT_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnT_consultar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnT_salir, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(864, 864, 864)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(jLabel2)))
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnT_consultar, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnT_salir, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtT_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnT_insertar, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnT_guardar, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnT_anular, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnT_modificar, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnT_actualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnT_insertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnT_insertarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnT_insertarActionPerformed

    private void btnT_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnT_guardarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnT_guardarActionPerformed

    private void btnT_guardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnT_guardarMouseClicked
          
    }//GEN-LAST:event_btnT_guardarMouseClicked

    private void btnT_insertarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnT_insertarMouseClicked

    }//GEN-LAST:event_btnT_insertarMouseClicked

    private void btnT_anularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnT_anularActionPerformed
        // TODO add your handling code here:
      
    }//GEN-LAST:event_btnT_anularActionPerformed

    private void btnT_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnT_salirActionPerformed
      dispose();
    }//GEN-LAST:event_btnT_salirActionPerformed

    private void jPanel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel2MouseClicked

    private void txtT_cuenta_credito_transferenciaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtT_cuenta_credito_transferenciaKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtT_cuenta_credito_transferenciaKeyTyped

    private void txtT_nat_transKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtT_nat_transKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtT_nat_transKeyTyped

    private void txtT_mto_moneda_extranjera_transActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtT_mto_moneda_extranjera_transActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtT_mto_moneda_extranjera_transActionPerformed

    private void txtT_NRO_CTA_BCOKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtT_NRO_CTA_BCOKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtT_NRO_CTA_BCOKeyTyped

    private void txtT_cuenta_debito_transferenciaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtT_cuenta_debito_transferenciaKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtT_cuenta_debito_transferenciaKeyTyped

    private void txtT_monto_comis_aplicadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtT_monto_comis_aplicadaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtT_monto_comis_aplicadaActionPerformed

    private void txtT_indicador_exoneracion_impuesto_deito_bancoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtT_indicador_exoneracion_impuesto_deito_bancoKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtT_indicador_exoneracion_impuesto_deito_bancoKeyTyped

    private void txtT_descrip_comis_transKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtT_descrip_comis_transKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtT_descrip_comis_transKeyTyped

    private void txtT_ci_rif_beneficiario_transferenciaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtT_ci_rif_beneficiario_transferenciaKeyTyped
        // TODO add your handling code here:
        JTextField fuente=(JTextField)evt.getSource();
        if(fuente.getText().length()>=6)
        {
        controlador.getPersona(fuente.getText());
        }else
        {
            txtT_nombre_apellido.setText("");
        }
    }//GEN-LAST:event_txtT_ci_rif_beneficiario_transferenciaKeyTyped
    
    public void carga_datos_persona(String ci_rif)
    {
        controlador.getPersona(ci_rif);
    }
    
    private void txtT_ci_rif_beneficiario_transferenciaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtT_ci_rif_beneficiario_transferenciaKeyReleased
   

    }//GEN-LAST:event_txtT_ci_rif_beneficiario_transferenciaKeyReleased

    private void txtT_ci_rif_beneficiario_transferenciaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtT_ci_rif_beneficiario_transferenciaKeyPressed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_txtT_ci_rif_beneficiario_transferenciaKeyPressed

    private void txtT_nombre_apellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtT_nombre_apellidoKeyTyped
        char car = evt.getKeyChar();
        if((car<'a' || car>'z') && (car<'á' || car>'ó') && (car<'A' || car>'Z') && (car!=(char)KeyEvent.VK_BACK_SPACE)
            && (car!=(char)KeyEvent.VK_SPACE)){
            evt.consume();
            JOptionPane.showMessageDialog(null, "Solo se admite texto","",JOptionPane.INFORMATION_MESSAGE);
        }

    }//GEN-LAST:event_txtT_nombre_apellidoKeyTyped

    private void cmbCPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCPActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_cmbCPActionPerformed

    private void txtT_nro_transaccionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtT_nro_transaccionKeyTyped
        char num = evt.getKeyChar();
        if((num<'0' || num>'9') && (num!=(char)KeyEvent.VK_BACK_SPACE)){
            evt.consume();
            JOptionPane.showMessageDialog(null, "Solo se admiten números enteros","",JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_txtT_nro_transaccionKeyTyped

/*
 */
    private void txtT_codigo_operador_swiftKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtT_codigo_operador_swiftKeyTyped
        char num = evt.getKeyChar();
        if((num<'0' || num>'9') && (num!=(char)KeyEvent.VK_BACK_SPACE)){
            evt.consume();
            JOptionPane.showMessageDialog(null, "Solo se admiten números enteros","",JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_txtT_codigo_operador_swiftKeyTyped

    private void txtT_CiRif_BeneficiarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtT_CiRif_BeneficiarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtT_CiRif_BeneficiarioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FSOSEGTABFRM001.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FSOSEGTABFRM001.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FSOSEGTABFRM001.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FSOSEGTABFRM001.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
       
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FSOSEGTABFRM001().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnT_actualizar;
    public static javax.swing.JButton btnT_anular;
    public static javax.swing.JButton btnT_consultar;
    public static javax.swing.JButton btnT_guardar;
    public static javax.swing.JButton btnT_insertar;
    public static javax.swing.JButton btnT_modificar;
    public static javax.swing.JButton btnT_salir;
    public javax.swing.JComboBox<String> cmbCP;
    public javax.swing.JComboBox<String> cmbFP;
    public javax.swing.JComboBox<String> cmbMon;
    public javax.swing.JComboBox<String> cmbST;
    public javax.swing.JComboBox<String> cmbTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane4;
    public static javax.swing.JTextField txtT_CiRif_Beneficiario;
    public static javax.swing.JTextField txtT_NRO_CTA_BCO;
    public static javax.swing.JTextField txtT_buscar;
    public static javax.swing.JTextField txtT_ci_rif_beneficiario_transferencia;
    public static javax.swing.JTextField txtT_codigo_operador_swift;
    public static javax.swing.JTextField txtT_cuenta_credito_transferencia;
    public static javax.swing.JTextField txtT_cuenta_debito_transferencia;
    public static javax.swing.JTextField txtT_descrip_comis_trans;
    public static com.toedter.calendar.JDateChooser txtT_fecha_proceso_transaccion;
    public static com.toedter.calendar.JDateChooser txtT_fecha_registro_transaccion;
    public static com.toedter.calendar.JDateChooser txtT_fecha_transaccion;
    public static com.toedter.calendar.JDateChooser txtT_fecha_utima_actualizacion;
    public static javax.swing.JTextField txtT_indicador_exoneracion_impuesto_deito_banco;
    public static javax.swing.JTextField txtT_monto_comis_aplicada;
    public static javax.swing.JTextField txtT_monto_impuesto_deito_banco;
    public static javax.swing.JTextField txtT_monto_transaccion;
    public static javax.swing.JTextField txtT_mto_moneda_extranjera_trans;
    public static javax.swing.JTextField txtT_nat_trans;
    public static javax.swing.JTextField txtT_nombre_apellido;
    public static javax.swing.JTextField txtT_nro_transaccion;
    public static javax.swing.JTextField txtT_tasa_cambio_trans;
    // End of variables declaration//GEN-END:variables
}
