package com.bimodal.app.support.controller;

import java.awt.Desktop;
import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.lang.Object;
import java.lang.reflect.Array;
import org.apache.commons.lang3.ArrayUtils;
 
 class Explorer extends Thread{
       private File[] filelist;
       private String program;
       private Map<String,Boolean> results;
               
              public Explorer(File[] filelist,String programs,Map<String,Boolean> results){
                    this.filelist=filelist;
                    this.program=programs;
                    this.results=results;
                    
            }
              public void find(File[] list,String program){
                  
                 for(File dat:list){
                 if(dat.getName().indexOf("Apache")>=0){
                    if(dat.isDirectory()){
                     File th=new File(dat.getAbsoluteFile().toString());
                        find(th.listFiles(),program);
                        
                    } 
                  }
               
               if(dat.getName().indexOf(program)>=0){
                  
                    System.out.println(program+" instalado");
                    break;
                  }
         
              }
              
        }
         public void run(){
               find(filelist,program);
             
      }
        
}
public class FSOSEG_REQAPP_MODULE {
       private Map<String,Boolean> results;
      // private Map<String,String> paths;
       private String[] softwares;
       @SuppressWarnings("empty-statement")
    public FSOSEG_REQAPP_MODULE(){
     
        results = new HashMap<String,Boolean>();
        results.put("Apache Tomcat", true);
        results.put("MySQL", Boolean.TRUE);
         
        results.put("SQL Server", Boolean.TRUE);
        results.put("Oracle",Boolean.TRUE);
        results.put("MongoDB",Boolean.TRUE);
       
        String[] soft={"Apache Tomcat","MySQL","MongoDB"};
         this.softwares=soft;
           
        
    }
    public void startCheck(){
         File file=new File("C:\\Program Files");
        
               
             if(!file.exists()){
                JFrame fram=new JFrame();
               JOptionPane.showMessageDialog(fram,"El sistema operativo has sido alterado, por lo tanto la instalacion se aborto. Verifique que las carpetas del sistema esten correctamente colocadas","Error al intentar realizar la verificacion de requerimientos", 0);
                System.exit(0);
              }
              
              File[] filelist=file.listFiles();
              int len=softwares.length;
              Explorer[] explor=new Explorer[len];
              
                  for(int i=0; i<len; i++)
                  {  
                     explor[i]=new Explorer(filelist,softwares[i],results);
                     explor[i].start();
                  }   
                  
                   for(int i=0; i<len; i++){
             try {
                 explor[i].join();
             } catch (InterruptedException ex) {
                 Logger.getLogger(FSOSEG_REQAPP_MODULE.class.getName()).log(Level.SEVERE, null, ex);
             }
                   }
           
             
    
    }
    
}
