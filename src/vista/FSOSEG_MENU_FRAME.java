/*
 * Nombre Objeto: FSOSEG_MENU_FRAME
 * Autor: Hetsy Rodríguez
 * Fecha Elab: 24/01/2017
 * Descripción: JFrame del Menú del aplicativo donde se muestran todas 
 * las funcionalidades que se pueden ejecutar.
 * Última Modificación:
 * Autor Última Modificación:
 */
package vista;
import com.bimodal.app.modulos.view.FSOSEGTABFRM005;
import com.bimodal.app.administracion.view.FSOSEGTABFRM003;
import com.bimodal.app.administracion.view.FSOSEGTABFRM001;
import com.bimodal.app.administracion.view.FSOSEGTABFRM002;
import com.bimodal.app.administracion.view.FSOSEGTABFRM013;
import com.bimodal.app.administracion.view.FSOSEGTABFRM007;
import com.bimodal.app.administracion.view.FSOSEGTABFRM012;
import com.bimodal.app.procesos.view.FSOSEGTABFRM009;
import com.bimodal.app.modulos.view.FSOSEGTABFRM015;
import com.bimodal.app.modulos.view.FSOSEGTABFRM010;
import com.bimodal.app.modulos.view.FSOSEGTABFRM018;
import com.bimodal.app.modulos.view.FSOSEGTABFRM014;
import com.bimodal.app.modulos.view.FSOSEGTABFRM017;
import com.bimodal.app.modulos.view.FSOSEGTABFRM011;
import com.bimodal.app.support.view.FSOSEG_SYSINFO_FRAME;
import com.bimodal.app.modulos.view.FSOSEGTABFRM016;
import vista.reportes.FSOSEG_REP_RESCONTGRAL_FRAME;
import vista.reportes.FSOSEG_REP_MOVCTAYFID_FRAME;
import vista.reportes.FSOSEG_REP_INTERESESMEN_FRAME;
import vista.reportes.FSOSEG_REP_BALANCE_FRAME;
import vista.reportes.FSOSEG_REP_CTABENEF_FRAME;
import vista.calendario.DrawCalendar;
import com.bimodal.app.db.FSOSEG_CONNECT_MODULE;
import com.bimodal.app.support.controller.FSOSEG_HELP_MODULE;
import com.bimodal.app.support.controller.FSOSEG_SYSCHK_MODULE;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.ImageIcon;
import com.bimodal.app.support.controller.FSOSEG_SYSINFO_MODULE;
import Tablas.*;
import com.bimodal.app.administracion.view.FSOSEGTABFRM020;
import com.bimodal.app.administracion.view.FSOSEGTABFRM021;
import com.bimodal.app.administracion.view.FSOSEGTABFRM022;
import controlador.Auditoria;
import controlador.Controlador;
import controlador.FSOSEG_SYSTEM_CONTROLLER;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import com.bimodal.app.db.Modelo;
import com.bimodal.app.beans.Principal;
import com.bimodal.app.conf.GlobalConstants;
import com.bimodal.app.procesos.view.FSOSEGTABFRM024;
import com.bimodal.app.procesos.view.FSOSEGTABFRM025;
import com.bimodal.app.administracion.view.FSOSEGTABFRM026;
import com.bimodal.app.procesos.view.FSOSEGTABFRM027;
import com.bimodal.app.procesos.view.FSOSEGTABFRM028;
import controlador.*;


public class FSOSEG_MENU_FRAME extends javax.swing.JFrame implements Runnable {
    static Object jTextField_Username;
    private FSOSEG_FONDO_PANEL fond;
    private FSOSEG_HELP_MODULE help;
    private FSOSEG_SYSTEM_CONTROLLER sysCon;
    private FSOSEG_SYSINFO_FRAME sysinfo;
    private FSOSEG_SYSINFO_MODULE system;
    private FSOSEG_SYSCHK_MODULE checker;
    private FSOSEG_CONNECT_MODULE conector;
    private Auditoria log;
    private static String modulo;
    static String tiempo;
    String hora;
    String minutos;
    String segundos;
    Thread hilo;
         
    public static void setModulo(String modulo){
        FSOSEG_MENU_FRAME.modulo=modulo;
    }
        
    public static String getModulo(){
        return FSOSEG_MENU_FRAME.modulo;
    }
      
    /**
     * Creates new form Menu
     */
    public FSOSEG_MENU_FRAME() {
        initComponents();
        setLocationRelativeTo(null);
        this.setExtendedState(MAXIMIZED_BOTH);
        setIconImage(new ImageIcon("resources/img/seguros_mercantil.png").getImage());
        this.jLabel_Username.setText(Principal.getUser());
        fond=new FSOSEG_FONDO_PANEL();
        fond.setSize(getWidth(),getHeight());
        this.add(fond);
        Calendar cal = Calendar.getInstance();
        system=new FSOSEG_SYSINFO_MODULE();
        String hora = cal.get(cal.HOUR_OF_DAY)+":"+cal.get(cal.MINUTE);
        this.jlblDate.setText(date());
        hilo = new Thread(this);
        hilo.start();
        this.setMinimumSize(new Dimension(500,500));
        this.jlblTime.setLocation(this.getWidth()-120,this.jlblTime.getY());
        this.jlblDate.setLocation(this.getHeight()-200,this.jlblDate.getY());
        help=new FSOSEG_HELP_MODULE();
        log=new Auditoria(this);
        log.setModel(Principal.getModel());
        
    }
     
    public static String date() {
        Date fecha = new Date();
        SimpleDateFormat formatofecha = new SimpleDateFormat("dd/MM/yyyy");
        return formatofecha.format(fecha);  
    }
    

    public synchronized void hora() {
       Calendar calendario = new GregorianCalendar();
       Date horactual = new Date();
       calendario.setTime(horactual);
        hora = calendario.get(Calendar.HOUR_OF_DAY)>9?""+calendario.get(Calendar.HOUR_OF_DAY):"0"+calendario.get(Calendar.HOUR_OF_DAY);
        minutos = calendario.get(Calendar.MINUTE)>9?""+calendario.get(Calendar.MINUTE):"0"+calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND)>9?""+calendario.get(Calendar.SECOND):"0"+calendario.get(Calendar.SECOND);  
        tiempo = hora+":"+minutos+":"+segundos;
    }
    public static synchronized String getHora(){
         return tiempo;
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jlblDate = new javax.swing.JLabel();
        jlblTime = new javax.swing.JLabel();
        jLabel_Username = new javax.swing.JLabel();
        FSOSEG_MENU_MN = new javax.swing.JMenuBar();
        FSOSEG_MENU_AR = new javax.swing.JMenu();
        FSOSEG_MENU_AR01 = new javax.swing.JMenuItem();
        FSOSEG_MENU_AR02 = new javax.swing.JMenuItem();
        FSOSEG_MENU_MO = new javax.swing.JMenu();
        FSOSEG_MENU_MO01 = new javax.swing.JMenuItem();
        FSOSEG_MENU_MO02 = new javax.swing.JMenu();
        FSOSEG_MENU_TB0121 = new javax.swing.JMenuItem();
        FSOSEG_MENU_TB0123 = new javax.swing.JMenuItem();
        FSOSEG_MENU_MO03 = new javax.swing.JMenu();
        FSOSEG_MENU_TB0131 = new javax.swing.JMenuItem();
        FSOSEG_MENU_TB0132 = new javax.swing.JMenuItem();
        FSOSEG_MENU_TB0133 = new javax.swing.JMenuItem();
        FSOSEG_MENU_TB0134 = new javax.swing.JMenuItem();
        FSOSEG_MENU_MO05 = new javax.swing.JMenuItem();
        FSOSEG_MENU_PR = new javax.swing.JMenu();
        FSOSEG_MENU_PR07 = new javax.swing.JMenuItem();
        FSOSEG_MENU_PR06 = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        FSOSEG_MENU_PR08 = new javax.swing.JMenuItem();
        FSOSEG_MENU_PR09 = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        FSOSEG_MENU_PR10 = new javax.swing.JMenuItem();
        FSOSEG_MENU_TB = new javax.swing.JMenu();
        FSOSEG_MENU_TB010 = new javax.swing.JMenu();
        FSOSEG_MENU_TB0101 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        FSOSEG_MENU_TB102 = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        FSOSEG_MENU_TB011 = new javax.swing.JMenu();
        FSOSEG_MENU_TB0111 = new javax.swing.JMenuItem();
        FSOSEG_MENU_TB0112 = new javax.swing.JMenuItem();
        FSOSEG_MENU_TB0113 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        FSOSEG_MENU_TB015 = new javax.swing.JMenuItem();
        FSOSEG_MENU_TB016 = new javax.swing.JMenuItem();
        FSOSEG_MENU_RE = new javax.swing.JMenu();
        FSOSEG_MENU_RE07 = new javax.swing.JMenuItem();
        FSOSEG_MENU_UT = new javax.swing.JMenu();
        FSOSEG_MENU_UT01 = new javax.swing.JMenuItem();
        FSOSEG_MENU_UT02 = new javax.swing.JMenuItem();
        FSOSEG_MENU_UT03 = new javax.swing.JMenuItem();
        FSOSEG_MENU_AY = new javax.swing.JMenu();
        FSOSEG_MENU_AY01 = new javax.swing.JMenuItem();
        FSOSEG_MENU_AY02 = new javax.swing.JMenuItem();
        FSOSEG_MENU_AY03 = new javax.swing.JMenuItem();
        FSOSEG_MENU_AY04 = new javax.swing.JMenuItem();
        FSOSEG_MENU_AY05 = new javax.swing.JMenuItem();
        FSOSEG_MENU_SA = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });
        getContentPane().setLayout(null);

        jlblDate.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jlblDate.setForeground(new java.awt.Color(255, 255, 255));
        jlblDate.setText("00/00/00");
        getContentPane().add(jlblDate);
        jlblDate.setBounds(560, 10, 70, 14);

        jlblTime.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jlblTime.setForeground(new java.awt.Color(255, 255, 255));
        jlblTime.setText("00:00");
        getContentPane().add(jlblTime);
        jlblTime.setBounds(640, 10, 70, 14);

        jLabel_Username.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel_Username.setForeground(new java.awt.Color(255, 255, 255));
        jLabel_Username.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel_Username.setText("jLabel1");
        jLabel_Username.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        getContentPane().add(jLabel_Username);
        jLabel_Username.setBounds(740, 10, 50, 14);

        FSOSEG_MENU_MN.setAlignmentY(0.1F);
        FSOSEG_MENU_MN.setMargin(new java.awt.Insets(15, 0, 0, 0));

        FSOSEG_MENU_AR.setText("Sesión");
        FSOSEG_MENU_AR.setAlignmentY(0.1F);
        FSOSEG_MENU_AR.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        FSOSEG_MENU_AR.setFont(new java.awt.Font("Utsaah", 0, 24)); // NOI18N
        FSOSEG_MENU_AR.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        FSOSEG_MENU_AR.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        FSOSEG_MENU_AR.setMargin(new java.awt.Insets(15, 0, 0, 15));
        FSOSEG_MENU_AR.setPreferredSize(new java.awt.Dimension(90, 43));
        FSOSEG_MENU_AR.setRequestFocusEnabled(false);

        FSOSEG_MENU_AR01.setText("Seleccionar Usuario");
        FSOSEG_MENU_AR01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_AR01ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_AR.add(FSOSEG_MENU_AR01);

        FSOSEG_MENU_AR02.setText("Salir");
        FSOSEG_MENU_AR02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_AR02ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_AR.add(FSOSEG_MENU_AR02);

        FSOSEG_MENU_MN.add(FSOSEG_MENU_AR);

        FSOSEG_MENU_MO.setText("Módulos");
        FSOSEG_MENU_MO.setAlignmentY(0.1F);
        FSOSEG_MENU_MO.setFont(new java.awt.Font("Utsaah", 0, 24)); // NOI18N
        FSOSEG_MENU_MO.setMargin(new java.awt.Insets(15, 0, 0, 10));
        FSOSEG_MENU_MO.setPreferredSize(new java.awt.Dimension(90, 43));

        FSOSEG_MENU_MO01.setText("Instalación");
        FSOSEG_MENU_MO01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_MO01ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_MO.add(FSOSEG_MENU_MO01);

        FSOSEG_MENU_MO02.setText("Seguridad");

        FSOSEG_MENU_TB0121.setText("Usuarios");
        FSOSEG_MENU_TB0121.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TB0121ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_MO02.add(FSOSEG_MENU_TB0121);

        FSOSEG_MENU_TB0123.setText("Logs Auditoría");
        FSOSEG_MENU_TB0123.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TB0123ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_MO02.add(FSOSEG_MENU_TB0123);

        FSOSEG_MENU_MO.add(FSOSEG_MENU_MO02);

        FSOSEG_MENU_MO03.setText("Configuración");

        FSOSEG_MENU_TB0131.setText("Monedas");
        FSOSEG_MENU_TB0131.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TB0131ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_MO03.add(FSOSEG_MENU_TB0131);

        FSOSEG_MENU_TB0132.setText("Días Feriados");
        FSOSEG_MENU_TB0132.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TB0132ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_MO03.add(FSOSEG_MENU_TB0132);

        FSOSEG_MENU_TB0133.setText("Opciones del Sistema");
        FSOSEG_MENU_TB0133.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TB0133ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_MO03.add(FSOSEG_MENU_TB0133);

        FSOSEG_MENU_TB0134.setText("Localidad");
        FSOSEG_MENU_TB0134.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TB0134ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_MO03.add(FSOSEG_MENU_TB0134);

        FSOSEG_MENU_MO.add(FSOSEG_MENU_MO03);

        FSOSEG_MENU_MO05.setText("Perfiles");
        FSOSEG_MENU_MO05.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_MO05ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_MO.add(FSOSEG_MENU_MO05);

        FSOSEG_MENU_MN.add(FSOSEG_MENU_MO);

        FSOSEG_MENU_PR.setText("Procesos");
        FSOSEG_MENU_PR.setAlignmentY(0.1F);
        FSOSEG_MENU_PR.setFont(new java.awt.Font("Utsaah", 0, 24)); // NOI18N
        FSOSEG_MENU_PR.setMargin(new java.awt.Insets(15, 0, 0, 10));
        FSOSEG_MENU_PR.setPreferredSize(new java.awt.Dimension(90, 43));
        FSOSEG_MENU_PR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_PRActionPerformed(evt);
            }
        });

        FSOSEG_MENU_PR07.setText("Cálculo de Comisiones");
        FSOSEG_MENU_PR07.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_PR07ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_PR.add(FSOSEG_MENU_PR07);

        FSOSEG_MENU_PR06.setText("Pago de Rendimiento");
        FSOSEG_MENU_PR06.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_PR06ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_PR.add(FSOSEG_MENU_PR06);
        FSOSEG_MENU_PR.add(jSeparator5);

        FSOSEG_MENU_PR08.setText("Cierre Mensual");
        FSOSEG_MENU_PR08.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_PR08ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_PR.add(FSOSEG_MENU_PR08);

        FSOSEG_MENU_PR09.setText("Cierre Diario");
        FSOSEG_MENU_PR09.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_PR09ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_PR.add(FSOSEG_MENU_PR09);
        FSOSEG_MENU_PR.add(jSeparator3);

        FSOSEG_MENU_PR10.setText("Ejecutar Reverso");
        FSOSEG_MENU_PR10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_PR10ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_PR.add(FSOSEG_MENU_PR10);

        FSOSEG_MENU_MN.add(FSOSEG_MENU_PR);

        FSOSEG_MENU_TB.setText("Administración");
        FSOSEG_MENU_TB.setAlignmentY(0.1F);
        FSOSEG_MENU_TB.setFont(new java.awt.Font("Utsaah", 0, 24)); // NOI18N
        FSOSEG_MENU_TB.setMargin(new java.awt.Insets(15, 0, 0, 10));
        FSOSEG_MENU_TB.setMaximumSize(new java.awt.Dimension(130, 32767));
        FSOSEG_MENU_TB.setPreferredSize(new java.awt.Dimension(90, 43));
        FSOSEG_MENU_TB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TBActionPerformed(evt);
            }
        });

        FSOSEG_MENU_TB010.setText("Transacciones");

        FSOSEG_MENU_TB0101.setText("Registro de entradas y salidas de dinero");
        FSOSEG_MENU_TB0101.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TB0101ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_TB010.add(FSOSEG_MENU_TB0101);

        jMenuItem1.setText("Consulta de Transacciones por día");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_TB010.add(jMenuItem1);

        FSOSEG_MENU_TB102.setText("Tipo de Transacciones");
        FSOSEG_MENU_TB102.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TB102ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_TB010.add(FSOSEG_MENU_TB102);

        FSOSEG_MENU_TB.add(FSOSEG_MENU_TB010);
        FSOSEG_MENU_TB.add(jSeparator4);

        FSOSEG_MENU_TB011.setText("Contabilidad");

        FSOSEG_MENU_TB0111.setText("Plan de Cuentas");
        FSOSEG_MENU_TB0111.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TB0111ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_TB011.add(FSOSEG_MENU_TB0111);

        FSOSEG_MENU_TB0112.setText("Asientos Contables");
        FSOSEG_MENU_TB0112.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TB0112ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_TB011.add(FSOSEG_MENU_TB0112);

        FSOSEG_MENU_TB0113.setText("Contabilidad");
        FSOSEG_MENU_TB0113.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TB0113ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_TB011.add(FSOSEG_MENU_TB0113);

        jMenuItem2.setText("Consulta de Transacciones Contables");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_TB011.add(jMenuItem2);

        FSOSEG_MENU_TB.add(FSOSEG_MENU_TB011);
        FSOSEG_MENU_TB.add(jSeparator6);

        FSOSEG_MENU_TB015.setText("Personas");
        FSOSEG_MENU_TB015.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TB015ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_TB.add(FSOSEG_MENU_TB015);

        FSOSEG_MENU_TB016.setText("Productos");
        FSOSEG_MENU_TB016.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_TB016ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_TB.add(FSOSEG_MENU_TB016);

        FSOSEG_MENU_MN.add(FSOSEG_MENU_TB);

        FSOSEG_MENU_RE.setText("Reportes");
        FSOSEG_MENU_RE.setAlignmentY(0.1F);
        FSOSEG_MENU_RE.setFont(new java.awt.Font("Utsaah", 0, 24)); // NOI18N
        FSOSEG_MENU_RE.setMargin(new java.awt.Insets(15, 0, 0, 10));
        FSOSEG_MENU_RE.setPreferredSize(new java.awt.Dimension(90, 43));

        FSOSEG_MENU_RE07.setText("Generar  Reportes FSOSEG");
        FSOSEG_MENU_RE07.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_RE07ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_RE.add(FSOSEG_MENU_RE07);

        FSOSEG_MENU_MN.add(FSOSEG_MENU_RE);

        FSOSEG_MENU_UT.setText("Utilitarios");
        FSOSEG_MENU_UT.setAlignmentY(0.1F);
        FSOSEG_MENU_UT.setFont(new java.awt.Font("Utsaah", 0, 24)); // NOI18N
        FSOSEG_MENU_UT.setMargin(new java.awt.Insets(15, 0, 0, 10));
        FSOSEG_MENU_UT.setPreferredSize(new java.awt.Dimension(90, 43));

        FSOSEG_MENU_UT01.setText("Calculadora");
        FSOSEG_MENU_UT01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_UT01ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_UT.add(FSOSEG_MENU_UT01);

        FSOSEG_MENU_UT02.setText("Calendario");
        FSOSEG_MENU_UT02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_UT02ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_UT.add(FSOSEG_MENU_UT02);

        FSOSEG_MENU_UT03.setText("Agenda Diaria");
        FSOSEG_MENU_UT03.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_UT03ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_UT.add(FSOSEG_MENU_UT03);

        FSOSEG_MENU_MN.add(FSOSEG_MENU_UT);

        FSOSEG_MENU_AY.setText("Ayuda");
        FSOSEG_MENU_AY.setAlignmentY(0.1F);
        FSOSEG_MENU_AY.setFont(new java.awt.Font("Utsaah", 0, 24)); // NOI18N
        FSOSEG_MENU_AY.setMargin(new java.awt.Insets(15, 0, 0, 15));
        FSOSEG_MENU_AY.setPreferredSize(new java.awt.Dimension(90, 43));

        FSOSEG_MENU_AY01.setText("Acerca del Sistema");
        FSOSEG_MENU_AY01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_AY01ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_AY.add(FSOSEG_MENU_AY01);

        FSOSEG_MENU_AY02.setText("Contenido e Índice de la Ayuda");
        FSOSEG_MENU_AY.add(FSOSEG_MENU_AY02);

        FSOSEG_MENU_AY03.setText("Asistencia Técnica");
        FSOSEG_MENU_AY03.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_AY03ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_AY.add(FSOSEG_MENU_AY03);

        FSOSEG_MENU_AY04.setText("Manual de Usuario");
        FSOSEG_MENU_AY04.addMenuDragMouseListener(new javax.swing.event.MenuDragMouseListener() {
            public void menuDragMouseDragged(javax.swing.event.MenuDragMouseEvent evt) {
                FSOSEG_MENU_AY04MenuDragMouseDragged(evt);
            }
            public void menuDragMouseEntered(javax.swing.event.MenuDragMouseEvent evt) {
                FSOSEG_MENU_AY04MenuDragMouseEntered(evt);
            }
            public void menuDragMouseExited(javax.swing.event.MenuDragMouseEvent evt) {
                FSOSEG_MENU_AY04MenuDragMouseExited(evt);
            }
            public void menuDragMouseReleased(javax.swing.event.MenuDragMouseEvent evt) {
            }
        });
        FSOSEG_MENU_AY04.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                FSOSEG_MENU_AY04MouseClicked(evt);
            }
        });
        FSOSEG_MENU_AY04.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_AY04ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_AY.add(FSOSEG_MENU_AY04);

        FSOSEG_MENU_AY05.setText("Preguntas Frecuentes");
        FSOSEG_MENU_AY05.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_AY05ActionPerformed(evt);
            }
        });
        FSOSEG_MENU_AY.add(FSOSEG_MENU_AY05);

        FSOSEG_MENU_MN.add(FSOSEG_MENU_AY);

        FSOSEG_MENU_SA.setText("Salir");
        FSOSEG_MENU_SA.setAlignmentY(0.1F);
        FSOSEG_MENU_SA.setFont(new java.awt.Font("Utsaah", 0, 24)); // NOI18N
        FSOSEG_MENU_SA.setMargin(new java.awt.Insets(15, 0, 0, 15));
        FSOSEG_MENU_SA.setPreferredSize(new java.awt.Dimension(90, 43));
        FSOSEG_MENU_SA.addMenuKeyListener(new javax.swing.event.MenuKeyListener() {
            public void menuKeyPressed(javax.swing.event.MenuKeyEvent evt) {
                FSOSEG_MENU_SAMenuKeyPressed(evt);
            }
            public void menuKeyReleased(javax.swing.event.MenuKeyEvent evt) {
            }
            public void menuKeyTyped(javax.swing.event.MenuKeyEvent evt) {
                FSOSEG_MENU_SAMenuKeyTyped(evt);
            }
        });
        FSOSEG_MENU_SA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                FSOSEG_MENU_SAMouseClicked(evt);
            }
        });
        FSOSEG_MENU_SA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_MENU_SAActionPerformed(evt);
            }
        });
        FSOSEG_MENU_MN.add(FSOSEG_MENU_SA);

        setJMenuBar(FSOSEG_MENU_MN);

        pack();
    }// </editor-fold>//GEN-END:initComponents
   
    private void FSOSEG_MENU_AR01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_AR01ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else{
                FSOSEG_SESION_FRAME fr = new FSOSEG_SESION_FRAME ();
                fr.setVisible(true);
            }  
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_AR01ActionPerformed

    private void FSOSEG_MENU_AR02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_AR02ActionPerformed
        Salir s = new Salir();
        s.setVisible(true);
    }//GEN-LAST:event_FSOSEG_MENU_AR02ActionPerformed

    private void FSOSEG_MENU_TBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TBActionPerformed
      //
    }//GEN-LAST:event_FSOSEG_MENU_TBActionPerformed

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        // TODO add your handling code here:
        if(fond!=null)
            fond.setSize(getWidth(),getHeight());
            this.jlblTime.setLocation(this.getSize().width-120,this.jlblTime.getY());
            this.jlblDate.setLocation(this.getSize().width-200,this.jlblDate.getY());
    }//GEN-LAST:event_formComponentResized

    private void FSOSEG_MENU_SAMenuKeyTyped(javax.swing.event.MenuKeyEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_SAMenuKeyTyped
        GlobalConstants.CloseConnection();
        System.exit(0);
    }//GEN-LAST:event_FSOSEG_MENU_SAMenuKeyTyped

    private void FSOSEG_MENU_SAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_SAActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_FSOSEG_MENU_SAActionPerformed

    private void FSOSEG_MENU_SAMenuKeyPressed(javax.swing.event.MenuKeyEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_SAMenuKeyPressed
        // TODO add your handling code here: System.exit(0);
    }//GEN-LAST:event_FSOSEG_MENU_SAMenuKeyPressed

    private void FSOSEG_MENU_SAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_SAMouseClicked
        Salir s = new Salir();
        s.setVisible(true);
    }//GEN-LAST:event_FSOSEG_MENU_SAMouseClicked

    private void FSOSEG_MENU_AY04MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_AY04MouseClicked
        // TODO add your handling code here:
        help.invokeHelp();
    }//GEN-LAST:event_FSOSEG_MENU_AY04MouseClicked

    private void FSOSEG_MENU_AY04MenuDragMouseEntered(javax.swing.event.MenuDragMouseEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_AY04MenuDragMouseEntered
        // TODO add your handling code here:
        
    }//GEN-LAST:event_FSOSEG_MENU_AY04MenuDragMouseEntered

    private void FSOSEG_MENU_AY04MenuDragMouseDragged(javax.swing.event.MenuDragMouseEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_AY04MenuDragMouseDragged
        // TODO add your handling code here:
        
    }//GEN-LAST:event_FSOSEG_MENU_AY04MenuDragMouseDragged

    private void FSOSEG_MENU_AY04MenuDragMouseExited(javax.swing.event.MenuDragMouseEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_AY04MenuDragMouseExited
        // TODO add your handling code here:
        
    }//GEN-LAST:event_FSOSEG_MENU_AY04MenuDragMouseExited

    private void FSOSEG_MENU_AY04ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_AY04ActionPerformed
        // TODO add your handling code here:
        help.invokeHelp();
    }//GEN-LAST:event_FSOSEG_MENU_AY04ActionPerformed
   
    private void FSOSEG_MENU_AY01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_AY01ActionPerformed
       sysinfo= new FSOSEG_SYSINFO_FRAME();
        sysinfo.setVisible(true);
        sysinfo.setLocationRelativeTo(null);
        sysinfo.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }//GEN-LAST:event_FSOSEG_MENU_AY01ActionPerformed

    private void FSOSEG_MENU_AY05ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_AY05ActionPerformed
        help.invokeHelp("./Documents/java2.pdf");
    }//GEN-LAST:event_FSOSEG_MENU_AY05ActionPerformed

    private void FSOSEG_MENU_AY03ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_AY03ActionPerformed
        help.invokeHelpOnLine();
    }//GEN-LAST:event_FSOSEG_MENU_AY03ActionPerformed
  
    private void FSOSEG_MENU_TB0101ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TB0101ActionPerformed
       String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                FSOSEGTABFRM012 transacciones =new FSOSEGTABFRM012();
                Modelo m = new Modelo();
                m.view12(transacciones);
                Controlador c = new Controlador(m, transacciones);
                m.Iniciar_T_Transacciones();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_TB0101ActionPerformed

    private void FSOSEG_MENU_TB015ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TB015ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
                
            }else {
                FSOSEGTABFRM001 persona=new FSOSEGTABFRM001();
           //     persona.setName("0403");
                persona.setName(log.getModulo());
                persona.setVisible(true);
                persona.setLocationRelativeTo(null);
               /* Modelo m = new Modelo();
                m.view(persona);
                Controlador c = new Controlador(m, persona);
                m.Iniciar_T_Persona();*/
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_FSOSEG_MENU_TB015ActionPerformed

    private void FSOSEG_MENU_TB0112ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TB0112ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                FSOSEGTABFRM003 asiento =new FSOSEGTABFRM003();
                Modelo m = new Modelo();
                m.view3(asiento);
                Controlador c = new Controlador(m, asiento);
                m.Iniciar_T_Asiento();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    }//GEN-LAST:event_FSOSEG_MENU_TB0112ActionPerformed

    private void FSOSEG_MENU_TB016ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TB016ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                FSOSEGTABFRM002 producto =new FSOSEGTABFRM002();
                producto.setName(log.getModulo());
                producto.setVisible(true);
              //  Modelo m = new Modelo();
              //  m.view2(producto);
              //  Controlador c = new Controlador(m, producto);
             //   m.Iniciar_T_Producto();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_TB016ActionPerformed

    private void FSOSEG_MENU_TB0111ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TB0111ActionPerformed
       String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                FSOSEGTABFRM013 plan_contable =new FSOSEGTABFRM013();
                Modelo m = new Modelo();
                m.view13(plan_contable);
                Controlador c = new Controlador(m, plan_contable);
                m.Iniciar_T_Plan_Contable();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_TB0111ActionPerformed

    private void FSOSEG_MENU_TB102ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TB102ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                FSOSEGTABFRM007 t_trans =new FSOSEGTABFRM007();
                t_trans.setName(log.getModulo());
                Modelo m = new Modelo();
                m.view7(t_trans);
                Controlador c = new Controlador(m, t_trans);
                m.Iniciar_T_Tipo_Transaccion();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_TB102ActionPerformed

    private void FSOSEG_MENU_TB0113ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TB0113ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                FSOSEGTABFRM020 contabilidad =new FSOSEGTABFRM020();
                Modelo m = new Modelo();
                m.view20(contabilidad);
                Controlador c = new Controlador(m, contabilidad);
                m.Iniciar_T_Contabilidad();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_TB0113ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        FSOSEGTABFRM021 transaccion=new FSOSEGTABFRM021();
            transaccion.setLocationRelativeTo(null);
            transaccion.setVisible(true);
        
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        FSOSEGTABFRM022 transaccion_contable=new FSOSEGTABFRM022();
        transaccion_contable.setVisible(true);
        transaccion_contable.setLocationRelativeTo(null);
        
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void FSOSEG_MENU_UT02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_UT02ActionPerformed
        String cond=log.getModulo();//evt.getActionCommand();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else{
                FSOSEG_CALCULADORA_FRAME cal = new FSOSEG_CALCULADORA_FRAME ();
                cal.setTitle("Calendario");
                cal.setVisible(true);

                //FSOSEG_FUNCIONES.iniciarFrame(cal,"Calculadora");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_UT02ActionPerformed

    private void FSOSEG_MENU_UT01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_UT01ActionPerformed
        String cond=log.getModulo();//evt.getActionCommand();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else{
                FSOSEG_CALCULADORA_FRAME c = new FSOSEG_CALCULADORA_FRAME ();
                c.setTitle("Calculadora");
                c.setVisible(true);

                // FSOSEG_FUNCIONES.iniciarFrame(c,"Calculadora");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_UT01ActionPerformed

    private void FSOSEG_MENU_PR09ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_PR09ActionPerformed
        // TODO add your handling code here:
        
        String cond=log.getModulo();//evt.getActionCommand();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else{
                FSOSEGTABFRM024 cierrediario = new FSOSEGTABFRM024();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
        

    }//GEN-LAST:event_FSOSEG_MENU_PR09ActionPerformed

    private void FSOSEG_MENU_PR07ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_PR07ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                FSOSEGTABFRM009 calculo =new FSOSEGTABFRM009();
                calculo.setName(log.getModulo());
                Modelo m = new Modelo();
                m.view9(calculo);
                Controlador c = new Controlador(m, calculo);
                m.Iniciar_T_Calculo();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_PR07ActionPerformed

    private void FSOSEG_MENU_PR06ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_PR06ActionPerformed
        // TODO add your handling code here:
        String cond=log.getModulo();//evt.getActionCommand();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else{
                FSOSEGTABFRM025 rend = new FSOSEGTABFRM025 ();
                rend.setTitle("Pago de Rendimiento");
                rend.setVisible(true);

                // FSOSEG_FUNCIONES.iniciarFrame(c,"Calculadora");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }//GEN-LAST:event_FSOSEG_MENU_PR06ActionPerformed

    private void FSOSEG_MENU_PRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_PRActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_FSOSEG_MENU_PRActionPerformed

    private void FSOSEG_MENU_RE07ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_RE07ActionPerformed
        // TODO add your handling code here:
          FSOSEG_PARAMREPORT_FRAME fram=new FSOSEG_PARAMREPORT_FRAME();
    }//GEN-LAST:event_FSOSEG_MENU_RE07ActionPerformed

    private void FSOSEG_MENU_MO05ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_MO05ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            //perm = FSOSEG_FUNCIONES.getPermission(cond);
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                FSOSEGTABFRM005 perfil =new FSOSEGTABFRM005();
                perfil.setName(log.getModulo());
                Modelo m = new Modelo();
                m.view5(perfil);
                Controlador c = new Controlador(m, perfil);
                m.Iniciar_T_Perfil();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_MO05ActionPerformed

    private void FSOSEG_MENU_TB0134ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TB0134ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                FSOSEGTABFRM015 localizacion =new FSOSEGTABFRM015();
                localizacion.setName(log.getModulo());
                Modelo m = new Modelo();
                m.view15(localizacion);
                Controlador c = new Controlador(m, localizacion);
                m.Iniciar_T_Localizacion();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_TB0134ActionPerformed

    private void FSOSEG_MENU_TB0133ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TB0133ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                FSOSEGTABFRM010 configuracion =new FSOSEGTABFRM010();
                configuracion.setName(log.getModulo());
                Modelo m = new Modelo();
                m.view10(configuracion);
                Controlador c = new Controlador(m, configuracion);
                m.Iniciar_T_Configuracion();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_TB0133ActionPerformed

    private void FSOSEG_MENU_TB0132ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TB0132ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                /*  FSOSEGTABFRM018 dia_feriado =new FSOSEGTABFRM018();
                Modelo m = new Modelo();
                m.view18(dia_feriado);
                Controlador c = new Controlador(m, dia_feriado);
                m.Iniciar_T_dia_feriado();*/
                FSOSEGTABFRM026 dia_feriado=new FSOSEGTABFRM026();
                //  dia_feriado.setVisible(true);
                //  dia_feriado.setLocationRelativeTo(null);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_TB0132ActionPerformed

    private void FSOSEG_MENU_TB0131ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TB0131ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                FSOSEGTABFRM014 moneda =new FSOSEGTABFRM014();
                Modelo m = new Modelo();
                m.view14(moneda);
                Controlador c = new Controlador(m, moneda);
                m.Iniciar_T_Moneda();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_TB0131ActionPerformed

    private void FSOSEG_MENU_TB0123ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TB0123ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else{
                FSOSEGTABFRM016 log=new FSOSEGTABFRM016();
                log.setSize(Principal.width,Principal.height);
                log.setVisible(true);
                log.setLocationRelativeTo(null);
                log.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_FSOSEG_MENU_TB0123ActionPerformed

    private void FSOSEG_MENU_TB0121ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_TB0121ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                FSOSEGTABFRM017 usuario =new FSOSEGTABFRM017();
                usuario.setName(log.getModulo());
                Modelo m = new Modelo();
                m.view17(usuario);
                Controlador c = new Controlador(m, usuario);
                m.Iniciar_T_Usuario();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_TB0121ActionPerformed

    private void FSOSEG_MENU_MO01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_MO01ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                FSOSEGTABFRM011 instalacion =new FSOSEGTABFRM011();
                instalacion.setName(log.getModulo());
                Modelo m = new Modelo();
                m.view11(instalacion);
                Controlador c = new Controlador(m, instalacion);
                m.Iniciar_T_Instalacion();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_MO01ActionPerformed

    private void FSOSEG_MENU_UT03ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_UT03ActionPerformed

        try {
            DrawCalendar myCalendar = new DrawCalendar(); // window for drawing
            JFrame application = new JFrame("Calendar"); // the program itself
            application.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // set frame to exit when it is closed
            application.setSize(800, 800);      // Set default size of JFrame Calendar
            application.setLocation(230, 0);
            application.setResizable(true);     // Allow JFrame to be resized
            application.add(myCalendar);        // add calender GUI to JFrame
            application.setVisible(true);
        } catch (InstantiationException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_FSOSEG_MENU_UT03ActionPerformed

    private void FSOSEG_MENU_PR10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_PR10ActionPerformed
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else{
                FSOSEGTABFRM027 reverso = new FSOSEGTABFRM027();
                reverso.setTitle("Reverso");
                reverso.setVisible(true);

            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_FSOSEG_MENU_PR10ActionPerformed

    private void FSOSEG_MENU_PR08ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_MENU_PR08ActionPerformed
        // TODO add your handling code here:
         String cond=log.getModulo();//evt.getActionCommand();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else{
                FSOSEGTABFRM028 cierrediario = new FSOSEGTABFRM028();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_FSOSEG_MENU_PR08ActionPerformed
        
    

    protected void isAuthenticatedPerfilMenu(int codeMenu){
        String cond=log.getModulo();
        int perm = 0;
        try {
            perm = FSOSEG_FUNCIONES.getPermission(cond);
            //perm = FSOSEG_FUNCIONES.getPermissionPrueba(cond);
            if (perm ==0){
                FSOSEG_FUNCIONES.setMessagePerm();
            }else {
                switch(codeMenu){
                    //Definir ventanas
                }
                FSOSEGTABFRM005 perfil =new FSOSEGTABFRM005();
                perfil.setName(log.getModulo());
                Modelo m = new Modelo();
                m.view5(perfil);
                Controlador c = new Controlador(m, perfil);
                m.Iniciar_T_Perfil();
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu FSOSEG_MENU_AR;
    private javax.swing.JMenuItem FSOSEG_MENU_AR01;
    private javax.swing.JMenuItem FSOSEG_MENU_AR02;
    private javax.swing.JMenu FSOSEG_MENU_AY;
    private javax.swing.JMenuItem FSOSEG_MENU_AY01;
    private javax.swing.JMenuItem FSOSEG_MENU_AY02;
    private javax.swing.JMenuItem FSOSEG_MENU_AY03;
    private javax.swing.JMenuItem FSOSEG_MENU_AY04;
    private javax.swing.JMenuItem FSOSEG_MENU_AY05;
    private javax.swing.JMenuBar FSOSEG_MENU_MN;
    private javax.swing.JMenu FSOSEG_MENU_MO;
    private javax.swing.JMenuItem FSOSEG_MENU_MO01;
    private javax.swing.JMenu FSOSEG_MENU_MO02;
    private javax.swing.JMenu FSOSEG_MENU_MO03;
    private javax.swing.JMenuItem FSOSEG_MENU_MO05;
    private javax.swing.JMenu FSOSEG_MENU_PR;
    private javax.swing.JMenuItem FSOSEG_MENU_PR06;
    private javax.swing.JMenuItem FSOSEG_MENU_PR07;
    private javax.swing.JMenuItem FSOSEG_MENU_PR08;
    private javax.swing.JMenuItem FSOSEG_MENU_PR09;
    private javax.swing.JMenuItem FSOSEG_MENU_PR10;
    private javax.swing.JMenu FSOSEG_MENU_RE;
    private javax.swing.JMenuItem FSOSEG_MENU_RE07;
    private javax.swing.JMenu FSOSEG_MENU_SA;
    private javax.swing.JMenu FSOSEG_MENU_TB;
    private javax.swing.JMenu FSOSEG_MENU_TB010;
    private javax.swing.JMenuItem FSOSEG_MENU_TB0101;
    private javax.swing.JMenu FSOSEG_MENU_TB011;
    private javax.swing.JMenuItem FSOSEG_MENU_TB0111;
    private javax.swing.JMenuItem FSOSEG_MENU_TB0112;
    private javax.swing.JMenuItem FSOSEG_MENU_TB0113;
    private javax.swing.JMenuItem FSOSEG_MENU_TB0121;
    private javax.swing.JMenuItem FSOSEG_MENU_TB0123;
    private javax.swing.JMenuItem FSOSEG_MENU_TB0131;
    private javax.swing.JMenuItem FSOSEG_MENU_TB0132;
    private javax.swing.JMenuItem FSOSEG_MENU_TB0133;
    private javax.swing.JMenuItem FSOSEG_MENU_TB0134;
    private javax.swing.JMenuItem FSOSEG_MENU_TB015;
    private javax.swing.JMenuItem FSOSEG_MENU_TB016;
    private javax.swing.JMenuItem FSOSEG_MENU_TB102;
    private javax.swing.JMenu FSOSEG_MENU_UT;
    private javax.swing.JMenuItem FSOSEG_MENU_UT01;
    private javax.swing.JMenuItem FSOSEG_MENU_UT02;
    private javax.swing.JMenuItem FSOSEG_MENU_UT03;
    private javax.swing.JLabel jLabel_Username;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JLabel jlblDate;
    private javax.swing.JLabel jlblTime;
    // End of variables declaration//GEN-END:variables

    public void run() {
        Thread current = Thread.currentThread();
        while(current==hilo){
            hora();
            jlblTime.setText(tiempo); 
        }
    }
}
