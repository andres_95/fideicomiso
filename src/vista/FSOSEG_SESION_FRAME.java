/*
 * Nombre Objeto: FSOSEG_SESION_FRAME
 * Autor: Abel Laguna
 * Fecha Elab: 24/01/2017
 * Descripción: JFrame del inicio de Sesión en el aplicativo
 * Última Modificación:
 * Autor Última Modificación:
 */
package vista;

import controlador.Auditoria;
import controlador.ControladorSesion;
import controlador.FSOSEG_SYSTEM_CONTROLLER;
import controlador.SesionController;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import com.bimodal.app.beans.Principal;
import com.bimodal.app.conf.GlobalConstants;

public class FSOSEG_SESION_FRAME extends javax.swing.JFrame {
      private FSOSEG_MENU_FRAME men;
      private FSOSEG_SYSTEM_CONTROLLER sys;
      private ControladorSesion controlador;
      private SesionController sesion;
      private Auditoria log;
      public static String roll;
      
    /**
     * Creates new form InterfazP
     */
    public FSOSEG_SESION_FRAME() {
        initComponents();
        setLocationRelativeTo(null);
        log=new Auditoria(this);
        log.setModelo(Principal.getModel());
        this.FSOSEG_SESION_SA.setActionCommand("S");
        this.jButton_Login.setActionCommand("Q");
       controlador = Principal.getSesion();
     }
    
    
   
    public FSOSEG_SESION_FRAME(FSOSEG_SYSTEM_CONTROLLER s) {
        initComponents();
        men=new FSOSEG_MENU_FRAME();
        setLocationRelativeTo(null);
        this.sys=s; 
     }

    @SuppressWarnings("override")
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage("./img/seguros_mercantil.png");
        return retValue;
    } 

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        FSOSEG_SESION_SA = new javax.swing.JButton();
        FSOSEG_SESION_LU = new javax.swing.JLabel();
        FSOSEG_SESION_LP = new javax.swing.JLabel();
        jTextField_Username = new javax.swing.JTextField();
        jPasswordField_Password = new javax.swing.JPasswordField();
        jButton_Login = new javax.swing.JButton();
        jLabel_Mensaje = new javax.swing.JLabel();
        jLabel_Message = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();

        jLabel2.setText("jLabel2");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 102));
        setIconImage(getIconImage());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        FSOSEG_SESION_SA.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        FSOSEG_SESION_SA.setForeground(new java.awt.Color(0, 0, 51));
        FSOSEG_SESION_SA.setText("Salir");
        FSOSEG_SESION_SA.setBorderPainted(false);
        FSOSEG_SESION_SA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                FSOSEG_SESION_SAMouseClicked(evt);
            }
        });
        FSOSEG_SESION_SA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FSOSEG_SESION_SAActionPerformed(evt);
            }
        });
        getContentPane().add(FSOSEG_SESION_SA, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 410, 110, 30));

        FSOSEG_SESION_LU.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        FSOSEG_SESION_LU.setForeground(new java.awt.Color(204, 204, 204));
        FSOSEG_SESION_LU.setText("Usuario:");
        getContentPane().add(FSOSEG_SESION_LU, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 310, 80, 20));

        FSOSEG_SESION_LP.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        FSOSEG_SESION_LP.setForeground(new java.awt.Color(204, 204, 204));
        FSOSEG_SESION_LP.setText("Password:");
        getContentPane().add(FSOSEG_SESION_LP, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 370, 100, -1));

        jTextField_Username.setBackground(new java.awt.Color(0, 0, 51));
        jTextField_Username.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTextField_Username.setForeground(new java.awt.Color(255, 255, 255));
        jTextField_Username.setCaretColor(new java.awt.Color(255, 255, 255));
        jTextField_Username.setName("login"); // NOI18N
        jTextField_Username.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField_UsernameMouseClicked(evt);
            }
        });
        jTextField_Username.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_UsernameActionPerformed(evt);
            }
        });
        jTextField_Username.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField_UsernameKeyPressed(evt);
            }
        });
        getContentPane().add(jTextField_Username, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 310, 240, 30));

        jPasswordField_Password.setBackground(new java.awt.Color(0, 0, 51));
        jPasswordField_Password.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jPasswordField_Password.setForeground(new java.awt.Color(255, 255, 255));
        jPasswordField_Password.setCaretColor(new java.awt.Color(255, 255, 255));
        jPasswordField_Password.setName("password"); // NOI18N
        jPasswordField_Password.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jPasswordField_PasswordActionPerformed(evt);
            }
        });
        jPasswordField_Password.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPasswordField_PasswordKeyPressed(evt);
            }
        });
        getContentPane().add(jPasswordField_Password, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 360, 240, 30));

        jButton_Login.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton_Login.setForeground(new java.awt.Color(0, 0, 51));
        jButton_Login.setText("Ingresar");
        jButton_Login.setBorderPainted(false);
        jButton_Login.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton_LoginMouseClicked(evt);
            }
        });
        jButton_Login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_LoginActionPerformed(evt);
            }
        });
        getContentPane().add(jButton_Login, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 410, 110, 30));

        jLabel_Mensaje.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jLabel_Mensaje.setForeground(new java.awt.Color(255, 102, 102));
        getContentPane().add(jLabel_Mensaje, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 470, 460, 20));
        getContentPane().add(jLabel_Message, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/User.png"))); // NOI18N
        jLabel1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 30, 270, 270));
        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 410, -1, -1));

        jPanel4.setBackground(new java.awt.Color(0, 0, 51));
        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 470, 490, 40));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/FondoAzul.png"))); // NOI18N
        jLabel3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jLabel3KeyTyped(evt);
            }
        });
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 490, 470));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void FSOSEG_SESION_SAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FSOSEG_SESION_SAActionPerformed
        dispose();
    }//GEN-LAST:event_FSOSEG_SESION_SAActionPerformed

    private void jButton_LoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_LoginActionPerformed
       if(controlador.isExistUser(this.jTextField_Username.getText(),String.valueOf(this.jPasswordField_Password.getPassword()))){
          //  Principal.setRol(controlador.getRol());
          //  Principal.setUser(this.jTextField_Username.getText());
            GlobalConstants.setUser(this.jTextField_Username.getText());
            GlobalConstants.setRol(controlador.getRol());
            FSOSEG_MENU_FRAME win2 = new FSOSEG_MENU_FRAME();
            win2.setVisible(true);
            this.dispose();
            
            
        }
       else{
          
           this.jLabel_Mensaje.setText("Usuario y clave inválida");
       }
    }//GEN-LAST:event_jButton_LoginActionPerformed

    private void jButton_LoginMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton_LoginMouseClicked
  
    }//GEN-LAST:event_jButton_LoginMouseClicked

    private void FSOSEG_SESION_SAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_FSOSEG_SESION_SAMouseClicked
        System.exit(0);
    }//GEN-LAST:event_FSOSEG_SESION_SAMouseClicked

    private void jTextField_UsernameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_UsernameKeyPressed
      //  jLabel_Mensaje.setText("");
    }//GEN-LAST:event_jTextField_UsernameKeyPressed

    private void jPasswordField_PasswordKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPasswordField_PasswordKeyPressed
       if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if(controlador.isExistUser(this.jTextField_Username.getText(),String.valueOf(this.jPasswordField_Password.getPassword()))){
                Principal.setRol(controlador.getRol());
                Principal.setUser(this.jTextField_Username.getText());
                GlobalConstants.setUser(this.jTextField_Username.getText());
                GlobalConstants.setRol(controlador.getRol());
                FSOSEG_MENU_FRAME win2 = new FSOSEG_MENU_FRAME();
                win2.setVisible(true);
                this.dispose();
            }
             else{
          
           this.jLabel_Mensaje.setText("Usuario y clave inválida");
       }
        }
    }//GEN-LAST:event_jPasswordField_PasswordKeyPressed

    private void jLabel3KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jLabel3KeyTyped
        // TODO add your handling code here:
        //this.jLabel_Mensaje.setText("");
    }//GEN-LAST:event_jLabel3KeyTyped

    private void jTextField_UsernameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField_UsernameMouseClicked
        // TODO add your handling code here:
       // this.jLabel_Mensaje.setText("");
    }//GEN-LAST:event_jTextField_UsernameMouseClicked

    private void jPasswordField_PasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jPasswordField_PasswordActionPerformed
        // TODO add your handling code here:
          // this.jLabel_Mensaje.setText("");
        //  this.jLabel_Mensaje.setText("");
    }//GEN-LAST:event_jPasswordField_PasswordActionPerformed

    private void jTextField_UsernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_UsernameActionPerformed
        // TODO add your handling code here
       // this.jLabel_Mensaje.setText("");
    }//GEN-LAST:event_jTextField_UsernameActionPerformed

    @SuppressWarnings("Convert2Lambda")
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FSOSEG_SESION_FRAME.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FSOSEG_SESION_FRAME.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FSOSEG_SESION_FRAME.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FSOSEG_SESION_FRAME.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FSOSEG_SESION_FRAME().setVisible(true);
            }
        });
    }
	
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel FSOSEG_SESION_LP;
    private javax.swing.JLabel FSOSEG_SESION_LU;
    private javax.swing.JButton FSOSEG_SESION_SA;
    public static javax.swing.JButton jButton_Login;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel_Mensaje;
    private javax.swing.JLabel jLabel_Message;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPasswordField jPasswordField_Password;
    private javax.swing.JTextField jTextField_Username;
    // End of variables declaration//GEN-END:variables
}
