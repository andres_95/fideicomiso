/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.util;

import java.util.StringTokenizer;

/**
 *
 * @author Becerra
 */
public class Tokens {
   private String[] tokens;
   private int count;
     public Tokens(String string,String delim){
         StringTokenizer str=new StringTokenizer(string,delim);
           tokens=new String[str.countTokens()];
           int i=0;
           count=str.countTokens();
           while(str.hasMoreTokens()){
                  tokens[i]=str.nextToken();
                  i++;
              }
     }
    public int getCount(){
       return count;
    }
    public String[] getTokens(){
        return tokens;
    }
}
