/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.util;


import com.toedter.calendar.JDateChooser;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.Component;
import java.util.Date;
import javax.swing.*;

/**
 * @author usuario1 
 */
public class TextParse extends javax.swing.JTextField {
     Component comp;
       public TextParse(Component comp){
             this.comp=comp;
          
       }
       public String getName(){
           if(comp instanceof JComboBox)
              return (((JComboBox)comp).getName());
           else
           if(comp instanceof JDateChooser)
               return (((JDateChooser)comp).getName());
           
             return null; 
           }
        public String getText(){
              DateFormat date=new SimpleDateFormat("yyyy-MM-dd");
           if(comp instanceof JComboBox)
              return ((String)((JComboBox)comp).getSelectedItem()).split("-")[0];
           else
           if(comp.getClass().getName().equals("com.toedter.calendar.JDateChooser")){
               if((!((JDateChooser)comp).getName().equals("fecha_activacion")) &&  (!((JDateChooser)comp).getName().equals("fecha_actualizacion"))){
                    return date.format(((JDateChooser)comp).getDate());
              }
               else{
                   Date fecha = new Date();
                  return date.format(fecha);
               }
           }
             return null; 
           
        }
    
    
}
