/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.conf;

import com.bimodal.app.beans.Principal;
import com.bimodal.app.db.FSOSEG_CONNECT_MODULE;
import com.bimodal.app.db.LoggerModel;
import controlador.Auditoria;
import javax.swing.JFrame;

/**
 * Esta define valores que seran de uso en los distintos procesos del sistema
 * 
 * @author Jvaldez
 * @version 1.0
 * 
 */

 
  
public class GlobalConstants {
    public static int PERFIL_USER;
    public static FSOSEG_CONNECT_MODULE conector=new FSOSEG_CONNECT_MODULE();
    public static Auditoria log;
    public static LoggerModel model=new LoggerModel();
    public static String user;
    public static String rol;
    public static LoggerModel LogRepository(){
           return model;
    }
    public static void setUser(String user){
         GlobalConstants.user=user;
    }
    public static String getUser(){
       return GlobalConstants.user;
    }
    public static void setRol(String rol){
       GlobalConstants.rol=rol;
    }
    public static String getRol(){
       return rol;
    }
    public static void InitializeConnection(){
        conector.setUser(FilePropertiesDB.getUSER_DB(),FilePropertiesDB.getUSER_PASS_DB());
    }
    public static void CloseConnection(){
        conector.endConnection();
    }
    public static int getPERFIL_USER() {
        return PERFIL_USER;
    }

    public static void setPERFIL_USER(int PERFIL_USER) {
        GlobalConstants.PERFIL_USER = PERFIL_USER;
    }
    public static FSOSEG_CONNECT_MODULE getConnector(){
        return conector;
    }
    public static void InitAuditoria(JFrame frame){
        log = new Auditoria(frame);
        log.setModel(Principal.getModel());
    } 
    public static Auditoria getAuditoria(){
         return log;
    }
}
