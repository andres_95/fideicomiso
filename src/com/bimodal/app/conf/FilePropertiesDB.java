
package com.bimodal.app.conf;


public class FilePropertiesDB {
    
    public static String USER_DB="AppFSOSEG";
    public static String USER_PASS_DB="bimodal2017";
    public static String URL_DB="localhost";
    public static String PORT_DB="3306";
    public static String DB_NAME="fsosegdba01";

    public static String getUSER_DB() {
        return USER_DB;
    }

    public static void setUSER_DB(String USER_DB) {
        FilePropertiesDB.USER_DB = USER_DB;
    }

    public static String getUSER_PASS_DB() {
        return USER_PASS_DB;
    }

    public static void setUSER_PASS_DB(String USER_PASS_DB) {
        FilePropertiesDB.USER_PASS_DB = USER_PASS_DB;
    }

    public static String getURL_DB() {
        return URL_DB;
    }

    public static void setURL_DB(String URL_DB) {
        FilePropertiesDB.URL_DB = URL_DB;
    }

    public static String getPORT_DB() {
        return PORT_DB;
    }

    public static void setPORT_DB(String PORT_DB) {
        FilePropertiesDB.PORT_DB = PORT_DB;
    }

    public static String getDB_NAME() {
        return DB_NAME;
    }

    public static void setDB_NAME(String DB_NAME) {
        FilePropertiesDB.DB_NAME = DB_NAME;
    }
    
    
    
}
