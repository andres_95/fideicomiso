 /*
 * Nombre Objeto: Principal
 * Autor: Josue Becerra
 * Fecha Elab: 
 * Descripción: 
 * Última Modificación:
 * Autor Última Modificación:
 */
package com.bimodal.app.beans;
import com.bimodal.app.administracion.view.FSOSEGTABFRM001;
import com.bimodal.app.administracion.view.FSOSEGTABFRM002;
import controlador.Controlador;
import controlador.FSOSEG_SYSTEM_CONTROLLER;
import controlador.SystemSet;
import java.awt.Image;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import com.bimodal.app.db.DataModel;
import com.bimodal.app.db.Modelo;
import com.bimodal.app.modulos.view.FSOSEGTABFRM016;
import vista.FSOSEG_SESION_FRAME;
import vista.FSOSEG_MENU_FRAME;
import vista.Vista;
import Tablas.*;
import controlador.ControladorSesion;
import controlador.SesionController;
import com.bimodal.app.db.Modelo;

public class Principal {
    private static SystemSet set;
    private static String user;  
    private static String rol;
    public static final int width=1141;
    public static final int height=766;
    private static String UserId;
    public static DataModel getModel(){
        return set.getModel();
    }
    
    public static void setUser(String user){
        Principal.user=user;
    }
    public static void setUserId(String id){
       Principal.UserId=id;
    }
    public static String getUserId(){
        return UserId;
    }        
    public static String getUser(){
        return user;
    }
 
    public static void setRol(String rol){
        Principal.rol=rol;
    }
    
    public static String getRol(){
        return rol;
    }

    public static void iniciar(){
        set=new SystemSet();
    }
    
    public static FSOSEG_SYSTEM_CONTROLLER getVersion(){
        return set.getSystem();
    }
    
    public static SystemSet getSystemCore(){
        return set;
    }
    public static ControladorSesion getSesion() {
      return set.getSesionControl();
    }
            
    public static void main(String[] args) throws SQLException{    
       try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
        set=new SystemSet();
        FSOSEG_SESION_FRAME interfaz=new FSOSEG_SESION_FRAME();
        interfaz.setVisible(true);
        interfaz.setLocationRelativeTo(null);
        interfaz.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
    
}
