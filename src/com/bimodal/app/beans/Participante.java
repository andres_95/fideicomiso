/*
 * Nombre Objeto: Participante
 * Autor: Hetsy Rodríguez
 * Fecha Elab: 10/02/2017
 * Descripción: Estructura del Reporte ## 
 * Última Modificación:
 * Autor Última Modificación:
 */
package com.bimodal.app.beans;
import static org.eclipse.persistence.platform.database.oracle.plsql.OraclePLSQLTypes.Int;

public class Participante {
    
    private String id;
    private String nombre;
    private String apellido;
    private String edad;
    private String sexo;
    private String direccion;
    private String saldo;
    
    public Participante(String nombre, String apellido, String edad, String sexo, String direccion, String saldo){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.sexo = sexo;
        this.direccion= direccion;
        this.saldo = saldo;
    }

    public Participante() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }
    
}
