/*
 * Nombre Objeto: SessionModel
 * Autor: 
 * Fecha Elab: 
 * Descripción: 
 * Última Modificación:
 * Autor Última Modificación:
 */
package com.bimodal.app.db;

import com.bimodal.app.db.FSOSEG_CONNECT_MODULE;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SessionModel {
    FSOSEG_CONNECT_MODULE conect;
    String rol;
        public SessionModel(FSOSEG_CONNECT_MODULE conect){
          this.conect=conect;
        }
       public boolean find(String user,String password){
           ArrayList<String> list=new ArrayList<>();
            list.add(user);
            list.add(password);
            ResultSet res=null;
            res=conect.executeQuery("SELECT `login`, `password`,cod_perfil FROM `t_usuario` WHERE `login` = ? AND `password` = ?",list);
        try {
              if(res.isBeforeFirst()){
                  res.next();
                 rol=res.getString(3);
                 res.beforeFirst();
              }
            return res.isBeforeFirst();
        } catch (SQLException ex) {
            Logger.getLogger(SessionModel.class.getName()).log(Level.SEVERE, null, ex);
        }
           return false;
       }
       
       public String getRol(){
           return rol;
        }
}
