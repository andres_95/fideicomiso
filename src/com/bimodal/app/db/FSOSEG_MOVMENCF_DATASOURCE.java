/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;


import java.util.List;
import java.util.List;
import java.util.ArrayList;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
/**
 *
 * @author Becerra
 */
public class FSOSEG_MOVMENCF_DATASOURCE extends Datasource {
    private List<FSOSEG_MOVMENCF_MODEL> list=new ArrayList<>();
    private int indice=-1;

    @Override
    public boolean next()  {
        return ++indice<list.size();
    }

    @Override
    public Object getFieldValue(JRField jrf)  {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       Object obj=null;
       
           if("numfso".equals(jrf.getName()))
               obj=list.get(indice).getNumfso();
           else
           /*if("saldoInicial".equals(jrf.getName()))
               obj=list.get(indice).getSaldoInicial();
           else*/
           if("Debitos".equals(jrf.getName()))
               obj=list.get(indice).getDebito();
           else
           if("Creditos".equals(jrf.getName()))
               obj=list.get(indice).getCredito();
           else
             if("saldoActual".equals(jrf.getName()))
              obj=list.get(indice).getSaldoFinal();
           
           
           return obj;
               
    }
    
    public void addAll(List<FSOSEG_MOVMENCF_MODEL> list){
       this.list=list;
    }
    public void add(FSOSEG_MOVMENCF_MODEL modelo){
       list.add(modelo);
    }

    @Override
    public void add(Object modelo) {
       list.add((FSOSEG_MOVMENCF_MODEL) modelo);//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
