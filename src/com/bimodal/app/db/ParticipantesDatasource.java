
package com.bimodal.app.db;

import com.bimodal.app.beans.Participante;
import java.util.List;
import java.util.ArrayList;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;


public class ParticipantesDatasource implements JRDataSource {

    private final List<Participante> listaParticipantes = new ArrayList<Participante>();
    private int indiceParticipanteActual = -1;

    @Override
    public boolean next() throws JRException {
        return ++indiceParticipanteActual < listaParticipantes.size();
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        Object valor = null;

        if ("nombre".equals(jrf.getName())) {
            valor = listaParticipantes.get(indiceParticipanteActual).getNombre();
        } else if ("apellido".equals(jrf.getName())) {
            valor = listaParticipantes.get(indiceParticipanteActual).getApellido();
        } else if ("edad".equals(jrf.getName())) {
            valor = listaParticipantes.get(indiceParticipanteActual).getEdad();
        } else if ("sexo".equals(jrf.getName())) {
            valor = listaParticipantes.get(indiceParticipanteActual).getSexo();
        } else if ("direccion".equals(jrf.getName())) {
            valor = listaParticipantes.get(indiceParticipanteActual).getDireccion();
        } else if ("saldo".equals(jrf.getName())) {
            valor = listaParticipantes.get(indiceParticipanteActual).getSaldo();
        }

        return valor;
    }

    public void addParticipante(Participante participante) {
        this.listaParticipantes.add(participante);
    }

}
