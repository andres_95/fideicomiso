/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;

import com.bimodal.app.administracion.view.FSOSEGTABFRM020;
import com.bimodal.app.modulos.view.FSOSEGTABFRM006;
import com.bimodal.app.modulos.view.FSOSEGTABFRM005;
import com.bimodal.app.administracion.view.FSOSEGTABFRM003;
import com.bimodal.app.administracion.view.FSOSEGTABFRM001;
import com.bimodal.app.administracion.view.FSOSEGTABFRM002;
import com.bimodal.app.administracion.view.FSOSEGTABFRM013;
import com.bimodal.app.administracion.view.FSOSEGTABFRM012;
import com.bimodal.app.administracion.view.FSOSEGTABFRM007;
import com.bimodal.app.procesos.view.FSOSEGTABFRM009;
import com.bimodal.app.modulos.view.FSOSEGTABFRM015;
import com.bimodal.app.modulos.view.FSOSEGTABFRM010;
import com.bimodal.app.modulos.view.FSOSEGTABFRM018;
import com.bimodal.app.modulos.view.FSOSEGTABFRM014;
import com.bimodal.app.modulos.view.FSOSEGTABFRM017;
import com.bimodal.app.modulos.view.FSOSEGTABFRM011;
import vista.Vista;
import java.awt.TextField;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Tablas.*;
import com.bimodal.app.conf.GlobalConstants;
import static com.bimodal.app.modulos.view.FSOSEGTABFRM006.Tabla_acciones;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;
import java.awt.Checkbox;
import java.awt.Color;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.control.cell.CheckBoxTreeTableCell;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableModel;

public class Modelo {
    //JVALDEZ 07/04/2017
    //Esta conexion se debe abrir unicamente al realizar la operacion, y se debe cerrar la misma luego de la operacion
    Connection cn = GlobalConstants.getConnector().getConnection();
    Vista v = new Vista();
    FSOSEGTABFRM001 persona;
    FSOSEGTABFRM002 producto;
    FSOSEGTABFRM003 asiento;
    FSOSEGTABFRM005 perfil;
    FSOSEGTABFRM006 t_perfil;
    FSOSEGTABFRM007 t_trans;
    FSOSEGTABFRM008 p_persona;
    FSOSEGTABFRM009 calculo;
    FSOSEGTABFRM010 configuracion;
    FSOSEGTABFRM011 instalacion;
    FSOSEGTABFRM012 transaccion;
    FSOSEGTABFRM013 plan_contable;
    FSOSEGTABFRM014 moneda;
    FSOSEGTABFRM015 localizacion;
    FSOSEGTABFRM017 usuario;
    FSOSEGTABFRM018 dia_feriado;
    FSOSEGTABFRM020 contabilidad;
    private boolean exitosa;

    public void view(FSOSEGTABFRM001 persona) {
        this.persona = persona;
    }

    public void view2(FSOSEGTABFRM002 producto) {
        this.producto = producto;
    }

    public void view3(FSOSEGTABFRM003 asiento) {
        this.asiento = asiento;
    }

    public void view5(FSOSEGTABFRM005 perfil) {
        this.perfil = perfil;
    }

    public void view6(FSOSEGTABFRM006 t_perfil) {
        this.t_perfil = t_perfil;
    }

    public void view7(FSOSEGTABFRM007 t_trans) {
        this.t_trans = t_trans;
    }

    public void view8(FSOSEGTABFRM008 p_persona) {
        this.p_persona = p_persona;
    }

    public void view9(FSOSEGTABFRM009 calculo) {
        this.calculo = calculo;
    }

    public void view10(FSOSEGTABFRM010 configuracion) {
        this.configuracion = configuracion;
    }

    public void view11(FSOSEGTABFRM011 instalacion) {
        this.instalacion = instalacion;
    }

    public void view12(FSOSEGTABFRM012 transaccion) {
        this.transaccion = transaccion;
    }

    public void view13(FSOSEGTABFRM013 plan_contable) {
        this.plan_contable = plan_contable;
    }

    public void view14(FSOSEGTABFRM014 moneda) {
        this.moneda = moneda;
    }

    public void view15(FSOSEGTABFRM015 localizacion) {
        this.localizacion = localizacion;
    }

    public void view17(FSOSEGTABFRM017 usuario) {
        this.usuario = usuario;
    }

    public void view18(FSOSEGTABFRM018 dia_feriado) {
        this.dia_feriado = dia_feriado;
    }
    public void view20(FSOSEGTABFRM020 contabilidad) {
        this.contabilidad = contabilidad;
    }

   /* public Connection Conexion() {
        Connection cc=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cc = DriverManager.getConnection("jdbc:mysql://localhost/fsosegdba01", "AppFSOSEG", "bimodal2017");
            System.out.println("Se ha iniciado la conexión con la base de datos de forma exitosa");

        } catch (Exception ex) {
            System.out.println(ex);
        }
        return cc;
    }*/

   /* public void Iniciar_T_Persona() {
        persona.btnPS_guardar.setEnabled(false);
        persona.btnPS_actualizar.setEnabled(false);
        Limpiar_Tab_Persona();
        Bloquear_Tab_Persona();
    }*/

 /*   public void Iniciar_T_Producto() {
        producto.btnPT_guardar.setEnabled(false);
        Bloquear_Tab_Producto();
        producto.btnPT_actualizar.setEnabled(false);
    }*/

    public void Iniciar_T_Asiento() {
        asiento.btnAC_guardar.setEnabled(false);
        Bloquear_Tab_Asiento();
        asiento.btnAC_actualizar.setEnabled(false);
    }

    public void Iniciar_T_Perfil() throws SQLException {
        perfil.btnPF_guardar.setEnabled(false);
        Limpiar_Tab_Perfil();
        Tabla_Perfil_Mostrar();
    }

    public void Iniciar_T_Tipo_Transaccion() throws SQLException {
        t_trans.btnTT_guardar.setEnabled(false);
        Bloquear_Tab_Tipo_Transaccion();
        t_trans.btnTT_actualizar.setEnabled(false);
        Tabla_Tipo_Transaccion_Mostrar_Modo();
    }

    public void Iniciar_T_Producto_Persona() {
        p_persona.btnPP_guardar.setEnabled(false);
        Bloquear_Tab_Producto_Persona();
        p_persona.btnPP_actualizar.setEnabled(false);
    }

    public void Iniciar_T_Configuracion() {
        configuracion.btnC_guardar.setEnabled(false);
        Bloquear_Tab_Configuracion();
        configuracion.btnC_actualizar.setEnabled(false);
    }

    public void Iniciar_T_Instalacion() {
        instalacion.btnIS_guardar.setEnabled(false);
        Bloquear_Tab_Instalacion();
        instalacion.btnIS_actualizar.setEnabled(false);
    }

    public void Iniciar_T_Transacciones() {
        transaccion.btnT_guardar.setEnabled(false);
        Bloquear_Tab_Transaccion();
        transaccion.btnT_actualizar.setEnabled(false);
    }

    public void Iniciar_T_Plan_Contable() throws SQLException {
        plan_contable.btnPC_guardar.setEnabled(false);
        Bloquear_Tab_Plan_Contable();
        plan_contable.btnPC_actualizar.setEnabled(false);
        Tabla_Plan_Contable_Mostrar_Modo();
    }

    public void Iniciar_T_Usuario() {
        usuario.btnUS_guardar.setEnabled(false);
        usuario.btnUS_actualizar.setEnabled(false);
        Limpiar_Tab_Usuario();
        Bloquear_Tab_Usuario();
    }

    public void Iniciar_T_Moneda() throws SQLException {
        moneda.btnM_guardar.setEnabled(false);
        moneda.btnM_actualizar.setEnabled(false);
        Limpiar_Tab_Moneda();
        Bloquear_Tab_Moneda();
    }

    public void Iniciar_T_Localizacion() throws SQLException {
        localizacion.btnl_guardar.setEnabled(false);
        localizacion.btnl_actualizar.setEnabled(false);
        Limpiar_Tab_Localizacion();
        Bloquear_Tab_Localizacion();
    }

    public void Iniciar_T_dia_feriado() throws SQLException {
        dia_feriado.btnDF_guardar.setEnabled(false);
        dia_feriado.btnDF_actualizar.setEnabled(false);
        Limpiar_Tab_dia_feriado();
        Bloquear_Tab_dia_feriado();
    }

    public void Iniciar_T_Calculo() throws SQLException {
        calculo.btnCC_guardar.setEnabled(false);
        calculo.btnCC_actualizar.setEnabled(false);
        Limpiar_Tab_Calculo();
        Bloquear_Tab_Calculo();
        Tabla_Calculo_Comisiones_Mostrar_Modo();
    }
    
    public void Iniciar_T_Contabilidad() throws SQLException {
        contabilidad.btnC_guardar.setEnabled(false);
        contabilidad.btnC_actualizar.setEnabled(false);
        Bloquear_Tab_Contabilidad();
        
    }

    /**
     * ***************************TABLA PERSONA 001 ******************************
     */
   /* public void Bloquear_Tab_Persona() {
        persona.txtPS_Cod_Persona.setEditable(false);
        persona.txtPS_Ci_Rif.setEditable(false);
        persona.txtPS_Tipo_Persona.setEditable(false);
        persona.txtPS_Id_Persona.setEditable(false);
        persona.txtPS_Sexo.setEnabled(false);
        persona.txtPS_Nom_Persona1.setEditable(false);
        persona.txtPS_Nom_Persona2.setEditable(false);
        persona.txtPS_Ape_Persona1.setEditable(false);
        persona.txtPS_Ape_Persona2.setEditable(false);
        persona.txtPS_Razón_Social_Persona.setEditable(false);
        persona.txtPS_Persona_Contacto.setEditable(false);
        persona.txtPS_Cod_Localización.setEditable(false);
        persona.txtPS_Ape_Persona1.setEditable(false);
        persona.txtPS_Fecha_Nacimiento.setEnabled(false);
        persona.txtPS_Lugar_Nacimiento.setEditable(false);
        persona.txtPS_Estado_Civil.setEnabled(false);
        persona.txtPS_Cod_País_Nacimiento.setEditable(false);
        persona.txtPS_Cod_Ciudad_Nacimiento.setEditable(false);
        persona.txtPS_Cod_Estado_Nacimiento.setEditable(false);
        persona.txtPS_zonapostal.setEditable(false);
        persona.txtPS_Sitio_Referencia.setEditable(false);
        persona.txtPS_Fecha_Activación.setEnabled(false);
        persona.txtPS_Fecha_Actualización.setEnabled(false);
        ((JTextFieldDateEditor) persona.txtPS_Fecha_Nacimiento.getDateEditor()).setDisabledTextColor(Color.BLACK);
        ((JTextFieldDateEditor) persona.txtPS_Fecha_Activación.getDateEditor()).setDisabledTextColor(Color.BLACK);
        ((JTextFieldDateEditor) persona.txtPS_Fecha_Actualización.getDateEditor()).setDisabledTextColor(Color.BLACK);
        UIManager.put("ComboBox.disabledForeground", Color.BLACK);
    }*/

   /* public void Desbloquear_Tab_Persona() {
        persona.txtPS_Cod_Persona.setEditable(true);
        persona.txtPS_Ci_Rif.setEditable(true);
        persona.txtPS_Tipo_Persona.setEditable(true);
        persona.txtPS_Id_Persona.setEditable(true);
        persona.txtPS_Sexo.setEnabled(true);
        persona.txtPS_Nom_Persona1.setEditable(true);
        persona.txtPS_Nom_Persona2.setEditable(true);
        persona.txtPS_Ape_Persona1.setEditable(true);
        persona.txtPS_Ape_Persona2.setEditable(true);
        persona.txtPS_Razón_Social_Persona.setEditable(true);
        persona.txtPS_Persona_Contacto.setEditable(true);
        persona.txtPS_Cod_Localización.setEditable(true);
        persona.txtPS_Ape_Persona1.setEditable(true);
        persona.txtPS_Fecha_Nacimiento.setEnabled(true);
        persona.txtPS_Lugar_Nacimiento.setEditable(true);
        persona.txtPS_Estado_Civil.setEnabled(true);
        persona.txtPS_Cod_País_Nacimiento.setEditable(true);
        persona.txtPS_Cod_Ciudad_Nacimiento.setEditable(true);
        persona.txtPS_Cod_Estado_Nacimiento.setEditable(true);
        persona.txtPS_zonapostal.setEditable(true);
        persona.txtPS_Sitio_Referencia.setEditable(true);
        persona.txtPS_Fecha_Activación.setEnabled(true);
        persona.txtPS_Fecha_Actualización.setEnabled(true);
        persona.txtPS_Sexo.setBackground(Color.WHITE);
    }*/

  /*  public void Limpiar_Tab_Persona() {
        persona.txtPS_Cod_Persona.setText("");
        persona.txtPS_Ci_Rif.setText("");
        persona.txtPS_Tipo_Persona.setText("");
        persona.txtPS_Id_Persona.setText("");
        persona.txtPS_Sexo.setSelectedItem(null);
        persona.txtPS_Nom_Persona1.setText("");
        persona.txtPS_Nom_Persona2.setText("");
        persona.txtPS_Ape_Persona1.setText("");
        persona.txtPS_Ape_Persona2.setText("");
        persona.txtPS_Razón_Social_Persona.setText("");
        persona.txtPS_Persona_Contacto.setText("");
        persona.txtPS_Cod_Localización.setText("");
        persona.txtPS_Ape_Persona1.setText("");
        persona.txtPS_Fecha_Nacimiento.setDate(null);
        persona.txtPS_Lugar_Nacimiento.setText("");
        persona.txtPS_Estado_Civil.setSelectedItem(null);
        persona.txtPS_Cod_País_Nacimiento.setText("");
        persona.txtPS_Cod_Ciudad_Nacimiento.setText("");
        persona.txtPS_Cod_Estado_Nacimiento.setText("");
        persona.txtPS_zonapostal.setText("");
        persona.txtPS_Sitio_Referencia.setText("");
        persona.txtPS_Fecha_Activación.setDate(null);
        persona.txtPS_Fecha_Actualización.setDate(null);
        persona.txtPS_buscar.setText("");
    }*/

   /* public void Tabla_Persona_Insertar() throws SQLException {
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        this.exitosa = true;
        String cod_persona = persona.txtPS_Cod_Persona.getText();
        String ci_rif = persona.txtPS_Ci_Rif.getText();
        String tipo_persona = persona.txtPS_Tipo_Persona.getText();
        String Id_Persona = persona.txtPS_Id_Persona.getText();
        String sexo = (String) persona.txtPS_Sexo.getSelectedItem();
        String nom_Persona1 = persona.txtPS_Nom_Persona1.getText();
        String nom_Persona2 = persona.txtPS_Nom_Persona2.getText();
        String Ape_Persona1 = persona.txtPS_Ape_Persona1.getText();
        String Ape_Persona2 = persona.txtPS_Ape_Persona2.getText();
        String razon_social_persona = persona.txtPS_Razón_Social_Persona.getText();
        String persona_contacto = persona.txtPS_Persona_Contacto.getText();
        String cod_localizacion = persona.txtPS_Cod_Localización.getText();
        String fecha_nacimiento = (persona.txtPS_Fecha_Nacimiento.getDate() != null) ? date.format(persona.txtPS_Fecha_Nacimiento.getDate()) : "";
        String lugar_nacimiento = persona.txtPS_Lugar_Nacimiento.getText();
        String estado_civil = (String) persona.txtPS_Estado_Civil.getSelectedItem();
        String cod_pais_nacimiento = persona.txtPS_Cod_País_Nacimiento.getText();
        String cod_ciudad_nacimiento = persona.txtPS_Cod_Ciudad_Nacimiento.getText();
        String cod_estado_nacimiento = persona.txtPS_Cod_Estado_Nacimiento.getText();
        String zona_postal = persona.txtPS_zonapostal.getText();
        String sitio_referencia = persona.txtPS_Sitio_Referencia.getText();
        try {
            if (cod_persona.equals("") || ci_rif.equals("") || tipo_persona.equals("")
                    || Id_Persona.equals("") || sexo.equals("") || nom_Persona1.equals("")
                    || nom_Persona2.equals("") || Ape_Persona1.equals("") || Ape_Persona2.equals("")
                    || razon_social_persona.equals("") || persona_contacto.equals("") || cod_localizacion.equals("")
                    || fecha_nacimiento.equals("") || lugar_nacimiento.equals("") || estado_civil.equals("")
                    || cod_pais_nacimiento.equals("") || cod_ciudad_nacimiento.equals("") || cod_estado_nacimiento.equals("")
                    || zona_postal.equals("") || sitio_referencia.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                persona.txtPS_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPERINS (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_persona);
                ppt.setString(2, ci_rif);
                ppt.setString(3, tipo_persona);
                ppt.setString(4, Id_Persona);
                ppt.setString(5, sexo.substring(0, 1));
                ppt.setString(6, nom_Persona1);
                ppt.setString(7, nom_Persona2);
                ppt.setString(8, Ape_Persona1);
                ppt.setString(9, Ape_Persona2);
                ppt.setString(10, razon_social_persona);
                ppt.setString(11, persona_contacto);
                ppt.setString(12, cod_localizacion);
                ppt.setString(13, fecha_nacimiento);
                ppt.setString(14, lugar_nacimiento);
                ppt.setString(15, estado_civil.substring(0, 1));
                ppt.setString(16, cod_pais_nacimiento);
                ppt.setString(17, cod_ciudad_nacimiento);
                ppt.setString(18, cod_estado_nacimiento);
                ppt.setString(19, zona_postal);
                ppt.setString(20, sitio_referencia);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                GlobalConstants.getAuditoria().setLog("I");
                persona.txtPS_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
            System.out.println(ex);
            this.exitosa = false;
            persona.txtPS_buscar.setEnabled(true);
        }
        
    }*/

  /*  public boolean Tabla_Persona_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = persona.txtPS_buscar.getText();
        persona.txtPS_buscar.setText("");
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPERCON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            if (usuario.next()) {
                persona.txtPS_Cod_Persona.setText(usuario.getString(1));
                persona.txtPS_Ci_Rif.setText(usuario.getString(2));
                persona.txtPS_Tipo_Persona.setText(usuario.getString(3));
                persona.txtPS_Id_Persona.setText(usuario.getString(4));
                persona.txtPS_Sexo.setSelectedItem((usuario.getString(5).equals("M")) ? "Masculino" : "Femenino");
                persona.txtPS_Nom_Persona1.setText(usuario.getString(6));
                persona.txtPS_Nom_Persona2.setText(usuario.getString(7));
                persona.txtPS_Ape_Persona1.setText(usuario.getString(8));
                persona.txtPS_Ape_Persona2.setText(usuario.getString(9));
                persona.txtPS_Razón_Social_Persona.setText(usuario.getString(10));
                persona.txtPS_Persona_Contacto.setText(usuario.getString(11));
                persona.txtPS_Cod_Localización.setText(usuario.getString(12));
                persona.txtPS_Fecha_Nacimiento.setDate(usuario.getDate(13));
                persona.txtPS_Lugar_Nacimiento.setText(usuario.getString(14));
                persona.txtPS_Estado_Civil.setSelectedItem((usuario.getString(15).equals("S")) ? "Soltero" : "Casado");
                persona.txtPS_Cod_País_Nacimiento.setText(usuario.getString(16));
                persona.txtPS_Cod_Ciudad_Nacimiento.setText(usuario.getString(17));
                persona.txtPS_Cod_Estado_Nacimiento.setText(usuario.getString(18));
                persona.txtPS_zonapostal.setText(usuario.getString(19));
                persona.txtPS_Sitio_Referencia.setText(usuario.getString(20));
                persona.txtPS_Fecha_Activación.setDate(usuario.getDate(21));
                persona.txtPS_Fecha_Actualización.setDate(usuario.getDate(22));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exitosa = false;
                Limpiar_Tab_Persona();
            }
        } catch (SQLException ex) {
            exito = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
        Bloquear_Tab_Persona();
        return exito;
    }*/

  /*  public void Tabla_Persona_Modificar() throws SQLException {
        persona.btnPS_actualizar.setEnabled(true);
        this.exitosa = true;
        String buscar = persona.txtPS_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPERCON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            if (usuario.next()) {
                Desbloquear_Tab_Persona();
                persona.btnPS_insertar.setEnabled(false);
                persona.btnPS_anular.setEnabled(false);
                persona.btnPS_consultar.setEnabled(false);
                persona.txtPS_Cod_Persona.setText(usuario.getString(1));
                persona.txtPS_Ci_Rif.setText(usuario.getString(2));
                persona.txtPS_Tipo_Persona.setText(usuario.getString(3));
                persona.txtPS_Id_Persona.setText(usuario.getString(4));
                persona.txtPS_Sexo.setSelectedItem((usuario.getString(5).equals("M")) ? "Masculino" : "Femenino");
                persona.txtPS_Nom_Persona1.setText(usuario.getString(6));
                persona.txtPS_Nom_Persona2.setText(usuario.getString(7));
                persona.txtPS_Ape_Persona1.setText(usuario.getString(8));
                persona.txtPS_Ape_Persona2.setText(usuario.getString(9));
                persona.txtPS_Razón_Social_Persona.setText(usuario.getString(10));
                persona.txtPS_Persona_Contacto.setText(usuario.getString(11));
                persona.txtPS_Cod_Localización.setText(usuario.getString(12));
                persona.txtPS_Fecha_Nacimiento.setDate(usuario.getDate(13));
                persona.txtPS_Lugar_Nacimiento.setText(usuario.getString(14));
                persona.txtPS_Estado_Civil.setSelectedItem((usuario.getString(15).equals("S")) ? "Soltero" : "Casado");
                persona.txtPS_Cod_País_Nacimiento.setText(usuario.getString(16));
                persona.txtPS_Cod_Ciudad_Nacimiento.setText(usuario.getString(17));
                persona.txtPS_Cod_Estado_Nacimiento.setText(usuario.getString(18));
                persona.txtPS_zonapostal.setText(usuario.getString(19));
                persona.txtPS_Sitio_Referencia.setText(usuario.getString(20));
                persona.txtPS_Fecha_Activación.setDate(usuario.getDate(21));
                persona.txtPS_Fecha_Actualización.setDate(usuario.getDate(22));
                persona.txtPS_Fecha_Activación.setEnabled(false);
                persona.txtPS_Fecha_Actualización.setEnabled(false);
            } else {
                JOptionPane.showMessageDialog(null, "No existe el usuario");
                persona.txtPS_buscar.setText("");
                Bloquear_Tab_Persona();
                persona.btnPS_actualizar.setEnabled(false);
                persona.btnPS_insertar.setEnabled(true);
                persona.btnPS_anular.setEnabled(true);
                persona.btnPS_consultar.setEnabled(true);
                this.exitosa = false;
                Limpiar_Tab_Persona();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
            this.exitosa = false;
        }
    }*/

  /*  public void Tabla_Persona_Actualizar() throws SQLException {
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        String buscar = persona.txtPS_buscar.getText();
        String cod_persona = persona.txtPS_Cod_Persona.getText();
        String ci_rif = persona.txtPS_Ci_Rif.getText();
        String tipo_persona = persona.txtPS_Tipo_Persona.getText();
        String Id_Persona = persona.txtPS_Id_Persona.getText();
        String sexo = (String) persona.txtPS_Sexo.getSelectedItem();
        String nom_Persona1 = persona.txtPS_Nom_Persona1.getText();
        String nom_Persona2 = persona.txtPS_Nom_Persona2.getText();
        String Ape_Persona1 = persona.txtPS_Ape_Persona1.getText();
        String Ape_Persona2 = persona.txtPS_Ape_Persona2.getText();
        String razon_social_persona = persona.txtPS_Razón_Social_Persona.getText();
        String persona_contacto = persona.txtPS_Persona_Contacto.getText();
        String cod_localizacion = persona.txtPS_Cod_Localización.getText();
        String fecha_nacimiento = (persona.txtPS_Fecha_Nacimiento.getDate() != null) ? date.format(persona.txtPS_Fecha_Nacimiento.getDate()) : "";
        String lugar_nacimiento = persona.txtPS_Lugar_Nacimiento.getText();
        String estado_civil = (String) persona.txtPS_Estado_Civil.getSelectedItem();
        String cod_pais_nacimiento = persona.txtPS_Cod_País_Nacimiento.getText();
        String cod_ciudad_nacimiento = persona.txtPS_Cod_Ciudad_Nacimiento.getText();
        String cod_estado_nacimiento = persona.txtPS_Cod_Estado_Nacimiento.getText();
        String zona_postal = persona.txtPS_zonapostal.getText();
        String sitio_referencia = persona.txtPS_Sitio_Referencia.getText();
        try {
            if (cod_persona.equals("") || ci_rif.equals("") || tipo_persona.equals("")
                    || Id_Persona.equals("") || sexo.equals("") || nom_Persona1.equals("")
                    || nom_Persona2.equals("") || Ape_Persona1.equals("") || Ape_Persona2.equals("")
                    || razon_social_persona.equals("") || persona_contacto.equals("") || cod_localizacion.equals("")
                    || fecha_nacimiento.equals("") || lugar_nacimiento.equals("") || estado_civil.equals("")
                    || cod_pais_nacimiento.equals("") || cod_ciudad_nacimiento.equals("") || cod_estado_nacimiento.equals("")
                    || zona_postal.equals("") || sitio_referencia.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                persona.txtPS_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPERACT ('" + buscar + "',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_persona);
                ppt.setString(2, ci_rif);
                ppt.setString(3, tipo_persona);
                ppt.setString(4, Id_Persona);
                ppt.setString(5, sexo.substring(0, 1));
                ppt.setString(6, nom_Persona1);
                ppt.setString(7, nom_Persona2);
                ppt.setString(8, Ape_Persona1);
                ppt.setString(9, Ape_Persona2);
                ppt.setString(10, razon_social_persona);
                ppt.setString(11, persona_contacto);
                ppt.setString(12, cod_localizacion);
                ppt.setString(13, fecha_nacimiento);
                ppt.setString(14, lugar_nacimiento);
                ppt.setString(15, estado_civil.substring(0, 1));
                ppt.setString(16, cod_pais_nacimiento);
                ppt.setString(17, cod_ciudad_nacimiento);
                ppt.setString(18, cod_estado_nacimiento);
                ppt.setString(19, zona_postal);
                ppt.setString(20, sitio_referencia);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                GlobalConstants.getAuditoria().setLog("U");
                persona.btnPS_insertar.setEnabled(true);
                persona.btnPS_anular.setEnabled(true);
                persona.btnPS_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            this.exitosa = false;
            System.out.println(ex);
            persona.btnPS_insertar.setEnabled(true);
            persona.btnPS_anular.setEnabled(true);
            persona.btnPS_consultar.setEnabled(true);
        }
        persona.btnPS_actualizar.setEnabled(false);
        Limpiar_Tab_Persona();
        Bloquear_Tab_Persona();
    }*/

    public void Tabla_Persona_Anular() throws SQLException {
        String buscar = persona.txtPS_Ci_Rif.getText();
        this.exitosa = true;
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPERANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Usuario no existe");
            this.exitosa = false;
        }
    }

    /**
     * *********************** TABLA PRODUCTO 002 ********************************
     */
   /* public void Bloquear_Tab_Producto() {
        producto.txtPT_Comisiones_Admin_Producto.setEditable(false);
        producto.txtPT_Comisiones_Operativa_Producto.setEditable(false);
        producto.txtPT_Impuesto.setEditable(false);
        producto.txtPT_Frecuencia_Cierre_Producto.setEditable(false);
        producto.txtPT_Frecuencia_Pago_Dividendo.setEditable(false);
        producto.txtPT_Código_Producto.setEditable(false);
        producto.txtPT_Descripción_de_Producto.setEditable(false);
        producto.txtPT_Característica_de_Producto.setEditable(false);
        producto.txtPT_Código_de_Moneda.setEditable(false);
        producto.txtPT_Frecuencia_de_Producto.setEditable(false);
        producto.txtPT_Código_Comision_de_Producto.setEditable(false);
    }*/

  /*  public void Desbloquear_Tab_Producto() {
        producto.txtPT_Comisiones_Admin_Producto.setEditable(true);
        producto.txtPT_Comisiones_Operativa_Producto.setEditable(true);
        producto.txtPT_Impuesto.setEditable(true);
        producto.txtPT_Frecuencia_Cierre_Producto.setEditable(true);
        producto.txtPT_Frecuencia_Pago_Dividendo.setEditable(true);
        producto.txtPT_Código_Producto.setEditable(true);
        producto.txtPT_Descripción_de_Producto.setEditable(true);
        producto.txtPT_Característica_de_Producto.setEditable(true);
        producto.txtPT_Código_de_Moneda.setEditable(true);
        producto.txtPT_Frecuencia_de_Producto.setEditable(true);
        producto.txtPT_Código_Comision_de_Producto.setEditable(true);
    }*/

 /*   public void Limpiar_Tab_Producto() {
        producto.txtPT_buscar.setText("");
        producto.txtPT_Comisiones_Admin_Producto.setText("");
        producto.txtPT_Comisiones_Operativa_Producto.setText("");
        producto.txtPT_Impuesto.setText("");
        producto.txtPT_Frecuencia_Cierre_Producto.setText("");
        producto.txtPT_Frecuencia_Pago_Dividendo.setText("");
        producto.txtPT_Código_Producto.setText("");
        producto.txtPT_Descripción_de_Producto.setText("");
        producto.txtPT_Característica_de_Producto.setText("");
        producto.txtPT_Código_de_Moneda.setText("");
        producto.txtPT_Frecuencia_de_Producto.setText("");
        producto.txtPT_Código_Comision_de_Producto.setText("");
    }*/

  /*  public void Tabla_Producto_Insertar() throws SQLException {
        this.exitosa = true;
        String comision_admin = producto.txtPT_Comisiones_Admin_Producto.getText();
        String comision_operativa = producto.txtPT_Comisiones_Operativa_Producto.getText();
        String impuesto = producto.txtPT_Impuesto.getText();
        String frecuencia_cierre = producto.txtPT_Frecuencia_Cierre_Producto.getText();
        String frecuencia_pago = producto.txtPT_Frecuencia_Pago_Dividendo.getText();
        String cod_producto = producto.txtPT_Código_Producto.getText();
        String descripcion = producto.txtPT_Descripción_de_Producto.getText();
        String caracateristica = producto.txtPT_Característica_de_Producto.getText();
        String moneda = producto.txtPT_Código_de_Moneda.getText();
        String frecuencia_producto = producto.txtPT_Frecuencia_de_Producto.getText();
        String cod_comision = producto.txtPT_Código_Comision_de_Producto.getText();
        try {
            if (comision_admin.equals("") || comision_operativa.equals("") || impuesto.equals("")
                    || frecuencia_cierre.equals("") || frecuencia_pago.equals("") || cod_producto.equals("")
                    || descripcion.equals("") || caracateristica.equals("") || moneda.equals("")
                    || frecuencia_producto.equals("") || cod_comision.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                producto.txtPT_buscar.setEnabled(true);
                this.exitosa = false;
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPROINS (?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_producto);
                ppt.setString(2, descripcion);
                ppt.setString(3, caracateristica);
                ppt.setString(4, moneda);
                ppt.setString(5, frecuencia_producto);
                ppt.setString(6, cod_comision);
                ppt.setString(7, comision_admin);
                ppt.setString(8, comision_operativa);
                ppt.setString(9, impuesto);
                ppt.setString(10, frecuencia_cierre);
                ppt.setString(11, frecuencia_pago);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                GlobalConstants.getAuditoria().setLog("I");
                producto.txtPT_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
            System.out.println(ex);
            this.exitosa = false;
            producto.txtPT_buscar.setEnabled(true);
        }
    }

    public boolean Tabla_Producto_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = producto.txtPT_buscar.getText();
        producto.txtPT_buscar.setText("");
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPROCON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            this.exitosa = true;
            if (usuario.next()) {
                producto.txtPT_Código_Producto.setText(usuario.getString(1));
                producto.txtPT_Descripción_de_Producto.setText(usuario.getString(2));
                producto.txtPT_Característica_de_Producto.setText(usuario.getString(3));
                producto.txtPT_Código_de_Moneda.setText(usuario.getString(4));
                producto.txtPT_Frecuencia_de_Producto.setText(usuario.getString(5));
                producto.txtPT_Código_Comision_de_Producto.setText(usuario.getString(6));
                producto.txtPT_Comisiones_Admin_Producto.setText(usuario.getString(7));
                producto.txtPT_Comisiones_Operativa_Producto.setText(usuario.getString(8));
                producto.txtPT_Impuesto.setText(usuario.getString(9));
                producto.txtPT_Frecuencia_Cierre_Producto.setText(usuario.getString(10));
                producto.txtPT_Frecuencia_Pago_Dividendo.setText(usuario.getString(11));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exito = false;
                this.exitosa = false;
                Limpiar_Tab_Producto();
            }
        } catch (SQLException ex) {
            exito = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
        Bloquear_Tab_Producto();
        return exito;
    }*/

   /* public void Tabla_Producto_Modificar() throws SQLException {
        producto.btnPT_actualizar.setEnabled(true);
        this.exitosa = true;
        String buscar = producto.txtPT_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPROCON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            if (usuario.next()) {
                Desbloquear_Tab_Producto();
                producto.btnPT_insertar.setEnabled(false);
                producto.btnPT_anular.setEnabled(false);
                producto.btnPT_consultar.setEnabled(false);
                producto.txtPT_Código_Producto.setText(usuario.getString(1));
                producto.txtPT_Descripción_de_Producto.setText(usuario.getString(2));
                producto.txtPT_Característica_de_Producto.setText(usuario.getString(3));
                producto.txtPT_Código_de_Moneda.setText(usuario.getString(4));
                producto.txtPT_Frecuencia_de_Producto.setText(usuario.getString(5));
                producto.txtPT_Código_Comision_de_Producto.setText(usuario.getString(6));
                producto.txtPT_Comisiones_Admin_Producto.setText(usuario.getString(7));
                producto.txtPT_Comisiones_Operativa_Producto.setText(usuario.getString(8));
                producto.txtPT_Impuesto.setText(usuario.getString(9));
                producto.txtPT_Frecuencia_Cierre_Producto.setText(usuario.getString(10));
                producto.txtPT_Frecuencia_Pago_Dividendo.setText(usuario.getString(11));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el usuario");
                producto.txtPT_buscar.setText("");
                Bloquear_Tab_Producto();
                producto.btnPT_actualizar.setEnabled(false);
                producto.btnPT_insertar.setEnabled(true);
                producto.btnPT_anular.setEnabled(true);
                producto.btnPT_consultar.setEnabled(true);
                this.exitosa = false;
                Limpiar_Tab_Producto();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
            this.exitosa = false;
        }
    }*/

  /*  public void Tabla_Producto_Actualizar() throws SQLException {
        String buscar = producto.txtPT_buscar.getText();
        this.exitosa = true;
        String comision_admin = producto.txtPT_Comisiones_Admin_Producto.getText();
        String comision_operativa = producto.txtPT_Comisiones_Operativa_Producto.getText();
        String impuesto = producto.txtPT_Impuesto.getText();
        String frecuencia_cierre = producto.txtPT_Frecuencia_Cierre_Producto.getText();
        String frecuencia_pago = producto.txtPT_Frecuencia_Pago_Dividendo.getText();
        String cod_producto = producto.txtPT_Código_Producto.getText();
        String descripcion = producto.txtPT_Descripción_de_Producto.getText();
        String caracateristica = producto.txtPT_Característica_de_Producto.getText();
        String moneda = producto.txtPT_Código_de_Moneda.getText();
        String frecuencia_producto = producto.txtPT_Frecuencia_de_Producto.getText();
        String cod_comision = producto.txtPT_Código_Comision_de_Producto.getText();
        try {
            if (comision_admin.equals("") || comision_operativa.equals("") || impuesto.equals("")
                    || frecuencia_cierre.equals("") || frecuencia_pago.equals("") || cod_producto.equals("")
                    || descripcion.equals("") || caracateristica.equals("") || moneda.equals("")
                    || frecuencia_producto.equals("") || cod_comision.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                this.exitosa = false;
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPROACT ('" + buscar + "',?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_producto);
                ppt.setString(2, descripcion);
                ppt.setString(3, caracateristica);
                ppt.setString(4, moneda);
                ppt.setString(5, frecuencia_producto);
                ppt.setString(6, cod_comision);
                ppt.setString(7, comision_admin);
                ppt.setString(8, comision_operativa);
                ppt.setString(9, impuesto);
                ppt.setString(10, frecuencia_cierre);
                ppt.setString(11, frecuencia_pago);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                GlobalConstants.getAuditoria().setLog("U");
                producto.btnPT_insertar.setEnabled(true);
                producto.btnPT_anular.setEnabled(true);
                producto.btnPT_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            System.out.println(ex);
            this.exitosa = false;
            producto.btnPT_insertar.setEnabled(true);
            producto.btnPT_anular.setEnabled(true);
            producto.btnPT_consultar.setEnabled(true);
        }
        producto.btnPT_actualizar.setEnabled(false);
        Limpiar_Tab_Producto();
        Bloquear_Tab_Producto();
    }*/

   /* public void Tabla_Producto_Anular() throws SQLException {
        String buscar = producto.txtPT_Código_Producto.getText();
        this.exitosa = true;
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPROANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            System.out.println("Error NO eliminado " + ex);
            JOptionPane.showMessageDialog(null, "Usuario no existe");
            this.exitosa = false;
        }
    }*/

    /**
     * **********************TABLA ASIENTO CONTABLE 003 *************************
     */
    public void Bloquear_Tab_Asiento() {
        asiento.txtAC_cod_tipo_cuenta_transaccion.setEditable(false);
        asiento.txtAC_clase_transaccion.setEditable(false);
        asiento.txtAC_cod_cuenta_contable.setEditable(false);
        asiento.txtAC_Debe_Haber.setEditable(false);
        asiento.txtAC_campo_transaccion.setEditable(false);
        asiento.txtAC_calculo.setEditable(false);
        asiento.txtAC_referencia_transaccion.setEditable(false);
    }

    public void Desbloquear_Tab_Asiento() {
        asiento.txtAC_cod_tipo_cuenta_transaccion.setEditable(true);
        asiento.txtAC_clase_transaccion.setEditable(true);
        asiento.txtAC_cod_cuenta_contable.setEditable(true);
        asiento.txtAC_Debe_Haber.setEditable(true);
        asiento.txtAC_campo_transaccion.setEditable(true);
        asiento.txtAC_calculo.setEditable(true);
        asiento.txtAC_referencia_transaccion.setEditable(true);
    }

    public void Limpiar_Tab_Asiento() {
        asiento.txtAC_cod_tipo_cuenta_transaccion.setText("");
        asiento.txtAC_clase_transaccion.setText("");
        asiento.txtAC_cod_cuenta_contable.setText("");
        asiento.txtAC_Debe_Haber.setText("");
        asiento.txtAC_campo_transaccion.setText("");
        asiento.txtAC_calculo.setText("");
        asiento.txtAC_referencia_transaccion.setText("");
        asiento.txtAC_buscar.setText("");
    }

    public void Tabla_Asiento_Insertar() throws SQLException {
        this.exitosa = true;
        String cod_tipo_cuenta_trans = asiento.txtAC_cod_tipo_cuenta_transaccion.getText();
        String clase_trans = asiento.txtAC_clase_transaccion.getText();
        String cod_cuenta_contable = asiento.txtAC_cod_cuenta_contable.getText();
        String Debe_Haber = asiento.txtAC_Debe_Haber.getText();
        String campo_trans = asiento.txtAC_campo_transaccion.getText();
        String calculo = asiento.txtAC_calculo.getText();
        String referencia_trans = asiento.txtAC_referencia_transaccion.getText();
        try {
            if (cod_tipo_cuenta_trans.equals("") || clase_trans.equals("") || cod_cuenta_contable.equals("")
                    || Debe_Haber.equals("") || campo_trans.equals("") || calculo.equals("")
                    || referencia_trans.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                asiento.txtAC_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPASIINS (?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_tipo_cuenta_trans);
                ppt.setString(2, clase_trans);
                ppt.setString(3, cod_cuenta_contable);
                ppt.setString(4, Debe_Haber);
                ppt.setString(5, campo_trans);
                ppt.setString(6, calculo);
                ppt.setString(7, referencia_trans);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                asiento.txtAC_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
            this.exitosa = false;
            asiento.txtAC_buscar.setEnabled(true);

        }
    }

    public boolean Tabla_Asiento_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = asiento.txtAC_buscar.getText();
        asiento.txtAC_buscar.setText("");
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPASICON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            if (usuario.next()) {
                asiento.txtAC_cod_tipo_cuenta_transaccion.setText(usuario.getString(1));
                asiento.txtAC_clase_transaccion.setText(usuario.getString(2));
                asiento.txtAC_cod_cuenta_contable.setText(usuario.getString(3));
                asiento.txtAC_Debe_Haber.setText(usuario.getString(4));
                asiento.txtAC_campo_transaccion.setText(usuario.getString(5));
                asiento.txtAC_calculo.setText(usuario.getString(6));
                asiento.txtAC_referencia_transaccion.setText(usuario.getString(7));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exito = false;
                Limpiar_Tab_Asiento();
            }
        } catch (SQLException ex) {
            exito = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
        Bloquear_Tab_Asiento();
        return exito;
    }

    public void Tabla_Asiento_Modificar() throws SQLException {
        asiento.btnAC_actualizar.setEnabled(true);
        String buscar = asiento.txtAC_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPASICON ('" + buscar + "')}");
            this.exitosa = true;
            ResultSet usuario = ppt.executeQuery();
            if (usuario.next()) {
                Desbloquear_Tab_Asiento();
                asiento.btnAC_insertar.setEnabled(false);
                asiento.btnAC_anular.setEnabled(false);
                asiento.btnAC_consultar.setEnabled(false);
                asiento.txtAC_cod_tipo_cuenta_transaccion.setText(usuario.getString(1));
                asiento.txtAC_clase_transaccion.setText(usuario.getString(2));
                asiento.txtAC_cod_cuenta_contable.setText(usuario.getString(3));
                asiento.txtAC_Debe_Haber.setText(usuario.getString(4));
                asiento.txtAC_campo_transaccion.setText(usuario.getString(5));
                asiento.txtAC_calculo.setText(usuario.getString(6));
                asiento.txtAC_referencia_transaccion.setText(usuario.getString(7));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el usuario");
                asiento.txtAC_buscar.setText("");
                this.exitosa = false;
                Bloquear_Tab_Asiento();
                asiento.btnAC_actualizar.setEnabled(false);
                asiento.btnAC_insertar.setEnabled(true);
                asiento.btnAC_anular.setEnabled(true);
                asiento.btnAC_consultar.setEnabled(true);
                Limpiar_Tab_Asiento();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
            this.exitosa = false;
        }
    }

    public void Tabla_Asiento_Actualizar() throws SQLException {
        this.exitosa = true;
        String buscar = asiento.txtAC_buscar.getText();
        String cod_tipo_cuenta_trans = asiento.txtAC_cod_tipo_cuenta_transaccion.getText();
        String clase_trans = asiento.txtAC_clase_transaccion.getText();
        String cod_cuenta_contable = asiento.txtAC_cod_cuenta_contable.getText();
        String Debe_Haber = asiento.txtAC_Debe_Haber.getText();
        String campo_trans = asiento.txtAC_campo_transaccion.getText();
        String calculo = asiento.txtAC_calculo.getText();
        String referencia_trans = asiento.txtAC_referencia_transaccion.getText();
        try {
            if (cod_tipo_cuenta_trans.equals("") || clase_trans.equals("") || cod_cuenta_contable.equals("")
                    || Debe_Haber.equals("") || campo_trans.equals("") || calculo.equals("")
                    || referencia_trans.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                this.exitosa = false;
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPASIACT ('" + buscar + "',?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_tipo_cuenta_trans);
                ppt.setString(2, clase_trans);
                ppt.setString(3, cod_cuenta_contable);
                ppt.setString(4, Debe_Haber);
                ppt.setString(5, campo_trans);
                ppt.setString(6, calculo);
                ppt.setString(7, referencia_trans);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                asiento.btnAC_insertar.setEnabled(true);
                asiento.btnAC_anular.setEnabled(true);
                asiento.btnAC_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            this.exitosa = false;
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            asiento.btnAC_insertar.setEnabled(true);
            asiento.btnAC_anular.setEnabled(true);
            asiento.btnAC_consultar.setEnabled(true);
        }
        asiento.btnAC_actualizar.setEnabled(false);
        Limpiar_Tab_Asiento();
        Bloquear_Tab_Asiento();
    }

    public void Tabla_Asiento_Anular() throws SQLException {
        String buscar = asiento.txtAC_cod_cuenta_contable.getText();
        this.exitosa = true;
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPASIANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Usuario no existe");
            this.exitosa = false;
        }
    }

    /**
     * ************************ TABLA PERFIL 005 *********************************
     */
    public void Bloquear_Tab_Perfil() {
        perfil.txtPF_cod_Perfil.setEditable(false);
        perfil.txtPF_descripcion_perfil.setEditable(false);
        UIManager.put("ComboBox.disabledForeground", Color.BLACK);
    }

    public void Desbloquear_Tab_Perfil() {
        perfil.txtPF_cod_Perfil.setEditable(true);
        perfil.txtPF_descripcion_perfil.setEditable(true);
    }

    public void Limpiar_Tab_Perfil() {
        perfil.txtPF_cod_Perfil.setText("");
        perfil.txtPF_descripcion_perfil.setText("");
        perfil.txtPF_buscar.setText("");
    }

    public void Tabla_Perfil_Insertar() throws SQLException {
        String cod_perfil = perfil.txtPF_cod_Perfil.getText();
        String descripcion_perfil = perfil.txtPF_descripcion_perfil.getText();
        try {
            if (cod_perfil.equals("") || descripcion_perfil.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                perfil.txtPF_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPEFINS (?,?)}");
                ppt.setString(1, cod_perfil);
                ppt.setString(2, descripcion_perfil);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                GlobalConstants.getAuditoria().setLog("I");
                perfil.txtPF_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
        }
    }

    public boolean Tabla_Perfil_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = perfil.txtPF_buscar.getText();
        Map<String, String> status = new HashMap<>();
        DefaultTableModel modo = new DefaultTableModel();
        modo.addColumn("Código de Perfil");
        modo.addColumn("Estatus del Perfil ");
        modo.addColumn("Descripción del Perfil");
        FSOSEGTABFRM005.Tabla_Perfil.setModel(modo);
        String datos[] = new String[3];
        try {
            String sql;
            sql = "{call FSOSEGSTPPEFCNP ()}";
            if (buscar.isEmpty()) {
                sql = "{call FSOSEGSTPPEFCNP ()}";
            } else {
                sql = "{call FSOSEGSTPPEFCON ('" + buscar + "')}";
            }
            CallableStatement ppt = cn.prepareCall(sql);
            ResultSet rs = ppt.executeQuery();
            status.put("1", "Activo");
            status.put("0", "Inactivo");
            if (rs.next()) {
                datos[0] = rs.getString(1);
                datos[1] = status.get(rs.getString(2));
                datos[2] = rs.getString(3);
                modo.addRow(datos);
                while (rs.next()) {
                    datos[0] = rs.getString(1);
                    datos[1] = status.get(rs.getString(2));
                    datos[2] = rs.getString(3);
                    modo.addRow(datos);
                }
            } else {
                JOptionPane.showMessageDialog(null, "No existe el Perfil, Intente de Nuevo!");
            }
            FSOSEGTABFRM005.Tabla_Perfil.setModel(modo);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No existe el Perfil, Intente de Nuevo!");
        }
        Bloquear_Tab_Perfil();
        return exito;
    }

    public void Tabla_Perfil_Anular() throws SQLException {
        String buscar = perfil.txtPF_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPEFANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Perfil Anulado");
            perfil.txtPF_buscar.setText("");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Perfil no existe");
        }
    }

    public void Tabla_Perfil_Activar() throws SQLException {
        String buscar = perfil.txtPF_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPEFACT ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Perfil Activado");
            perfil.txtPF_buscar.setText("");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Perfil no existe");
        }
    }

    /**
     * ******************** TABLA TIPO DE PERFIL 006 *****************************
     */
    public void Limpiar_Tab_T_T_Perfil() {
        t_perfil.txtTP_buscar.setText("");
        DefaultTableModel modo = new DefaultTableModel() {
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 0:
                        return String.class;
                    case 1:
                        return String.class;
                    default:
                        return Boolean.class;
                }
            }
        };
    }

    public boolean Tabla_T_T_Perfil_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = t_perfil.txtTP_buscar.getText();
        Map<String, String> status = new HashMap<>();
        DefaultTableModel modo = new DefaultTableModel() {
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 0:
                        return String.class;
                    case 1:
                        return String.class;
                    default:
                        return Boolean.class;
                }
            }
        };
        modo.addColumn("Condición del Perfil");
        modo.addColumn("Estatus de Condición");
        modo.addColumn("Seleccionar");
        if (buscar.equals("")) {
        } else {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPTPFCON ('" + buscar + "')}");
            ResultSet rs = ppt.executeQuery();
        }
        Object datos[] = new Object[3];
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPTPFCON ('" + buscar + "')}");
            ResultSet rs = ppt.executeQuery();
            status.put("1", "Activo");
            status.put("0", "Inactivo");
            if (!rs.isBeforeFirst()) {
                JOptionPane.showMessageDialog(null, "No existe el Perfil, Intente nuevamente!");
                Limpiar_Tab_T_T_Perfil();
                return (exito = false);
            }
            while (rs.next()) {
                datos[0] = rs.getString(1);
                datos[1] = status.get(rs.getString(2));
                if (datos[1].equals("Activo")) {
                    datos[2] = new Boolean(true);
                } else {
                    datos[2] = new Boolean(false);
                }
                modo.addRow(datos);
            }
            FSOSEGTABFRM006.Tabla_acciones.setModel(modo);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No existe el Perfil, Intente de nuevo!");
        }
        return exito;
    }

    public void Tabla_T_T_Perfil_Agregar_Permisos() throws SQLException {
        Map<Boolean, String> status_condicion = new HashMap<>();
        status_condicion.put(Boolean.FALSE, "0");
        status_condicion.put(Boolean.TRUE, "1");
        TableModel def = FSOSEGTABFRM006.Tabla_acciones.getModel();
        String buscar = t_perfil.txtTP_buscar.getText();
        try {
            int i = 0;
            while (i < def.getRowCount()) {
                Object cond_perfil = def.getValueAt(i, 0);
                Object status = def.getValueAt(i, 2);
                if (((Boolean) status)) {
                    CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPEFHAB ('" + cond_perfil + "','" + buscar + "')}");
                    ppt.executeUpdate();
                } else {
                    CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPEFDES ('" + cond_perfil + "','" + buscar + "')}");
                    ppt.executeUpdate();
                }
                i++;
                Tabla_T_T_Perfil_Mostrar();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No se pudo Agregar los permisos al Perfil asignado, intente nuevamente!");
        }
    }

    /**
     * *********************** TABLA TIPO DE TRASANCCION 007 *********************
     */
    public void Bloquear_Tab_Tipo_Transaccion() {
        t_trans.txtTT_codigo_unico.setEditable(false);
        t_trans.txtTT_cuenta.setEditable(false);
        t_trans.txtTT_des_tipo_transaccion.setEditable(false);
        t_trans.txtTT_Referencia.setEditable(false);
        t_trans.txtTT_Debe_Haber.setEnabled(false);
        t_trans.txtTT_clase_transaccion.setEditable(false);
    }

    public void Desbloquear_Tab_Tipo_Transaccion() {
        t_trans.txtTT_codigo_unico.setEditable(true);
        t_trans.txtTT_cuenta.setEditable(true);
        t_trans.txtTT_des_tipo_transaccion.setEditable(true);
        t_trans.txtTT_Referencia.setEditable(true);
        t_trans.txtTT_Debe_Haber.setEnabled(true);
        t_trans.txtTT_clase_transaccion.setEditable(true);
    }

    public void Limpiar_Tab_Tipo_Transaccion() {
        t_trans.txtTT_buscar.setText("");
        t_trans.txtTT_codigo_unico.setText("");
        t_trans.txtTT_cuenta.setText("");
        t_trans.txtTT_des_tipo_transaccion.setText("");
        t_trans.txtTT_Referencia.setText("");
        t_trans.txtTT_Debe_Haber.setSelectedItem(null);
        t_trans.txtTT_clase_transaccion.setText("");
    }

    public void Tabla_Tipo_Transaccion_Insertar() throws SQLException {
        String codigo_unico = t_trans.txtTT_codigo_unico.getText();
        String cuenta = t_trans.txtTT_cuenta.getText();
        String des_tipo_transaccion = t_trans.txtTT_des_tipo_transaccion.getText();
        String Referencia = t_trans.txtTT_Referencia.getText();
        String Debe_Haber = (String) t_trans.txtTT_Debe_Haber.getSelectedItem();
        String clase_transaccion = t_trans.txtTT_clase_transaccion.getText();
        try {
            if (codigo_unico.equals("") || cuenta.equals("") || des_tipo_transaccion.equals("")
                    || Referencia.equals("") || Debe_Haber.equals("") || clase_transaccion.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                t_trans.txtTT_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPTPTINS (?,?,?,?,?,?)}");
                ppt.setString(1, codigo_unico);
                ppt.setString(2, cuenta);
                ppt.setString(3, des_tipo_transaccion);
                ppt.setString(4, Referencia);
                ppt.setString(5, Debe_Haber.substring(0, 1));
                ppt.setString(6, clase_transaccion);
                ppt.executeUpdate();
                GlobalConstants.getAuditoria().setLog("I");
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                t_trans.txtTT_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
            t_trans.txtTT_buscar.setEnabled(true);
        }
    }

    public boolean Tabla_Tipo_Transaccion_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = t_trans.txtTT_buscar.getText();
        DefaultTableModel modo = new DefaultTableModel();
        modo.addColumn("Código Único");
        modo.addColumn("Cuenta");
        modo.addColumn("Descripción Tipo de Transacción");
        modo.addColumn("Referencia");
        modo.addColumn("Debe Haber");
        modo.addColumn("Clase de Transacción");
        FSOSEGTABFRM007.Tabla_tipo_transacciones.setModel(modo);
        String datos[] = new String[6];
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPTPTCON ('" + buscar + "')}");
            ResultSet rs = ppt.executeQuery();
            if (rs.next()) {
                datos[0] = rs.getString(1);
                datos[1] = rs.getString(2);
                datos[2] = rs.getString(3);
                datos[3] = rs.getString(4);
                datos[4] = rs.getString(5).equals("A") ? "Activo" : "Pasivo";
                datos[5] = rs.getString(6);
                modo.addRow(datos);
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exito = false;
                Limpiar_Tab_Tipo_Transaccion();
                Tabla_Tipo_Transaccion_Mostrar_Modo();
            }
        } catch (SQLException ex) {
            exito = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
        Bloquear_Tab_Tipo_Transaccion();
        return exito;
    }

    public void Tabla_Tipo_Transaccion_Mostrar_Modo() throws SQLException {
        DefaultTableModel modo = new DefaultTableModel();
        modo.addColumn("Código Único");
        modo.addColumn("Cuenta");
        modo.addColumn("Descripción Tipo de Transacción");
        modo.addColumn("Referencia");
        modo.addColumn("Debe Haber");
        modo.addColumn("Clase de Transacción");
        FSOSEGTABFRM007.Tabla_tipo_transacciones.setModel(modo);
        String datos[] = new String[6];
        CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPTPTCON2 ()}");
        ResultSet rs = ppt.executeQuery();
        while (rs.next()) {
            datos[0] = rs.getString(1);
            datos[1] = rs.getString(2);
            datos[2] = rs.getString(3);
            datos[3] = rs.getString(4);
            datos[4] = rs.getString(5).equals("A") ? "Activo" : "Pasivo";
            datos[5] = rs.getString(6);
            modo.addRow(datos);
        }
    }

    public void Tabla_Tipo_Transaccion_Modificar() throws SQLException {
        DefaultTableModel modo = new DefaultTableModel();
        FSOSEGTABFRM007.Tabla_tipo_transacciones.setModel(modo);
        t_trans.btnTT_actualizar.setEnabled(true);
        String buscar = t_trans.txtTT_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPTPTCON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            if (usuario.next()) {
                Desbloquear_Tab_Tipo_Transaccion();
                t_trans.btnTT_insertar.setEnabled(false);
                t_trans.btnTT_anular.setEnabled(false);
                t_trans.btnTT_consultar.setEnabled(false);
                t_trans.txtTT_codigo_unico.setText(usuario.getString(1));
                t_trans.txtTT_cuenta.setText(usuario.getString(2));
                t_trans.txtTT_des_tipo_transaccion.setText(usuario.getString(3));
                t_trans.txtTT_Referencia.setText(usuario.getString(4));
                t_trans.txtTT_Debe_Haber.setSelectedItem(usuario.getString(5).equals("A") ? "Activo" : "Pasivo");
                t_trans.txtTT_clase_transaccion.setText(usuario.getString(6));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el usuario");
                t_trans.txtTT_buscar.setText("");
                Bloquear_Tab_Tipo_Transaccion();
                t_trans.btnTT_actualizar.setEnabled(false);
                t_trans.btnTT_insertar.setEnabled(true);
                t_trans.btnTT_anular.setEnabled(true);
                t_trans.btnTT_consultar.setEnabled(true);
                Limpiar_Tab_Tipo_Transaccion();
                Tabla_Tipo_Transaccion_Mostrar_Modo();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void Tabla_Tipo_Transaccion_Actualizar() throws SQLException {
        String buscar = t_trans.txtTT_buscar.getText();
        String codigo_unico = t_trans.txtTT_codigo_unico.getText();
        String cuenta = t_trans.txtTT_cuenta.getText();
        String des_tipo_transaccion = t_trans.txtTT_des_tipo_transaccion.getText();
        String Referencia = t_trans.txtTT_Referencia.getText();
        String Debe_Haber = (String) t_trans.txtTT_Debe_Haber.getSelectedItem();
        String clase_transaccion = t_trans.txtTT_clase_transaccion.getText();
        try {
            if (codigo_unico.equals("") || cuenta.equals("") || des_tipo_transaccion.equals("")
                    || Referencia.equals("") || Debe_Haber.equals("") || clase_transaccion.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                t_trans.txtTT_buscar.setEnabled(true);
            } else {

                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPTPTACT ('" + buscar + "',?,?,?,?,?,?)}");
                ppt.setString(1, codigo_unico);
                ppt.setString(2, cuenta);
                ppt.setString(3, des_tipo_transaccion);
                ppt.setString(4, Referencia);
                ppt.setString(5, Debe_Haber);
                ppt.setString(6, clase_transaccion);
                ppt.executeUpdate();
                GlobalConstants.getAuditoria().setLog("U");
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                t_trans.btnTT_insertar.setEnabled(true);
                t_trans.btnTT_anular.setEnabled(true);
                t_trans.btnTT_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            t_trans.btnTT_insertar.setEnabled(true);
            t_trans.btnTT_anular.setEnabled(true);
            t_trans.btnTT_consultar.setEnabled(true);
        }
        t_trans.btnTT_actualizar.setEnabled(false);
        Limpiar_Tab_Tipo_Transaccion();
        Bloquear_Tab_Tipo_Transaccion();
    }

    public void Tabla_Tipo__Transaccion_Anular() throws SQLException {
        String buscar = t_trans.txtTT_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPTPTANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Usuario no existe");
        }
    }

    /**
     * *********************** TABLA PRODUCTO PERSONA 008 ************************
     */
    public void Bloquear_Tab_Producto_Persona() {
        p_persona.txtPP_cod_Producto.setEditable(false);
        p_persona.txtPP_cod_Persona.setEditable(false);
        p_persona.txtPP_ci_rif.setEditable(false);
        p_persona.txtPP_fecha_proceso.setEnabled(false);
        p_persona.txtPP_fecha_actualizacion.setEnabled(false);
        p_persona.txtPP_saldo_intereses.setEditable(false);
        p_persona.txtPP_saldo_rendimiento.setEditable(false);
        p_persona.txtPP_saldo_prestamos.setEditable(false);
        p_persona.txtPP_saldo_anticipos.setEditable(false);
        p_persona.txtPP_saldo_aportes.setEditable(false);
        p_persona.txtPP_saldo_retiros.setEditable(false);
        p_persona.txtPP_saldo_haber.setEditable(false);
        p_persona.txtPP_saldo_debe.setEditable(false);
        p_persona.txtPP_saldo_dia.setEditable(false);
        p_persona.txtPP_saldo_comisiones.setEditable(false);
        ((JTextFieldDateEditor) p_persona.txtPP_fecha_proceso.getDateEditor()).setDisabledTextColor(Color.BLACK);
        ((JTextFieldDateEditor) p_persona.txtPP_fecha_actualizacion.getDateEditor()).setDisabledTextColor(Color.BLACK);
    }

    public void Desbloquear_Tab_Producto_Persona() {
        p_persona.txtPP_cod_Producto.setEditable(true);
        p_persona.txtPP_cod_Persona.setEditable(true);
        p_persona.txtPP_ci_rif.setEditable(true);
        p_persona.txtPP_fecha_proceso.setEnabled(true);
        p_persona.txtPP_fecha_actualizacion.setEnabled(true);
        p_persona.txtPP_saldo_intereses.setEditable(true);
        p_persona.txtPP_saldo_rendimiento.setEditable(true);
        p_persona.txtPP_saldo_prestamos.setEditable(true);
        p_persona.txtPP_saldo_anticipos.setEditable(true);
        p_persona.txtPP_saldo_aportes.setEditable(true);
        p_persona.txtPP_saldo_retiros.setEditable(true);
        p_persona.txtPP_saldo_haber.setEditable(true);
        p_persona.txtPP_saldo_debe.setEditable(true);
        p_persona.txtPP_saldo_dia.setEditable(true);
        p_persona.txtPP_saldo_comisiones.setEditable(true);
    }

    public void Limpiar_Tab_Producto_Persona() {
        p_persona.txtPP_buscar.setText("");
        p_persona.txtPP_cod_Producto.setText("");
        p_persona.txtPP_cod_Persona.setText("");
        p_persona.txtPP_ci_rif.setText("");
        p_persona.txtPP_fecha_proceso.setDate(null);
        p_persona.txtPP_fecha_actualizacion.setDate(null);
        p_persona.txtPP_saldo_intereses.setText("");
        p_persona.txtPP_saldo_rendimiento.setText("");
        p_persona.txtPP_saldo_prestamos.setText("");
        p_persona.txtPP_saldo_anticipos.setText("");
        p_persona.txtPP_saldo_aportes.setText("");
        p_persona.txtPP_saldo_retiros.setText("");
        p_persona.txtPP_saldo_haber.setText("");
        p_persona.txtPP_saldo_debe.setText("");
        p_persona.txtPP_saldo_dia.setText("");
        p_persona.txtPP_saldo_comisiones.setText("");
    }

    public void Tabla_Producto_Persona_Insertar() throws SQLException {
        String cod_Producto = p_persona.txtPP_cod_Producto.getText();
        String cod_Persona = p_persona.txtPP_cod_Persona.getText();
        String ci_rif = p_persona.txtPP_ci_rif.getText();
        String saldo_intereses = p_persona.txtPP_saldo_intereses.getText();
        String saldo_rendimiento = p_persona.txtPP_saldo_rendimiento.getText();
        String saldo_prestamos = p_persona.txtPP_saldo_prestamos.getText();
        String saldo_anticipos = p_persona.txtPP_saldo_anticipos.getText();
        String saldo_aportes = p_persona.txtPP_saldo_aportes.getText();
        String saldo_retiros = p_persona.txtPP_saldo_retiros.getText();
        String saldo_haber = p_persona.txtPP_saldo_haber.getText();
        String saldo_debe = p_persona.txtPP_saldo_debe.getText();
        String saldo_dia = p_persona.txtPP_saldo_dia.getText();
        String saldo_comisiones = p_persona.txtPP_saldo_comisiones.getText();
        try {
            if (cod_Producto.equals("") || cod_Persona.equals("") || ci_rif.equals("") || saldo_intereses.equals("")
                    || saldo_rendimiento.equals("") || saldo_prestamos.equals("") || saldo_anticipos.equals("")
                    || saldo_aportes.equals("") || saldo_retiros.equals("") || saldo_haber.equals("")
                    || saldo_debe.equals("") || saldo_dia.equals("") || saldo_comisiones.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                p_persona.txtPP_buscar.setEnabled(true);
                this.exitosa = false;
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPRPINS (?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_Producto);
                ppt.setString(2, cod_Persona);
                ppt.setString(3, ci_rif);
                ppt.setString(4, saldo_intereses);
                ppt.setString(5, saldo_rendimiento);
                ppt.setString(6, saldo_prestamos);
                ppt.setString(7, saldo_anticipos);
                ppt.setString(8, saldo_aportes);
                ppt.setString(9, saldo_retiros);
                ppt.setString(10, saldo_haber);
                ppt.setString(11, saldo_debe);
                ppt.setString(12, saldo_dia);
                ppt.setString(13, saldo_comisiones);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                GlobalConstants.getAuditoria().setLog("I");
                p_persona.txtPP_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
            p_persona.txtPP_buscar.setEnabled(true);

        }
    }

    public boolean Tabla_Producto_Persona_Mostrar() throws SQLException {
        boolean exito = true;
        this.exitosa = true;
        String buscar = p_persona.txtPP_buscar.getText();
        p_persona.txtPP_buscar.setText("");
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPRPCON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            if (usuario.next()) {
                p_persona.txtPP_cod_Producto.setText(usuario.getString(1));
                p_persona.txtPP_cod_Persona.setText(usuario.getString(2));
                p_persona.txtPP_ci_rif.setText(usuario.getString(3));
                p_persona.txtPP_fecha_proceso.setDate(usuario.getDate(4));
                p_persona.txtPP_fecha_actualizacion.setDate(usuario.getDate(5));
                p_persona.txtPP_saldo_intereses.setText(usuario.getString(6));
                p_persona.txtPP_saldo_rendimiento.setText(usuario.getString(7));
                p_persona.txtPP_saldo_prestamos.setText(usuario.getString(8));
                p_persona.txtPP_saldo_anticipos.setText(usuario.getString(9));
                p_persona.txtPP_saldo_aportes.setText(usuario.getString(10));
                p_persona.txtPP_saldo_retiros.setText(usuario.getString(11));
                p_persona.txtPP_saldo_haber.setText(usuario.getString(12));
                p_persona.txtPP_saldo_debe.setText(usuario.getString(13));
                p_persona.txtPP_saldo_dia.setText(usuario.getString(14));
                p_persona.txtPP_saldo_comisiones.setText(usuario.getString(15));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exito = false;
                this.exitosa = false;
                Limpiar_Tab_Producto_Persona();
            }
        } catch (SQLException ex) {
            exito = false;
            this.exitosa = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
            this.exitosa = false;
        }
        Bloquear_Tab_Producto_Persona();
        return exito;
    }

    public void Tabla_Producto_Persona_Modificar() throws SQLException {
        p_persona.btnPP_actualizar.setEnabled(true);
        String buscar = p_persona.txtPP_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPRPCON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            this.exitosa = true;
            if (usuario.next()) {
                Desbloquear_Tab_Producto_Persona();
                p_persona.btnPP_insertar.setEnabled(false);
                p_persona.btnPP_anular.setEnabled(false);
                p_persona.btnPP_consultar.setEnabled(false);
                p_persona.txtPP_cod_Producto.setText(usuario.getString(1));
                p_persona.txtPP_cod_Persona.setText(usuario.getString(2));
                p_persona.txtPP_ci_rif.setText(usuario.getString(3));
                p_persona.txtPP_fecha_proceso.setDate(usuario.getDate(4));
                p_persona.txtPP_fecha_actualizacion.setDate(usuario.getDate(5));
                p_persona.txtPP_saldo_intereses.setText(usuario.getString(6));
                p_persona.txtPP_saldo_rendimiento.setText(usuario.getString(7));
                p_persona.txtPP_saldo_prestamos.setText(usuario.getString(8));
                p_persona.txtPP_saldo_anticipos.setText(usuario.getString(9));
                p_persona.txtPP_saldo_aportes.setText(usuario.getString(10));
                p_persona.txtPP_saldo_retiros.setText(usuario.getString(11));
                p_persona.txtPP_saldo_haber.setText(usuario.getString(12));
                p_persona.txtPP_saldo_debe.setText(usuario.getString(13));
                p_persona.txtPP_saldo_dia.setText(usuario.getString(14));
                p_persona.txtPP_saldo_comisiones.setText(usuario.getString(15));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el usuario");
                p_persona.txtPP_buscar.setText("");
                this.exitosa = false;
                Bloquear_Tab_Producto_Persona();
                p_persona.btnPP_actualizar.setEnabled(false);
                p_persona.btnPP_insertar.setEnabled(true);
                p_persona.btnPP_anular.setEnabled(true);
                p_persona.btnPP_consultar.setEnabled(true);
                Limpiar_Tab_Producto_Persona();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
            this.exitosa = false;
        }
    }

    public void Tabla_Producto_Persona_Actualizar() throws SQLException {
        this.exitosa = true;
        String buscar = p_persona.txtPP_buscar.getText();
        String cod_Producto = p_persona.txtPP_cod_Producto.getText();
        String cod_Persona = p_persona.txtPP_cod_Persona.getText();
        String ci_rif = p_persona.txtPP_ci_rif.getText();
        String saldo_intereses = p_persona.txtPP_saldo_intereses.getText();
        String saldo_rendimiento = p_persona.txtPP_saldo_rendimiento.getText();
        String saldo_prestamos = p_persona.txtPP_saldo_prestamos.getText();
        String saldo_anticipos = p_persona.txtPP_saldo_anticipos.getText();
        String saldo_aportes = p_persona.txtPP_saldo_aportes.getText();
        String saldo_retiros = p_persona.txtPP_saldo_retiros.getText();
        String saldo_haber = p_persona.txtPP_saldo_haber.getText();
        String saldo_debe = p_persona.txtPP_saldo_debe.getText();
        String saldo_dia = p_persona.txtPP_saldo_dia.getText();
        String saldo_comisiones = p_persona.txtPP_saldo_comisiones.getText();
        try {
            if (cod_Producto.equals("") || cod_Persona.equals("") || ci_rif.equals("") || saldo_intereses.equals("")
                    || saldo_rendimiento.equals("") || saldo_prestamos.equals("") || saldo_anticipos.equals("")
                    || saldo_aportes.equals("") || saldo_retiros.equals("") || saldo_haber.equals("")
                    || saldo_debe.equals("") || saldo_dia.equals("") || saldo_comisiones.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                p_persona.txtPP_buscar.setEnabled(true);
                this.exitosa = false;
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPRPACT ('" + buscar + "',?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_Producto);
                ppt.setString(2, cod_Persona);
                ppt.setString(3, ci_rif);
                ppt.setString(4, saldo_intereses);
                ppt.setString(5, saldo_rendimiento);
                ppt.setString(6, saldo_prestamos);
                ppt.setString(7, saldo_anticipos);
                ppt.setString(8, saldo_aportes);
                ppt.setString(9, saldo_retiros);
                ppt.setString(10, saldo_haber);
                ppt.setString(11, saldo_debe);
                ppt.setString(12, saldo_dia);
                ppt.setString(13, saldo_comisiones);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                GlobalConstants.getAuditoria().setLog("U");
                p_persona.btnPP_insertar.setEnabled(true);
                p_persona.btnPP_anular.setEnabled(true);
                p_persona.btnPP_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            this.exitosa = false;
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            p_persona.btnPP_insertar.setEnabled(true);
            p_persona.btnPP_anular.setEnabled(true);
            p_persona.btnPP_consultar.setEnabled(true);
        }
        p_persona.btnPP_actualizar.setEnabled(false);
        Limpiar_Tab_Producto_Persona();
        Bloquear_Tab_Producto_Persona();
    }

    public void Tabla_Producto_Persona_Anular() throws SQLException {
        String buscar = p_persona.txtPP_ci_rif.getText();
        this.exitosa = true;
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPRPANL('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Usuario no existe");
            this.exitosa = false;
        }
    }

    /**
     * ********************** TABLA CONFIGURACION 010 ****************************
     */
    public void Bloquear_Tab_Configuracion() {
        configuracion.txtC_cod_empresa.setEditable(false);
        configuracion.txtC_ci_rif.setEditable(false);
        configuracion.txtC_razon_social_persona.setEditable(false);
        configuracion.txtC_cod_moneda.setEditable(false);
        configuracion.txtC_cod_pais.setEditable(false);
        configuracion.txtC_cod_postal.setEditable(false);
        configuracion.txtC_formato_fecha.setEditable(false);
        configuracion.txtC_formato_hora.setEditable(false);
        configuracion.txtC_idioma.setEditable(false);
        configuracion.txtC_direccion.setEditable(false);
        configuracion.txtC_telefonos.setEditable(false);
        configuracion.txtC_correo.setEditable(false);
        configuracion.txtC_fecha_cierre_actividades.setEditable(false);
        configuracion.txtC_frecuencia_cierre_operativo.setEditable(false);
        configuracion.txtC_comision_administracion01.setEditable(false);
        configuracion.txtC_comision_administracion02.setEditable(false);
        configuracion.txtC_comision_administracion03.setEditable(false);
        configuracion.txtC_comision_operativa01.setEditable(false);
        configuracion.txtC_comision_operativa02.setEditable(false);
        configuracion.txtC_comision_operativa03.setEditable(false);
        configuracion.txtC_mensaje_oficial_reportes.setEditable(false);
        configuracion.txtC_nombre_Sistema_aplicacion.setEditable(false);
        configuracion.txtC_fecha_configuracion_sistema.setEditable(false);
        configuracion.txtC_caracter_separa_campos.setEditable(false);
        configuracion.txtC_ruta_importar_archivo_plano.setEditable(false);
        configuracion.txtC_ruta_archivos_entrada.setEditable(false);
        configuracion.txtC_ruta_archivos_salida.setEditable(false);
        configuracion.txtC_ruta_pagina_web.setEditable(false);
        configuracion.txtC_ruta_reporte.setEditable(false);
        configuracion.txtC_ruta_formato_reportes.setEditable(false);
        configuracion.txtC_ruta_instalacion_aplicacion.setEditable(false);
        configuracion.txtC_ruta_log_auditoria.setEditable(false);
        configuracion.txtC_ruta_destino_reporte.setEditable(false);
        configuracion.txtC_ruta_conexion_servidor.setEditable(false);
        configuracion.txtC_ejecucion_procesos_batch.setEditable(false);
        configuracion.txtC_persona_contacto.setEditable(false);
    }

    public void Desbloquear_Tab_Configuracion() {
        configuracion.txtC_cod_empresa.setEditable(true);
        configuracion.txtC_ci_rif.setEditable(true);
        configuracion.txtC_razon_social_persona.setEditable(true);
        configuracion.txtC_cod_moneda.setEditable(true);
        configuracion.txtC_cod_pais.setEditable(true);
        configuracion.txtC_cod_postal.setEditable(true);
        configuracion.txtC_formato_fecha.setEditable(true);
        configuracion.txtC_formato_hora.setEditable(true);
        configuracion.txtC_idioma.setEditable(true);
        configuracion.txtC_direccion.setEditable(true);
        configuracion.txtC_telefonos.setEditable(true);
        configuracion.txtC_correo.setEditable(true);
        configuracion.txtC_fecha_cierre_actividades.setEditable(true);
        configuracion.txtC_frecuencia_cierre_operativo.setEditable(true);
        configuracion.txtC_comision_administracion01.setEditable(true);
        configuracion.txtC_comision_administracion02.setEditable(true);
        configuracion.txtC_comision_administracion03.setEditable(true);
        configuracion.txtC_comision_operativa01.setEditable(true);
        configuracion.txtC_comision_operativa02.setEditable(true);
        configuracion.txtC_comision_operativa03.setEditable(true);
        configuracion.txtC_mensaje_oficial_reportes.setEditable(true);
        configuracion.txtC_nombre_Sistema_aplicacion.setEditable(true);
        configuracion.txtC_fecha_configuracion_sistema.setEditable(true);
        configuracion.txtC_caracter_separa_campos.setEditable(true);
        configuracion.txtC_ruta_importar_archivo_plano.setEditable(true);
        configuracion.txtC_ruta_archivos_entrada.setEditable(true);
        configuracion.txtC_ruta_archivos_salida.setEditable(true);
        configuracion.txtC_ruta_pagina_web.setEditable(true);
        configuracion.txtC_ruta_reporte.setEditable(true);
        configuracion.txtC_ruta_formato_reportes.setEditable(true);
        configuracion.txtC_ruta_instalacion_aplicacion.setEditable(true);
        configuracion.txtC_ruta_log_auditoria.setEditable(true);
        configuracion.txtC_ruta_destino_reporte.setEditable(true);
        configuracion.txtC_ruta_conexion_servidor.setEditable(true);
        configuracion.txtC_ejecucion_procesos_batch.setEditable(true);
        configuracion.txtC_persona_contacto.setEditable(true);
    }

    public void Limpiar_Tab_Configuracion() {
        configuracion.txtC_buscar.setText("");
        configuracion.txtC_cod_empresa.setText("");
        configuracion.txtC_ci_rif.setText("");
        configuracion.txtC_razon_social_persona.setText("");
        configuracion.txtC_cod_moneda.setText("");
        configuracion.txtC_cod_pais.setText("");
        configuracion.txtC_cod_postal.setText("");
        configuracion.txtC_formato_fecha.setText("");
        configuracion.txtC_formato_hora.setText("");
        configuracion.txtC_idioma.setText("");
        configuracion.txtC_direccion.setText("");
        configuracion.txtC_telefonos.setText("");
        configuracion.txtC_correo.setText("");
        configuracion.txtC_fecha_cierre_actividades.setText("");
        configuracion.txtC_frecuencia_cierre_operativo.setText("");
        configuracion.txtC_comision_administracion01.setText("");
        configuracion.txtC_comision_administracion02.setText("");
        configuracion.txtC_comision_administracion03.setText("");
        configuracion.txtC_comision_operativa01.setText("");
        configuracion.txtC_comision_operativa02.setText("");
        configuracion.txtC_comision_operativa03.setText("");
        configuracion.txtC_mensaje_oficial_reportes.setText("");
        configuracion.txtC_nombre_Sistema_aplicacion.setText("");
        configuracion.txtC_fecha_configuracion_sistema.setText("");
        configuracion.txtC_caracter_separa_campos.setText("");
        configuracion.txtC_ruta_importar_archivo_plano.setText("");
        configuracion.txtC_ruta_archivos_entrada.setText("");
        configuracion.txtC_ruta_archivos_salida.setText("");
        configuracion.txtC_ruta_pagina_web.setText("");
        configuracion.txtC_ruta_reporte.setText("");
        configuracion.txtC_ruta_formato_reportes.setText("");
        configuracion.txtC_ruta_instalacion_aplicacion.setText("");
        configuracion.txtC_ruta_log_auditoria.setText("");
        configuracion.txtC_ruta_destino_reporte.setText("");
        configuracion.txtC_ruta_conexion_servidor.setText("");
        configuracion.txtC_ejecucion_procesos_batch.setText("");
        configuracion.txtC_persona_contacto.setText("");
    }

    public void Tabla_Configuracion_Insertar() throws SQLException {
        String cod_empresa = configuracion.txtC_cod_empresa.getText();
        String ci_rif = configuracion.txtC_ci_rif.getText();
        String razon_social_persona = configuracion.txtC_razon_social_persona.getText();
        String cod_moneda = configuracion.txtC_cod_moneda.getText();
        String cod_pais = configuracion.txtC_cod_pais.getText();
        String cod_postal = configuracion.txtC_cod_postal.getText();
        String formato_fecha = configuracion.txtC_formato_fecha.getText();
        String formato_hora = configuracion.txtC_formato_hora.getText();
        String idioma = configuracion.txtC_idioma.getText();
        String direccion = configuracion.txtC_direccion.getText();
        String telefonos = configuracion.txtC_telefonos.getText();
        String correo = configuracion.txtC_correo.getText();
        String fecha_cierre_actividades = configuracion.txtC_fecha_cierre_actividades.getText();
        String frecuencia_cierre_operativo = configuracion.txtC_frecuencia_cierre_operativo.getText();
        String comision_administracion01 = configuracion.txtC_comision_administracion01.getText();
        String comision_administracion02 = configuracion.txtC_comision_administracion02.getText();
        String comision_administracion03 = configuracion.txtC_comision_administracion03.getText();
        String comision_operativa01 = configuracion.txtC_comision_operativa01.getText();
        String comision_operativa02 = configuracion.txtC_comision_operativa02.getText();
        String comision_operativa03 = configuracion.txtC_comision_operativa03.getText();
        String mensaje_oficial_reportes = configuracion.txtC_mensaje_oficial_reportes.getText();
        String nombre_Sistema_aplicacion = configuracion.txtC_nombre_Sistema_aplicacion.getText();
        String fecha_configuracion_sistema = configuracion.txtC_fecha_configuracion_sistema.getText();
        String caracter_separa_campos = configuracion.txtC_caracter_separa_campos.getText();
        String ruta_importar_archivo_plano = configuracion.txtC_ruta_importar_archivo_plano.getText();
        String ruta_archivos_entrada = configuracion.txtC_ruta_archivos_entrada.getText();
        String ruta_archivos_salida = configuracion.txtC_ruta_archivos_salida.getText();
        String ruta_pagina_web = configuracion.txtC_ruta_pagina_web.getText();
        String ruta_reporte = configuracion.txtC_ruta_reporte.getText();
        String ruta_formato_reportes = configuracion.txtC_ruta_formato_reportes.getText();
        String ruta_instalacion_aplicacion = configuracion.txtC_ruta_instalacion_aplicacion.getText();
        String ruta_log_auditoria = configuracion.txtC_ruta_log_auditoria.getText();
        String ruta_destino_reporte = configuracion.txtC_ruta_destino_reporte.getText();
        String ruta_conexion_servidor = configuracion.txtC_ruta_conexion_servidor.getText();
        String ejecucion_procesos_batch = configuracion.txtC_ejecucion_procesos_batch.getText();
        String persona_contacto = configuracion.txtC_persona_contacto.getText();
        try {
            if (cod_empresa.equals("") || ci_rif.equals("") || razon_social_persona.equals("")
                    || cod_moneda.equals("") || cod_pais.equals("") || cod_postal.equals("") || formato_fecha.equals("")
                    || formato_hora.equals("") || idioma.equals("") || direccion.equals("")
                    || telefonos.equals("") || correo.equals("") || fecha_cierre_actividades.equals("")
                    || frecuencia_cierre_operativo.equals("") || comision_administracion01.equals("") || comision_administracion02.equals("")
                    || comision_administracion03.equals("") || comision_operativa01.equals("") || comision_operativa02.equals("")
                    || comision_operativa03.equals("") || mensaje_oficial_reportes.equals("") || nombre_Sistema_aplicacion.equals("")
                    || fecha_configuracion_sistema.equals("") || caracter_separa_campos.equals("") || ruta_importar_archivo_plano.equals("") || ruta_archivos_entrada.equals("")
                    || ruta_archivos_salida.equals("") || ruta_pagina_web.equals("") || ruta_reporte.equals("") || ruta_formato_reportes.equals("")
                    || ruta_instalacion_aplicacion.equals("") || ruta_log_auditoria.equals("") || ruta_destino_reporte.equals("") || ruta_conexion_servidor.equals("")
                    || ejecucion_procesos_batch.equals("") || persona_contacto.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                configuracion.txtC_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPCONINS (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_empresa);
                ppt.setString(2, ci_rif);
                ppt.setString(3, razon_social_persona);
                ppt.setString(4, cod_moneda);
                ppt.setString(5, cod_pais);
                ppt.setString(6, cod_postal);
                ppt.setString(7, formato_fecha);
                ppt.setString(8, formato_hora);
                ppt.setString(9, idioma);
                ppt.setString(10, direccion);
                ppt.setString(11, telefonos);
                ppt.setString(12, correo);
                ppt.setString(13, fecha_cierre_actividades);
                ppt.setString(14, frecuencia_cierre_operativo);
                ppt.setString(15, comision_administracion01);
                ppt.setString(16, comision_administracion02);
                ppt.setString(17, comision_administracion03);
                ppt.setString(18, comision_operativa01);
                ppt.setString(19, comision_operativa02);
                ppt.setString(20, comision_operativa03);
                ppt.setString(21, mensaje_oficial_reportes);
                ppt.setString(22, nombre_Sistema_aplicacion);
                ppt.setString(23, fecha_configuracion_sistema);
                ppt.setString(24, caracter_separa_campos);
                ppt.setString(25, ruta_importar_archivo_plano);
                ppt.setString(26, ruta_archivos_entrada);
                ppt.setString(27, ruta_archivos_salida);
                ppt.setString(28, ruta_pagina_web);
                ppt.setString(29, ruta_reporte);
                ppt.setString(30, ruta_formato_reportes);
                ppt.setString(31, ruta_instalacion_aplicacion);
                ppt.setString(32, ruta_log_auditoria);
                ppt.setString(33, ruta_destino_reporte);
                ppt.setString(34, ruta_conexion_servidor);
                ppt.setString(35, ejecucion_procesos_batch);
                ppt.setString(36, persona_contacto);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                configuracion.txtC_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
            configuracion.txtC_buscar.setEnabled(true);
        }

    }

    public boolean Tabla_Configuracion_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = configuracion.txtC_buscar.getText();
        configuracion.txtC_buscar.setText("");
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPCONCON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            if (usuario.next()) {
                configuracion.txtC_cod_empresa.setText(usuario.getString(1));
                configuracion.txtC_ci_rif.setText(usuario.getString(2));
                configuracion.txtC_razon_social_persona.setText(usuario.getString(3));
                configuracion.txtC_cod_moneda.setText(usuario.getString(4));
                configuracion.txtC_cod_pais.setText(usuario.getString(5));
                configuracion.txtC_cod_postal.setText(usuario.getString(6));
                configuracion.txtC_formato_fecha.setText(usuario.getString(7));
                configuracion.txtC_formato_hora.setText(usuario.getString(8));
                configuracion.txtC_idioma.setText(usuario.getString(9));
                configuracion.txtC_direccion.setText(usuario.getString(10));
                configuracion.txtC_telefonos.setText(usuario.getString(11));
                configuracion.txtC_correo.setText(usuario.getString(12));
                configuracion.txtC_fecha_cierre_actividades.setText(usuario.getString(13));
                configuracion.txtC_frecuencia_cierre_operativo.setText(usuario.getString(14));
                configuracion.txtC_comision_administracion01.setText(usuario.getString(15));
                configuracion.txtC_comision_administracion02.setText(usuario.getString(16));
                configuracion.txtC_comision_administracion03.setText(usuario.getString(17));
                configuracion.txtC_comision_operativa01.setText(usuario.getString(18));
                configuracion.txtC_comision_operativa02.setText(usuario.getString(19));
                configuracion.txtC_comision_operativa03.setText(usuario.getString(20));
                configuracion.txtC_mensaje_oficial_reportes.setText(usuario.getString(21));
                configuracion.txtC_nombre_Sistema_aplicacion.setText(usuario.getString(21));
                configuracion.txtC_fecha_configuracion_sistema.setText(usuario.getString(23));
                configuracion.txtC_caracter_separa_campos.setText(usuario.getString(24));
                configuracion.txtC_ruta_importar_archivo_plano.setText(usuario.getString(25));
                configuracion.txtC_ruta_archivos_entrada.setText(usuario.getString(26));
                configuracion.txtC_ruta_archivos_salida.setText(usuario.getString(27));
                configuracion.txtC_ruta_pagina_web.setText(usuario.getString(28));
                configuracion.txtC_ruta_reporte.setText(usuario.getString(29));
                configuracion.txtC_ruta_formato_reportes.setText(usuario.getString(30));
                configuracion.txtC_ruta_instalacion_aplicacion.setText(usuario.getString(31));
                configuracion.txtC_ruta_log_auditoria.setText(usuario.getString(32));
                configuracion.txtC_ruta_destino_reporte.setText(usuario.getString(33));
                configuracion.txtC_ruta_conexion_servidor.setText(usuario.getString(34));
                configuracion.txtC_ejecucion_procesos_batch.setText(usuario.getString(35));
                configuracion.txtC_persona_contacto.setText(usuario.getString(36));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exito = false;
                Limpiar_Tab_Configuracion();
            }
        } catch (SQLException ex) {
            exito = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
        Bloquear_Tab_Configuracion();
        return exito;
    }

    public void Tabla_Configuracion_Modificar() throws SQLException {
        configuracion.btnC_actualizar.setEnabled(true);
        String buscar = configuracion.txtC_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPCONCON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            if (usuario.next()) {
                Desbloquear_Tab_Configuracion();
                configuracion.btnC_insertar.setEnabled(false);
                configuracion.btnC_anular.setEnabled(false);
                configuracion.btnC_consultar.setEnabled(false);
                configuracion.txtC_cod_empresa.setText(usuario.getString(1));
                configuracion.txtC_ci_rif.setText(usuario.getString(2));
                configuracion.txtC_razon_social_persona.setText(usuario.getString(3));
                configuracion.txtC_cod_moneda.setText(usuario.getString(4));
                configuracion.txtC_cod_pais.setText(usuario.getString(5));
                configuracion.txtC_cod_postal.setText(usuario.getString(6));
                configuracion.txtC_formato_fecha.setText(usuario.getString(7));
                configuracion.txtC_formato_hora.setText(usuario.getString(8));
                configuracion.txtC_idioma.setText(usuario.getString(9));
                configuracion.txtC_direccion.setText(usuario.getString(10));
                configuracion.txtC_telefonos.setText(usuario.getString(11));
                configuracion.txtC_correo.setText(usuario.getString(12));
                configuracion.txtC_fecha_cierre_actividades.setText(usuario.getString(13));
                configuracion.txtC_frecuencia_cierre_operativo.setText(usuario.getString(14));
                configuracion.txtC_comision_administracion01.setText(usuario.getString(15));
                configuracion.txtC_comision_administracion02.setText(usuario.getString(16));
                configuracion.txtC_comision_administracion03.setText(usuario.getString(17));
                configuracion.txtC_comision_operativa01.setText(usuario.getString(18));
                configuracion.txtC_comision_operativa02.setText(usuario.getString(19));
                configuracion.txtC_comision_operativa03.setText(usuario.getString(20));
                configuracion.txtC_mensaje_oficial_reportes.setText(usuario.getString(21));
                configuracion.txtC_nombre_Sistema_aplicacion.setText(usuario.getString(21));
                configuracion.txtC_fecha_configuracion_sistema.setText(usuario.getString(23));
                configuracion.txtC_caracter_separa_campos.setText(usuario.getString(24));
                configuracion.txtC_ruta_importar_archivo_plano.setText(usuario.getString(25));
                configuracion.txtC_ruta_archivos_entrada.setText(usuario.getString(26));
                configuracion.txtC_ruta_archivos_salida.setText(usuario.getString(27));
                configuracion.txtC_ruta_pagina_web.setText(usuario.getString(28));
                configuracion.txtC_ruta_reporte.setText(usuario.getString(29));
                configuracion.txtC_ruta_formato_reportes.setText(usuario.getString(30));
                configuracion.txtC_ruta_instalacion_aplicacion.setText(usuario.getString(31));
                configuracion.txtC_ruta_log_auditoria.setText(usuario.getString(32));
                configuracion.txtC_ruta_destino_reporte.setText(usuario.getString(33));
                configuracion.txtC_ruta_conexion_servidor.setText(usuario.getString(34));
                configuracion.txtC_ejecucion_procesos_batch.setText(usuario.getString(35));
                configuracion.txtC_persona_contacto.setText(usuario.getString(36));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el usuario");
                configuracion.txtC_buscar.setText("");
                Bloquear_Tab_Configuracion();
                configuracion.btnC_actualizar.setEnabled(false);
                configuracion.btnC_insertar.setEnabled(true);
                configuracion.btnC_anular.setEnabled(true);
                configuracion.btnC_consultar.setEnabled(true);
                Limpiar_Tab_Configuracion();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void Tabla_Configuracion_Actualizar() throws SQLException {
        String buscar = configuracion.txtC_buscar.getText();
        String cod_empresa = configuracion.txtC_cod_empresa.getText();
        String ci_rif = configuracion.txtC_ci_rif.getText();
        String razon_social_persona = configuracion.txtC_razon_social_persona.getText();
        String cod_moneda = configuracion.txtC_cod_moneda.getText();
        String cod_pais = configuracion.txtC_cod_pais.getText();
        String cod_postal = configuracion.txtC_cod_postal.getText();
        String formato_fecha = configuracion.txtC_formato_fecha.getText();
        String formato_hora = configuracion.txtC_formato_hora.getText();
        String idioma = configuracion.txtC_idioma.getText();
        String direccion = configuracion.txtC_direccion.getText();
        String telefonos = configuracion.txtC_telefonos.getText();
        String correo = configuracion.txtC_correo.getText();
        String fecha_cierre_actividades = configuracion.txtC_fecha_cierre_actividades.getText();
        String frecuencia_cierre_operativo = configuracion.txtC_frecuencia_cierre_operativo.getText();
        String comision_administracion01 = configuracion.txtC_comision_administracion01.getText();
        String comision_administracion02 = configuracion.txtC_comision_administracion02.getText();
        String comision_administracion03 = configuracion.txtC_comision_administracion03.getText();
        String comision_operativa01 = configuracion.txtC_comision_operativa01.getText();
        String comision_operativa02 = configuracion.txtC_comision_operativa02.getText();
        String comision_operativa03 = configuracion.txtC_comision_operativa03.getText();
        String mensaje_oficial_reportes = configuracion.txtC_mensaje_oficial_reportes.getText();
        String nombre_Sistema_aplicacion = configuracion.txtC_nombre_Sistema_aplicacion.getText();
        String fecha_configuracion_sistema = configuracion.txtC_fecha_configuracion_sistema.getText();
        String caracter_separa_campos = configuracion.txtC_caracter_separa_campos.getText();
        String ruta_importar_archivo_plano = configuracion.txtC_ruta_importar_archivo_plano.getText();
        String ruta_archivos_entrada = configuracion.txtC_ruta_archivos_entrada.getText();
        String ruta_archivos_salida = configuracion.txtC_ruta_archivos_salida.getText();
        String ruta_pagina_web = configuracion.txtC_ruta_pagina_web.getText();
        String ruta_reporte = configuracion.txtC_ruta_reporte.getText();
        String ruta_formato_reportes = configuracion.txtC_ruta_formato_reportes.getText();
        String ruta_instalacion_aplicacion = configuracion.txtC_ruta_instalacion_aplicacion.getText();
        String ruta_log_auditoria = configuracion.txtC_ruta_log_auditoria.getText();
        String ruta_destino_reporte = configuracion.txtC_ruta_destino_reporte.getText();
        String ruta_conexion_servidor = configuracion.txtC_ruta_conexion_servidor.getText();
        String ejecucion_procesos_batch = configuracion.txtC_ejecucion_procesos_batch.getText();
        String persona_contacto = configuracion.txtC_persona_contacto.getText();
        try {
            if (cod_empresa.equals("") || ci_rif.equals("") || razon_social_persona.equals("")
                    || cod_moneda.equals("") || cod_pais.equals("") || cod_postal.equals("") || formato_fecha.equals("")
                    || formato_hora.equals("") || idioma.equals("") || direccion.equals("")
                    || telefonos.equals("") || correo.equals("") || fecha_cierre_actividades.equals("")
                    || frecuencia_cierre_operativo.equals("") || comision_administracion01.equals("") || comision_administracion02.equals("")
                    || comision_administracion03.equals("") || comision_operativa01.equals("") || comision_operativa02.equals("")
                    || comision_operativa03.equals("") || mensaje_oficial_reportes.equals("") || nombre_Sistema_aplicacion.equals("")
                    || fecha_configuracion_sistema.equals("") || caracter_separa_campos.equals("") || ruta_importar_archivo_plano.equals("") || ruta_archivos_entrada.equals("")
                    || ruta_archivos_salida.equals("") || ruta_pagina_web.equals("") || ruta_reporte.equals("") || ruta_formato_reportes.equals("")
                    || ruta_instalacion_aplicacion.equals("") || ruta_log_auditoria.equals("") || ruta_destino_reporte.equals("") || ruta_conexion_servidor.equals("")
                    || ejecucion_procesos_batch.equals("") || persona_contacto.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                configuracion.txtC_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPCONACT ('" + buscar + "',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_empresa);
                ppt.setString(2, ci_rif);
                ppt.setString(3, razon_social_persona);
                ppt.setString(4, cod_moneda);
                ppt.setString(5, cod_pais);
                ppt.setString(6, cod_postal);
                ppt.setString(7, formato_fecha);
                ppt.setString(8, formato_hora);
                ppt.setString(9, idioma);
                ppt.setString(10, direccion);
                ppt.setString(11, telefonos);
                ppt.setString(12, correo);
                ppt.setString(13, fecha_cierre_actividades);
                ppt.setString(14, frecuencia_cierre_operativo);
                ppt.setString(15, comision_administracion01);
                ppt.setString(16, comision_administracion02);
                ppt.setString(17, comision_administracion03);
                ppt.setString(18, comision_operativa01);
                ppt.setString(19, comision_operativa02);
                ppt.setString(20, comision_operativa03);
                ppt.setString(21, mensaje_oficial_reportes);
                ppt.setString(22, nombre_Sistema_aplicacion);
                ppt.setString(23, fecha_configuracion_sistema);
                ppt.setString(24, caracter_separa_campos);
                ppt.setString(25, ruta_importar_archivo_plano);
                ppt.setString(26, ruta_archivos_entrada);
                ppt.setString(27, ruta_archivos_salida);
                ppt.setString(28, ruta_pagina_web);
                ppt.setString(29, ruta_reporte);
                ppt.setString(30, ruta_formato_reportes);
                ppt.setString(31, ruta_instalacion_aplicacion);
                ppt.setString(32, ruta_log_auditoria);
                ppt.setString(33, ruta_destino_reporte);
                ppt.setString(34, ruta_conexion_servidor);
                ppt.setString(35, ejecucion_procesos_batch);
                ppt.setString(36, persona_contacto);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                configuracion.btnC_insertar.setEnabled(true);
                configuracion.btnC_anular.setEnabled(true);
                configuracion.btnC_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            configuracion.btnC_insertar.setEnabled(true);
            configuracion.btnC_anular.setEnabled(true);
            configuracion.btnC_consultar.setEnabled(true);
        }
        configuracion.btnC_actualizar.setEnabled(false);
        Limpiar_Tab_Configuracion();
        Bloquear_Tab_Configuracion();
    }

    public void Tabla_Configuracion_Anular() throws SQLException {
        String buscar = configuracion.txtC_ci_rif.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPCONANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Usuario no existe");
        }
    }

    /**
     * *******************TABLA INSTALACION SISTEMA 011 ***********************
     */
    public void Bloquear_Tab_Instalacion() {
        instalacion.txtIS_codigo_sistema.setEditable(false);
        instalacion.txtIS_descripcion_Sistema.setEditable(false);
        instalacion.txtIS_version_sistema.setEditable(false);
        instalacion.txtIS_fecha_instalacion.setEnabled(false);
        instalacion.txtIS_fecha_actualizacion.setEnabled(false);
        instalacion.txtIS_memoria_RAM_requerida.setEditable(false);
        instalacion.txtIS_manejador_documentos_sistema.setEditable(false);
        instalacion.txtIS_instalar_servidor_ApacheTOMCAT.setEditable(false);
        instalacion.txtIS_tipo_Sistema_Operativo.setEditable(false);
        instalacion.txtIS_ArquitecturaNro_Bits.setEditable(false);
        instalacion.txtIS_tipo_Manejador_BaseDatos.setEditable(false);
        instalacion.txtIS_usuario_Admin_BD.setEditable(false);
        instalacion.txtIS_clave_Admin_BD.setEditable(false);
        instalacion.txtIS_nombre_Base_Datos.setEditable(false);
        instalacion.txtIS_bd_manejo_reportesDocumentos.setEditable(false);
        instalacion.txtIS_nombre_Servidor.setEditable(false);
        instalacion.txtIS_autenticacion_directorioActivo.setEditable(false);
        instalacion.txtIS_manejo_multi_hilos.setEditable(false);
        instalacion.txtIS_manejo_chat_ayuda_online.setEditable(false);
        instalacion.txtIS_requiereInstalar_software_manejador_contenido.setEditable(false);
        instalacion.txtIS_manejo_historicos_Tablas.setEditable(false);
        instalacion.txtIS_cod_perfil_usuario_instalador.setEditable(false);
        instalacion.txtIS_responsable_instalacion.setEditable(false);
        instalacion.txtIS_ruta_log_auditoria_instalacion.setEditable(false);
        instalacion.txtIS_direccion_ip.setEditable(false);
        instalacion.txtIS_puerto_conexion.setEditable(false);
        instalacion.txtIS_espacio_minimo_DiscoDuro_servidor.setEditable(false);
        instalacion.txtIS_espacio_minimo_manejador_BD.setEditable(false);
        instalacion.txtIS_activar_directorioActivo.setEditable(false);
        instalacion.txtIS_servicio_directorioActivo.setEditable(false);
        instalacion.txtIS_servidor_correos.setEditable(false);
        ((JTextFieldDateEditor) instalacion.txtIS_fecha_instalacion.getDateEditor()).setDisabledTextColor(Color.BLACK);
        ((JTextFieldDateEditor) instalacion.txtIS_fecha_actualizacion.getDateEditor()).setDisabledTextColor(Color.BLACK);
    }

    public void Desbloquear_Tab_Instalacion() {
        instalacion.txtIS_codigo_sistema.setEditable(true);
        instalacion.txtIS_descripcion_Sistema.setEditable(true);
        instalacion.txtIS_version_sistema.setEditable(true);
        instalacion.txtIS_fecha_instalacion.setEnabled(true);
        instalacion.txtIS_fecha_actualizacion.setEnabled(true);
        instalacion.txtIS_memoria_RAM_requerida.setEditable(true);
        instalacion.txtIS_manejador_documentos_sistema.setEditable(true);
        instalacion.txtIS_instalar_servidor_ApacheTOMCAT.setEditable(true);
        instalacion.txtIS_tipo_Sistema_Operativo.setEditable(true);
        instalacion.txtIS_ArquitecturaNro_Bits.setEditable(true);
        instalacion.txtIS_tipo_Manejador_BaseDatos.setEditable(true);
        instalacion.txtIS_usuario_Admin_BD.setEditable(true);
        instalacion.txtIS_clave_Admin_BD.setEditable(true);
        instalacion.txtIS_nombre_Base_Datos.setEditable(true);
        instalacion.txtIS_bd_manejo_reportesDocumentos.setEditable(true);
        instalacion.txtIS_nombre_Servidor.setEditable(true);
        instalacion.txtIS_autenticacion_directorioActivo.setEditable(true);
        instalacion.txtIS_manejo_multi_hilos.setEditable(true);
        instalacion.txtIS_manejo_chat_ayuda_online.setEditable(true);
        instalacion.txtIS_requiereInstalar_software_manejador_contenido.setEditable(true);
        instalacion.txtIS_manejo_historicos_Tablas.setEditable(true);
        instalacion.txtIS_cod_perfil_usuario_instalador.setEditable(true);
        instalacion.txtIS_responsable_instalacion.setEditable(true);
        instalacion.txtIS_ruta_log_auditoria_instalacion.setEditable(true);
        instalacion.txtIS_direccion_ip.setEditable(true);
        instalacion.txtIS_puerto_conexion.setEditable(true);
        instalacion.txtIS_espacio_minimo_DiscoDuro_servidor.setEditable(true);
        instalacion.txtIS_espacio_minimo_manejador_BD.setEditable(true);
        instalacion.txtIS_activar_directorioActivo.setEditable(true);
        instalacion.txtIS_servicio_directorioActivo.setEditable(true);
        instalacion.txtIS_servidor_correos.setEditable(true);
    }

    public void Limpiar_Tab_Instalacion() {
        instalacion.txtIS_buscar.setText("");
        instalacion.txtIS_codigo_sistema.setText("");
        instalacion.txtIS_descripcion_Sistema.setText("");
        instalacion.txtIS_version_sistema.setText("");
        instalacion.txtIS_fecha_instalacion.setDate(null);
        instalacion.txtIS_fecha_actualizacion.setDate(null);
        instalacion.txtIS_memoria_RAM_requerida.setText("");
        instalacion.txtIS_manejador_documentos_sistema.setText("");
        instalacion.txtIS_instalar_servidor_ApacheTOMCAT.setText("");
        instalacion.txtIS_tipo_Sistema_Operativo.setText("");
        instalacion.txtIS_ArquitecturaNro_Bits.setText("");
        instalacion.txtIS_tipo_Manejador_BaseDatos.setText("");
        instalacion.txtIS_usuario_Admin_BD.setText("");
        instalacion.txtIS_clave_Admin_BD.setText("");
        instalacion.txtIS_nombre_Base_Datos.setText("");
        instalacion.txtIS_bd_manejo_reportesDocumentos.setText("");
        instalacion.txtIS_nombre_Servidor.setText("");
        instalacion.txtIS_autenticacion_directorioActivo.setText("");
        instalacion.txtIS_manejo_multi_hilos.setText("");
        instalacion.txtIS_manejo_chat_ayuda_online.setText("");
        instalacion.txtIS_requiereInstalar_software_manejador_contenido.setText("");
        instalacion.txtIS_manejo_historicos_Tablas.setText("");
        instalacion.txtIS_cod_perfil_usuario_instalador.setText("");
        instalacion.txtIS_responsable_instalacion.setText("");
        instalacion.txtIS_ruta_log_auditoria_instalacion.setText("");
        instalacion.txtIS_direccion_ip.setText("");
        instalacion.txtIS_puerto_conexion.setText("");
        instalacion.txtIS_espacio_minimo_DiscoDuro_servidor.setText("");
        instalacion.txtIS_espacio_minimo_manejador_BD.setText("");
        instalacion.txtIS_activar_directorioActivo.setText("");
        instalacion.txtIS_servicio_directorioActivo.setText("");
        instalacion.txtIS_servidor_correos.setText("");
    }

    public void Tabla_Instalacion_Insertar() throws SQLException {
        String codigo_sistema = instalacion.txtIS_codigo_sistema.getText();
        String descripcion_Sistema = instalacion.txtIS_descripcion_Sistema.getText();
        String version_sistema = instalacion.txtIS_version_sistema.getText();
        String memoria_RAM_requerida = instalacion.txtIS_memoria_RAM_requerida.getText();
        String manejador_documentos_sistema = instalacion.txtIS_manejador_documentos_sistema.getText();
        String instalar_servidor_ApacheTOMCAT = instalacion.txtIS_instalar_servidor_ApacheTOMCAT.getText();
        String tipo_Sistema_Operativo = instalacion.txtIS_tipo_Sistema_Operativo.getText();
        String ArquitecturaNro_Bits = instalacion.txtIS_ArquitecturaNro_Bits.getText();
        String tipo_Manejador_BaseDatos = instalacion.txtIS_tipo_Manejador_BaseDatos.getText();
        String usuario_Admin_BD = instalacion.txtIS_usuario_Admin_BD.getText();
        String clave_Admin_BD = instalacion.txtIS_clave_Admin_BD.getText();
        String nombre_Base_Datos = instalacion.txtIS_nombre_Base_Datos.getText();
        String bd_manejo_reportesDocumentos = instalacion.txtIS_bd_manejo_reportesDocumentos.getText();
        String nombre_Servidor = instalacion.txtIS_nombre_Servidor.getText();
        String autenticacion_directorioActivo = instalacion.txtIS_autenticacion_directorioActivo.getText();
        String manejo_multi_hilos = instalacion.txtIS_manejo_multi_hilos.getText();
        String manejo_chat_ayuda_online = instalacion.txtIS_manejo_chat_ayuda_online.getText();
        String requiereInstalar_software_manejador_contenido = instalacion.txtIS_requiereInstalar_software_manejador_contenido.getText();
        String manejo_historicos_Tablas = instalacion.txtIS_manejo_historicos_Tablas.getText();
        String cod_perfil_usuario_instalador = instalacion.txtIS_cod_perfil_usuario_instalador.getText();
        String responsable_instalacion = instalacion.txtIS_responsable_instalacion.getText();
        String ruta_log_auditoria_instalacion = instalacion.txtIS_ruta_log_auditoria_instalacion.getText();
        String direccion_ip = instalacion.txtIS_direccion_ip.getText();
        String puerto_conexion = instalacion.txtIS_puerto_conexion.getText();
        String espacio_minimo_DiscoDuro_servidor = instalacion.txtIS_espacio_minimo_DiscoDuro_servidor.getText();
        String espacio_minimo_manejador_BD = instalacion.txtIS_espacio_minimo_manejador_BD.getText();
        String activar_directorioActivo = instalacion.txtIS_activar_directorioActivo.getText();
        String servicio_directorioActivo = instalacion.txtIS_servicio_directorioActivo.getText();
        String servidor_correos = instalacion.txtIS_servidor_correos.getText();
        try {
            if (codigo_sistema.equals("") || descripcion_Sistema.equals("") || version_sistema.equals("")
                    || memoria_RAM_requerida.equals("") || manejador_documentos_sistema.equals("")
                    || instalar_servidor_ApacheTOMCAT.equals("") || tipo_Sistema_Operativo.equals("") || ArquitecturaNro_Bits.equals("")
                    || tipo_Manejador_BaseDatos.equals("") || usuario_Admin_BD.equals("") || clave_Admin_BD.equals("")
                    || nombre_Base_Datos.equals("") || bd_manejo_reportesDocumentos.equals("") || nombre_Servidor.equals("")
                    || autenticacion_directorioActivo.equals("") || manejo_multi_hilos.equals("") || manejo_chat_ayuda_online.equals("")
                    || requiereInstalar_software_manejador_contenido.equals("") || manejo_historicos_Tablas.equals("") || cod_perfil_usuario_instalador.equals("")
                    || responsable_instalacion.equals("") || ruta_log_auditoria_instalacion.equals("") || direccion_ip.equals("") || puerto_conexion.equals("")
                    || espacio_minimo_DiscoDuro_servidor.equals("") || espacio_minimo_manejador_BD.equals("") || activar_directorioActivo.equals("") || servicio_directorioActivo.equals("")
                    || servidor_correos.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                instalacion.txtIS_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPIDSINS (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, codigo_sistema);
                ppt.setString(2, descripcion_Sistema);
                ppt.setString(3, version_sistema);
                ppt.setString(4, memoria_RAM_requerida);
                ppt.setString(5, manejador_documentos_sistema);
                ppt.setString(6, instalar_servidor_ApacheTOMCAT);
                ppt.setString(7, tipo_Sistema_Operativo);
                ppt.setString(8, ArquitecturaNro_Bits);
                ppt.setString(9, tipo_Manejador_BaseDatos);
                ppt.setString(10, usuario_Admin_BD);
                ppt.setString(11, clave_Admin_BD);
                ppt.setString(12, nombre_Base_Datos);
                ppt.setString(13, bd_manejo_reportesDocumentos);
                ppt.setString(14, nombre_Servidor);
                ppt.setString(15, autenticacion_directorioActivo);
                ppt.setString(16, manejo_multi_hilos);
                ppt.setString(17, manejo_chat_ayuda_online);
                ppt.setString(18, requiereInstalar_software_manejador_contenido);
                ppt.setString(19, manejo_historicos_Tablas);
                ppt.setString(20, cod_perfil_usuario_instalador);
                ppt.setString(21, responsable_instalacion);
                ppt.setString(22, ruta_log_auditoria_instalacion);
                ppt.setString(23, direccion_ip);
                ppt.setString(24, puerto_conexion);
                ppt.setString(25, espacio_minimo_DiscoDuro_servidor);
                ppt.setString(26, espacio_minimo_manejador_BD);
                ppt.setString(27, activar_directorioActivo);
                ppt.setString(28, servicio_directorioActivo);
                ppt.setString(29, servidor_correos);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                instalacion.txtIS_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
            instalacion.txtIS_buscar.setEnabled(true);
        }
    }

    public boolean Tabla_Instalacion_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = instalacion.txtIS_buscar.getText();
        instalacion.txtIS_buscar.setText("");
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPIDSCON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            if (usuario.next()) {
                instalacion.txtIS_codigo_sistema.setText(usuario.getString(1));
                instalacion.txtIS_descripcion_Sistema.setText(usuario.getString(2));
                instalacion.txtIS_version_sistema.setText(usuario.getString(3));
                instalacion.txtIS_fecha_instalacion.setDate(usuario.getDate(4));
                instalacion.txtIS_fecha_actualizacion.setDate(usuario.getDate(5));
                instalacion.txtIS_memoria_RAM_requerida.setText(usuario.getString(6));
                instalacion.txtIS_manejador_documentos_sistema.setText(usuario.getString(7));
                instalacion.txtIS_instalar_servidor_ApacheTOMCAT.setText(usuario.getString(8));
                instalacion.txtIS_tipo_Sistema_Operativo.setText(usuario.getString(9));
                instalacion.txtIS_ArquitecturaNro_Bits.setText(usuario.getString(10));
                instalacion.txtIS_tipo_Manejador_BaseDatos.setText(usuario.getString(11));
                instalacion.txtIS_usuario_Admin_BD.setText(usuario.getString(12));
                instalacion.txtIS_clave_Admin_BD.setText(usuario.getString(13));
                instalacion.txtIS_nombre_Base_Datos.setText(usuario.getString(14));
                instalacion.txtIS_bd_manejo_reportesDocumentos.setText(usuario.getString(15));
                instalacion.txtIS_nombre_Servidor.setText(usuario.getString(16));
                instalacion.txtIS_autenticacion_directorioActivo.setText(usuario.getString(17));
                instalacion.txtIS_manejo_multi_hilos.setText(usuario.getString(18));
                instalacion.txtIS_manejo_chat_ayuda_online.setText(usuario.getString(19));
                instalacion.txtIS_requiereInstalar_software_manejador_contenido.setText(usuario.getString(20));
                instalacion.txtIS_manejo_historicos_Tablas.setText(usuario.getString(21));
                instalacion.txtIS_cod_perfil_usuario_instalador.setText(usuario.getString(21));
                instalacion.txtIS_responsable_instalacion.setText(usuario.getString(23));
                instalacion.txtIS_ruta_log_auditoria_instalacion.setText(usuario.getString(24));
                instalacion.txtIS_direccion_ip.setText(usuario.getString(25));
                instalacion.txtIS_puerto_conexion.setText(usuario.getString(26));
                instalacion.txtIS_espacio_minimo_DiscoDuro_servidor.setText(usuario.getString(27));
                instalacion.txtIS_espacio_minimo_manejador_BD.setText(usuario.getString(28));
                instalacion.txtIS_activar_directorioActivo.setText(usuario.getString(29));
                instalacion.txtIS_servicio_directorioActivo.setText(usuario.getString(30));
                instalacion.txtIS_servidor_correos.setText(usuario.getString(31));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exito = false;
                Limpiar_Tab_Instalacion();
            }
        } catch (SQLException ex) {
            exito = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
        Bloquear_Tab_Instalacion();
        return exito;
    }

    public void Tabla_Instalacion_Modificar() throws SQLException {
        instalacion.btnIS_actualizar.setEnabled(true);
        String buscar = instalacion.txtIS_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPIDSCON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            if (usuario.next()) {
                Desbloquear_Tab_Instalacion();
                instalacion.btnIS_insertar.setEnabled(false);
                instalacion.btnIS_anular.setEnabled(false);
                instalacion.btnIS_consultar.setEnabled(false);
                instalacion.txtIS_codigo_sistema.setText(usuario.getString(1));
                instalacion.txtIS_descripcion_Sistema.setText(usuario.getString(2));
                instalacion.txtIS_version_sistema.setText(usuario.getString(3));
                instalacion.txtIS_fecha_instalacion.setDate(usuario.getDate(4));
                instalacion.txtIS_fecha_actualizacion.setDate(usuario.getDate(5));
                instalacion.txtIS_memoria_RAM_requerida.setText(usuario.getString(6));
                instalacion.txtIS_manejador_documentos_sistema.setText(usuario.getString(7));
                instalacion.txtIS_instalar_servidor_ApacheTOMCAT.setText(usuario.getString(8));
                instalacion.txtIS_tipo_Sistema_Operativo.setText(usuario.getString(9));
                instalacion.txtIS_ArquitecturaNro_Bits.setText(usuario.getString(10));
                instalacion.txtIS_tipo_Manejador_BaseDatos.setText(usuario.getString(11));
                instalacion.txtIS_usuario_Admin_BD.setText(usuario.getString(12));
                instalacion.txtIS_clave_Admin_BD.setText(usuario.getString(13));
                instalacion.txtIS_nombre_Base_Datos.setText(usuario.getString(14));
                instalacion.txtIS_bd_manejo_reportesDocumentos.setText(usuario.getString(15));
                instalacion.txtIS_nombre_Servidor.setText(usuario.getString(16));
                instalacion.txtIS_autenticacion_directorioActivo.setText(usuario.getString(17));
                instalacion.txtIS_manejo_multi_hilos.setText(usuario.getString(18));
                instalacion.txtIS_manejo_chat_ayuda_online.setText(usuario.getString(19));
                instalacion.txtIS_requiereInstalar_software_manejador_contenido.setText(usuario.getString(20));
                instalacion.txtIS_manejo_historicos_Tablas.setText(usuario.getString(21));
                instalacion.txtIS_cod_perfil_usuario_instalador.setText(usuario.getString(21));
                instalacion.txtIS_responsable_instalacion.setText(usuario.getString(23));
                instalacion.txtIS_ruta_log_auditoria_instalacion.setText(usuario.getString(24));
                instalacion.txtIS_direccion_ip.setText(usuario.getString(25));
                instalacion.txtIS_puerto_conexion.setText(usuario.getString(26));
                instalacion.txtIS_espacio_minimo_DiscoDuro_servidor.setText(usuario.getString(27));
                instalacion.txtIS_espacio_minimo_manejador_BD.setText(usuario.getString(28));
                instalacion.txtIS_activar_directorioActivo.setText(usuario.getString(29));
                instalacion.txtIS_servicio_directorioActivo.setText(usuario.getString(30));
                instalacion.txtIS_servidor_correos.setText(usuario.getString(31));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el usuario");
                instalacion.txtIS_buscar.setText("");
                Bloquear_Tab_Instalacion();
                instalacion.btnIS_actualizar.setEnabled(false);
                instalacion.btnIS_insertar.setEnabled(true);
                instalacion.btnIS_anular.setEnabled(true);
                instalacion.btnIS_consultar.setEnabled(true);
                Limpiar_Tab_Instalacion();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void Tabla_Instalacion_Actualizar() throws SQLException {
        String buscar = instalacion.txtIS_buscar.getText();
        String codigo_sistema = instalacion.txtIS_codigo_sistema.getText();
        String descripcion_Sistema = instalacion.txtIS_descripcion_Sistema.getText();
        String version_sistema = instalacion.txtIS_version_sistema.getText();
        String memoria_RAM_requerida = instalacion.txtIS_memoria_RAM_requerida.getText();
        String manejador_documentos_sistema = instalacion.txtIS_manejador_documentos_sistema.getText();
        String instalar_servidor_ApacheTOMCAT = instalacion.txtIS_instalar_servidor_ApacheTOMCAT.getText();
        String tipo_Sistema_Operativo = instalacion.txtIS_tipo_Sistema_Operativo.getText();
        String ArquitecturaNro_Bits = instalacion.txtIS_ArquitecturaNro_Bits.getText();
        String tipo_Manejador_BaseDatos = instalacion.txtIS_tipo_Manejador_BaseDatos.getText();
        String usuario_Admin_BD = instalacion.txtIS_usuario_Admin_BD.getText();
        String clave_Admin_BD = instalacion.txtIS_clave_Admin_BD.getText();
        String nombre_Base_Datos = instalacion.txtIS_nombre_Base_Datos.getText();
        String bd_manejo_reportesDocumentos = instalacion.txtIS_bd_manejo_reportesDocumentos.getText();
        String nombre_Servidor = instalacion.txtIS_nombre_Servidor.getText();
        String autenticacion_directorioActivo = instalacion.txtIS_autenticacion_directorioActivo.getText();
        String manejo_multi_hilos = instalacion.txtIS_manejo_multi_hilos.getText();
        String manejo_chat_ayuda_online = instalacion.txtIS_manejo_chat_ayuda_online.getText();
        String requiereInstalar_software_manejador_contenido = instalacion.txtIS_requiereInstalar_software_manejador_contenido.getText();
        String manejo_historicos_Tablas = instalacion.txtIS_manejo_historicos_Tablas.getText();
        String cod_perfil_usuario_instalador = instalacion.txtIS_cod_perfil_usuario_instalador.getText();
        String responsable_instalacion = instalacion.txtIS_responsable_instalacion.getText();
        String ruta_log_auditoria_instalacion = instalacion.txtIS_ruta_log_auditoria_instalacion.getText();
        String direccion_ip = instalacion.txtIS_direccion_ip.getText();
        String puerto_conexion = instalacion.txtIS_puerto_conexion.getText();
        String espacio_minimo_DiscoDuro_servidor = instalacion.txtIS_espacio_minimo_DiscoDuro_servidor.getText();
        String espacio_minimo_manejador_BD = instalacion.txtIS_espacio_minimo_manejador_BD.getText();
        String activar_directorioActivo = instalacion.txtIS_activar_directorioActivo.getText();
        String servicio_directorioActivo = instalacion.txtIS_servicio_directorioActivo.getText();
        String servidor_correos = instalacion.txtIS_servidor_correos.getText();
        try {
            if (codigo_sistema.equals("") || descripcion_Sistema.equals("") || version_sistema.equals("")
                    || memoria_RAM_requerida.equals("") || manejador_documentos_sistema.equals("")
                    || instalar_servidor_ApacheTOMCAT.equals("") || tipo_Sistema_Operativo.equals("") || ArquitecturaNro_Bits.equals("")
                    || tipo_Manejador_BaseDatos.equals("") || usuario_Admin_BD.equals("") || clave_Admin_BD.equals("")
                    || nombre_Base_Datos.equals("") || bd_manejo_reportesDocumentos.equals("") || nombre_Servidor.equals("")
                    || autenticacion_directorioActivo.equals("") || manejo_multi_hilos.equals("") || manejo_chat_ayuda_online.equals("")
                    || requiereInstalar_software_manejador_contenido.equals("") || manejo_historicos_Tablas.equals("") || cod_perfil_usuario_instalador.equals("")
                    || responsable_instalacion.equals("") || ruta_log_auditoria_instalacion.equals("") || direccion_ip.equals("") || puerto_conexion.equals("")
                    || espacio_minimo_DiscoDuro_servidor.equals("") || espacio_minimo_manejador_BD.equals("") || activar_directorioActivo.equals("") || servicio_directorioActivo.equals("")
                    || servidor_correos.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                instalacion.txtIS_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPIDSACT ('" + buscar + "',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, codigo_sistema);
                ppt.setString(2, descripcion_Sistema);
                ppt.setString(3, version_sistema);
                ppt.setString(4, memoria_RAM_requerida);
                ppt.setString(5, manejador_documentos_sistema);
                ppt.setString(6, instalar_servidor_ApacheTOMCAT);
                ppt.setString(7, tipo_Sistema_Operativo);
                ppt.setString(8, ArquitecturaNro_Bits);
                ppt.setString(9, tipo_Manejador_BaseDatos);
                ppt.setString(10, usuario_Admin_BD);
                ppt.setString(11, clave_Admin_BD);
                ppt.setString(12, nombre_Base_Datos);
                ppt.setString(13, bd_manejo_reportesDocumentos);
                ppt.setString(14, nombre_Servidor);
                ppt.setString(15, autenticacion_directorioActivo);
                ppt.setString(16, manejo_multi_hilos);
                ppt.setString(17, manejo_chat_ayuda_online);
                ppt.setString(18, requiereInstalar_software_manejador_contenido);
                ppt.setString(19, manejo_historicos_Tablas);
                ppt.setString(20, cod_perfil_usuario_instalador);
                ppt.setString(21, responsable_instalacion);
                ppt.setString(22, ruta_log_auditoria_instalacion);
                ppt.setString(23, direccion_ip);
                ppt.setString(24, puerto_conexion);
                ppt.setString(25, espacio_minimo_DiscoDuro_servidor);
                ppt.setString(26, espacio_minimo_manejador_BD);
                ppt.setString(27, activar_directorioActivo);
                ppt.setString(28, servicio_directorioActivo);
                ppt.setString(29, servidor_correos);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                instalacion.btnIS_insertar.setEnabled(true);
                instalacion.btnIS_anular.setEnabled(true);
                instalacion.btnIS_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            instalacion.btnIS_insertar.setEnabled(true);
            instalacion.btnIS_anular.setEnabled(true);
            instalacion.btnIS_consultar.setEnabled(true);
        }
        instalacion.btnIS_actualizar.setEnabled(false);
        Limpiar_Tab_Instalacion();
        Bloquear_Tab_Instalacion();
    }

    public void Tabla_Instalacion_Anular() throws SQLException {
        String buscar = instalacion.txtIS_codigo_sistema.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPIDSANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Usuario no existe");
        }
    }

    /**
     * *******************TABLA TRANSACCIONES 012 *******************************
     */
    public void Bloquear_Tab_Transaccion() {
        transaccion.txtT_nro_transaccion.setEditable(false);
        transaccion.txtT_fecha_transaccion.setEnabled(false);
        transaccion.txtT_fecha_registro_transaccion.setEnabled(false);
        transaccion.txtT_fecha_proceso_transaccion.setEnabled(false);
        transaccion.txtT_fecha_utima_actualizacion.setEnabled(false);
        transaccion.txtT_monto_transaccion.setEditable(false);
        transaccion.txtT_NRO_CTA_BCO.setEditable(false);
        transaccion.txtT_tasa_cambio_trans.setEditable(false);
        transaccion.txtT_monto_comis_aplicada.setEditable(false);
        transaccion.txtT_nombre_apellido.setEditable(false);
        transaccion.txtT_codigo_operador_swift.setEditable(false);
        transaccion.txtT_mto_moneda_extranjera_trans.setEditable(false);
        transaccion.txtT_nat_trans.setEditable(false);
        transaccion.txtT_cuenta_debito_transferencia.setEditable(false);
        transaccion.txtT_cuenta_credito_transferencia.setEditable(false);
        transaccion.txtT_indicador_exoneracion_impuesto_deito_banco.setEditable(false);
        transaccion.txtT_monto_impuesto_deito_banco.setEditable(false);
        transaccion.txtT_descrip_comis_trans.setEditable(false);
        transaccion.txtT_ci_rif_beneficiario_transferencia.setEditable(false);
        transaccion.txtT_CiRif_Beneficiario.setEditable(false);
        transaccion.cmbCP.setEnabled(false);
        transaccion.cmbMon.setEnabled(false);
        transaccion.cmbTipo.setEnabled(false);
        transaccion.cmbFP.setEnabled(false);
        transaccion.cmbST.setEnabled(false);
        ((JTextFieldDateEditor) transaccion.txtT_fecha_transaccion.getDateEditor()).setDisabledTextColor(Color.BLACK);
        ((JTextFieldDateEditor) transaccion.txtT_fecha_registro_transaccion.getDateEditor()).setDisabledTextColor(Color.BLACK);
        ((JTextFieldDateEditor) transaccion.txtT_fecha_proceso_transaccion.getDateEditor()).setDisabledTextColor(Color.BLACK);
        ((JTextFieldDateEditor) transaccion.txtT_fecha_utima_actualizacion.getDateEditor()).setDisabledTextColor(Color.BLACK);
    }

    public void Desbloquear_Tab_Transaccion() {
        transaccion.txtT_monto_transaccion.setEditable(true);
        transaccion.txtT_NRO_CTA_BCO.setEditable(true);
        transaccion.txtT_tasa_cambio_trans.setEditable(true);
        transaccion.txtT_monto_comis_aplicada.setEditable(true);
        transaccion.txtT_codigo_operador_swift.setEditable(true);
        transaccion.txtT_mto_moneda_extranjera_trans.setEditable(true);
        transaccion.txtT_nat_trans.setEditable(true);
        transaccion.txtT_cuenta_debito_transferencia.setEditable(true);
        transaccion.txtT_cuenta_credito_transferencia.setEditable(true);
        transaccion.txtT_indicador_exoneracion_impuesto_deito_banco.setEditable(true);
        transaccion.txtT_monto_impuesto_deito_banco.setEditable(true);
        transaccion.txtT_descrip_comis_trans.setEditable(true);
        transaccion.txtT_ci_rif_beneficiario_transferencia.setEditable(true);
        transaccion.txtT_fecha_transaccion.setEnabled(true);
        transaccion.txtT_CiRif_Beneficiario.setEditable(true);
        transaccion.cmbCP.setEnabled(true);
        transaccion.cmbMon.setEnabled(true);
        transaccion.cmbTipo.setEnabled(true);
        transaccion.cmbFP.setEnabled(true);
        
    }

    public void Limpiar_Tab_Transaccion() {
        transaccion.txtT_buscar.setText("");
        transaccion.txtT_nro_transaccion.setText("");
        transaccion.txtT_nombre_apellido.setText("");
        transaccion.txtT_fecha_transaccion.setDate(null);
        transaccion.txtT_fecha_registro_transaccion.setDate(null);
        transaccion.txtT_fecha_proceso_transaccion.setDate(null);
        transaccion.txtT_fecha_utima_actualizacion.setDate(null);
        transaccion.txtT_monto_transaccion.setText("");
        transaccion.txtT_NRO_CTA_BCO.setText("");
        transaccion.txtT_tasa_cambio_trans.setText("");
        transaccion.txtT_monto_comis_aplicada.setText("");
        transaccion.txtT_codigo_operador_swift.setText("");
        transaccion.txtT_mto_moneda_extranjera_trans.setText("");
        transaccion.txtT_nat_trans.setText("");
        transaccion.txtT_cuenta_debito_transferencia.setText("");
        transaccion.txtT_cuenta_credito_transferencia.setText("");
        transaccion.txtT_indicador_exoneracion_impuesto_deito_banco.setText("");
        transaccion.txtT_monto_impuesto_deito_banco.setText("");
        transaccion.txtT_descrip_comis_trans.setText("");
        transaccion.txtT_ci_rif_beneficiario_transferencia.setText("");
        transaccion.txtT_CiRif_Beneficiario.setText("");
        transaccion.cmbCP.removeAllItems();
        transaccion.cmbFP.setSelectedIndex(0);
        transaccion.cmbMon.setSelectedIndex(0);
        transaccion.cmbTipo.setSelectedIndex(0);
        transaccion.cmbST.setSelectedIndex(0);
        transaccion.cmbST.setEnabled(false);
        
        
    }

    public void Tabla_Transaccion_Insertar() throws SQLException {
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        String cod_tipo_transaccion = transaccion.cmbTipo.getItemAt(transaccion.cmbTipo.getSelectedIndex()).split("-")[0];
        String cod_Producto = transaccion.cmbCP.getItemAt(transaccion.cmbCP.getSelectedIndex()).split("-")[0];
        String ci_rif = transaccion.txtT_ci_rif_beneficiario_transferencia.getText();
        String cod_moneda = transaccion.cmbMon.getItemAt(transaccion.cmbMon.getSelectedIndex()).split("-")[0];
        String fecha_transaccion=(transaccion.txtT_fecha_transaccion.getDate() != null) ? date.format(transaccion.txtT_fecha_transaccion.getDate()) : "";
        String monto_transaccion = transaccion.txtT_monto_transaccion.getText();
        String NRO_CTA_BCO = transaccion.txtT_NRO_CTA_BCO.getText();
        String mto_moneda_extranjera_trans = transaccion.txtT_mto_moneda_extranjera_trans.getText();
        String tasa_cambio_trans = transaccion.txtT_tasa_cambio_trans.getText();
        String nat_trans = transaccion.txtT_nat_trans.getText();
        String descrip_comis_trans = transaccion.txtT_descrip_comis_trans.getText();
        String monto_comis_aplicada = transaccion.txtT_monto_comis_aplicada.getText();
        String monto_impuesto_deito_banco = transaccion.txtT_monto_impuesto_deito_banco.getText();
        String indicador_exoneracion_impuesto_deito_banco = transaccion.txtT_indicador_exoneracion_impuesto_deito_banco.getText();
        String cuenta_debito_transferencia = transaccion.txtT_cuenta_debito_transferencia.getText();
        String cuenta_credito_transferencia = transaccion.txtT_cuenta_credito_transferencia.getText();
        String ci_rif_beneficiario_transferencia = transaccion.txtT_CiRif_Beneficiario.getText();
        String codigo_operador_swift = transaccion.txtT_codigo_operador_swift.getText();
        String cod_forma_pago = transaccion.cmbFP.getItemAt(transaccion.cmbFP.getSelectedIndex()).split("-")[0];
        try {
            if (cod_tipo_transaccion.equals("") && cod_Producto.equals("") && ci_rif.equals("") 
             && cod_moneda.equals("") && fecha_transaccion.equals("") && monto_transaccion.equals("")
             && cod_forma_pago.equals("") && codigo_operador_swift.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                transaccion.txtT_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPTRAINS (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_tipo_transaccion);
                ppt.setString(2, cod_Producto);
                ppt.setString(3, ci_rif);
                ppt.setString(4, cod_moneda);
                ppt.setString(5, fecha_transaccion);
                ppt.setString(6, monto_transaccion);
                ppt.setString(7, NRO_CTA_BCO);
                ppt.setString(8, mto_moneda_extranjera_trans);
                ppt.setString(9, tasa_cambio_trans);
                ppt.setString(10, nat_trans);
                ppt.setString(11, descrip_comis_trans);
                ppt.setString(12, monto_comis_aplicada);
                ppt.setString(13, monto_impuesto_deito_banco);
                ppt.setString(14, indicador_exoneracion_impuesto_deito_banco);
                ppt.setString(15, cuenta_debito_transferencia);
                ppt.setString(16, cuenta_credito_transferencia);
                ppt.setString(17, ci_rif_beneficiario_transferencia);
                ppt.setString(18, codigo_operador_swift);
                ppt.setString(19, cod_forma_pago);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                //GlobalConstants.getAuditoria().setLog("I");
                transaccion.txtT_buscar.setEnabled(true);
                Limpiar_Tab_Transaccion();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
        }
    }

    public boolean Tabla_Transaccion_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = transaccion.txtT_buscar.getText();
        transaccion.txtT_buscar.setText("");
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPTRACON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            if (usuario.next()) {
                    CallableStatement ppt2 = cn.prepareCall("{call FSOSEGSTPPERCON ('" + usuario.getString(23) + "')}");
                    ResultSet usuario2 = ppt2.executeQuery();
                    if(usuario2.next())
                    {
                        transaccion.txtT_ci_rif_beneficiario_transferencia.setText(usuario.getString(23));
                        transaccion.carga_datos_persona(usuario2.getString(2));
                        transaccion.txtT_nro_transaccion.setText(usuario.getString(1));
                        transaccion.txtT_fecha_registro_transaccion.setDate(usuario.getDate(5));
                        transaccion.txtT_fecha_transaccion.setDate(usuario.getDate(6));
                        transaccion.txtT_fecha_proceso_transaccion.setDate(usuario.getDate(7));
                        transaccion.txtT_fecha_utima_actualizacion.setDate(usuario.getDate(8));
                        transaccion.txtT_codigo_operador_swift.setText(usuario.getString(9));
                        transaccion.txtT_monto_transaccion.setText(usuario.getString(12));
                        transaccion.txtT_NRO_CTA_BCO.setText(usuario.getString(13));
                        transaccion.txtT_mto_moneda_extranjera_trans.setText(usuario.getString(14));
                        transaccion.txtT_indicador_exoneracion_impuesto_deito_banco.setText(usuario.getString(15));
                        transaccion.txtT_monto_comis_aplicada.setText(usuario.getString(16));
                        transaccion.txtT_cuenta_debito_transferencia.setText(usuario.getString(17));
                        transaccion.txtT_nat_trans.setText(usuario.getString(18));
                        transaccion.txtT_descrip_comis_trans.setText(usuario.getString(19));
                        transaccion.txtT_tasa_cambio_trans.setText(usuario.getString(20));
                        transaccion.txtT_monto_impuesto_deito_banco.setText(usuario.getString(21));
                        transaccion.txtT_cuenta_credito_transferencia.setText(usuario.getString(22));
                        transaccion.txtT_CiRif_Beneficiario.setText(usuario.getString(24));
                        PreparedStatement sql = cn.prepareStatement("SELECT Des_Producto FROM t_producto where cod_Producto='"+
                                usuario.getString(2)+"'");
                        ResultSet prod = sql.executeQuery();
                        if(prod.next())
                        {
                            transaccion.cmbCP.setSelectedItem(usuario.getString(2)+"-"+prod.getString(1));
                        }
                        
                        sql = cn.prepareStatement("SELECT clase_transaccion FROM t_tipo_transaccion where cod_tipo_oper='"+
                                usuario.getString(4)+"'");
                        prod = sql.executeQuery();
                        if(prod.next())
                        {
                            transaccion.cmbTipo.setSelectedItem(usuario.getString(4)+"-"+prod.getString(1));
                        }
                        sql = cn.prepareStatement("SELECT descripcion_moneda FROM t_moneda where cod_moneda='"+
                                usuario.getString(3)+"'");
                        prod = sql.executeQuery();
                        if(prod.next())
                        {
                            transaccion.cmbMon.setSelectedItem(usuario.getString(3)+"-"+prod.getString(1));
                        }
                        sql = cn.prepareStatement("SELECT des_forma_pago FROM t_forma_pago where cod_forma_pago='"+
                                usuario.getString(11)+"'");
                        prod = sql.executeQuery();
                        if(prod.next())
                        {
                            transaccion.cmbMon.setSelectedItem(usuario.getString(11)+"-"+prod.getString(1));
                        }
                        transaccion.cmbST.setSelectedItem(usuario.getString(10));
                        
                    }else
                    {
                        JOptionPane.showMessageDialog(null, "El registro no esta asignado a un producto o persona");
                        exito = false;
                        Limpiar_Tab_Transaccion();
                    }
                
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exito = false;
                Limpiar_Tab_Transaccion();
            }
        } catch (SQLException ex) {
            exito = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
        Bloquear_Tab_Transaccion();
        return exito;
    }

    public void Tabla_Transaccion_Modificar() throws SQLException {
        boolean exito = true;
        transaccion.btnT_actualizar.setEnabled(true);
        Desbloquear_Tab_Transaccion();
        transaccion.cmbST.setEnabled(true);
    }

    public void Tabla_Transaccion_Actualizar() throws SQLException {
        /*
       // String buscar = transaccion.txtT_ci_rif.getText();
       // String cod_tipo_transaccion = transaccion.txtT_cod_tipo_transaccion.getText();
       // String cod_Producto = transaccion.txtT_cod_Producto.getText();
       // String ci_rif = transaccion.txtT_ci_rif.getText();
//        String cod_moneda = transaccion.txtT_cod_moneda.getText();
//        String clase_trans = transaccion.txtT_clase_trans.getText();
        String monto_transaccion = transaccion.txtT_monto_transaccion.getText();
        String NRO_CTA_BCO = transaccion.txtT_NRO_CTA_BCO.getText();
        String mto_moneda_extranjera_trans = transaccion.txtT_mto_moneda_extranjera_trans.getText();
        String tasa_cambio_trans = transaccion.txtT_tasa_cambio_trans.getText();
        String nat_trans = transaccion.txtT_nat_trans.getText();
        String descrip_comis_trans = transaccion.txtT_descrip_comis_trans.getText();
        String monto_comis_aplicada = transaccion.txtT_monto_comis_aplicada.getText();
        String monto_impuesto_deito_banco = transaccion.txtT_monto_impuesto_deito_banco.getText();
        String indicador_exoneracion_impuesto_deito_banco = transaccion.txtT_indicador_exoneracion_impuesto_deito_banco.getText();
       // String indicador_situacion_operacion = transaccion.txtT_indicador_situacion_operacion.getText();
        //String cod_usuario_operativo = transaccion.txtT_cod_usuario_operativo.getText();
        String cuenta_debito_transferencia = transaccion.txtT_cuenta_debito_transferencia.getText();
        String cuenta_credito_transferencia = transaccion.txtT_cuenta_credito_transferencia.getText();
        String ci_rif_beneficiario_transferencia = transaccion.txtT_ci_rif_beneficiario_transferencia.getText();
        String codigo_operador_swift = transaccion.txtT_codigo_operador_swift.getText();
      //  String cod_forma_pago = transaccion.txtT_cod_forma_pago.getText();
        try {
            if (cod_tipo_transaccion.equals("") && cod_Producto.equals("") /*|| 
               tipo_per_fdte.equals("")  || cod_moneda.equals("") || clase_trans.equals("")||
               cod_tipo_asiento_contable.equals("") || cod_usuario_operativo.equals("") || fecha_transaccion.equals("")||
               fecha_proceso_transaccion.equals("") || cod_forma_pago.equals("") || monto_transaccion.equals("")||
               NRO_CTA_BCO.equals("") || tasa_cambio_trans.equals("") || nro_chq_gerencia.equals("")||
               proc_comis_trans.equals("") || monto_comis_aplicada.equals("") || fecha_contabilidad.equals("")*//*) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                transaccion.txtT_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPTRAACT ('" + buscar + "',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_tipo_transaccion);
                ppt.setString(2, cod_Producto);
                //ppt.setString(3, tipo_por_fdte);
           //     ppt.setString(3, cod_moneda);
               // ppt.setString(4, clase_trans);
                ppt.setString(5, monto_transaccion);
                ppt.setString(6, NRO_CTA_BCO);
                //ppt.setString(8, indic_reverso);
                //ppt.setString(9, indic_custodio);
                ppt.setString(7, mto_moneda_extranjera_trans);
                ppt.setString(8, tasa_cambio_trans);
                //ppt.setString(12, tipo_oper);
                ppt.setString(9, nat_trans);
                //ppt.setString(14, nro_chq_preimpreso);
                //ppt.setString(15, nro_chq_gerencia);
                //ppt.setString(16, nro_comis_trans);
                ppt.setString(10, descrip_comis_trans);
                //ppt.setString(18, proc_comis_trans);
                ppt.setString(11, monto_comis_aplicada);
                ppt.setString(12, monto_impuesto_deito_banco);
                ppt.setString(13, indicador_exoneracion_impuesto_deito_banco);
                //ppt.setString(22, nro_precinto);
                //ppt.setString(14, indicador_situacion_operacion);
                //ppt.setString(24, cod_usuario_operativo);
                //ppt.setString(25, indicador_contable);
                //ppt.setString(26, indicador_autorizacion_operacion);
                //ppt.setString(27, numer_cheque_operacion);
                ppt.setString(14, cuenta_debito_transferencia);
                ppt.setString(15, cuenta_credito_transferencia);
                ppt.setString(16, ci_rif_beneficiario_transferencia);
                ppt.setString(17, codigo_operador_swift);
                //ppt.setString(32, siglas_origen_operacion);
                //ppt.setString(33, rol_fideicomitente);
                ppt.setString(18, cod_forma_pago);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                GlobalConstants.getAuditoria().setLog("U");
                transaccion.btnT_insertar.setEnabled(true);
                transaccion.btnT_anular.setEnabled(true);
                transaccion.btnT_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            System.out.println(ex);
            transaccion.btnT_insertar.setEnabled(true);
            transaccion.btnT_anular.setEnabled(true);
            transaccion.btnT_consultar.setEnabled(true);
        }
        transaccion.btnT_actualizar.setEnabled(false);
        Limpiar_Tab_Transaccion();
        Bloquear_Tab_Transaccion();*/
    }

    public void Tabla_Transaccion_Anular() throws SQLException {
        /*String buscar = transaccion.txtT_ci_rif.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPTRAANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            System.out.println("Error NO eliminado " + ex);
            JOptionPane.showMessageDialog(null, "Usuario no existe");
        }*/
    }

    /**
     * ************************ TABLA PLAN CONTABLE 013 **************************
     */
    public void Bloquear_Tab_Plan_Contable() {
        plan_contable.txtPC_Referencia.setEditable(false);
        plan_contable.txtPC_cod_cuenta_contable.setEditable(false);
        plan_contable.txtPC_des_cuenta_contable.setEditable(false);
        plan_contable.txtPC_tipo_cuenta_contable.setEditable(false);
    }

    public void Desbloquear_Tab_Plan_Contable() {
        plan_contable.txtPC_Referencia.setEditable(true);
        plan_contable.txtPC_cod_cuenta_contable.setEditable(true);
        plan_contable.txtPC_des_cuenta_contable.setEditable(true);
        plan_contable.txtPC_tipo_cuenta_contable.setEditable(true);
    }

    public void Limpiar_Tab_Plan_Contable() {
        plan_contable.txtPC_Referencia.setText("");
        plan_contable.txtPC_cod_cuenta_contable.setText("");
        plan_contable.txtPC_des_cuenta_contable.setText("");
        plan_contable.txtPC_tipo_cuenta_contable.setText("");
        plan_contable.txtPC_buscar.setText("");
    }

    public void Tabla_Plan_Contable_Insertar() throws SQLException {
        this.exitosa = true;
        String cod_cuenta_contable = plan_contable.txtPC_cod_cuenta_contable.getText();
        String des_cuenta_contable = plan_contable.txtPC_des_cuenta_contable.getText();
        String tipo_cuenta_contable = plan_contable.txtPC_tipo_cuenta_contable.getText();
        String referencia = plan_contable.txtPC_Referencia.getText();
        try {
            if (cod_cuenta_contable.equals("") || des_cuenta_contable.equals("")
                    || tipo_cuenta_contable.equals("") || referencia.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                plan_contable.txtPC_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPLCINS (?,?,?,?)}");
                ppt.setString(1, cod_cuenta_contable);
                ppt.setString(2, des_cuenta_contable);
                ppt.setString(3, tipo_cuenta_contable);
                ppt.setString(4, referencia);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                GlobalConstants.getAuditoria().setLog("U");
                plan_contable.txtPC_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
            this.exitosa = false;
            plan_contable.txtPC_buscar.setEnabled(true);

        }
    }

    public boolean Tabla_Plan_Contable_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = plan_contable.txtPC_buscar.getText();
        DefaultTableModel modo = new DefaultTableModel();
        modo.addColumn("Código de Cuenta Contable");
        modo.addColumn("Descripción Cuenta Contable");
        modo.addColumn("Tipo de Cuenta Contable ");
        modo.addColumn("Referencia");
        FSOSEGTABFRM013.Tabla_plan_contable.setModel(modo);
        String datos[] = new String[4];
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPLCCON ('" + buscar + "')}");
            ResultSet rs = ppt.executeQuery();
            if (rs.next()) {
                datos[0] = rs.getString(1);
                datos[1] = rs.getString(2);
                datos[2] = rs.getString(3);
                datos[3] = rs.getString(4);
                modo.addRow(datos);
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exito = false;
                Limpiar_Tab_Plan_Contable();
                Tabla_Plan_Contable_Mostrar_Modo();
            }
        } catch (SQLException ex) {
            exito = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
        Bloquear_Tab_Plan_Contable();
        return exito;
    }

    public void Tabla_Plan_Contable_Mostrar_Modo() throws SQLException {
        DefaultTableModel modo = new DefaultTableModel();
        modo.addColumn("Código de Cuenta Contable");
        modo.addColumn("Descripción Cuenta Contable");
        modo.addColumn("Tipo de Cuenta Contable ");
        modo.addColumn("Referencia");
        FSOSEGTABFRM013.Tabla_plan_contable.setModel(modo);
        String datos[] = new String[4];
        CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPLCCON2 ()}");
        ResultSet rs = ppt.executeQuery();
        while (rs.next()) {
            datos[0] = rs.getString(1);
            datos[1] = rs.getString(2);
            datos[2] = rs.getString(3);
            datos[3] = rs.getString(4);
            modo.addRow(datos);
        }
    }

    public void Tabla_Plan_Contable_Modificar() throws SQLException {
        DefaultTableModel modo = new DefaultTableModel();
        FSOSEGTABFRM013.Tabla_plan_contable.setModel(modo);
        plan_contable.btnPC_actualizar.setEnabled(true);
        this.exitosa = true;
        String buscar = plan_contable.txtPC_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPLCCON ('" + buscar + "')}");
            ResultSet usuario = ppt.executeQuery();
            if (usuario.next()) {
                Desbloquear_Tab_Plan_Contable();
                plan_contable.btnPC_insertar.setEnabled(false);
                plan_contable.btnPC_anular.setEnabled(false);
                plan_contable.btnPC_consultar.setEnabled(false);
                plan_contable.txtPC_cod_cuenta_contable.setText(usuario.getString(1));
                plan_contable.txtPC_des_cuenta_contable.setText(usuario.getString(2));
                plan_contable.txtPC_tipo_cuenta_contable.setText(usuario.getString(3));
                plan_contable.txtPC_Referencia.setText(usuario.getString(4));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el usuario");
                plan_contable.txtPC_buscar.setText("");
                Bloquear_Tab_Plan_Contable();
                plan_contable.btnPC_actualizar.setEnabled(false);
                plan_contable.btnPC_insertar.setEnabled(true);
                plan_contable.btnPC_anular.setEnabled(true);
                plan_contable.btnPC_consultar.setEnabled(true);
                this.exitosa = false;
                Limpiar_Tab_Plan_Contable();
                Tabla_Plan_Contable_Mostrar_Modo();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
            this.exitosa = false;
        }
    }

    public void Tabla_Plan_Contable_Actualizar() throws SQLException {
        this.exitosa = true;
        String buscar = plan_contable.txtPC_buscar.getText();
        String cod_cuenta_contable = plan_contable.txtPC_cod_cuenta_contable.getText();
        String des_cuenta_contable = plan_contable.txtPC_des_cuenta_contable.getText();
        String tipo_cuenta_contable = plan_contable.txtPC_tipo_cuenta_contable.getText();
        String referencia = plan_contable.txtPC_Referencia.getText();
        try {
            if (cod_cuenta_contable.equals("") || des_cuenta_contable.equals("") || tipo_cuenta_contable.equals("")
                    || referencia.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                plan_contable.txtPC_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPLCACT ('" + buscar + "',?,?,?,?)}");
                ppt.setString(1, cod_cuenta_contable);
                ppt.setString(2, des_cuenta_contable);
                ppt.setString(3, tipo_cuenta_contable);
                ppt.setString(4, referencia);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                GlobalConstants.getAuditoria().setLog("U");
                plan_contable.btnPC_insertar.setEnabled(true);
                plan_contable.btnPC_anular.setEnabled(true);
                plan_contable.btnPC_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            this.exitosa = false;
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            plan_contable.btnPC_insertar.setEnabled(true);
            plan_contable.btnPC_anular.setEnabled(true);
            plan_contable.btnPC_consultar.setEnabled(true);
        }
        plan_contable.btnPC_actualizar.setEnabled(false);
        Limpiar_Tab_Plan_Contable();
        Bloquear_Tab_Plan_Contable();
    }

    public void Tabla_Plan_Contable_Anular() throws SQLException {
        String buscar = plan_contable.txtPC_buscar.getText();
        this.exitosa = true;
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPPLCANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Usuario no existe");
            this.exitosa = false;
        }
    }

    /**
     * ***************************TABLA USUARIO 017 ******************************
     */
    public void Bloquear_Tab_Usuario() {
        usuario.txtUS_Cod_Usuario.setEditable(false);
        usuario.txtUS_Cod_Perfil.setEditable(false);
        usuario.txtUS_Nom_Usuario.setEditable(false);
        usuario.txtUS_codigo_unidad.setEditable(false);
        usuario.txtUS_Telefono_Usuario.setEditable(false);
        usuario.txtUS_Cod_Status.setEnabled(false);
        usuario.txtUS_Login.setEditable(false);
        usuario.txtUS_Password.setEditable(false);
        usuario.txtUS_Días_Vigencia_Clave.setEditable(false);
        usuario.txtUS_Email.setEditable(false);
        usuario.txtUS_Fecha_Inicio.setEnabled(false);
        usuario.txtUS_Fecha_Fin.setEnabled(false);
        usuario.txtUS_Fecha_Último_Ingreso.setEnabled(false);
        ((JTextFieldDateEditor) usuario.txtUS_Fecha_Inicio.getDateEditor()).setDisabledTextColor(Color.BLACK);
        ((JTextFieldDateEditor) usuario.txtUS_Fecha_Fin.getDateEditor()).setDisabledTextColor(Color.BLACK);
        ((JTextFieldDateEditor) usuario.txtUS_Fecha_Último_Ingreso.getDateEditor()).setDisabledTextColor(Color.BLACK);
        UIManager.put("ComboBox.disabledForeground", Color.BLACK);
    }

    public void Desbloquear_Tab_Usuario() {
        usuario.txtUS_Cod_Usuario.setEditable(true);
        usuario.txtUS_Cod_Perfil.setEditable(true);
        usuario.txtUS_Nom_Usuario.setEditable(true);
        usuario.txtUS_codigo_unidad.setEditable(true);
        usuario.txtUS_Telefono_Usuario.setEditable(true);
        usuario.txtUS_Cod_Status.setEnabled(true);
        usuario.txtUS_Login.setEditable(true);
        usuario.txtUS_Password.setEditable(true);
        usuario.txtUS_Días_Vigencia_Clave.setEditable(true);
        usuario.txtUS_Email.setEditable(true);
        usuario.txtUS_Cod_Status.setBackground(Color.WHITE);
    }

    public void Limpiar_Tab_Usuario() {
        usuario.txtUS_Cod_Usuario.setText("");
        usuario.txtUS_Cod_Perfil.setText("");
        usuario.txtUS_Nom_Usuario.setText("");
        usuario.txtUS_codigo_unidad.setText("");
        usuario.txtUS_Telefono_Usuario.setText("");
        usuario.txtUS_Cod_Status.setSelectedItem(null);
        usuario.txtUS_Login.setText("");
        usuario.txtUS_Password.setText("");
        usuario.txtUS_Días_Vigencia_Clave.setText("");
        usuario.txtUS_Email.setText("");
        usuario.txtUS_Fecha_Inicio.setDate(null);
        usuario.txtUS_Fecha_Fin.setDate(null);
        usuario.txtUS_Fecha_Último_Ingreso.setDate(null);
        usuario.txtUS_buscar.setText("");
    }

    public void Tabla_Usuario_Insertar() throws SQLException {
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        this.exitosa = true;
        String cod_usuario = usuario.txtUS_Cod_Usuario.getText();
        String cod_perfil = usuario.txtUS_Cod_Perfil.getText();
        String nom_usuario = usuario.txtUS_Nom_Usuario.getText();
        String cod_unidad = usuario.txtUS_codigo_unidad.getText();
        String telefono_usuario = usuario.txtUS_Telefono_Usuario.getText();
        String login = usuario.txtUS_Login.getText();
        String password = usuario.txtUS_Password.getText();
        String dias_vigencia_clave = usuario.txtUS_Días_Vigencia_Clave.getText();
        String email = usuario.txtUS_Email.getText();
        String cod_status = (String) usuario.txtUS_Cod_Status.getSelectedItem();
        try {
            if (cod_usuario.equals("") || cod_perfil.equals("") || nom_usuario.equals("")
                    || cod_unidad.equals("") || telefono_usuario.equals("") || login.equals("")
                    || password.equals("") || dias_vigencia_clave.equals("") || email.equals("")
                    || cod_status.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                usuario.txtUS_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPUSUINS (?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_usuario);
                ppt.setString(2, cod_perfil);
                ppt.setString(3, nom_usuario);
                ppt.setString(4, cod_unidad);
                ppt.setString(5, telefono_usuario);
                ppt.setString(6, login);
                ppt.setString(7, password);
                ppt.setString(8, dias_vigencia_clave);
                ppt.setString(9, email);
                ppt.setString(10, cod_status.substring(0, 1));
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                usuario.txtUS_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
            this.exitosa = false;
            usuario.txtUS_buscar.setEnabled(true);
        }
    }

    public boolean Tabla_Usuario_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = usuario.txtUS_buscar.getText();
        usuario.txtUS_buscar.setText("");
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPUSUCON ('" + buscar + "')}");
            ResultSet rs = ppt.executeQuery();
            if (rs.next()) {
                usuario.txtUS_Cod_Usuario.setText(rs.getString(1));
                usuario.txtUS_Cod_Perfil.setText(rs.getString(2));
                usuario.txtUS_Nom_Usuario.setText(rs.getString(3));
                usuario.txtUS_codigo_unidad.setText(rs.getString(4));
                usuario.txtUS_Telefono_Usuario.setText(rs.getString(5));
                usuario.txtUS_Login.setText(rs.getString(6));
                usuario.txtUS_Password.setText(rs.getString(7));
                usuario.txtUS_Días_Vigencia_Clave.setText(rs.getString(8));
                usuario.txtUS_Email.setText(rs.getString(9));
                usuario.txtUS_Fecha_Inicio.setDate(rs.getDate(10));
                usuario.txtUS_Fecha_Fin.setDate(rs.getDate(11));
                usuario.txtUS_Fecha_Último_Ingreso.setDate(rs.getDate(12));
                usuario.txtUS_Cod_Status.setSelectedItem((rs.getString(13).equals("A")) ? "Activo" : "Inactivo");
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exito = false;
                Limpiar_Tab_Usuario();
            }
        } catch (SQLException ex) {
            exito = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
        Bloquear_Tab_Usuario();
        return exito;
    }

    public void Tabla_Usuario_Modificar() throws SQLException {
        usuario.btnUS_actualizar.setEnabled(true);
        this.exitosa = true;
        String buscar = usuario.txtUS_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPUSUCON ('" + buscar + "')}");
            ResultSet rs = ppt.executeQuery();
            if (rs.next()) {
                Desbloquear_Tab_Usuario();
                usuario.btnUS_insertar.setEnabled(false);
                usuario.btnUS_anular.setEnabled(false);
                usuario.btnUS_consultar.setEnabled(false);
                usuario.txtUS_Cod_Usuario.setText(rs.getString(1));
                usuario.txtUS_Cod_Perfil.setText(rs.getString(2));
                usuario.txtUS_Nom_Usuario.setText(rs.getString(3));
                usuario.txtUS_codigo_unidad.setText(rs.getString(4));
                usuario.txtUS_Telefono_Usuario.setText(rs.getString(5));
                usuario.txtUS_Login.setText(rs.getString(6));
                usuario.txtUS_Password.setText(rs.getString(7));
                usuario.txtUS_Días_Vigencia_Clave.setText(rs.getString(8));
                usuario.txtUS_Email.setText(rs.getString(9));
                usuario.txtUS_Fecha_Inicio.setDate(rs.getDate(10));
                usuario.txtUS_Fecha_Fin.setDate(rs.getDate(11));
                usuario.txtUS_Fecha_Último_Ingreso.setDate(rs.getDate(12));
                usuario.txtUS_Cod_Status.setSelectedItem((rs.getString(13).equals("A")) ? "Activo" : "Inactivo");
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                usuario.txtUS_buscar.setText("");
                Bloquear_Tab_Usuario();
                usuario.btnUS_actualizar.setEnabled(false);
                usuario.btnUS_insertar.setEnabled(true);
                usuario.btnUS_anular.setEnabled(true);
                usuario.btnUS_consultar.setEnabled(true);
                this.exitosa = false;
                Limpiar_Tab_Usuario();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
            this.exitosa = false;
        }
    }

    public void Tabla_Usuario_Actualizar() throws SQLException {
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        String buscar = usuario.txtUS_buscar.getText();
        String cod_usuario = usuario.txtUS_Cod_Usuario.getText();
        String cod_perfil = usuario.txtUS_Cod_Perfil.getText();
        String nom_usuario = usuario.txtUS_Nom_Usuario.getText();
        String cod_unidad = usuario.txtUS_codigo_unidad.getText();
        String telefono_usuario = usuario.txtUS_Telefono_Usuario.getText();
        String login = usuario.txtUS_Login.getText();
        String password = usuario.txtUS_Password.getText();
        String dias_vigencia_clave = usuario.txtUS_Días_Vigencia_Clave.getText();
        String email = usuario.txtUS_Email.getText();
        String cod_status = (String) usuario.txtUS_Cod_Status.getSelectedItem();
        try {
            if (cod_usuario.equals("") || cod_perfil.equals("") || nom_usuario.equals("")
                    || cod_unidad.equals("") || telefono_usuario.equals("") || login.equals("")
                    || password.equals("") || dias_vigencia_clave.equals("") || email.equals("")
                    || cod_status.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                usuario.txtUS_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPUSUACT ('" + buscar + "',?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_usuario);
                ppt.setString(2, cod_perfil);
                ppt.setString(3, nom_usuario);
                ppt.setString(4, cod_unidad);
                ppt.setString(5, telefono_usuario);
                ppt.setString(6, login);
                ppt.setString(7, password);
                ppt.setString(8, dias_vigencia_clave);
                ppt.setString(9, email);
                ppt.setString(10, cod_status.substring(0, 1));
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                usuario.btnUS_insertar.setEnabled(true);
                usuario.btnUS_anular.setEnabled(true);
                usuario.btnUS_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            this.exitosa = false;
            usuario.btnUS_insertar.setEnabled(true);
            usuario.btnUS_anular.setEnabled(true);
            usuario.btnUS_consultar.setEnabled(true);
        }
        usuario.btnUS_actualizar.setEnabled(false);
        Limpiar_Tab_Usuario();
        Bloquear_Tab_Usuario();
    }

    public void Tabla_Usuario_Anular() throws SQLException {
        String buscar = usuario.txtUS_Cod_Usuario.getText();
        this.exitosa = true;
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPUSUANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Usuario no existe");
            this.exitosa = false;
        }
    }

    /**
     * *************************** TABLA MONEDA 014 ******************************
     */
    public void Bloquear_Tab_Moneda() {
        moneda.txtM_cod_moneda.setEditable(false);
        moneda.txtM_descripcion_moneda.setEnabled(false);
        moneda.txtM_tasa.setEditable(false);
        UIManager.put("ComboBox.disabledForeground", Color.BLACK);
    }

    public void Desbloquear_Tab_Moneda() {
        moneda.txtM_cod_moneda.setEditable(true);
        moneda.txtM_descripcion_moneda.setEnabled(true);
        moneda.txtM_tasa.setEditable(true);
        moneda.txtM_descripcion_moneda.setBackground(Color.WHITE);
    }

    public void Limpiar_Tab_Moneda() {
        moneda.txtM_descripcion_moneda.setSelectedItem(null);
        moneda.txtM_tasa.setText("");
        moneda.txtM_cod_moneda.setText("");
        moneda.txtM_buscar.setText("");
    }

    public void Tabla_Moneda_Insertar() throws SQLException {
        this.exitosa = true;
        String cod_moneda = moneda.txtM_cod_moneda.getText();
        String descripcion_moneda = (String) moneda.txtM_descripcion_moneda.getSelectedItem();
        String tasa = moneda.txtM_tasa.getText();
        try {
            if (cod_moneda.equals("") || tasa.equals("") || descripcion_moneda.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                moneda.txtM_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPMONINS (?,?,?)}");
                ppt.setString(1, cod_moneda);
                ppt.setString(2, descripcion_moneda.substring(0, 1));
                ppt.setString(2, descripcion_moneda.substring(0, 2));
                ppt.setString(2, descripcion_moneda.substring(0, 3));
                ppt.setString(3, tasa);
                ppt.executeUpdate();
                GlobalConstants.getAuditoria().setLog("I");
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                moneda.txtM_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
            this.exitosa = false;
            moneda.txtM_buscar.setEnabled(true);
        }
    }

    public boolean Tabla_Moneda_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = moneda.txtM_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPMONCON ('" + buscar + "')}");
            ResultSet rs = ppt.executeQuery();
            if (rs.next()) {
                moneda.txtM_cod_moneda.setText(rs.getString(1));
                moneda.txtM_descripcion_moneda.setSelectedItem((rs.getString(2).equals("USD")) ? "USD - Dólar Americano" : "VEF - Bolívar Fuerte");
                moneda.txtM_tasa.setText(rs.getString(3));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exito = false;
                Limpiar_Tab_Moneda();
            }
        } catch (SQLException ex) {
            exito = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
        Bloquear_Tab_Moneda();
        return exito;
    }

    public void Tabla_Moneda_Modificar() throws SQLException {
        moneda.btnM_actualizar.setEnabled(true);
        this.exitosa = true;
        String buscar = moneda.txtM_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPMONCON ('" + buscar + "')}");
            ResultSet rs = ppt.executeQuery();
            if (rs.next()) {
                moneda.txtM_tasa.setEditable(true);
                moneda.txtM_cod_moneda.setEditable(true);
                moneda.btnM_insertar.setEnabled(false);
                moneda.btnM_anular.setEnabled(false);
                moneda.btnM_consultar.setEnabled(false);
                moneda.txtM_cod_moneda.setText(rs.getString(1));
                moneda.txtM_descripcion_moneda.setSelectedItem((rs.getString(2).equals("USD")) ? "USD - Dólar Americano" : "VEF - Bolívar Fuerte");
                moneda.txtM_tasa.setText(rs.getString(3));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el usuario");
                moneda.txtM_buscar.setText("");
                Bloquear_Tab_Moneda();
                moneda.btnM_actualizar.setEnabled(false);
                moneda.btnM_insertar.setEnabled(true);
                moneda.btnM_anular.setEnabled(true);
                moneda.btnM_consultar.setEnabled(true);
                this.exitosa = false;
                Limpiar_Tab_Moneda();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
            this.exitosa = false;
        }
    }

    public void Tabla_Moneda_Actualizar() throws SQLException {
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        String buscar = moneda.txtM_buscar.getText();
        String cod_moneda = moneda.txtM_cod_moneda.getText();
        String descripcion_moneda = (String) moneda.txtM_descripcion_moneda.getSelectedItem();
        String tasa = moneda.txtM_tasa.getText();
        try {
            if (cod_moneda.equals("") || descripcion_moneda.equals("") || tasa.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                moneda.txtM_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPMONACT ('" + buscar + "',?,?)}");
                ppt.setString(1, cod_moneda);
                ppt.setString(2, tasa);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                GlobalConstants.getAuditoria().setLog("U");
                moneda.btnM_insertar.setEnabled(true);
                moneda.btnM_anular.setEnabled(true);
                moneda.btnM_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            this.exitosa = false;
            moneda.btnM_insertar.setEnabled(true);
            moneda.btnM_anular.setEnabled(true);
            moneda.btnM_consultar.setEnabled(true);
        }
        moneda.btnM_actualizar.setEnabled(false);
        Limpiar_Tab_Moneda();
        Bloquear_Tab_Moneda();
    }

    public void Tabla_Moneda_Anular() throws SQLException {
        String buscar = moneda.txtM_buscar.getText();
        this.exitosa = true;
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPMONANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Moneda no existe");
            this.exitosa = false;
        }
    }

    public void Bloquear_Tab_dia_feriado() {
        dia_feriado.txtDF_fecha_feriado.setEnabled(false);
        dia_feriado.txtDF_des_feriado.setEditable(false);
        dia_feriado.txtDF_dia_feriado.setEnabled(false);
        dia_feriado.txtDF_mes_feriado.setEnabled(false);
        dia_feriado.txtDF_año.setEditable(false);
        ((JTextFieldDateEditor) dia_feriado.txtDF_fecha_feriado.getDateEditor()).setDisabledTextColor(Color.BLACK);
        UIManager.put("ComboBox.disabledForeground", Color.BLACK);
    }

    public void Desbloquear_Tab_dia_feriado() {
        dia_feriado.txtDF_fecha_feriado.setEnabled(true);
        dia_feriado.txtDF_des_feriado.setEditable(true);
        dia_feriado.txtDF_dia_feriado.setEnabled(true);
        dia_feriado.txtDF_mes_feriado.setEnabled(true);
        dia_feriado.txtDF_dia_feriado.setBackground(Color.WHITE);
        dia_feriado.txtDF_mes_feriado.setBackground(Color.WHITE);
    }
    
    

    public void Limpiar_Tab_dia_feriado() {
        dia_feriado.txtDF_fecha_feriado.setDate(null);
        dia_feriado.txtDF_des_feriado.setText("");
        dia_feriado.txtDF_dia_feriado.setSelectedItem(null);
        dia_feriado.txtDF_mes_feriado.setSelectedItem(null);
        dia_feriado.txtDF_año.setText("");
    }
    
    
    public void Tabla_dia_feriado_Insertar() throws SQLException {
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        this.exitosa = true;
        String fecha_feriado = (dia_feriado.txtDF_fecha_feriado.getDate() != null) ? date.format(dia_feriado.txtDF_fecha_feriado.getDate()) : "";
        String des_feriado = dia_feriado.txtDF_des_feriado.getText();
        String dia_feriado = (String) this.dia_feriado.txtDF_dia_feriado.getSelectedItem();
        String mes_feriado = (String) this.dia_feriado.txtDF_mes_feriado.getSelectedItem();
        try {
            if (fecha_feriado.equals("") || des_feriado.equals("") || dia_feriado.equals("") || mes_feriado.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                this.dia_feriado.txtDF_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPDIFINS (?,?,?,?)}");
                ppt.setString(1, fecha_feriado);
                ppt.setString(2, des_feriado);
                ppt.setString(3, dia_feriado);
                ppt.setString(4, mes_feriado);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                GlobalConstants.getAuditoria().setLog("I");
                this.dia_feriado.txtDF_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
            this.exitosa = false;
            this.dia_feriado.txtDF_buscar.setEnabled(true);
        }
    }

    public boolean Tabla_dia_feriado_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = dia_feriado.txtDF_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPDIFCON ('" + buscar + "')}");
            ResultSet rs = ppt.executeQuery();
            if (rs.next()) {
                dia_feriado.txtDF_fecha_feriado.setDate(rs.getDate(1));
                dia_feriado.txtDF_des_feriado.setText(rs.getString(2));
                dia_feriado.txtDF_dia_feriado.setSelectedItem(rs.getString(3));
                dia_feriado.txtDF_mes_feriado.setSelectedItem(rs.getString(4));
                dia_feriado.txtDF_año.setText(rs.getString(5).substring(0, 4));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exitosa = false;
                Limpiar_Tab_dia_feriado();
            }
        } catch (SQLException ex) {
            exito = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
        Bloquear_Tab_dia_feriado();
        return exito;
    }

    public void Tabla_dia_feriado_Modificar() throws SQLException {
        dia_feriado.btnDF_actualizar.setEnabled(true);
        this.exitosa = true;
        String buscar = dia_feriado.txtDF_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPDIFCON ('" + buscar + "')}");
            ResultSet rs = ppt.executeQuery();
            if (rs.next()) {
                Desbloquear_Tab_dia_feriado();
                dia_feriado.btnDF_insertar.setEnabled(false);
                dia_feriado.btnDF_anular.setEnabled(false);
                dia_feriado.btnDF_consultar.setEnabled(false);
                dia_feriado.txtDF_fecha_feriado.setDate(rs.getDate(1));
                dia_feriado.txtDF_des_feriado.setText(rs.getString(2));
                dia_feriado.txtDF_dia_feriado.setSelectedItem(rs.getString(3));
                dia_feriado.txtDF_mes_feriado.setSelectedItem(rs.getString(4));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el usuario");
                dia_feriado.txtDF_buscar.setText("");
                Bloquear_Tab_dia_feriado();
                dia_feriado.btnDF_actualizar.setEnabled(false);
                dia_feriado.btnDF_insertar.setEnabled(true);
                dia_feriado.btnDF_anular.setEnabled(true);
                dia_feriado.btnDF_consultar.setEnabled(true);
                this.exitosa = false;
                Limpiar_Tab_dia_feriado();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
            this.exitosa = false;
        }
    }

    public void Tabla_dia_feriado_Actualizar() throws SQLException {
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        this.exitosa = true;
        String buscar = dia_feriado.txtDF_buscar.getText();
        String fecha_feriado = (dia_feriado.txtDF_fecha_feriado.getDate() != null) ? date.format(dia_feriado.txtDF_fecha_feriado.getDate()) : "";
        String des_feriado = dia_feriado.txtDF_des_feriado.getText();
        String dia_feriado = (String) this.dia_feriado.txtDF_dia_feriado.getSelectedItem();
        String mes_feriado = (String) this.dia_feriado.txtDF_mes_feriado.getSelectedItem();
        try {
            if (fecha_feriado.equals("") || des_feriado.equals("") || dia_feriado.equals("") || mes_feriado.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                this.dia_feriado.txtDF_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPDIFACT ('" + buscar + "',?,?,?,?)}");
                ppt.setString(1, fecha_feriado);
                ppt.setString(2, des_feriado);
                ppt.setString(3, dia_feriado);
                ppt.setString(4, mes_feriado);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                GlobalConstants.getAuditoria().setLog("U");
                this.dia_feriado.btnDF_insertar.setEnabled(true);
                this.dia_feriado.btnDF_anular.setEnabled(true);
                this.dia_feriado.btnDF_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            this.exitosa = false;
            this.dia_feriado.btnDF_insertar.setEnabled(true);
            this.dia_feriado.btnDF_anular.setEnabled(true);
            this.dia_feriado.btnDF_consultar.setEnabled(true);
        }
        this.dia_feriado.btnDF_actualizar.setEnabled(false);
        Limpiar_Tab_dia_feriado();
        Bloquear_Tab_dia_feriado();
    }

    public void Tabla_dia_feriado_Anular() throws SQLException {
        String buscar = dia_feriado.txtDF_buscar.getText();
        this.exitosa = true;
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPDIFANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Usuario no existe");
            this.exitosa = false;
        }
    }

    /**
     * ***************************TABLA PERSONA 015 ******************************
     */
    public void Bloquear_Tab_Localizacion() {
        localizacion.txtl_cod_tipo_localizacion.setEditable(false);
        localizacion.txtl_cod_Persona.setEditable(false);
        localizacion.txtl_ci_rif.setEditable(false);
        localizacion.txtl_fecha_proceso.setEnabled(false);
        localizacion.txtl_fecha_actualizacion.setEnabled(false);
        localizacion.txtl_Descripcion_dir1.setEditable(false);
        localizacion.txtl_Descripcion_dir2.setEditable(false);
        localizacion.txtl_Descripcion_dir3.setEditable(false);
        localizacion.txtl_Descripcion_dir4.setEditable(false);
        localizacion.txtl_cod_pais.setEditable(false);
        localizacion.txtl_cod_ciudad.setEditable(false);
        localizacion.txtl_cod_estado.setEditable(false);
        localizacion.txtl_zona_postal.setEditable(false);
        localizacion.txtl_telefonos.setEditable(false);
        localizacion.txtl_correo.setEditable(false);
        localizacion.txtl_Twiter.setEditable(false);
        localizacion.txtl_celular.setEditable(false);
        localizacion.txtl_instagram.setEditable(false);
        localizacion.txtl_facebook.setEditable(false);
        localizacion.txtl_paginaweb.setEditable(false);
        localizacion.txtl_coordenadas.setEditable(false);
        localizacion.txtl_cod_localizacion.setEditable(false);
        ((JTextFieldDateEditor) localizacion.txtl_fecha_proceso.getDateEditor()).setDisabledTextColor(Color.BLACK);
        ((JTextFieldDateEditor) localizacion.txtl_fecha_actualizacion.getDateEditor()).setDisabledTextColor(Color.BLACK);
        UIManager.put("ComboBox.disabledForeground", Color.BLACK);
    }

    public void Desbloquear_Tab_Localizacion() {
        localizacion.txtl_cod_tipo_localizacion.setEditable(true);
        localizacion.txtl_cod_Persona.setEditable(true);
        localizacion.txtl_ci_rif.setEditable(true);
        localizacion.txtl_Descripcion_dir1.setEditable(true);
        localizacion.txtl_Descripcion_dir2.setEditable(true);
        localizacion.txtl_Descripcion_dir3.setEditable(true);
        localizacion.txtl_Descripcion_dir4.setEditable(true);
        localizacion.txtl_cod_pais.setEditable(true);
        localizacion.txtl_cod_ciudad.setEditable(true);
        localizacion.txtl_cod_estado.setEditable(true);
        localizacion.txtl_zona_postal.setEditable(true);
        localizacion.txtl_telefonos.setEditable(true);
        localizacion.txtl_correo.setEditable(true);
        localizacion.txtl_Twiter.setEditable(true);
        localizacion.txtl_celular.setEditable(true);
        localizacion.txtl_instagram.setEditable(true);
        localizacion.txtl_facebook.setEditable(true);
        localizacion.txtl_paginaweb.setEditable(true);
        localizacion.txtl_coordenadas.setEditable(true);
        localizacion.txtl_cod_localizacion.setEditable(true);
    }

    public void Limpiar_Tab_Localizacion() {
        localizacion.txtl_cod_tipo_localizacion.setText("");
        localizacion.txtl_cod_Persona.setText("");
        localizacion.txtl_ci_rif.setText("");
        localizacion.txtl_fecha_proceso.setDate(null);
        localizacion.txtl_fecha_actualizacion.setDate(null);
        localizacion.txtl_Descripcion_dir1.setText("");
        localizacion.txtl_Descripcion_dir2.setText("");
        localizacion.txtl_Descripcion_dir3.setText("");
        localizacion.txtl_Descripcion_dir4.setText("");
        localizacion.txtl_cod_pais.setText("");
        localizacion.txtl_cod_ciudad.setText("");
        localizacion.txtl_cod_estado.setText("");
        localizacion.txtl_zona_postal.setText("");
        localizacion.txtl_telefonos.setText("");
        localizacion.txtl_correo.setText("");
        localizacion.txtl_Twiter.setText("");
        localizacion.txtl_celular.setText("");
        localizacion.txtl_instagram.setText("");
        localizacion.txtl_facebook.setText("");
        localizacion.txtl_paginaweb.setText("");
        localizacion.txtl_coordenadas.setText("");
        localizacion.txtl_cod_localizacion.setText("");
        localizacion.txtl_buscar.setText("");
    }

    public void Tabla_Localizacion_Insertar() throws SQLException {
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        this.exitosa = true;
        String cod_tipo_localizacion = localizacion.txtl_cod_tipo_localizacion.getText();
        String cod_Persona = localizacion.txtl_cod_Persona.getText();
        String ci_rif = localizacion.txtl_ci_rif.getText();
        String Descripcion_dir1 = localizacion.txtl_Descripcion_dir1.getText();
        String Descripcion_dir2 = localizacion.txtl_Descripcion_dir2.getText();
        String Descripcion_dir3 = localizacion.txtl_Descripcion_dir3.getText();
        String Descripcion_dir4 = localizacion.txtl_Descripcion_dir4.getText();
        String cod_pais = localizacion.txtl_cod_pais.getText();
        String cod_ciudad = localizacion.txtl_cod_ciudad.getText();
        String cod_estado = localizacion.txtl_cod_estado.getText();
        String zona_postal = localizacion.txtl_zona_postal.getText();
        String telefonos = localizacion.txtl_telefonos.getText();
        String correo = localizacion.txtl_correo.getText();
        String Twiter = localizacion.txtl_Twiter.getText();
        String celular = localizacion.txtl_celular.getText();
        String histagram = localizacion.txtl_instagram.getText();
        String facebook = localizacion.txtl_facebook.getText();
        String paginaweb = localizacion.txtl_paginaweb.getText();
        String coordenadas = localizacion.txtl_coordenadas.getText();
        String cod_localizacion = localizacion.txtl_cod_localizacion.getText();
        try {
            if (cod_tipo_localizacion.equals("") || cod_Persona.equals("") || ci_rif.equals("")
                    || Descripcion_dir1.equals("") || Descripcion_dir2.equals("") || Descripcion_dir3.equals("")
                    || Descripcion_dir4.equals("") || cod_pais.equals("") || cod_ciudad.equals("")
                    || cod_estado.equals("") || zona_postal.equals("") || telefonos.equals("")
                    || correo.equals("") || Twiter.equals("") || celular.equals("")
                    || histagram.equals("") || facebook.equals("") || paginaweb.equals("")
                    || coordenadas.equals("") || cod_localizacion.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                localizacion.txtl_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPLOCINS (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_tipo_localizacion);
                ppt.setString(2, cod_Persona);
                ppt.setString(3, ci_rif);
                ppt.setString(4, Descripcion_dir1);
                ppt.setString(5, Descripcion_dir2);
                ppt.setString(6, Descripcion_dir3);
                ppt.setString(7, Descripcion_dir4);
                ppt.setString(8, cod_pais);
                ppt.setString(9, cod_ciudad);
                ppt.setString(10, cod_estado);
                ppt.setString(11, zona_postal);
                ppt.setString(12, telefonos);
                ppt.setString(13, correo);
                ppt.setString(14, Twiter);
                ppt.setString(15, celular);
                ppt.setString(16, histagram);
                ppt.setString(17, facebook);
                ppt.setString(18, paginaweb);
                ppt.setString(19, coordenadas);
                ppt.setString(20, cod_localizacion);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                GlobalConstants.getAuditoria().setLog("I");
                localizacion.txtl_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
            this.exitosa = false;
            localizacion.txtl_buscar.setEnabled(true);
        }
    }

    public boolean Tabla_Localizacion_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = localizacion.txtl_buscar.getText();
        localizacion.txtl_buscar.setText("");
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPLOCCON ('" + buscar + "')}");
            ResultSet rs = ppt.executeQuery();
            if (rs.next()) {
                localizacion.txtl_cod_tipo_localizacion.setText(rs.getString(1));
                localizacion.txtl_cod_Persona.setText(rs.getString(2));
                localizacion.txtl_ci_rif.setText(rs.getString(3));
                localizacion.txtl_fecha_proceso.setDate(rs.getDate(4));
                localizacion.txtl_fecha_actualizacion.setDate(rs.getDate(5));
                localizacion.txtl_Descripcion_dir1.setText(rs.getString(6));
                localizacion.txtl_Descripcion_dir2.setText(rs.getString(7));
                localizacion.txtl_Descripcion_dir3.setText(rs.getString(8));
                localizacion.txtl_Descripcion_dir4.setText(rs.getString(9));
                localizacion.txtl_cod_pais.setText(rs.getString(10));
                localizacion.txtl_cod_ciudad.setText(rs.getString(11));
                localizacion.txtl_cod_estado.setText(rs.getString(12));
                localizacion.txtl_zona_postal.setText(rs.getString(13));
                localizacion.txtl_telefonos.setText(rs.getString(14));
                localizacion.txtl_correo.setText(rs.getString(15));
                localizacion.txtl_Twiter.setText(rs.getString(16));
                localizacion.txtl_celular.setText(rs.getString(17));
                localizacion.txtl_instagram.setText(rs.getString(18));
                localizacion.txtl_facebook.setText(rs.getString(19));
                localizacion.txtl_paginaweb.setText(rs.getString(20));
                localizacion.txtl_coordenadas.setText(rs.getString(21));
                localizacion.txtl_cod_localizacion.setText(rs.getString(22));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exitosa = false;
                Limpiar_Tab_Localizacion();
            }
        } catch (SQLException ex) {
            exito = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
        Bloquear_Tab_Localizacion();
        return exito;
    }

    public void Tabla_Localizacion_Modificar() throws SQLException {
        localizacion.btnl_actualizar.setEnabled(true);
        this.exitosa = true;
        String buscar = localizacion.txtl_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPLOCCON ('" + buscar + "')}");
            ResultSet rs = ppt.executeQuery();
            if (rs.next()) {
                Desbloquear_Tab_Localizacion();
                localizacion.btnl_insertar.setEnabled(false);
                localizacion.btnl_anular.setEnabled(false);
                localizacion.btnl_consultar.setEnabled(false);
                localizacion.txtl_cod_tipo_localizacion.setText(rs.getString(1));
                localizacion.txtl_cod_Persona.setText(rs.getString(2));
                localizacion.txtl_ci_rif.setText(rs.getString(3));
                localizacion.txtl_fecha_proceso.setDate(rs.getDate(4));
                localizacion.txtl_fecha_actualizacion.setDate(rs.getDate(5));
                localizacion.txtl_Descripcion_dir1.setText(rs.getString(6));
                localizacion.txtl_Descripcion_dir2.setText(rs.getString(7));
                localizacion.txtl_Descripcion_dir3.setText(rs.getString(8));
                localizacion.txtl_Descripcion_dir4.setText(rs.getString(9));
                localizacion.txtl_cod_pais.setText(rs.getString(10));
                localizacion.txtl_cod_ciudad.setText(rs.getString(11));
                localizacion.txtl_cod_estado.setText(rs.getString(12));
                localizacion.txtl_zona_postal.setText(rs.getString(13));
                localizacion.txtl_telefonos.setText(rs.getString(14));
                localizacion.txtl_correo.setText(rs.getString(15));
                localizacion.txtl_Twiter.setText(rs.getString(16));
                localizacion.txtl_celular.setText(rs.getString(17));
                localizacion.txtl_instagram.setText(rs.getString(18));
                localizacion.txtl_facebook.setText(rs.getString(19));
                localizacion.txtl_paginaweb.setText(rs.getString(20));
                localizacion.txtl_coordenadas.setText(rs.getString(21));
                localizacion.txtl_cod_localizacion.setText(rs.getString(22));
                localizacion.txtl_fecha_proceso.setEnabled(false);
                localizacion.txtl_fecha_actualizacion.setEnabled(false);
            } else {
                JOptionPane.showMessageDialog(null, "No existe el usuario");
                localizacion.txtl_buscar.setText("");
                Bloquear_Tab_Localizacion();
                localizacion.btnl_actualizar.setEnabled(false);
                localizacion.btnl_insertar.setEnabled(true);
                localizacion.btnl_anular.setEnabled(true);
                localizacion.btnl_consultar.setEnabled(true);
                this.exitosa = false;
                Limpiar_Tab_Localizacion();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
            this.exitosa = false;
        }
    }

    public void Tabla_Localizacion_Actualizar() throws SQLException {
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        String buscar = localizacion.txtl_buscar.getText();
        String cod_tipo_localizacion = localizacion.txtl_cod_tipo_localizacion.getText();
        String cod_Persona = localizacion.txtl_cod_Persona.getText();
        String ci_rif = localizacion.txtl_ci_rif.getText();
        String Descripcion_dir1 = localizacion.txtl_Descripcion_dir1.getText();
        String Descripcion_dir2 = localizacion.txtl_Descripcion_dir2.getText();
        String Descripcion_dir3 = localizacion.txtl_Descripcion_dir3.getText();
        String Descripcion_dir4 = localizacion.txtl_Descripcion_dir4.getText();
        String cod_pais = localizacion.txtl_cod_pais.getText();
        String cod_ciudad = localizacion.txtl_cod_ciudad.getText();
        String cod_estado = localizacion.txtl_cod_estado.getText();
        String zona_postal = localizacion.txtl_zona_postal.getText();
        String telefonos = localizacion.txtl_telefonos.getText();
        String correo = localizacion.txtl_correo.getText();
        String Twiter = localizacion.txtl_Twiter.getText();
        String celular = localizacion.txtl_celular.getText();
        String histagram = localizacion.txtl_instagram.getText();
        String facebook = localizacion.txtl_facebook.getText();
        String paginaweb = localizacion.txtl_paginaweb.getText();
        String coordenadas = localizacion.txtl_coordenadas.getText();
        String cod_localizacion = localizacion.txtl_cod_localizacion.getText();
        try {
            if (cod_tipo_localizacion.equals("") || cod_Persona.equals("") || ci_rif.equals("")
                    || Descripcion_dir1.equals("") || Descripcion_dir2.equals("") || Descripcion_dir3.equals("")
                    || Descripcion_dir4.equals("") || cod_pais.equals("") || cod_ciudad.equals("")
                    || cod_estado.equals("") || zona_postal.equals("") || telefonos.equals("")
                    || correo.equals("") || Twiter.equals("") || celular.equals("")
                    || histagram.equals("") || facebook.equals("") || paginaweb.equals("")
                    || coordenadas.equals("") || cod_localizacion.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                localizacion.txtl_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPLOCACT ('" + buscar + "',?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                ppt.setString(1, cod_tipo_localizacion);
                ppt.setString(2, cod_Persona);
                ppt.setString(3, ci_rif);
                ppt.setString(4, Descripcion_dir1);
                ppt.setString(5, Descripcion_dir2);
                ppt.setString(6, Descripcion_dir3);
                ppt.setString(7, Descripcion_dir4);
                ppt.setString(8, cod_pais);
                ppt.setString(9, cod_ciudad);
                ppt.setString(10, cod_estado);
                ppt.setString(11, zona_postal);
                ppt.setString(12, telefonos);
                ppt.setString(13, correo);
                ppt.setString(14, Twiter);
                ppt.setString(15, celular);
                ppt.setString(16, histagram);
                ppt.setString(17, facebook);
                ppt.setString(18, paginaweb);
                ppt.setString(19, coordenadas);
                ppt.setString(20, cod_localizacion);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                localizacion.btnl_insertar.setEnabled(true);
                localizacion.btnl_anular.setEnabled(true);
                localizacion.btnl_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            this.exitosa = false;
            localizacion.btnl_insertar.setEnabled(true);
            localizacion.btnl_anular.setEnabled(true);
            localizacion.btnl_consultar.setEnabled(true);
        }
        localizacion.btnl_actualizar.setEnabled(false);
        Limpiar_Tab_Localizacion();
        Bloquear_Tab_Localizacion();
    }

    public void Tabla_Localizacion_Anular() throws SQLException {
        String buscar = localizacion.txtl_ci_rif.getText();
        this.exitosa = true;
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPLOCANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Usuario no existe");
            this.exitosa = false;
        }
    }

    /**
     * ************************ TABLA PLAN CONTABLE 009 **************************
     */
    public void Bloquear_Tab_Calculo() {
        calculo.txtCC_codigo.setEditable(false);
        calculo.txtCC_descripcion.setEditable(false);
        calculo.txtCC_formula.setEditable(false);
    }

    public void Desbloquear_Tab_Calculo() {
        calculo.txtCC_codigo.setEditable(true);
        calculo.txtCC_descripcion.setEditable(true);
        calculo.txtCC_formula.setEditable(true);
    }

    public void Limpiar_Tab_Calculo() {
        calculo.txtCC_codigo.setText("");
        calculo.txtCC_descripcion.setText("");
        calculo.txtCC_formula.setText("");
        calculo.txtCC_buscar.setText("");
    }

    public void Tabla_Calculo_Insertar() throws SQLException {
        this.exitosa = true;
        String codigo = calculo.txtCC_codigo.getText();
        String descripcion = calculo.txtCC_descripcion.getText();
        String formula = calculo.txtCC_formula.getText();
        try {
            if (codigo.equals("") || descripcion.equals("") || formula.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                calculo.txtCC_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPCLCINS (?,?,?)}");
                ppt.setString(1, codigo);
                ppt.setString(2, descripcion);
                ppt.setString(3, formula);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Inserción Exitosa");
                calculo.txtCC_buscar.setEnabled(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Insercion NO Exitosa " + ex);
            this.exitosa = false;
            calculo.txtCC_buscar.setEnabled(true);
        }
    }

    public boolean Tabla_Calculo_Mostrar() throws SQLException {
        boolean exito = true;
        String buscar = calculo.txtCC_buscar.getText();
        DefaultTableModel modo = new DefaultTableModel();
        modo.addColumn("Código");
        modo.addColumn("Descripción");
        modo.addColumn("Fórmula");
        FSOSEGTABFRM009.Tabla_calculo_comisiones.setModel(modo);
        String datos[] = new String[3];
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPCLCCON ('" + buscar + "')}");
            ResultSet rs = ppt.executeQuery();
            if (rs.next()) {
                datos[0] = rs.getString(1);
                datos[1] = rs.getString(2);
                datos[2] = rs.getString(3);
                modo.addRow(datos);
            } else {
                JOptionPane.showMessageDialog(null, "No existe el registro");
                exito = false;
                Limpiar_Tab_Calculo();
                Tabla_Calculo_Comisiones_Mostrar_Modo();
            }
        } catch (SQLException ex) {
            exito = false;
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
        }
        Bloquear_Tab_Calculo();
        return exito;
    }

    public void Tabla_Calculo_Comisiones_Mostrar_Modo() throws SQLException {
        DefaultTableModel modo = new DefaultTableModel();
        modo.addColumn("Código");
        modo.addColumn("Descripción");
        modo.addColumn("Fórmula");
        FSOSEGTABFRM009.Tabla_calculo_comisiones.setModel(modo);
        String datos[] = new String[3];
        CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPCLCCON2 ()}");
        ResultSet rs = ppt.executeQuery();
        while (rs.next()) {
            datos[0] = rs.getString(1);
            datos[1] = rs.getString(2);
            datos[2] = rs.getString(3);
            modo.addRow(datos);
        }
    }

    public void Tabla_Calculo_Modificar() throws SQLException {
        DefaultTableModel modo = new DefaultTableModel();
        FSOSEGTABFRM009.Tabla_calculo_comisiones.setModel(modo);
        calculo.btnCC_actualizar.setEnabled(true);
        String buscar = calculo.txtCC_buscar.getText();
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPCLCCON ('" + buscar + "')}");
            this.exitosa = true;
            ResultSet rs = ppt.executeQuery();
            if (rs.next()) {
                Desbloquear_Tab_Calculo();
                calculo.btnCC_insertar.setEnabled(false);
                calculo.btnCC_anular.setEnabled(false);
                calculo.btnCC_consultar.setEnabled(false);
                calculo.txtCC_codigo.setText(rs.getString(1));
                calculo.txtCC_descripcion.setText(rs.getString(2));
                calculo.txtCC_formula.setText(rs.getString(3));
            } else {
                JOptionPane.showMessageDialog(null, "No existe el usuario");
                calculo.txtCC_buscar.setText("");
                this.exitosa = false;
                Bloquear_Tab_Calculo();
                calculo.btnCC_actualizar.setEnabled(false);
                calculo.btnCC_insertar.setEnabled(true);
                calculo.btnCC_anular.setEnabled(true);
                calculo.btnCC_consultar.setEnabled(true);
                Limpiar_Tab_Calculo();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TextField.class.getName()).log(Level.SEVERE, null, ex);
            this.exitosa = false;
        }
    }

    public void Tabla_Calculo_Actualizar() throws SQLException {
        this.exitosa = true;
        String codigo = calculo.txtCC_codigo.getText();
        String descripcion = calculo.txtCC_descripcion.getText();
        String formula = calculo.txtCC_formula.getText();
        String buscar = calculo.txtCC_buscar.getText();
        try {
            if (codigo.equals("") || descripcion.equals("") || formula.equals("")) {
                JOptionPane.showMessageDialog(null, "No pueden haber campos vacíos");
                calculo.txtCC_buscar.setEnabled(true);
            } else {
                CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPCLCACT ('" + buscar + "',?,?,?)}");
                ppt.setString(1, codigo);
                ppt.setString(2, descripcion);
                ppt.setString(3, formula);
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Registro Actualizado");
                calculo.btnCC_insertar.setEnabled(true);
                calculo.btnCC_anular.setEnabled(true);
                calculo.btnCC_consultar.setEnabled(true);
            }
        } catch (SQLException ex) {
            this.exitosa = false;
            JOptionPane.showMessageDialog(null, "Actualización NO Exitosa");
            calculo.btnCC_insertar.setEnabled(true);
            calculo.btnCC_anular.setEnabled(true);
            calculo.btnCC_consultar.setEnabled(true);
        }
        calculo.btnCC_actualizar.setEnabled(false);
        Limpiar_Tab_Calculo();
        Bloquear_Tab_Calculo();
    }

    public void Tabla_Calculo_Anular() throws SQLException {
        String buscar = calculo.txtCC_buscar.getText();
        this.exitosa = true;
        try {
            CallableStatement ppt = cn.prepareCall("{call FSOSEGSTPCLCANL ('" + buscar + "')}");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Registro Anulado");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Usuario no existe");
            this.exitosa = false;
        }
    }
    
    /**
     * ***************************TABLA CONTABILIDAD 020 ******************************
     */
    
    
    public void Bloquear_Tab_Contabilidad()
    {
       contabilidad.txtC_nro_transaccion.setEditable(false);
       contabilidad.txtC_cod_tipo_operacion.setEditable(false);
       contabilidad.txtC_clase_trans.setEditable(false);
       contabilidad.txtC_cod_tipo_transaccion.setEditable(false);
       contabilidad.txtC_nro_operacion.setEditable(false);
       contabilidad.txtC_cta_contable.setEditable(false);
       contabilidad.txtC_monto.setEditable(false);
       contabilidad.txtC_fecha_transaccion.setEnabled(false);
       contabilidad.txtC_fecha_contable.setEnabled(false);
       ((JTextFieldDateEditor) contabilidad.txtC_fecha_transaccion.getDateEditor()).setDisabledTextColor(Color.BLACK);
       ((JTextFieldDateEditor) contabilidad.txtC_fecha_contable.getDateEditor()).setDisabledTextColor(Color.BLACK);
       contabilidad.txtC_status.setEditable(false);
       contabilidad.jR_Debe.setEnabled(false);
       contabilidad.jR_Haber.setEnabled(false);
    }
    public void Desbloquear_Tab_Contabilidad() {
        contabilidad.txtC_cod_tipo_operacion.setEditable(true);
        contabilidad.txtC_clase_trans.setEditable(true);
        contabilidad.txtC_cod_tipo_transaccion.setEditable(true);
        contabilidad.txtC_nro_operacion.setEditable(true);
        contabilidad.txtC_cta_contable.setEditable(true);
        contabilidad.txtC_monto.setEditable(true);
        contabilidad.txtC_fecha_transaccion.setEnabled(true);
        contabilidad.txtC_fecha_contable.setEnabled(true);
        contabilidad.txtC_status.setEditable(true);
        contabilidad.jR_Debe.setEnabled(true);
        contabilidad.jR_Haber.setEnabled(true);
    }
    
    
    public void Limpiar_Tab_Contabilidad()
    {
            contabilidad.txtC_cod_tipo_operacion.setText("");
            contabilidad.txtC_clase_trans.setText("");
            contabilidad.txtC_cod_tipo_transaccion.setText("");
            contabilidad.txtC_nro_operacion.setText("");
            contabilidad.txtC_cta_contable.setText("");
            contabilidad.txtC_monto.setText("");
            contabilidad.txtC_fecha_transaccion.setDate(null);
            contabilidad.txtC_fecha_contable.setDate(null);
            contabilidad.txtC_status.setText("");
            contabilidad.Grupo.clearSelection();
            
            
    }

    public boolean esExito() {
        return exitosa;
    }
}
