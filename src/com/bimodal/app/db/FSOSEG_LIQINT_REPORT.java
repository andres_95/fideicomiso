/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;

import com.bimodal.app.db.FSOSEG_CONNECT_MODULE;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Becerra
 */
public class FSOSEG_LIQINT_REPORT extends ReportManager {
  private String[] date=new String[2];
  private String[] fideicomiso=new String[2];
  private String[] cuenta=new String[2];

    public void setDate(String initial,String end){
        date[0]=initial;
        date[1]=end;
        addParameter("inicio", date[0]);
        addParameter("final",date[1]);
    }
    public void setFideicomiso(String initial,String end){
      fideicomiso[0]=initial;
      fideicomiso[1]=end;
    }
    public void setCuenta(String cuentaI,String cuentaF){
       cuenta[0]=cuentaI;
       cuenta[1]=cuentaF;
    }
    public FSOSEG_LIQINT_REPORT(FSOSEG_CONNECT_MODULE conector) {
        super(conector);
    }
    
    
    @Override
    public void genReport() {
        String sql1 = "{call FSOSEGLIQINTREPORT(?,?,?,?,?,?)}";
        ArrayList<String> param = new ArrayList<>();
        param.add(date[0]);
        param.add(date[1]);
        param.add(fideicomiso[0]);
        param.add(fideicomiso[1]);
//  Statement st = null;
        ResultSet rs = conector.executeQuery(sql1, param);
        datasource=new FSOSEG_LIQINT_DATASOURCE();
        
        try {
            while (rs.next()) {
               FSOSEG_LIQINT_MODEL liquidacion=new FSOSEG_LIQINT_MODEL(rs.getString(1),rs.getString(2),rs.getDouble(3),rs.getDouble(4),rs.getDouble(5),rs.getDouble(6),rs.getDouble(7),rs.getDouble(8),rs.getDouble(9),rs.getDouble(10));
               datasource.add(liquidacion);
            }
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_LIQINT_REPORT.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
}
