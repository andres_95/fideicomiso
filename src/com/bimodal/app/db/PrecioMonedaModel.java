/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario1
 */
public class PrecioMonedaModel {
    FSOSEG_CONNECT_MODULE connect;
    
              public PrecioMonedaModel(FSOSEG_CONNECT_MODULE connect){
                  this.connect=connect;
              }
              public ResultSet LoadMonedas(){
                 ResultSet set;
                 ArrayList<String> parameters=new ArrayList<>(); 
                      parameters.add("All");
                       set=connect.executeQuery("{call FSOSEGSTPMONCON(?)}",parameters);
                   return set;    
              }
             public ResultSet LoadPaises(){
                   ResultSet set; 
          
                       set=connect.executeQuery("call FSOSEGPAICONS()");
                   return set;      
                         
             }
           public void addPrecioMoneda(ArrayList<String> parameters){
                String sql="{call FSOSEGPREMONINS(?,?,?,?)}"; //FSOSEGPREMONINS`(in cod_monedaX varchar(10),in cod_paisX varchar(10),in tasa_precioX decimal,in fecha_precioX date,in fecha_actualizacionX date)
        try {
            connect.execute(sql, parameters);
        } catch (SQLException ex) {
            Logger.getLogger(PrecioMonedaModel.class.getName()).log(Level.SEVERE, null, ex);
        }
           }
           public void UpdPrecioMoneda(ArrayList<String> parameters){
               String sql="{call FSOSEGPREMONACT(?,?,?,?)}"; 
        try {
            connect.execute(sql, parameters);
        } catch (SQLException ex) {
            Logger.getLogger(PrecioMonedaModel.class.getName()).log(Level.SEVERE, null, ex);
        }
           }
          public void AnlPrecioMoneda(ArrayList<String> parameters){
             String sql="{call FSOSEGPREMONANL(?)}"; 
        try {
            connect.execute(sql, parameters);
        } catch (SQLException ex) {
            Logger.getLogger(PrecioMonedaModel.class.getName()).log(Level.SEVERE, null, ex);
        }
          }
          public ResultSet FndPrecioMoneda(String buscar){
              String sql="{call FSOSEGPREMONCON(?)}";
              ArrayList<String> param=new ArrayList<>();
                param.add(buscar);
               ResultSet set = connect.executeQuery(sql,param);
               return set;
          }
         public ResultSet ShowPrecioMoneda(){
            String sql="{call FSOSEGPREMONSHOWCON()}";
                ResultSet set = connect.executeQuery(sql);
               return set;
         }
            
}
