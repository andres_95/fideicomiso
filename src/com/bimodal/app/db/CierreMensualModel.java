/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Andres
 */
public class CierreMensualModel {
    private FSOSEG_CONNECT_MODULE conn;
    
    public CierreMensualModel(FSOSEG_CONNECT_MODULE conn) {
        this.conn=conn;
    }
    
    public boolean sizetable()
    {
        String sql="select count(*) from t_cierre_mensual";
        ResultSet rs=conn.executeQuery(sql);
        try {
            //return rs;
            rs.next();
            if(rs.getString(1).equals("0"))
                return false;
            else
                return true;
        } catch (SQLException ex) {
            Logger.getLogger(CierreMensualModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean Chequeo_ultimo_cierre()
    {
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        Calendar c1 = new GregorianCalendar();
        c1.set(Calendar.DAY_OF_MONTH, c1.get(Calendar.DAY_OF_MONTH)-1);
        String fecha = date.format(c1.getTime());
        ResultSet rs;
        try {
            CallableStatement ppt = conn.getConnection().prepareCall("{call FSOSEGSTPCIMCON ('" + fecha + "')}");
            rs= ppt.executeQuery();
            rs.next();
            if(rs.getString(2).equals("S"))
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CierreMensualModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public ResultSet transaciones(String Fecha)
    {
        ResultSet rs=null;
        try {
            CallableStatement ppt = conn.getConnection().prepareCall("{call FSOSEGSTPCIMTRAN ('" + Fecha + "')}");
            rs= ppt.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(CierreMensualModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
    
    public ResultSet Asientos(String cod_trans)
    {
        ResultSet rs=null;
        try {
            CallableStatement ppt = conn.getConnection().prepareCall("{call FSOSEGSTPCIDASI ('" + cod_trans + "')}");
            rs= ppt.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(CierreMensualModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
    
    public void insertar_contabilidad(String[] data)
    {
        CallableStatement ppt;
        try {
            ppt = conn.getConnection().prepareCall("{call FSOSEGSTPCONTINS (?,?,?,?,?,?,?)}");
            for(int i=1; i<=7; i++)
            {
                ppt.setString(i, data[i-1]);
            }
            ppt.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(CierreMensualModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void procesar_estados_transacciones(String Fecha)
    {
        try {
            CallableStatement ppt;
            ppt = conn.getConnection().prepareCall("{call FSOSEGSTPCIMUPT ('" + Fecha + "')}");
            ppt.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(CierreMensualModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     public void create_registro_cierre()
    {
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        Calendar c1 = new GregorianCalendar();
        String fecha = date.format(c1.getTime());
        try {
            CallableStatement ppt;
            ppt = conn.getConnection().prepareCall("{call FSOSEGSTPCIMREGC ('" + fecha + "')}");
            ppt.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(CierreDiarioModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void cambiar_estado_cierre(int state)
    {
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        Calendar c1 = new GregorianCalendar();
        String fecha = date.format(c1.getTime());
        if(state<5)
        {
            try {
            CallableStatement ppt;
            ppt = conn.getConnection().prepareCall("{call FSOSEGSTPCIMREGUP (?,?,?)}");
            ppt.setString(1, fecha);
            ppt.setString(2, "N");
            ppt.setInt(3, state);
            
            ppt.executeQuery();
            } catch (SQLException ex) {
                Logger.getLogger(CierreDiarioModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else
        {
             try {
            CallableStatement ppt;
            ppt = conn.getConnection().prepareCall("{call FSOSEGSTPCIMREGUP (?,?,?)}");
            ppt.setString(1, fecha);
            ppt.setString(2, "S");
            ppt.setInt(3, 5);
            
            ppt.executeQuery();
            } catch (SQLException ex) {
                Logger.getLogger(CierreDiarioModel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
