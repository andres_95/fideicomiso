/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;

import com.bimodal.app.db.FSOSEG_CONNECT_MODULE;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRDataSource;

/**
 *
 * @author Becerra
 */
public class FSOSEG_BALCOM_REPORT extends ReportManager{
    private String[] date=new String[2];
    public FSOSEG_BALCOM_REPORT(FSOSEG_CONNECT_MODULE conector) {
        super(conector);
       
    }
    class info{
        public String tipo;
        public double saldo;
        public String Descripcion;
        info()
        {
            tipo="";
            saldo=0.0;
        }
    }
    public void setDate(String inicio,String fin){
        date[0]=inicio;
        date[1]=fin;
        addParameter("Inicio",date[0]);
        addParameter("Final",date[1]);
    } 
    @Override
    public void genReport() {
        String sql1 = "{call FSOSEGBALCOMREPORT(?,?)}";
        Statement st = null;
        
        ArrayList<String> param=new ArrayList<>();
          param.add(date[0]);
          param.add(date[1]);
        ResultSet rs = conector.executeQuery(sql1,param);   
           datasource=new FSOSEG_BALCOM_DATASOURCE();
        try {
            Map<String, info> lista = new HashMap<String, info>();
            while(rs.next()){
                info in = new info();
                in.Descripcion=rs.getString(2);
                in.tipo=rs.getString(3);
                if(lista.containsKey(rs.getString(1)))
                {
                    in=lista.get(rs.getString(1));
                }
                if(rs.getString(3).equals("Activo"))
                {
                    if(rs.getString(5).equals("D"))
                    {
                        in.saldo=in.saldo+ rs.getDouble(4);
                    }else in.saldo=in.saldo- rs.getDouble(4);
                    
                }else
                {
                    if(rs.getString(5).equals("H"))
                    {
                        in.saldo=in.saldo+ rs.getDouble(4);
                    }else in.saldo=in.saldo- rs.getDouble(4);
                }
                lista.put(rs.getString(1), in);
                        
            }
            Iterator it = lista.keySet().iterator();
            while(it.hasNext())
            {
                String i = (String) it.next();
                 FSOSEG_BALCOM_MODEL balance=new FSOSEG_BALCOM_MODEL(i,lista.get(i).Descripcion,lista.get(i).tipo,new BigDecimal(lista.get(i).saldo));
                datasource.add(balance);
                
            }
            
            // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_BALCOM_REPORT.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
