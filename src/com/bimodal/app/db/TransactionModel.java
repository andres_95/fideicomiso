/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario1
 */
public class TransactionModel {
    FSOSEG_CONNECT_MODULE conn;
   
        public TransactionModel(FSOSEG_CONNECT_MODULE conn){
           this.conn=conn;
         }
        public ResultSet LoadTransactionList(String inicio,String fin){ 
           String sql="{call FSOSEGTRANSPERCON(?,?)}";
           ArrayList<String> parameters=new ArrayList<String>();
           parameters.add(inicio);
           parameters.add(fin);
           ResultSet rs=conn.executeQuery(sql, parameters);
           return rs;
        }
       private ResultSet fideicomisos;
          
       public ResultSet LoadPersonaInfo(String cedula){
             String sql="{call FSOSEGSTPPERCON(?)}";
             ArrayList<String> parameters=new ArrayList<>();
             parameters.add(cedula);
             ResultSet rs=conn.executeQuery(sql,parameters);
             sql="SELECT pp.cod_Producto , p.Des_Producto FROM t_producto_persona as pp , t_producto as p where pp.ci_rif='"+
                     cedula+"' and pp.cod_Producto=p.cod_Producto";
             fideicomisos=conn.executeQuery(sql);
               
             return rs;
       }
       
      public ResultSet getFideicomisos(){
        return fideicomisos;
      }
      
     public ResultSet LoadMonedas(){
        String sql="{ call FSOSEGSTPMONCON(?)}";
         ArrayList<String> parameters=new ArrayList<>();
         parameters.add("All");
         ResultSet rs=conn.executeQuery(sql, parameters);
         return rs;
     }

    public ResultSet LoadTipoTransaccion() {
      String sql="{ call FSOSEGTIPTRANSCON() }";
      ArrayList<String> parameters=new ArrayList();
          ResultSet rs=conn.executeQuery(sql,parameters);
          return rs;
    }

    public ResultSet LoadFormas() {
      String sql="{call FSOSEGFPCON()}";
        ArrayList<String> parameters=new ArrayList();
          ResultSet rs=conn.executeQuery(sql,parameters);
          return rs;        
    }
    
}
