/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;


import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author Becerra
 */
public class FSOSEG_BALCOM_DATASOURCE extends Datasource {
      private List<FSOSEG_BALCOM_MODEL> list=new ArrayList<>();
      private int indice=-1;

    @Override
    public boolean next()  {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       return ++indice<list.size();
    }

    @Override
    public Object getFieldValue(JRField jrf){
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
      Object obj=null;   
        if("cuenta".equals(jrf.getName()))
           obj=list.get(indice).getCuenta();
        else
        if("descripcion".equals(jrf.getName()))
            obj=list.get(indice).getDescripcion();
        else
        if("Tipo".equals(jrf.getName()))
            obj=list.get(indice).getTipo();
        else
        if("monto".equals(jrf.getName()))
            obj=list.get(indice).getMonto();
        
        return obj;
    }
     public void add(List<FSOSEG_BALCOM_MODEL> list){
       this.list=list;
     } 

    @Override
    public void add(Object modelo) {
           list.add((FSOSEG_BALCOM_MODEL) modelo);//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
