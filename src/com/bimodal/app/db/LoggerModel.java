/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;

import com.bimodal.util.Tokens;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Becerra
 */
public class LoggerModel<T> {
    private Map<String,Map<String,String>> bitacoras;
    private String proceso;
      public LoggerModel(){
          String dir="resources/Loggers/";
         
         bitacoras=new HashMap<>();
          bitacoras.put("Diario",new HashMap<>());
          bitacoras.put("VEF",new HashMap<>());
           bitacoras.put("USD",new HashMap<>());
          BufferedReader reader;
        try {
            File file=new File(dir+"Diarios.txt");
            reader = new BufferedReader(new FileReader(dir+"Diarios.txt"));
            ReadFile(reader,"Diario");
            file=new File(dir+"VEF.txt");
            reader = new BufferedReader(new FileReader(dir+"VEF.txt"));
            ReadFile(reader,"VEF");
            file=new File(dir+"USD.txt");
            reader = new BufferedReader(new FileReader(dir+"USD.txt"));
            ReadFile(reader,"USD");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LoggerModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
               
      }
     public Map<String,DiarioLogger> getLoggersDiario(){
        Map<String,DiarioLogger> cierres=new HashMap<>();
         for(Map.Entry<String,String> p:bitacoras.get("Diario").entrySet()){
             DiarioLogger cierre=new DiarioLogger(p.getValue());
             cierres.put(p.getKey(),cierre);
         }
           return cierres;    
     }
     
      public Map<String,VEFLogger> getLoggersVEF(){
        Map<String,VEFLogger> cierres=new HashMap<>();
         for(Map.Entry<String,String> p:bitacoras.get("Diario").entrySet()){
             VEFLogger cierre=new VEFLogger(p.getValue());
             cierres.put(p.getKey(),cierre);
         }
           return cierres;    
     }
      public Map<String,USDLogger> getLoggersUSD(){
        Map<String,USDLogger> cierres=new HashMap<>();
         for(Map.Entry<String,String> p:bitacoras.get("Diario").entrySet()){
             USDLogger cierre=new USDLogger(p.getValue());
             cierres.put(p.getKey(),cierre);
         }
           return cierres;    
      }
    /* public void add(String proceso){
        String dir="./resources/Loggers/";
         File file=new File(dir+proceso+".txt");
       //  bitacoras=new HashMap<>();
          bitacoras.put(proceso,new HashMap<>());
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(dir+proceso+".txt"));
            ReadFile(reader);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LoggerModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
      }*/

    private  void ReadFile(BufferedReader reader,String proceso) {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       String line; 
        try {
            while((line=reader.readLine())!=null){
                Tokens tok=new Tokens(line,"-");
                String[] tokens=tok.getTokens();
                Map eventos=bitacoras.get(proceso);
                 eventos.put(tokens[0],tokens[1]);
            } 
        } catch (IOException ex) {
            Logger.getLogger(LoggerModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
   public String getMessage(String proceso,String key){
       return bitacoras.get(proceso).get(key);
   }
    
}
