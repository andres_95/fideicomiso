/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;
/**
 *
 * @author Becerra
 */
public class FSOSEG_LIQINT_MODEL {
    private String codFid;
    private String nombre;
    private double interesCobrado;
    private double NinteresCobrado;
    private double devengadoCobrado;
    private double pagadoAnticipado;
    private double cobradoAnticipado;
    private double capitalPromedio;
    private double porcPromedio;
    private double tasaEquiv;

    public FSOSEG_LIQINT_MODEL(String codFid, String nombre, double interesCobrado, double NinteresCobrado, double devengadoCobrado, double pagadoAnticipado, double cobradoAnticipado, double capitalPromedio, double porcPromedio, double tasaEquiv) {
        this.codFid = codFid;
        this.nombre = nombre;
        this.interesCobrado = interesCobrado;
        this.NinteresCobrado = NinteresCobrado;
        this.devengadoCobrado = devengadoCobrado;
        this.pagadoAnticipado = pagadoAnticipado;
        this.cobradoAnticipado = cobradoAnticipado;
        this.capitalPromedio = capitalPromedio;
        this.porcPromedio = porcPromedio;
        this.tasaEquiv = tasaEquiv;
    }

    public String getCodFid() {
        return codFid;
    }

    public void setCodFid(String codFid) {
        this.codFid = codFid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getInteresCobrado() {
        return interesCobrado;
    }

    public void setInteresCobrado(double interesCobrado) {
        this.interesCobrado = interesCobrado;
    }

    public double getNinteresCobrado() {
        return NinteresCobrado;
    }

    public void setNinteresCobrado(double NinteresCobrado) {
        this.NinteresCobrado = NinteresCobrado;
    }

    public double getDevengadoCobrado() {
        return devengadoCobrado;
    }

    public void setDevengadoCobrado(double devengadoCobrado) {
        this.devengadoCobrado = devengadoCobrado;
    }

    public double getPagadoAnticipado() {
        return pagadoAnticipado;
    }

    public void setPagadoAnticipado(double pagadoAnticipado) {
        this.pagadoAnticipado = pagadoAnticipado;
    }

    public double getCobradoAnticipado() {
        return cobradoAnticipado;
    }

    public void setCobradoAnticipado(double cobradoAnticipado) {
        this.cobradoAnticipado = cobradoAnticipado;
    }

    public double getCapitalPromedio() {
        return capitalPromedio;
    }

    public void setCapitalPromedio(double capitalPromedio) {
        this.capitalPromedio = capitalPromedio;
    }

    public double getPorcPromedio() {
        return porcPromedio;
    }

    public void setPorcPromedio(double porcPromedio) {
        this.porcPromedio = porcPromedio;
    }

    public double getTasaEquiv() {
        return tasaEquiv;
    }

    public void setTasaEquiv(double tasaEquiv) {
        this.tasaEquiv = tasaEquiv;
    }
    
      
}
