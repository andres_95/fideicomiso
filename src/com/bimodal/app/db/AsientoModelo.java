/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;

import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author usuario1
 */
public class AsientoModelo {
    FSOSEG_CONNECT_MODULE connector;
    
       public AsientoModelo(FSOSEG_CONNECT_MODULE conn){
             connector=conn;
           }
      public ResultSet LoadAsientos(){
        String sql="{call FSOSEGSTPASICON(?)}";
          ArrayList<String> parameters=new ArrayList<String>();
            parameters.add("All");
            ResultSet rs=connector.executeQuery(sql, parameters);
            return rs;
      
      }
      public void add(ArrayList<String> parameters){
          String sql="{call FSOSEGSTPASIINS(?,?,?,?,?,?,?)}";
          connector.executeQuery(sql, parameters);
      }
}
