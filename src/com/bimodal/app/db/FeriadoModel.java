/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;

import com.bimodal.app.conf.GlobalConstants;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author usuario1
 */
public class FeriadoModel {
    FSOSEG_CONNECT_MODULE conn;
    
         public FeriadoModel(){
              conn=GlobalConstants.getConnector();
         }
         public void add(ArrayList<Object> param) throws SQLException{
              String sql="{call FSOSEGSTPDIFINS(?,?,?,?)}";
              conn.executeGen(sql, param);
        }
         public ResultSet Load(){
              ResultSet rs=conn.executeQuery("{call FSOSEGSTPDIFALLCON()}");
              return rs;
         }
         public void Update(ArrayList<Object> param) throws SQLException{
             String sql="{call FSOSEGSTPDIFACT(?,?,?,?)}";
              conn.executeGen(sql, param);
         }
         public void NULL(Date date) throws SQLException{
             String sql="{call FSOSEGSTPDIFANL(?)}";
             ArrayList<Object> param=new ArrayList<>();
              param.add(date);
             conn.executeGen(sql,param);
         }
}
