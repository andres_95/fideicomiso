/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;
/**
 *
 * @author Becerra
 */
public class FSOSEG_RESCONGEN_MODEL {
     private String fecha_asiento;
     private String num_asiento;
     private String concepto;
     private double debe,haber,saldo;

    public FSOSEG_RESCONGEN_MODEL(String fecha_asiento, String num_asiento, String concepto, double debe, double haber, double saldo) {
        this.fecha_asiento = fecha_asiento;
        this.num_asiento = num_asiento;
        this.concepto = concepto;
        this.debe = debe;
        this.haber = haber;
        this.saldo = saldo;
    }
     
     

    public String getFecha_asiento() {
        return fecha_asiento;
    }

    public void setFecha_asiento(String fecha_asiento) {
        this.fecha_asiento = fecha_asiento;
    }

    public String getNum_asiento() {
        return num_asiento;
    }

    public void setNum_asiento(String num_asiento) {
        this.num_asiento = num_asiento;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public double getDebe() {
        return debe;
    }

    public void setDebe(double debe) {
        this.debe = debe;
    }

    public double getHaber() {
        return haber;
    }

    public void setHaber(double haber) {
        this.haber = haber;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
   
     
}
