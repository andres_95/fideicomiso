
package com.bimodal.app.db;

import com.bimodal.app.conf.GlobalConstants;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class MonedaModel {
        FSOSEG_CONNECT_MODULE cn;
      public MonedaModel(){
         cn=GlobalConstants.getConnector();
      }
      public ArrayList<String> getMonedas(){
          ArrayList<String> list=new ArrayList<>();
          
             ResultSet rs=cn.executeQuery("{call FSOSEGMONCON()}");
            try {
                while(rs.next()){
                    list.add(rs.getString(1)+" "+rs.getString(2));
                }
            } catch (SQLException ex) {
                Logger.getLogger(MonedaModel.class.getName()).log(Level.SEVERE, null, ex);
            }
          return list;     
      }
     public ResultSet getMonedasComplete(){
        //  ArrayList<String> list=new ArrayList<>();
          
             ResultSet rs=cn.executeQuery("{call FSOSEGMONCON()}");
            return rs;
     }
}
