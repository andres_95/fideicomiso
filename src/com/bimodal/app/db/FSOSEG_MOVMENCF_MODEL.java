/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;
/**
 *
 * @author Becerra
 */
public class FSOSEG_MOVMENCF_MODEL {
    private String numfso;
  //  private String Administrador;
    private double SaldoInicial;
    private double Debito;
    private double Credito;
    private double SaldoFinal;

    public FSOSEG_MOVMENCF_MODEL(String numfso,double Debito, double Credito, double SaldoFinal) {
        this.numfso = numfso;
        //this.Administrador = Administrador;
      //  this.SaldoInicial = SaldoInicial;
        this.Debito = Debito;
        this.Credito = Credito;
        this.SaldoFinal = SaldoFinal;
    }
   
    
    public String getNumfso() {
        return numfso;
    }

    public void setNumfso(String numfso) {
        this.numfso = numfso;
    }

  /*  public String getAdministrador() {
        return Administrador;
    }*/

  /*  public void setAdministrador(String Administrador) {
        this.Administrador = Administrador;
    }*/

    public double getSaldoInicial() {
        return SaldoInicial;
    }

    public void setSaldoInicial(double SaldoInicial) {
        this.SaldoInicial = SaldoInicial;
    }

    public double getDebito() {
        return Debito;
    }

    public void setDebito(double Debito) {
        this.Debito = Debito;
    }

    public double getCredito() {
        return Credito;
    }

    public void setCredito(double Credito) {
        this.Credito = Credito;
    }

    public double getSaldoFinal() {
        return SaldoFinal;
    }

    public void setSaldoFinal(double SaldoFinal) {
        this.SaldoFinal = SaldoFinal;
    }
    
}
