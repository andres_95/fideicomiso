package com.bimodal.app.db;

import com.bimodal.app.db.FSOSEG_CONNECT_MODULE;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Becerra
 */
public class FSOSEG_MOVMENCF_REPORT extends ReportManager {
  private String[] date;
  private String[] fideicomiso;
  private String[] cuenta;
  private String[] estatus;
  
    public FSOSEG_MOVMENCF_REPORT(FSOSEG_CONNECT_MODULE conector) {
        super(conector);
         date=new String[2];
         fideicomiso=new String[2];
         cuenta=new String[2];
         
    }

  
     public void setDate(String initial,String end){
       
         date[0]=initial;
         date[1]=end;
     }
    public void setFideicomiso(String fii,String fie){
      
      
         fideicomiso[0]=fii;
         fideicomiso[1]=fie;
    }
    public void setCuenta(String cuentaI,String cuentaE){
     
      cuenta[0]=cuentaI;
      cuenta[1]=cuentaE;
    }
    public void setEstatus(String[] estatus)
    {
      this.estatus=estatus;
    }
    @Override
    public void genReport() {
        String sql1 = "{call FSOSEGMOVMENREPORT(?,?,?,?,?,?)}";
        ArrayList<String> param=new ArrayList<>();
          param.add(date[0]);
          param.add(date[1]);
          param.add(fideicomiso[0]);
          param.add(fideicomiso[1]);
          param.add(cuenta[0]);
          param.add(cuenta[1]);
          double[] saldo=new double[2];
//  Statement st = null;
        ResultSet rs = conector.executeQuery(sql1,param);  
          datasource=new FSOSEG_MOVMENCF_DATASOURCE();
      try {
          while(rs.next()){
              if(rs.getString(4).equals("D"))
              {  saldo[0]= rs.getDouble(2);
                 saldo[1]=0.00;
              }
              else{
                saldo[0]=0.00;
                 saldo[1]=rs.getDouble(2);
              }
                  
              FSOSEG_MOVMENCF_MODEL movimiento=new FSOSEG_MOVMENCF_MODEL(rs.getString(1),saldo[0],saldo[1],0.00);
              datasource.add(movimiento);
              
          }
          //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
      } catch (SQLException ex) {
          Logger.getLogger(FSOSEG_MOVMENCF_REPORT.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    
}
