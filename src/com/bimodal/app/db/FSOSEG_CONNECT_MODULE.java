/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author Josue
 */
public class FSOSEG_CONNECT_MODULE {
   private Connection driver;
   private DatabaseMetaData dmd;
   private boolean connected;
    public FSOSEG_CONNECT_MODULE() {
          driver=null;
    }
           
     public void Connect(String host,String database,String port,String user,String password)
     {   connected=true;
        try {
            driver=DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+database,user,password);
        } catch (SQLException ex) {
          connected=false;
        }
        
     }
     
       private String user;
      private String password;
     public void setUser(String user,String password){
       this.user=user;
       this.password=password;
     }
      private String host,port,database;
     public void setSystem(String host,String port,String database){
        this.host=host;
        this.port=port;
        this.database=database;
     
     }
     
     public void Connect(){
         connected=true;
        try {
            driver=DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+database,user,password);
        } catch (SQLException ex) {
          connected=false;
        }
     
      }
    public boolean isConnected(){
         return (connected==true);
    
    }
    
    
    public DatabaseMetaData ConnectorInfo(){
        DatabaseMetaData dmd=null;
       try {
           dmd=driver.getMetaData();
           
           
       } catch (SQLException ex) {
           Logger.getLogger(FSOSEG_CONNECT_MODULE.class.getName()).log(Level.SEVERE, null, ex);
       }
          return dmd;
    
    }
   
    public ResultSet executeQuery(String sql){
         ResultSet res=null;
       try {
           res=driver.createStatement().executeQuery(sql);
       } catch (SQLException ex) {
           Logger.getLogger(FSOSEG_CONNECT_MODULE.class.getName()).log(Level.SEVERE, null, ex);
       }
         return res;
    }
    
    public void execute(String sql){ 
       try {
           driver.createStatement().execute(sql);
       } catch (SQLException ex) {
           Logger.getLogger(FSOSEG_CONNECT_MODULE.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    public void execute(String sql,ArrayList<String> list) throws SQLException{
         int len=-1;
     
          
           CallableStatement stat=driver.prepareCall(sql);
           
           for(int i=0; i<list.size(); i++){
              len=i;  
              stat.setObject(i+1,list.get(i));
           }
           stat.execute();
      
       
    }
    
     public void executeGen(String sql,ArrayList<Object> list) throws SQLException{
      
           CallableStatement stat=driver.prepareCall(sql);
           
           for(int i=0; i<list.size(); i++){
              stat.setObject(i+1,list.get(i));
           }
           stat.execute();
       
       
    }
   
    public ResultSet executeQuery(String sql,ArrayList<String> list){
         ResultSet res=null;
        try {
           CallableStatement stat=driver.prepareCall(sql);
           
               
           for(int i=0; i<list.size(); i++){
              stat.setObject(i+1,list.get(i));
           }
         
           stat.execute();
           res=stat.getResultSet();
           
       } catch (SQLException ex) {
           Logger.getLogger(FSOSEG_CONNECT_MODULE.class.getName()).log(Level.SEVERE, null, ex);
       }
         return res;
    }
   
    public Connection getConnection(){
         return driver;
    }
    
    public void endConnection(){
       try {
           driver.close();
       } catch (SQLException ex) {
           Logger.getLogger(FSOSEG_CONNECT_MODULE.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    
     
}
