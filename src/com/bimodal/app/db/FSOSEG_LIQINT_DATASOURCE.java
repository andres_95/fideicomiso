/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;


import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author Becerra
 */
public class FSOSEG_LIQINT_DATASOURCE extends Datasource{
      private ArrayList<FSOSEG_LIQINT_MODEL> list=new ArrayList<>();
      private int indice=-1;
     
    @Override
    public boolean next() {
          
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
         return ++indice<list.size();
    }

    @Override
    public Object getFieldValue(JRField jrf)  {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       Object value=null;
         
          if("codFid".equals(jrf.getName()))
              value=list.get(indice).getCodFid();
          else
          if("Nombre".equals(jrf.getName()))
              value=list.get(indice).getNombre();
          else
          if("InCob".equals(jrf.getName()))
              value=list.get(indice).getInteresCobrado();
          else
          if("NinCob".equals(jrf.getName()))
              value=list.get(indice).getNinteresCobrado();
          else
          if("DevCob".equals(jrf.getName()))
              value=list.get(indice).getDevengadoCobrado();
          if("Pagan".equals(jrf.getName()))
              value=list.get(indice).getPagadoAnticipado();
          else
          if("Cobran".equals(jrf.getName()))
              value=list.get(indice).getCobradoAnticipado();
          else
          if("CapProm".equals(jrf.getName()))
              value=list.get(indice).getCapitalPromedio();
          else
          if("PorcProm".equals(jrf.getName()))
              value=list.get(indice).getPorcPromedio();
          else
          if("TasaEquiv".equals(jrf.getName()))
              value=list.get(indice).getTasaEquiv();
          
          return value;
    }
    
    public void addAll(List<FSOSEG_LIQINT_MODEL> list){
        this.list=(ArrayList<FSOSEG_LIQINT_MODEL>) list;
    }
    public void add(FSOSEG_LIQINT_MODEL modelo){
       this.list.add(modelo); 
    }  

    @Override
    public void add(Object modelo) {
      list.add((FSOSEG_LIQINT_MODEL) modelo); // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
      
       
}
