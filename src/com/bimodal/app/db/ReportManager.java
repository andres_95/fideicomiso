package com.bimodal.app.db;

import com.bimodal.app.db.Datasource;
import com.bimodal.app.db.FSOSEG_CONNECT_MODULE;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Becerra
 */
public abstract class ReportManager {
    protected FSOSEG_CONNECT_MODULE conector;
    protected JasperReport reporte;
    protected Datasource datasource;
    protected String title;
    protected Map<String,Object> parameters=new HashMap<>();
        public ReportManager(FSOSEG_CONNECT_MODULE conector){
           this.conector=conector;
        }
      public abstract void genReport();
      public void setTitle(String title){
         this.title=title;
      }
      public void OpenReport(String file){
            file+=".jasper";
            
            File archivo = new File(file);    
        try {
            reporte = (JasperReport) JRLoader.loadObject(archivo);
        } catch (JRException ex) {
           // Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }    
           
      }
      public void addParameter(String key,Object value){
          parameters.put(key,value);
      }
      public void printReport(){
         JasperPrint print = null;
        try {
            print = JasperFillManager.fillReport(reporte,parameters,datasource); // Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
            
        } catch (JRException ex) {
            Logger.getLogger(ReportManager.class.getName()).log(Level.SEVERE, null, ex);
        }
     
        JasperViewer ver;
       
            ver = new JasperViewer(print,false);
             ver.setTitle(title);
            ver.setVisible(true);
       
           
      }
    
}
