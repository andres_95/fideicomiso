/*
 * Nombre Objeto: ReportModel
 * Autor: Hetsy Rodríguez
 * Fecha Elab: 10/02/2017
 * Descripción: Archivo de sentencias en SQL para modulo de reportes
 * Última Modificación:
 * Autor Última Modificación:
 */
package com.bimodal.app.db;
import com.bimodal.app.db.FSOSEG_CONNECT_MODULE;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.FSOSEG_MENU_FRAME;


public class ReportModel {
    FSOSEG_CONNECT_MODULE conector;
      
    public ReportModel( FSOSEG_CONNECT_MODULE conector){
        this.conector=conector;
    }
         
    public ResultSet extract(){
        String sql1 = "SELECT Nombre, Apellido, Edad, Sexo, direccion, saldo from registros r, datos d WHERE r.ID= d.ID";
        Statement st = null;
        ResultSet rs = conector.executeQuery(sql1);
        return rs;
    }
          
}
