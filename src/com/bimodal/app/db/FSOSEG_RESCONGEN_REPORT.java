/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;

import com.bimodal.app.db.FSOSEG_CONNECT_MODULE;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Becerra
 */
public class FSOSEG_RESCONGEN_REPORT extends ReportManager {

    private String[] date, fideicomiso, cuenta, asiento;

    public FSOSEG_RESCONGEN_REPORT(FSOSEG_CONNECT_MODULE conector) {
        super(conector);
        date=new String[2];
        fideicomiso=new String[2];
        cuenta=new String[2];
        asiento=new String[2];
    }

    public void setDate(String initial, String end) {
        
        date=new String[2];
        date[0] = initial;
        date[1] = end;
        addParameter("Inicio", date[0]);
        addParameter("final",date[1]);
    }

    public void setFideicomiso(String initial, String end) {
        fideicomiso[0] = initial;
        fideicomiso[1] = end;
    }

    public void setCuenta(String initial, String end) {
        cuenta[0] = initial;
        cuenta[1] = end;
    }
    
   public void setAsiento(String initial,String end){
      asiento[0]=initial;
      asiento[1]=end;
   }

    @Override
    public void genReport() {
         String sql1 = "{call FSOSEGMOVMENREPORT(?,?,?,?,?,?)}";
        ArrayList<String> param=new ArrayList<>();
          param.add(date[0]);
          param.add(date[1]);
          param.add(fideicomiso[0]);
          param.add(fideicomiso[1]);
          param.add(cuenta[0]);
          param.add(cuenta[1]);
          ResultSet set=conector.executeQuery(sql1,param);
           datasource=new FSOSEG_RESCONGEN_DATASOURCE();
               
        try {
              if(!set.isBeforeFirst())
                  return;
            while(set.next()){
               FSOSEG_RESCONGEN_MODEL resumen=new FSOSEG_RESCONGEN_MODEL(set.getString(1),set.getString(2),set.getString(3),set.getDouble(4),set.getDouble(5),set.getDouble(6));
               datasource.add(resumen);
            }
            //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_RESCONGEN_REPORT.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
