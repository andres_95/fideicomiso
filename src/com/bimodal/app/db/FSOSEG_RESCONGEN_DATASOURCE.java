/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;


import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author Becerra
 */
public class FSOSEG_RESCONGEN_DATASOURCE extends Datasource {
    private List<FSOSEG_RESCONGEN_MODEL> list=new ArrayList<>();
    private int indice=-1;
    @Override
    public boolean next() {
       //perationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       return ++indice<list.size();
    }

    @Override
    public Object getFieldValue(JRField jrf) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
      Object obj=null;
      
            if("fechas".equals(jrf.getName()))
                obj=list.get(indice).getFecha_asiento();
            else
            if("numas".equals(jrf.getName()))
                obj=list.get(indice).getNum_asiento();
            else
            if("conmov".equals(jrf.getName()))
               obj=list.get(indice).getConcepto();
            else
            if("Debe".equals(jrf.getName()))
                obj=list.get(indice).getDebe();
            else
            if("Haber".equals(jrf.getName()))
                obj=list.get(indice).getHaber();
            else
            if("Saldo".equals(jrf.getName()))
                obj=list.get(indice).getSaldo();
            
            return obj;
    }
      public void addAll(List<FSOSEG_RESCONGEN_MODEL> l){
          this.list=l;
      }
      public void add(FSOSEG_RESCONGEN_MODEL model){
         list.add(model);
      }

    @Override
    public void add(Object modelo) {
       list.add((FSOSEG_RESCONGEN_MODEL) modelo);// throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}     
