
package com.bimodal.app.db;

import com.bimodal.app.conf.GlobalConstants;
import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;


public class DataModel {

    Map<String, String> instrucciones;
    private Map<Boolean, String> status;
    private Map<String, String> tables;
    private ResultSet columns;
    private FSOSEG_CONNECT_MODULE conector;
    private String request;
    private Map<String, String> procesos;
    private String keytabla;

    public DataModel() {
        instrucciones = new HashMap<String, String>();
        instrucciones.put("I", "insert into <Tabla> values(<valores>)");
        instrucciones.put("U", "update <Tabla> set <camposyvalores> where <condiciones>");
        instrucciones.put("A","update <Tabla> set cod_Status=I where <condicion>");
        instrucciones.put("Q", "select * from <Tabla> where <condiciones>");
        status = new HashMap<Boolean, String>();
        status.put(Boolean.TRUE, "A");
        status.put(Boolean.FALSE, "I");
        tables = new HashMap<String, String>();
        tables.put("000000", "t_usuario");
        tables.put("000001", "t_persona");
        tables.put("000002", "t_log_auditoria");
        tables.put("000003", "t_producto");
        tables.put("000004", "t_asiento_contable");

        procesos = new HashMap<String, String>();
        //  procesos.put("030100","Se ejecutó aportes masivos");
        // procesos.put("030200", "Se ejecutó retiros masivos");
        //   procesos.put("030300","Se ejecutó proceso de desincorporación");
        //  procesos.put("030400","Se ejecutó un recibo de inversiones");
        // procesos.put("030500","Se realizó un calculo de rendimiento");      
        //  procesos.put("030600","Se realizó un pago de rendimiento");
        // procesos.put("030700","Se realizó un calculo de comisiones");
        // procesos.put("030800","Se efectuó cierre mensual");
        procesos.put("030900", "Se efectuó cierre diario");
        procesos.put("031000", "Se ejecutó un una programación de procesos");
        procesos.put("031100", "Se ejecutó una lectura de Operaciones de cartera diaria");

    }

    public void loadTablesAuditables() {
        String sql = "{call FSOSEGAUDCONS()}";
        ResultSet res = conector.executeQuery(sql);

        try {
            while (res.next()) {
                if (res.getString(1).indexOf("04") >= 0) {
                    tables.put(res.getString(1), res.getString(2));
                    continue;
                }
                procesos.put(res.getString(1), res.getString(2));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   
    private String user, perfil;

    public void setUser(String user, String perfil) {
        this.user = user;
        this.perfil = perfil;
    }

    public void setConector(FSOSEG_CONNECT_MODULE conector) {
        this.conector = conector;

    }

    

    public boolean isAuditable(String tabla) {
        return (this.tables.get(tabla) != null);
    }
     
    private ResultSet DescTable(String tabla){
        return conector.executeQuery("desc "+tables.get(tabla));
        
    }
    private String chainCampos(ResultSet set,ArrayList<JTextField> values) throws SQLException{
          int sz=values.size();
          String str="";
        for(int i=0; i<sz; i++){
              set.next();
                 if(i==(sz-1))
                    str+=(!set.isAfterLast())?set.getString(1):"Other data"+"="+values.get(i).getText();
            
            str+=(!set.isAfterLast())?set.getString(1):"Other data"+"="+values.get(i).getText()+",";
        }
          return str;
    }
     public void addLog(String key,String tabla,ArrayList<JTextField> values,String condicion){
        this.keytabla=tabla;
        String sql=instrucciones.get(key);
            if(tables.get(tabla)==null)
                return;
            
        sql=sql.replace("<Tabla>",tables.get(tabla));
        ResultSet columns=DescTable(tabla);
        String datas="";
        try {
            datas=chainCampos(columns,values);
        } catch (SQLException ex) {
            Logger.getLogger(DataModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         sql=sql.replace("<camposyvalores>",datas);
         
          if(key.equals("U") || key.equals("A"))
             sql=sql.replace("<condicion>",condicion);
        
         String user=GlobalConstants.getUser();
         String perfil=GlobalConstants.getRol();
         toLog(user,tabla,tabla.substring(0,2),perfil,sql); 
     
     }

    private void toLog(String user, String modulo, String opcion, String perfil, String desc) {
        String sql = "{call FSOSEGLOGINS(?,?,?,?,?,?)}";

        ArrayList<String> list = new ArrayList<String>();
        list.add(this.keytabla);
        list.add(user);
        list.add(modulo);
        list.add(opcion);
        list.add(perfil);
        list.add(procesos.get(desc));
        try {
            conector.execute(sql, list);
        } catch (SQLException ex) {
            Logger.getLogger(DataModel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void toLog(String user, String modulo, String opcion, String perfil) {
        String sql = "{call FSOSEGLOGINS(?,?,?,?,?,?)}";

        ArrayList<String> list = new ArrayList<String>();
        list.add(this.keytabla);
        list.add(user);
        list.add(modulo);
        list.add(opcion);
        list.add(perfil);
        list.add(request);
        try {
            conector.execute(sql, list);
        } catch (SQLException ex) {
            Logger.getLogger(DataModel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void Update() {
        conector.execute(request);

    }

    public ResultSet findLog(String inicio, String fin) {

        String sql = "{call FSOSEGLOGCONS(?,?)}";
        // int index;
        ArrayList<String> param = new ArrayList<>(2);
        param.add(inicio);
        param.add(fin);
        return conector.executeQuery(sql, param);

    }

    public ResultSet find() {

        return conector.executeQuery(request);

    }
}
