/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;

import java.math.BigDecimal;

/**
 *
 * @author Becerra
 */
public class FSOSEG_BALCOM_MODEL {
    private String cuenta;
    private String descripcion;
    private String tipo;

   
    private BigDecimal monto;
    
    public FSOSEG_BALCOM_MODEL(String cuenta, String descripcion, String tipo,BigDecimal monto) {
        this.cuenta = cuenta;
        this.descripcion = descripcion;
        this.tipo=tipo;
        this.monto = monto;
    }
     public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }
    
    
      
    
}
