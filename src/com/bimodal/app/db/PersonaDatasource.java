/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bimodal.app.db;

import com.bimodal.app.conf.GlobalConstants;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author usuario1
 */
public class PersonaDatasource {
    FSOSEG_CONNECT_MODULE connect;
    private boolean result=true;
              public PersonaDatasource(){
                  connect=GlobalConstants.getConnector();
              
              }
             public void add(ArrayList<String> parameters){
                String sql = "{call FSOSEGSTPPERSINS(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        ArrayList<String> arr = new ArrayList<>();

        for (int i = 0; i < 22; i++) {
            arr.add(parameters.get(i));

        }
           result=true;
        try {
            connect.execute(sql, arr);
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, "Registro Persona fallido");
            Logger.getLogger(PersonaDatasource.class.getName()).log(Level.SEVERE, null, ex);
            result=false;
            return;
        }

        /*  FSOSEGSTPLOCINS`(in cod_Pers varchar (20), 
in ci varchar(20),in ave varchar(60),in edif varchar(60),
in urba varchar(60),in piso varchar(60),in pais varchar(20),
in ciu varchar(20),in edo varchar(20),in zona_post varchar(30),
in telef1 varchar(30),in telef2 varchar(30),in apto varchar(30))*/
        sql = "{call FSOSEGSTPLOCINS(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        arr.clear();
        for (int i = 22; i < parameters.size()-1; i++) {
            arr.add(parameters.get(i));
        }
        try {

            connect.execute(sql, arr);
        } catch (SQLException ex) {
             JOptionPane.showMessageDialog(null, "Registro Localización fallido");
            Logger.getLogger(PersonaDatasource.class.getName()).log(Level.SEVERE, null, ex);
            result=true;
            return;
        }
             JOptionPane.showMessageDialog(null,"Persona y localización registrado con éxito");
       
      }

    public void Update(ArrayList<String> parameters) {
        int end=parameters.size()-1;
        String sql = "{call FSOSEGSTPPERACT('"+parameters.get(end)+"',?,?,}";

        try {
            sql = "";
            connect.execute(sql, (ArrayList<String>) parameters.subList(0, 21));
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDatasource.class.getName()).log(Level.SEVERE, null, ex);
            result=true;
            return;
        }

    }
            public ArrayList<Object> Find(String buscar) throws SQLException{
            
               String sql="";
               ArrayList<String> param=new ArrayList<>();
               ArrayList<Object> set=new ArrayList<>();
                    param.add(buscar);
                      
                     ResultSet datos_personales=connect.executeQuery(sql,param);
                        if(!datos_personales.isBeforeFirst())
                               return null;
                        
                     datos_personales.next();
                     for(int i=0; i<datos_personales.getMetaData().getColumnCount(); i++){
                     
                        set.add(datos_personales.getObject(i+1));
                     }
                       
                  sql="";
                     ResultSet localizacion=connect.executeQuery(sql,param);
                     
                     localizacion.next();
                     for(int i=0; i<localizacion.getMetaData().getColumnCount(); i++){
                     
                        set.add(localizacion.getObject(i+1));
                     }
                return set;     
       
            }
     public boolean isSuccess(){
        return result;
     }
            
}
