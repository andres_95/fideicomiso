/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.bimodal.app.conf.GlobalConstants;
import com.bimodal.app.db.TransactionModel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTextField;

/**
 *
 * @author usuario1
 */
public class TransactionRegistryController {
    TransactionModel modelo;
    private String cedula;
    JTextField texto;
    JComboBox cmb;
       public TransactionRegistryController(){
            modelo=new TransactionModel(GlobalConstants.getConnector());
               
       }
      public void setPersona(JTextField texto){
          this.texto=texto;
      }
      public void setProducto(JComboBox cmb){
       this.cmb=cmb;
      }
      
      public void getPersona(String cedula){
          ResultSet rs=modelo.LoadPersonaInfo(cedula);
          this.cedula=cedula;
        try {
            if(!rs.isBeforeFirst())
            {
                texto.setText("");
                return;
            }
            rs.next();
             int i=6;
             String str="";
               for(i=5; i<9; i++)
                 str+=(rs.getString(i)==null)?"":rs.getString(i)+" ";
            texto.setText(str);
            getProductos();
        } catch (SQLException ex) {
            Logger.getLogger(TransactionRegistryController.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
      public void getProductos(){
         ResultSet productos=modelo.getFideicomisos();
          
        try {
            if(!productos.isBeforeFirst())
                return;
            
            while(productos.next()){
                cmb.addItem(productos.getString(1)+"-"+productos.getString(2));
            }
        } catch (SQLException ex) {
            Logger.getLogger(TransactionRegistryController.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
     public void getMonedas(JComboBox cmb){
         ResultSet monedas=modelo.LoadMonedas();
        try {
            if(!monedas.isBeforeFirst())
                return;
            
            while(monedas.next()){
               cmb.addItem(monedas.getString(1)+"-"+ monedas.getString(2));
            }
        } catch (SQLException ex) {
            Logger.getLogger(TransactionRegistryController.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
     public void getTipoTransaccion(JComboBox cmb){
        ResultSet transaccion=modelo.LoadTipoTransaccion();
        try {
            if(!transaccion.isBeforeFirst())
                return;
            
            while(transaccion.next())
                   cmb.addItem(transaccion.getString(1)+"-"+transaccion.getString(2));
            
        } catch (SQLException ex) {
            Logger.getLogger(TransactionRegistryController.class.getName()).log(Level.SEVERE, null, ex);
        }
     }

    public void getFormaPago(JComboBox<String> cmbFP) {
  //      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       ResultSet forma=modelo.LoadFormas();
        try {
            while(forma.next()){
                cmbFP.addItem(forma.getString(1)+"-"+forma.getString(2));
            }
        } catch (SQLException ex) {
            Logger.getLogger(TransactionRegistryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
