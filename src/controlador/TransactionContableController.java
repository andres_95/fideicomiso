/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.bimodal.app.conf.GlobalConstants;
import com.bimodal.app.db.TransactionContableModelo;
import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author usuario1
 */
public class TransactionContableController {
    TransactionContableModelo modelo;
    JTable table;
         public TransactionContableController(){
             modelo=new TransactionContableModelo(GlobalConstants.getConnector());
             
         }
     public void setTable(JTable tab){
         this.table=tab;
     } 
     public void LoadRecords(Date inicioF,Date finF){
          DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
           Date date=new Date();
            if(inicioF==null || finF==null)
                return;
           String inicio=dateFormat.format(inicioF);
           String fin=dateFormat.format(finF);
        ResultSet contables=modelo.LoadTransactions(inicio, fin);
        
        
           DefaultTableModel model=(DefaultTableModel) table.getModel();
          TableColumnModel tcm = table.getColumnModel();
            table.setFillsViewportHeight(true);
            table.setPreferredSize(new Dimension(1000,1000));
            int columnsCount;
        /*try {
            columnsCount = contables.getMetaData().getColumnCount();
        } catch (SQLException ex) {
            Logger.getLogger(TransactionContableController.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        try {
             columnsCount = contables.getMetaData().getColumnCount();
            while(contables.next()){
                Object[] obj=new Object[columnsCount];
                int count=1;
                for(int i=0; i<(columnsCount); i++){
                    
                    obj[i]=contables.getObject(i+1);
                    //tcm.getColumn(i-1).setMinWidth(60);
                    tcm.getColumn(i).setMaxWidth((count++)*100);
                    //tcm.getColumn(i-1).setWidth(1);
                }
                tcm.getColumn(6).setMaxWidth(20000);
                model.addRow(obj);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TransactionContableController.class.getName()).log(Level.SEVERE, null, ex);
        }
          table.setModel(model);
           
         }
     
}
