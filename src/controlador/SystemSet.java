/*
 * Nombre Objeto: SystemSet
 * Autor: Josue Becerra
 * Fecha Elab: 24/01/2017
 * Descripción: 
 * Última Modificación:
 * Autor Última Modificación:
 */
package controlador;

import com.bimodal.app.conf.GlobalConstants;
import com.bimodal.app.db.ReportModel;
import com.bimodal.app.db.FSOSEG_CONNECT_MODULE;
import com.bimodal.app.db.DataModel;
import com.bimodal.app.db.SessionModel;

public class SystemSet {
    
    private FSOSEG_CONNECT_MODULE conector;
    private DataModel model;
    private FSOSEG_SYSTEM_CONTROLLER sys;
    private SessionModel sesion;
    private ReportModel reportes;
    private ReportController controladorR;
    private ControladorSesion consesion;
    
    public DataModel getModel() {
        return model;
    }
    
    public FSOSEG_SYSTEM_CONTROLLER getSystem(){
       return sys;
    }
        
    public SystemSet(){
        GlobalConstants.InitializeConnection();
        conector=GlobalConstants.getConnector();
        model=new DataModel();
        conector.setSystem("localhost","3306","fsosegdba01");
        conector.Connect();
          
        if(conector.isConnected())
            model.setConector(conector);
            model.loadTablesAuditables();
            sys=new FSOSEG_SYSTEM_CONTROLLER(conector);
            sesion=new SessionModel(conector);
            consesion=new ControladorSesion(sesion);    
    }
          
    public ReportController ReportAssitance(){
        reportes=new ReportModel(conector);
        controladorR=new ReportController(reportes);
        return controladorR;
    }
         
    public ControladorSesion getSesionControl(){
        return consesion;
    } 
    
}
