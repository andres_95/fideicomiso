/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.bimodal.app.db.FeriadoModel;
import com.toedter.calendar.JDateChooser;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


   enum Dia{
   domingo(1),lunes(2),martes(3),miercoles(4),jueves(5),viernes(6),sabado(7);
   
   private final int id;
   
     private Dia(int id){
        this.id=id;
     }
  }
  enum Mes{
     enero(1),febrero(2),marzo(3),abril(4),mayo(5),junio(6),julio(7),agosto(8),
     septiembre(9),octubre(10),noviembre(11),diciembre(12);
     
     private int id;
     
        private Mes(int id){
           this.id=id;
        }
  }
/**
 *
 * @author usuario1
 */
public class FeriadoController {
    private JDateChooser fecha;
    private JTextField text;
    private JTable table;
    private FeriadoModel modelo;
    private Dia[] dia;
    private Mes[] mes;
    public FeriadoController(JDateChooser fecha,JTextField txt,JTable table){
        this.fecha=fecha;
        this.text=txt;
        this.table=table;
        modelo=new FeriadoModel();
        dia=Dia.values();
        mes=Mes.values();
     }
    public void enabled(boolean flag){
       this.fecha.setEnabled(flag);
       this.text.setEnabled(flag);
    }
    public void clean(){
       this.fecha.setDate(null);
       this.text.setText("");
    }
      private void addRowTable(Date date,String descripcion){
          SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
            String fecha=format.format(date);
            Object[] rowData=new Object[2];
             rowData[0]=fecha;
             rowData[1]=descripcion;
            ((DefaultTableModel)table.getModel()).addRow(rowData);
      }
      private void addRowDatabase(Date date,String descripcion){
           ArrayList<Object> param=new ArrayList<>();
             SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
            String fecha=format.format(date);
            Calendar c=Calendar.getInstance();
            c.setTime(date);
            int day=c.get(Calendar.DAY_OF_WEEK);
            int month=c.get(Calendar.MONTH);
            int year=c.get(Calendar.YEAR);
            
            
             param.add(fecha);
             param.add(descripcion);
             param.add(dia[day-1].name());
             param.add(mes[month-1].name());
            // param.add(year);
        try {
            modelo.add(param);
        } catch (SQLException ex) {
           // Logger.getLogger(FeriadoController.class.getName()).log(Level.SEVERE, null, ex);
           JOptionPane.showMessageDialog(null,"Esta fecha ha sido registrada");
        }
      }
    public void add(){
           addRowTable(fecha.getDate(),text.getText());
           addRowDatabase(fecha.getDate(),text.getText());
     
    }
    public void LoadFeriado(){
         ResultSet rs=modelo.Load();
        try {
            if(rs==null)
                return;
             if(!rs.isBeforeFirst())
                 return;
             
               while(rs.next()){
                     
                     addRowTable(rs.getDate(1),rs.getString(2));
                  /* Object[] rowData=new Object[2];
                   rowData[0]=rs.getString(1);
                   rowData[1]=rs.getString(2);
                  ((DefaultTableModel)table.getModel()).addRow(rowData);*/
               }
        } catch (SQLException ex) {
            Logger.getLogger(FeriadoController.class.getName()).log(Level.SEVERE, null, ex);
        }
         
          
         
        
    }
    
}
