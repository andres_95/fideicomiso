/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.toedter.calendar.JDateChooser;
import java.util.ArrayList;
import javax.swing.JTextField;
import com.bimodal.app.db.RendimientoDatasource;
import javax.swing.JOptionPane;
/**
 *
 * @author usuario
 */
public class RendimientoController{
    JTextField asignacionbs;
    JTextField asignacionusd;
    JTextField rendimientobs;
    JTextField rendimientousd;
    JDateChooser fecha;
     private RendimientoDatasource rendimiento; 
    public RendimientoController(JTextField asignacionbs, JTextField asignacionusd, JTextField rendimientobs, JTextField rendimientousd, JDateChooser fecha) {
        this.asignacionbs = asignacionbs;
        this.asignacionusd = asignacionusd;
        this.rendimientobs = rendimientobs;
        this.rendimientousd = rendimientousd;
        this.fecha = fecha;
        rendimiento = new RendimientoDatasource();
    }
   
    public void save(){
        ArrayList<Object> parametros = new ArrayList<>();
        parametros.add(asignacionbs.getText());
        parametros.add(asignacionusd.getText());
        parametros.add(rendimientobs.getText());
        parametros.add(rendimientousd.getText());
        parametros.add(fecha.getDate());
        if (rendimiento.check(parametros)){
            JOptionPane.showMessageDialog(null, "Ya existe un registro con esa fecha");
        }else{
            rendimiento.add(parametros);
        }
    }
    
    public void limpiar(){
        this.asignacionbs.setText("");
        this.asignacionusd.setText("");
        this.rendimientobs.setText("");
        this.rendimientousd.setText("");
        this.fecha.setDateFormatString("");
    }
    public boolean esVacio(){
        return this.asignacionbs.getText().isEmpty()&&
        this.asignacionusd.getText().isEmpty()&&
        this.rendimientobs.getText().isEmpty()&&
        this.rendimientousd.getText().isEmpty()&&
        (this.fecha.getDate()==null);
    }
}
