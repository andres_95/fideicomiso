/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.bimodal.app.conf.GlobalConstants;
import com.bimodal.app.db.PersonaDatasource;
import com.bimodal.app.db.ProductoDatasource;
import com.toedter.calendar.JDateChooser;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author usuario1
 */
public class ProductoControlador extends DataExplorer {
      private ProductoDatasource datasource=new ProductoDatasource();
    public ProductoControlador(JFrame fram) {
        super(fram);
    }
    public void Operar(String accion){
         ArrayList<String> parameters=new ArrayList<>();
          int size=lists.size();
                //Los primeros components son para registrar a la persona
             for(int i=0; i<(size-1); i++){
                 if(lists.get(i) instanceof JTextField){
                  
                     parameters.add(((JTextField)lists.get(i)).getText());
                 }
                else
                if(lists.get(i) instanceof JComboBox){
                    
                     parameters.add((String)(((JComboBox)lists.get(i)).getSelectedItem()));
                }
               else
                if(lists.get(i).getClass().getName().equals("com.toedter.calendar.JDateChooser")){
                         JDateChooser date=(JDateChooser)lists.get(i);
                      SimpleDateFormat formatofecha = new SimpleDateFormat("yyyy-MM-dd"); 
                       if(date.getDate()!=null){
                    
                       parameters.add(formatofecha.format(date.getDate()));}
                 }
               }
             if(accion.equals("I")){
             try {
                 datasource.add(parameters);
                 //JOptionPane.showMessageDialog(null,"Persona registrado con éxito");
             } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null,"Ocurrio un error mientras se intentó registrar el producto");
                 Logger.getLogger(ProductoControlador.class.getName()).log(Level.SEVERE, null, ex);
                 return;
             }
               JOptionPane.showMessageDialog(null,"Producto registrado con éxito");
             }
              else
              if(accion.equals("U")){
             try {
                 datasource.Update(parameters);
             } catch (SQLException ex) {   
                 JOptionPane.showMessageDialog(null,"Ocurrio un error mientras se intentó actualizar el producto");
                 Logger.getLogger(ProductoControlador.class.getName()).log(Level.SEVERE, null, ex);
                 return;
             }
                 JOptionPane.showMessageDialog(null,"Producto actualizado con éxito");
                  
           }
         GlobalConstants.getAuditoria().setLog(accion);
        Clean();
        Enabled(false);
    }
    
}
