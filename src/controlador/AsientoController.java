/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.bimodal.app.conf.GlobalConstants;
import com.bimodal.app.db.AsientoModelo;
import com.toedter.calendar.JDateChooser;
import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author usuario1
 */
public class AsientoController extends DataExplorer {
    private AsientoModelo asiento;
    private JTable table;
    private int column;
    private DefaultTableModel model;
      public AsientoController(JFrame fram){
          super(fram);
         asiento=new AsientoModelo(GlobalConstants.getConnector());
      }
     public void setModelo(JTable tab){
       /*String[] Header={"Tipo Operación","Clase Transacción","Cuenta Contable","Debe/Haber","Camp. Trans.","Calculo","Estatus"};
        DefaultTableModel modelo=new DefaultTableModel();
          modelo.setColumnIdentifiers(Header);
          modelo.setColumnCount(Header.length);
          tab.setModel(modelo);
          for(int i=0; i<Header.length; i++){
               TableColumn col=new TableColumn();
               col.setHeaderValue(Header[i]);
               modelo.addColumn(col);
           }
        tab.setColumnModel(modelo);
        this.table=tab;
        column=Header.length;*/
         table=tab;
     }
     public void Operar(String accion){
          ArrayList<String> parameters=new ArrayList<>();
          int size=lists.size();
                //Los primeros components son para registrar a la persona
             for(int i=0; i<(size-1); i++){
                 if(lists.get(i) instanceof JTextField){
                  
                     parameters.add(((JTextField)lists.get(i)).getText());
                 }
                else
                if(lists.get(i) instanceof JComboBox){
                    
                     parameters.add((String)(((JComboBox)lists.get(i)).getSelectedItem()));
                }
               else
                if(lists.get(i).getClass().getName().equals("com.toedter.calendar.JDateChooser")){
                         JDateChooser date=(JDateChooser)lists.get(i);
                      SimpleDateFormat formatofecha = new SimpleDateFormat("yyyy-MM-dd"); 
                       if(date.getDate()!=null){
                    
                       parameters.add(formatofecha.format(date.getDate()));}
                 }
               }
            
             asiento.add(parameters);
             
     }
     public void LoadRecords(){
         ResultSet rs=asiento.LoadAsientos();
         
          DefaultTableModel model=(DefaultTableModel) table.getModel();
          TableColumnModel tcm = table.getColumnModel();
        //  model.setRowCount(1);
           //Se estable las cabeceras de la tabla
           String[] Header={"Tipo Operación","Clase Transacción","Cuenta Contable","Debe/Haber","Camp. Trans.","Calculo","Estatus"};
           model.setColumnIdentifiers(Header);
          
          
         
            table.setFillsViewportHeight(true);
            table.setPreferredSize(new Dimension(1000,1000));
        
             //Se carga los registros de los asientos
            try {
               int columnsCount=column=model.getColumnCount();
               
              while(rs.next()){
                   Object[] obj=new Object[columnsCount];
                   int count=1;
                   for(int i=0; i<columnsCount; i++){
                        
                       obj[i]=rs.getObject(i+1);
                       //tcm.getColumn(i-1).setMinWidth(60);
                       tcm.getColumn(i).setMaxWidth((count++)*100);
                       //tcm.getColumn(i-1).setWidth(1);
                   }
                     tcm.getColumn(6).setMaxWidth(20000);
                 model.addRow(obj);
              }   
              
        } catch (SQLException ex) {
            Logger.getLogger(LogController.class.getName()).log(Level.SEVERE, null, ex);
        }
          this.model=model;
      //    table.setModel(model);    
     }
}
