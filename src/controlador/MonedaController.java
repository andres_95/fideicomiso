/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.bimodal.app.db.MonedaModel;
import com.bimodal.util.ComboValue;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario1
 */
public class MonedaController {
      private JComboBox cmb;
      private String key;
      private MonedaModel modelo;
      
    public MonedaController(){
        modelo=new MonedaModel();
    }
    public MonedaController(JComboBox cmb){
         this.cmb=cmb;
          modelo=new MonedaModel();
    } 
    public void setMonedas(JTable tab){
       ResultSet rs=modelo.getMonedasComplete();
       DefaultTableModel model=(DefaultTableModel)tab.getModel();
          try {
              while(rs.next()){
                 Object[] obj=new Object[3];
                 obj[0]=rs.getObject(1);
                 obj[1]=rs.getObject(2);
                 obj[2]=rs.getObject(3);
                 model.addRow(obj);
              }
          } catch (SQLException ex) {
              Logger.getLogger(MonedaController.class.getName()).log(Level.SEVERE, null, ex);
          }
    }
   public void getKey(){
     ComboValue cv=new ComboValue(cmb);
     key=cv.getKey();
   }
   
   
}
