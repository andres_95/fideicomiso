/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.toedter.calendar.JDateChooser;
import groovyjarjarbackport.java.util.Arrays;
import java.awt.Component;
import java.awt.Point;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Stack;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

/**
 *
 * @author usuario1
 */
public class DataExplorer {
      protected ArrayList<Component> lists=new ArrayList<>();
      
     protected void ExplorePanel(JPanel panel) {
        Component[] comp = panel.getComponents();
        ArrayList<Component> arr=new ArrayList<>();
        int size = comp.length;

        for (int i = 0; i < size; i++) {
            if((comp[i] instanceof JTextField) || (comp[i] instanceof JComboBox) || (comp[i].getClass().getName().equals("com.toedter.calendar.JDateChooser"))){
                      arr.add(comp[i]);
                   }
               else
                 if (comp[i] instanceof JPanel) {
                    ExplorePanel((JPanel) comp[i]);
            } else
               if (comp[i] instanceof JTabbedPane) {
                   JTabbedPane tab = (JTabbedPane) comp[i];
                   Component[] subcomp = tab.getComponents();
                   
                for (int j = 0; j < subcomp.length; j++) {
                    if (subcomp[j] instanceof JPanel) {
                        ExplorePanel((JPanel) subcomp[j]);
                    }
                }
           
              }
        }
        if(arr.size()==0)
            return;
         Component[] comp2=new Component[arr.size()];
              arr.toArray(comp2);
              
              quicksort(comp2,0,comp2.length-1);
              lists.addAll(Arrays.asList(comp2));
    }
       private boolean leq(Component a,Component b){
         Point pa,pb;
            pa=a.getLocation();
            pb=b.getLocation();
            //  System.out.println(pa+" "+pb);
                return (pa.getY()<=pb.getY()) 
                       || ((pa.getY()==pb.getY() && pa.getX()<=pb.getX()));
                    //    || ((pa.getY()<=pb.getY() && pb.getX()==pa.getX()));
                      //  || (pa.getY()<=pb.getY());
                            
    }
    private boolean lt(Component a,Component b){
               Point pa,pb;
            pa=a.getLocation();
            pb=b.getLocation();
           //   System.out.println(pa+" "+pb);
                return (pa.getY()<pb.getY()) 
                        || ((pa.getY()==pb.getY() && pa.getX()<pb.getX()));
                      //  || ((pa.getY()<pb.getY() && pb.getX()==pa.getX()));
                      //  || (pa.getY()<pb.getY());
        
    }
    private boolean geq(Component a,Component b){
            Point pa,pb;
            pa=a.getLocation();
            pb=b.getLocation();
         //     System.out.println(pa+" "+pb);
                return (pa.getY()>=pb.getY()) 
                        || ((pa.getY()<pb.getY() && pa.getX()>=pb.getX()));
                      //  || ((pa.getY()<=pb.getY() && pb.getX()==pa.getX()));
                       // || (pa.getY()<=pb.getY());
        
    }
    private boolean gt(Component a,Component b){
         Point pa,pb;
            pa=a.getLocation();
            pb=b.getLocation();
        //      System.out.println(pa+" "+pb);
                return (pa.getY()>pb.getY()) 
                        || ((pa.getY()==pb.getY() && pa.getX()>pb.getX()));
                       
                       //  || (pa.getY()>pb.getY());
                       
        
    }
    private boolean eq(Component a,Component b){
          Point pa,pb;
            pa=a.getLocation();
            pb=b.getLocation();
       //       System.out.println(pa+" "+pb);
                return (pb.getX()==pa.getX() && pb.getY()==pa.getY());
                       
    }
    private void quicksort(Component[] A,int izq,int der){
        Component pivote = A[izq+(der-izq)/2]; // tomamos primer elemento como pivote
        int i = izq; // i realiza la búsqueda de izquierda a derecha
        int j = der; // j realiza la búsqueda de derecha a izquierda
        Component aux;

        while (i <= j) {            // mientras no se crucen las búsquedas
            while (lt(A[i],pivote)){//A[i] <= pivote && i < j) {
                i++; // busca elemento mayor que pivote
            }
            while (gt(A[j],pivote)){//A[j] > pivote) {
                j--;         // busca elemento menor que pivote
            }
            if (i <= j) {                      // si no se han cruzado                      
                aux = A[i];                  // los intercambia
                A[i] = A[j];
                A[j] = aux;
                i++;
                j--;
            }
        }
       // A[izq] = A[j]; // se coloca el pivote en su lugar de forma que tendremos
    //    A[j] = pivote; // los menores a su izquierda y los mayores a su derecha
        
        if (izq < j) {
            quicksort(A, izq, j); // ordenamos subarray izquierdo
        }
        if (i < der) {
            quicksort(A, i, der); // ordenamos subarray derecho
    
        }
    }
    
    public void Enabled(boolean flag){
          for(int i=0; i<lists.size(); i++){
             lists.get(i).setEnabled(flag);
          }
    }
      public DataExplorer(JFrame fram){
           ArrayList<Component> arr=new ArrayList<>();
           Component[] comp=fram.getContentPane().getComponents();
           int size=comp.length;
               for(int i=0; i<size; i++){
                   if((comp[i] instanceof JTextField) || (comp[i] instanceof JComboBox) || (comp[i].getClass().getName().equals("com.toedter.calendar.JDateChooser"))){
                      arr.add(comp[i]);
                   }
                  else
                    if (comp[i] instanceof JPanel) {
                    ExplorePanel((JPanel) comp[i]);
            } else
               if (comp[i] instanceof JTabbedPane) {
                   JTabbedPane tab = (JTabbedPane) comp[i];
                   Component[] subcomp = tab.getComponents();
                   
                for (int j = 0; j < subcomp.length; j++) {
                    if (subcomp[j] instanceof JPanel) {
                        ExplorePanel((JPanel) subcomp[j]);
                    }
                }
           
               }
            }
          
                if(arr.size()==0)
                   return;
               
             Component[] comp2=new Component[arr.size()];
              arr.toArray(comp2);
              
              quicksort(comp2,0,comp2.length-1);
              lists.addAll(Arrays.asList(comp2));
              
      } 
     public void Clean(){
         for(int i=0; i<lists.size(); i++){
              if(lists.get(i) instanceof JTextField){
                  ((JTextField)lists.get(i)).setText("");
                   //  parameters.add(((JTextField)lists.get(i)).getText());
                 }
                else
                if(lists.get(i) instanceof JComboBox){
                     ((JComboBox)lists.get(i)).setSelectedItem(0);
                   //  parameters.add((String)(((JComboBox)lists.get(i)).getSelectedItem()));
                }
               else
                if(lists.get(i).getClass().getName().equals("com.toedter.calendar.JDateChooser")){
                         JDateChooser date=(JDateChooser)lists.get(i);
                      SimpleDateFormat formatofecha = new SimpleDateFormat("yyyy-MM-dd"); 
                       if(date.getDate()!=null){
                    
                        // parameters.add(formatofecha.format(date.getDate()));
                      }
                 }
          }
     }
}
