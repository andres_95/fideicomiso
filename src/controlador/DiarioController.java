/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.bimodal.app.conf.GlobalConstants;
import com.bimodal.app.db.CierreDiarioModel;
import com.bimodal.app.db.CierreLogger;
import com.bimodal.app.db.DiarioLogger;
import com.bimodal.app.db.LoggerModel;
import com.bimodal.app.procesos.view.FSOSEGTABFRM024;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Becerra
 */
public class DiarioController extends Thread{
    
    private CierreDiarioModel modelo;
    FSOSEGTABFRM024 frame;
    public boolean status_cierre;
    float value=0;
    public DiarioController(FSOSEGTABFRM024 frame) {
        modelo = new CierreDiarioModel(GlobalConstants.getConnector()); 
        this.frame=frame;
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        Calendar c1 = new GregorianCalendar();
        frame.JLfecha.setText(date.format(c1.getTime()));
        this.verificacion();
        
       
    }
    
    public void verificacion()
    {
        if(!modelo.sizetable())
        {
            frame.info.setText("Primer Cierre");
            frame.info.setForeground(new java.awt.Color(0, 153, 0));
            status_cierre=true;
        }else if (!modelo.Chequeo_ultimo_cierre())
        {
            status_cierre=false;
            frame.info.setText("Ultimo cierre no finalizado");
            frame.info.setForeground(new java.awt.Color(153, 0,0));
        }else
        {
            frame.info.setText("Abilitado para cierre");
            frame.info.setForeground(new java.awt.Color(0, 153, 0));
            status_cierre=true;
        }
    }

    public boolean getStatus_cierre() {
        return status_cierre;
    }
    
    public void run()
    {
        frame.msj2.setFont(new java.awt.Font("Tahoma", 0, 12));
        frame.msj3.setFont(new java.awt.Font("Tahoma", 0, 12));
        frame.msj4.setFont(new java.awt.Font("Tahoma", 0, 12));
        frame.msj5.setFont(new java.awt.Font("Tahoma", 0, 12));
        frame.msj6.setFont(new java.awt.Font("Tahoma", 0, 12));
        DateFormat date = new SimpleDateFormat("yyyy/MM/dd");
        Calendar c1 = new GregorianCalendar();
        String fecha = date.format(c1.getTime());
        this.value=0;
        
        frame.ProgressBar.setVisible(true);
        frame.ProgressBar.setValue((int)value);
           
        try {
             frame.msj2.setFont(new java.awt.Font("Tahoma", 1, 12));
             modelo.create_registro_cierre();
            Thread.sleep(1000);
            try {
                this.generador_asientos(fecha);
            } catch (SQLException ex) {
                Logger.getLogger(DiarioController.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.marcar_transaciones(fecha);
            frame.msj6.setFont(new java.awt.Font("Tahoma", 1, 12));
            frame.ProgressBar.setValue(99);
            Thread.sleep(300);
            frame.ProgressBar.setValue(100);
            Thread.sleep(500);
            modelo.cambiar_estado_cierre(5);
            frame.ProgressBar.setVisible(false);
            frame.BtnCierre.setEnabled(true);
            
        } catch (InterruptedException ex) {
            Logger.getLogger(DiarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        
        //this.generador_asientos("2017/05/04");this.marcar_transaciones(fecha);
    }
    
    public void generador_asientos(String fecha) throws InterruptedException, SQLException 
    {
        ResultSet rs = modelo.transaciones(fecha);
        value=15;
        rs.last();
        int row=rs.getRow();
        rs.first();
        frame.ProgressBar.setValue((int)value);
        frame.msj3.setFont(new java.awt.Font("Tahoma", 1, 12));
        modelo.cambiar_estado_cierre(2);
        Thread.sleep(700);
        frame.msj4.setFont(new java.awt.Font("Tahoma", 1, 12));
        double acum=65.0000/row;
        
        try {
            while(rs.next())
            {
                ResultSet as = modelo.Asientos(rs.getString(2));
                while(as.next())
                {
                    String[] data_contable = new String[7];
                    data_contable[0]=rs.getString(1);
                    data_contable[1]=as.getString(1);
                    data_contable[2]=as.getString(2);
                    data_contable[3]=as.getString(3);
                    data_contable[4]=as.getString(4);
                    data_contable[5]=rs.getString(3);
                    data_contable[6]=fecha;
                    modelo.insertar_contabilidad(data_contable);
                }
                value+=acum;
                frame.ProgressBar.setValue((int)value);
            }
            modelo.cambiar_estado_cierre(3);
        } catch (SQLException ex) {
            Logger.getLogger(DiarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void marcar_transaciones(String Fecha) throws InterruptedException
    {
        frame.ProgressBar.setValue(95);
        frame.msj5.setFont(new java.awt.Font("Tahoma", 1, 12));
        Thread.sleep(700);
        modelo.procesar_estados_transacciones(Fecha);
        modelo.cambiar_estado_cierre(4);
        
    }
    
       
    
}
