/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.bimodal.app.db.FSOSEG_CONNECT_MODULE;
import com.bimodal.app.support.controller.FSOSEG_SYSINFO_MODULE;
import java.sql.ResultSet;

/**
 *
 * @author usuario1
 */
public class FSOSEG_SYSTEM_CONTROLLER {
    FSOSEG_SYSINFO_MODULE sysControl;
    FSOSEG_CONNECT_MODULE connect;    
       public FSOSEG_SYSTEM_CONTROLLER(FSOSEG_CONNECT_MODULE connect){
             sysControl=new FSOSEG_SYSINFO_MODULE();
            sysControl.setConector(connect);
       }
       
      public ResultSet getVersion(){
      
        return sysControl.getVersion();
      }
    
}
