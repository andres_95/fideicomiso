
package controlador;

import com.bimodal.app.administracion.view.FSOSEGTABFRM020;
import com.bimodal.app.modulos.view.FSOSEGTABFRM006;
import com.bimodal.app.modulos.view.FSOSEGTABFRM005;
import com.bimodal.app.administracion.view.FSOSEGTABFRM003;
import com.bimodal.app.administracion.view.FSOSEGTABFRM001;
import com.bimodal.app.administracion.view.FSOSEGTABFRM002;
import com.bimodal.app.administracion.view.FSOSEGTABFRM013;
import com.bimodal.app.administracion.view.FSOSEGTABFRM012;
import com.bimodal.app.administracion.view.FSOSEGTABFRM007;
import com.bimodal.app.procesos.view.FSOSEGTABFRM009;
import com.bimodal.app.modulos.view.FSOSEGTABFRM015;
import com.bimodal.app.modulos.view.FSOSEGTABFRM010;
import com.bimodal.app.modulos.view.FSOSEGTABFRM018;
import com.bimodal.app.modulos.view.FSOSEGTABFRM014;
import com.bimodal.app.modulos.view.FSOSEGTABFRM017;
import com.bimodal.app.modulos.view.FSOSEGTABFRM011;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import vista.Vista;
import com.bimodal.app.db.Modelo;
import javax.swing.JOptionPane;
import Tablas.*;
import com.bimodal.app.conf.GlobalConstants;
import javax.swing.table.DefaultTableModel;

public class Controlador implements ActionListener {
    private boolean flag;
    private Modelo m;
    private Vista v;
    private FSOSEGTABFRM001 persona; 
    private FSOSEGTABFRM002 producto; 
    private FSOSEGTABFRM003 asiento; 
    private FSOSEGTABFRM005 perfil;
    private FSOSEGTABFRM006 t_perfil;
    private FSOSEGTABFRM007 t_trans;
    private FSOSEGTABFRM008 p_persona;
    private FSOSEGTABFRM009 calculo;
    private FSOSEGTABFRM010 configuracion;
    private FSOSEGTABFRM011 instalacion;
    private FSOSEGTABFRM012 transaccion;
    private FSOSEGTABFRM013 plan_contable;
    private FSOSEGTABFRM014 moneda;
    private FSOSEGTABFRM015 localizacion;
    private FSOSEGTABFRM017 usuario;
    private FSOSEGTABFRM018 dia_feriado;
    private FSOSEGTABFRM020 contabilidad;
   
 
    public Controlador(Modelo m, Vista v) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   public Controlador(Modelo m, FSOSEGTABFRM001 persona) {
        flag=false;
        this.m = m;
        this.persona = persona;
        this.persona.btnPS_insertar.addActionListener(this);
        this.persona.btnPS_consultar.addActionListener(this);
        this.persona.btnPS_guardar.addActionListener(this);
        this.persona.btnPS_modificar.addActionListener(this);
        this.persona.btnPS_actualizar.addActionListener(this);
        this.persona.btnPS_anular.addActionListener(this);
        this.persona.btnPS_salir.addActionListener(this);
    }
   
   public Controlador(Modelo m,FSOSEGTABFRM002 producto)
   {
       this.m = m;
       this.producto = producto;
       this.producto.btnPT_insertar.addActionListener(this);
       this.producto.btnPT_guardar.addActionListener(this);
       this.producto.btnPT_consultar.addActionListener(this);
       this.producto.btnPT_salir.addActionListener(this);
       this.producto.btnPT_modificar.addActionListener(this);
       this.producto.btnPT_actualizar.addActionListener(this);
       this.producto.btnPT_anular.addActionListener(this);
       
   }
   
     public Controlador(Modelo m,FSOSEGTABFRM003 asiento)
   {
       this.m = m;
       this.asiento = asiento; 
       this.asiento.btnAC_salir.addActionListener(this);
       this.asiento.btnAC_insertar.addActionListener(this);
       this.asiento.btnAC_guardar.addActionListener(this);
       this.asiento.btnAC_consultar.addActionListener(this);
       this.asiento.btnAC_modificar.addActionListener(this);
       this.asiento.btnAC_actualizar.addActionListener(this);
       this.asiento.btnAC_anular.addActionListener(this);
   }
    
        public Controlador(Modelo m,FSOSEGTABFRM005 perfil)
   {
       this.m = m;
       this.perfil = perfil; 
       this.perfil.btnPF_salir.addActionListener(this);
       this.perfil.btnPF_insertar.addActionListener(this);
       this.perfil.btnPF_guardar.addActionListener(this);
       this.perfil.btnPF_consultar.addActionListener(this);
       this.perfil.btnPF_anular.addActionListener(this);
       this.perfil.btnPF_refrescar.addActionListener(this);
       this.perfil.btnPF_acciones.addActionListener(this);
       this.perfil.btnPF_activar.addActionListener(this);
   }
            
        public Controlador(Modelo m,FSOSEGTABFRM006 t_perfil)
   {
       this.m = m;
       this.t_perfil = t_perfil; 
       this.t_perfil.btnTP_salir.addActionListener(this);
       this.t_perfil.btnTP_consultar.addActionListener(this);
       this.t_perfil.btnTP_guardar_permisos.addActionListener(this);
       this.t_perfil.btnTP_cancelar.addActionListener(this);
   }
                           
     public Controlador(Modelo m,FSOSEGTABFRM007 t_trans)
   {
       this.m = m;
       this.t_trans = t_trans; 
       this.t_trans.btnTT_salir.addActionListener(this);
       this.t_trans.btnTT_insertar.addActionListener(this);
       this.t_trans.btnTT_guardar.addActionListener(this);
       this.t_trans.btnTT_consultar.addActionListener(this);
       this.t_trans.btnTT_modificar.addActionListener(this);
       this.t_trans.btnTT_actualizar.addActionListener(this);
       this.t_trans.btnTT_anular.addActionListener(this);
       this.t_trans.btnTT_refrescar.addActionListener(this);
   }
            
     public Controlador(Modelo m,FSOSEGTABFRM008 p_persona)
   {
       this.m = m;
       this.p_persona = p_persona; 
       this.p_persona.btnPP_salir.addActionListener(this);
       this.p_persona.btnPP_insertar.addActionListener(this);
       this.p_persona.btnPP_guardar.addActionListener(this);
       this.p_persona.btnPP_consultar.addActionListener(this);
       this.p_persona.btnPP_modificar.addActionListener(this);
       this.p_persona.btnPP_actualizar.addActionListener(this);
       this.p_persona.btnPP_anular.addActionListener(this);
   }
     
     public Controlador(Modelo m,FSOSEGTABFRM009 calculo)
   {
       this.m = m;
       this.calculo = calculo; 
       this.calculo.btnCC_salir.addActionListener(this);
       this.calculo.btnCC_insertar.addActionListener(this);
       this.calculo.btnCC_guardar.addActionListener(this);
       this.calculo.btnCC_consultar.addActionListener(this);
       this.calculo.btnCC_modificar.addActionListener(this);
       this.calculo.btnCC_actualizar.addActionListener(this);
       this.calculo.btnCC_anular.addActionListener(this);
       this.calculo.btnCC_refrescar.addActionListener(this);
   }
        
     public Controlador(Modelo m,FSOSEGTABFRM010 configuracion)
   {
       this.m = m;
       this.configuracion = configuracion; 
       this.configuracion.btnC_salir.addActionListener(this);
       this.configuracion.btnC_insertar.addActionListener(this);
       this.configuracion.btnC_guardar.addActionListener(this);
       this.configuracion.btnC_consultar.addActionListener(this);
       this.configuracion.btnC_modificar.addActionListener(this);
       this.configuracion.btnC_actualizar.addActionListener(this);
       this.configuracion.btnC_anular.addActionListener(this);
   }
           
     public Controlador(Modelo m,FSOSEGTABFRM011 instalacion)
   {
       this.m = m;
       this.instalacion = instalacion; 
       this.instalacion.btnIS_salir.addActionListener(this);
       this.instalacion.btnIS_insertar.addActionListener(this);
       this.instalacion.btnIS_guardar.addActionListener(this);
       this.instalacion.btnIS_consultar.addActionListener(this);
       this.instalacion.btnIS_modificar.addActionListener(this);
       this.instalacion.btnIS_actualizar.addActionListener(this);
       this.instalacion.btnIS_anular.addActionListener(this);
   }
          
      public Controlador(Modelo m,FSOSEGTABFRM012 trasanccion)
   {
       this.m = m;
       this.transaccion = transaccion; 
       this.transaccion.btnT_salir.addActionListener(this);
       this.transaccion.btnT_insertar.addActionListener(this);
       this.transaccion.btnT_guardar.addActionListener(this);
       this.transaccion.btnT_consultar.addActionListener(this);
       this.transaccion.btnT_modificar.addActionListener(this);
       this.transaccion.btnT_actualizar.addActionListener(this);
       this.transaccion.btnT_anular.addActionListener(this);
   }
      
         public Controlador(Modelo m,FSOSEGTABFRM013 plan_contable)
   {
       this.m = m;
       this.plan_contable = plan_contable; 
       this.plan_contable.btnPC_salir.addActionListener(this);
       this.plan_contable.btnPC_insertar.addActionListener(this);
       this.plan_contable.btnPC_guardar.addActionListener(this);
       this.plan_contable.btnPC_consultar.addActionListener(this);
       this.plan_contable.btnPC_modificar.addActionListener(this);
       this.plan_contable.btnPC_actualizar.addActionListener(this);
       this.plan_contable.btnPC_anular.addActionListener(this);
       this.plan_contable.btnPC_refrescar.addActionListener(this);
   }
    
        public Controlador(Modelo m,FSOSEGTABFRM014 moneda)
   {
       this.m = m;
       this.moneda = moneda; 
       this.moneda.btnM_salir.addActionListener(this);
       this.moneda.btnM_insertar.addActionListener(this);
       this.moneda.btnM_guardar.addActionListener(this);
       this.moneda.btnM_consultar.addActionListener(this);
       this.moneda.btnM_modificar.addActionListener(this);
       this.moneda.btnM_actualizar.addActionListener(this);
       this.moneda.btnM_anular.addActionListener(this);
   } 
        
        public Controlador(Modelo m,FSOSEGTABFRM015 localizacion)
   {
       this.m = m;
       this.localizacion = localizacion; 
       this.localizacion.btnl_salir.addActionListener(this);
       this.localizacion.btnl_insertar.addActionListener(this);
       this.localizacion.btnl_guardar.addActionListener(this);
       this.localizacion.btnl_consultar.addActionListener(this);
       this.localizacion.btnl_modificar.addActionListener(this);
       this.localizacion.btnl_actualizar.addActionListener(this);
       this.localizacion.btnl_anular.addActionListener(this);
   }
  
         public Controlador(Modelo m,FSOSEGTABFRM017 usuario)
   {
       this.m = m;
       this.usuario = usuario; 
       this.usuario.btnUS_salir.addActionListener(this);
       this.usuario.btnUS_insertar.addActionListener(this);
       this.usuario.btnUS_guardar.addActionListener(this);
       this.usuario.btnUS_consultar.addActionListener(this);
       this.usuario.btnUS_modificar.addActionListener(this);
       this.usuario.btnUS_actualizar.addActionListener(this);
       this.usuario.btnUS_anular.addActionListener(this);
   }
      
        public Controlador(Modelo m,FSOSEGTABFRM018 dia_feriado)
   {
       this.m = m;
       this.dia_feriado = dia_feriado; 
       this.dia_feriado.btnDF_salir.addActionListener(this);
       this.dia_feriado.btnDF_insertar.addActionListener(this);
       this.dia_feriado.btnDF_guardar.addActionListener(this);
       this.dia_feriado.btnDF_consultar.addActionListener(this);
       this.dia_feriado.btnDF_modificar.addActionListener(this);
       this.dia_feriado.btnDF_actualizar.addActionListener(this);
       this.dia_feriado.btnDF_anular.addActionListener(this);
   }
        
        public Controlador(Modelo m,FSOSEGTABFRM020 contabilidad)
   {
       this.m = m;
       this.contabilidad = contabilidad; 
       this.contabilidad.btnC_salir.addActionListener(this);
       this.contabilidad.btnC_insertar.addActionListener(this);
       this.contabilidad.btnC_guardar.addActionListener(this);
       this.contabilidad.btnC_consultar.addActionListener(this);
       this.contabilidad.btnC_modificar.addActionListener(this);
       this.contabilidad.btnC_actualizar.addActionListener(this);
       this.contabilidad.btnC_anular.addActionListener(this);
   }

    @Override
    public void actionPerformed (ActionEvent e){
/******************* ACTION PERFORMED DE LA TABLA PERSONA 001 *****************/    
        /* if(persona.btnPS_insertar ==e.getSource()){
             try{
             m.Limpiar_Tab_Persona();
             m.Desbloquear_Tab_Persona();
             persona.btnPS_guardar.setEnabled(true);
             persona.btnPS_anular.setEnabled(false);
             persona.btnPS_modificar.setEnabled(false);
             persona.btnPS_consultar.setEnabled(false);
             persona.txtPS_buscar.setEnabled(false);
//             persona.txtPS_Fecha_Activación.setEnabled(false);
         //    persona.txtPS_Fecha_Actualización.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar"+ex);
              }
         }else if(persona.btnPS_guardar ==e.getSource()){
              try { 
                 m.Tabla_Persona_Insertar();
                // GlobalConstants.getAuditoria().setLog("I");
                 m.Limpiar_Tab_Persona();
                persona.btnPS_anular.setEnabled(true);
                persona.btnPS_modificar.setEnabled(true);
                persona.btnPS_consultar.setEnabled(true);
                persona.btnPS_guardar.setEnabled(false);
                m.Bloquear_Tab_Persona();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(persona.btnPS_consultar ==e.getSource()){
              try {
                 m.Tabla_Persona_Mostrar();
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(persona.btnPS_modificar ==e.getSource()){
             try {
                 m.Tabla_Persona_Modificar();
              //   persona.txtPS_Fecha_Activación.setEnabled(false);
                // persona.txtPS_Fecha_Actualización.setEnabled(false);
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(persona.btnPS_actualizar ==e.getSource()){
              try {
                 // GlobalConstants.getAuditoria().setLog("U");
                 m.Tabla_Persona_Actualizar();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         }else if(persona.btnPS_anular ==e.getSource()){
              try {
                boolean result=m.Tabla_Persona_Mostrar();
                    if(result){  
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular este registro?");
                             if(opt==JOptionPane.OK_OPTION){
                                   Modelo m=new Modelo();
           try {
               m.Tabla_Persona_Anular();
               m.Limpiar_Tab_Persona(); 
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM001.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }
               m.Limpiar_Tab_Persona();      
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
                                       }     
/******************** ACTION PERFORMED DE LA TABLA PRODUCTO 002 ***************/   
        //  }else*/ 
    /*         if(producto.btnPT_insertar ==e.getSource()){
            try {
          m.Limpiar_Tab_Producto();
          m.Desbloquear_Tab_Producto();
          producto.btnPT_guardar.setEnabled(true);
          producto.btnPT_anular.setEnabled(false);
          producto.btnPT_modificar.setEnabled(false);
          producto.btnPT_consultar.setEnabled(false);
          producto.txtPT_buscar.setEnabled(false);    
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
        } else  if(producto.btnPT_guardar ==e.getSource()){
              try { 
                  m.Tabla_Producto_Insertar();
                  m.Limpiar_Tab_Producto();
                  producto.btnPT_anular.setEnabled(true);
                  producto.btnPT_modificar.setEnabled(true);
                  producto.btnPT_consultar.setEnabled(true);
                  producto.btnPT_guardar.setEnabled(false);
                  m.Bloquear_Tab_Producto(); 
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(producto.btnPT_consultar ==e.getSource()){
              try {
                 m.Tabla_Producto_Mostrar();
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(producto.btnPT_modificar ==e.getSource()){
              try {
                 m.Tabla_Producto_Modificar();
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(producto.btnPT_actualizar ==e.getSource()){
              try {
                 m.Tabla_Producto_Actualizar();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         }else if(producto.btnPT_anular ==e.getSource()){
              try {
                boolean result=m.Tabla_Producto_Mostrar();
                    if(result){
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular este registro?");
                            if(opt==JOptionPane.OK_OPTION){
                                 Modelo m=new Modelo();
           try {
               m.Tabla_Producto_Anular();
               m.Limpiar_Tab_Producto();
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM002.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }
               m.Limpiar_Tab_Producto();     
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
                                       }
         }  
/**************** ACTION PERFORMED DE LA TABLA ASIENTO CONTABLE 003 ***********/  
       //else 
       if(asiento.btnAC_insertar ==e.getSource()){
            try{
          m.Limpiar_Tab_Asiento();
          m.Desbloquear_Tab_Asiento();
          asiento.btnAC_guardar.setEnabled(true);
          asiento.btnAC_anular.setEnabled(false);
          asiento.btnAC_modificar.setEnabled(false);
          asiento.btnAC_consultar.setEnabled(false);
          asiento.txtAC_buscar.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
        }else  if(asiento.btnAC_guardar ==e.getSource()){
              try { 
                  m.Tabla_Asiento_Insertar();
                  m.Limpiar_Tab_Asiento();
                  asiento.btnAC_anular.setEnabled(true);
                  asiento.btnAC_modificar.setEnabled(true);
                  asiento.btnAC_consultar.setEnabled(true);
                  asiento.btnAC_guardar.setEnabled(false);
                  m.Bloquear_Tab_Asiento();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(asiento.btnAC_consultar ==e.getSource()){
              try {
                 m.Tabla_Asiento_Mostrar(); 
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(asiento.btnAC_modificar ==e.getSource()){
              try {
                 m.Tabla_Asiento_Modificar();
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(asiento.btnAC_actualizar ==e.getSource()){
              try {
                 m.Tabla_Asiento_Actualizar();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         }else if(asiento.btnAC_anular ==e.getSource()){
              try { 
                boolean result=m.Tabla_Asiento_Mostrar();
                    if(result){  
                         int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular este registro?");
                            if(opt==JOptionPane.OK_OPTION)  {
                                Modelo m=new Modelo();
           try {
               m.Tabla_Asiento_Anular();
               m.Limpiar_Tab_Asiento();
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM003.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }
                m.Limpiar_Tab_Asiento();     
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
                                       }
         }  
/******************* ACTION PERFORMED DE LA TABLA PERFIL 005 ******************/  
        else if(perfil.btnPF_insertar ==e.getSource()){
            try{
          m.Limpiar_Tab_Perfil();
          m.Desbloquear_Tab_Perfil();
          perfil.btnPF_guardar.setEnabled(true);
          perfil.btnPF_anular.setEnabled(false);
          perfil.btnPF_consultar.setEnabled(false);
          perfil.txtPF_buscar.setEnabled(false);
          perfil.btnPF_activar.setEnabled(false);
          perfil.btnPF_refrescar.setEnabled(false);
          perfil.btnPF_acciones.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
        }else if(perfil.btnPF_guardar ==e.getSource()){
              try { 
                  m.Tabla_Perfil_Insertar();
                  m.Limpiar_Tab_Perfil();
                  perfil.btnPF_anular.setEnabled(true);
                  perfil.btnPF_consultar.setEnabled(true);
                  perfil.btnPF_guardar.setEnabled(false);
                  perfil.btnPF_activar.setEnabled(true);
                  perfil.btnPF_refrescar.setEnabled(true);
                  perfil.btnPF_acciones.setEnabled(true);
                  perfil.btnPF_anular.setEnabled(true);
                  m.Bloquear_Tab_Perfil();
                  m.Tabla_Perfil_Mostrar();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(perfil.btnPF_consultar ==e.getSource()){
              try {
                 m.Tabla_Perfil_Mostrar(); 
                 perfil.txtPF_buscar.setText("");
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
              }else if(perfil.btnPF_refrescar ==e.getSource()){
              try {
                 m.Tabla_Perfil_Mostrar(); 
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
              }else if(perfil.btnPF_acciones==e.getSource()){
                 FSOSEGTABFRM006 t_perfil = new FSOSEGTABFRM006(); 
                 Modelo m = new Modelo();
                 m.view6(t_perfil);
                 Controlador c = new Controlador(m, t_perfil); 
              }else if(perfil.btnPF_anular ==e.getSource()){
              try {
                boolean result=m.Tabla_Perfil_Mostrar();
                     if(result){
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular este Perfil?");
                            if(opt==JOptionPane.OK_OPTION){
                                Modelo m=new Modelo();
           try {
               m.Tabla_Perfil_Anular();
               m.Limpiar_Tab_Perfil();
               m.Tabla_Perfil_Mostrar();
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM005.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
                                       }
         }else if(perfil.btnPF_activar ==e.getSource()){
              try {
                boolean result=m.Tabla_Perfil_Mostrar();
                     if(result){
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea Activar este Perfil?");
                            if(opt==JOptionPane.OK_OPTION){
                                Modelo m=new Modelo();
           try {
               m.Tabla_Perfil_Activar();
               m.Limpiar_Tab_Perfil();
               m.Tabla_Perfil_Mostrar();
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM005.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }
                m.Limpiar_Tab_Perfil();       
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
                                       }
         }     
/****************** ACTION PERFORMED DE LA TABLA TIPO PERFIL 006 **************/  
            else if(t_perfil.btnTP_consultar ==e.getSource()){
              try {
                 m.Tabla_T_T_Perfil_Mostrar(); 
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(t_perfil.btnTP_guardar_permisos ==e.getSource()){
              try {
                m.Tabla_T_T_Perfil_Agregar_Permisos();
                JOptionPane.showMessageDialog(null, "Perfil Actualizado" );
                m.Limpiar_Tab_T_T_Perfil();
            } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(t_perfil.btnTP_cancelar ==e.getSource()){
              try {
                 m.Tabla_T_T_Perfil_Mostrar(); 
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
/************ ACTION PERFORMED DE LA TABLA TIPO DE TRANSACCIONES 007 **********/  
        } else if(t_trans.btnTT_insertar ==e.getSource()){
            DefaultTableModel modo = new DefaultTableModel();
            FSOSEGTABFRM007.Tabla_tipo_transacciones.setModel(modo);
            try{
          m.Limpiar_Tab_Tipo_Transaccion();
          m.Desbloquear_Tab_Tipo_Transaccion();
          t_trans.btnTT_guardar.setEnabled(true);
          t_trans.btnTT_anular.setEnabled(false);
          t_trans.btnTT_modificar.setEnabled(false);
          t_trans.btnTT_consultar.setEnabled(false);
          t_trans.txtTT_buscar.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
        }else  if(t_trans.btnTT_guardar ==e.getSource()){
              try { 
                  m.Tabla_Tipo_Transaccion_Insertar();
                  m.Limpiar_Tab_Tipo_Transaccion();
                  t_trans.btnTT_anular.setEnabled(true);
                  t_trans.btnTT_modificar.setEnabled(true);
                  t_trans.btnTT_consultar.setEnabled(true);
                  t_trans.btnTT_guardar.setEnabled(false);
                  m.Bloquear_Tab_Tipo_Transaccion();
                  m.Tabla_Tipo_Transaccion_Mostrar_Modo();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(t_trans.btnTT_consultar ==e.getSource()){
              try {
                 m.Tabla_Tipo_Transaccion_Mostrar(); 
                 t_trans.txtTT_buscar.setText("");
             } catch (SQLException ex) {
     Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
              }else if(t_trans.btnTT_refrescar ==e.getSource()){
              try {
                 m.Tabla_Tipo_Transaccion_Mostrar_Modo();
             } catch (SQLException ex) {
     Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(t_trans.btnTT_modificar ==e.getSource()){
              try {
                 m.Tabla_Tipo_Transaccion_Modificar();
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(t_trans.btnTT_actualizar ==e.getSource()){
              try {
                 m.Tabla_Tipo_Transaccion_Actualizar();
                 m.Tabla_Tipo_Transaccion_Mostrar_Modo();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         } else if(t_trans.btnTT_anular ==e.getSource()){
              try {
                boolean result=m.Tabla_Tipo_Transaccion_Mostrar();
                    if(result) {
                         int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular este registro?");
                            if(opt==JOptionPane.OK_OPTION) {
                                 Modelo m=new Modelo();
           try {
               m.Tabla_Tipo__Transaccion_Anular();
               m.Limpiar_Tab_Tipo_Transaccion();
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM007.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }
               m.Limpiar_Tab_Tipo_Transaccion();     
               m.Tabla_Tipo_Transaccion_Mostrar_Modo();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
                                       }
         }         
 /************ ACTION PERFORMED DE LA TABLA PRODUCTO PERSONA 008  *************/  
         else if(p_persona.btnPP_salir ==e.getSource()){
            System.exit(0);
        } else if(p_persona.btnPP_insertar ==e.getSource()){
            try{
          m.Limpiar_Tab_Producto_Persona();
          m.Desbloquear_Tab_Producto_Persona();
          p_persona.btnPP_guardar.setEnabled(true);
          p_persona.btnPP_anular.setEnabled(false);
          p_persona.btnPP_modificar.setEnabled(false);
          p_persona.btnPP_consultar.setEnabled(false);
          p_persona.txtPP_buscar.setEnabled(false);
          p_persona.txtPP_fecha_proceso.setEnabled(false);
          p_persona.txtPP_fecha_actualizacion.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
        }else if(p_persona.btnPP_guardar ==e.getSource()){
              try { 
                  m.Tabla_Producto_Persona_Insertar();
                  m.Limpiar_Tab_Producto_Persona();
                  p_persona.btnPP_anular.setEnabled(true);
                  p_persona.btnPP_modificar.setEnabled(true);
                  p_persona.btnPP_consultar.setEnabled(true);
                  p_persona.btnPP_guardar.setEnabled(false);
                  m.Bloquear_Tab_Producto_Persona();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(p_persona.btnPP_consultar ==e.getSource()){
              try {
                 m.Tabla_Producto_Persona_Mostrar(); 
             } catch (SQLException ex) {
     Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(p_persona.btnPP_modificar ==e.getSource()){
              try {
                 m.Tabla_Producto_Persona_Modificar();
                 p_persona.txtPP_fecha_proceso.setEnabled(false);
                 p_persona.txtPP_fecha_actualizacion.setEnabled(false);
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(p_persona.btnPP_actualizar ==e.getSource()){
              try {
                 m.Tabla_Producto_Persona_Actualizar();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         }else if(p_persona.btnPP_anular ==e.getSource()){
              try {   
                boolean result=m.Tabla_Producto_Persona_Mostrar();
                     if(result){ 
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular este registro?");
                            if(opt==JOptionPane.OK_OPTION){
                                Modelo m=new Modelo();
           try {
               m.Tabla_Producto_Persona_Anular();
               m.Limpiar_Tab_Producto_Persona();
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM008.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }
               m.Limpiar_Tab_Producto_Persona();       
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
                                       }
         }
/************** ACTION PERFORMED DE LA TABLA CONFIGURACION 010 ****************/  
         else if(configuracion.btnC_salir ==e.getSource())
        {
            System.exit(0);
        }  else if(configuracion.btnC_insertar ==e.getSource()){
            try{
          m.Limpiar_Tab_Configuracion();
          m.Desbloquear_Tab_Configuracion();
          configuracion.btnC_guardar.setEnabled(true);
          configuracion.btnC_anular.setEnabled(false);
          configuracion.btnC_modificar.setEnabled(false);
          configuracion.btnC_consultar.setEnabled(false);
          configuracion.txtC_buscar.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
        }else if(configuracion.btnC_guardar ==e.getSource()){
              try { 
                  m.Tabla_Configuracion_Insertar();
                  m.Limpiar_Tab_Configuracion();
                  configuracion.btnC_anular.setEnabled(true);
                  configuracion.btnC_modificar.setEnabled(true);
                  configuracion.btnC_consultar.setEnabled(true);
                  configuracion.btnC_guardar.setEnabled(false);
                  m.Bloquear_Tab_Configuracion();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(configuracion.btnC_consultar ==e.getSource()){
              try {
                 m.Tabla_Configuracion_Mostrar();
             } catch (SQLException ex) {
                 Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(configuracion.btnC_modificar ==e.getSource()){
              try {
                 m.Tabla_Configuracion_Modificar();
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(configuracion.btnC_actualizar ==e.getSource()){
              try {
                 m.Tabla_Configuracion_Actualizar();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         }else if(configuracion.btnC_anular ==e.getSource()){
              try {
                boolean result=m.Tabla_Configuracion_Mostrar();
                    if(result){ 
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular este registro?");
                            if(opt==JOptionPane.OK_OPTION) {
                                Modelo m=new Modelo();
           try {
               m.Tabla_Configuracion_Anular();
               m.Limpiar_Tab_Configuracion();
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM010.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }
              m.Limpiar_Tab_Configuracion();       
           } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
                                       }
         }
/************* ACTION PERFORMED DE LA TABLA INSTALACION SISTEMA 011 ***********/  
        else if(instalacion.btnIS_insertar ==e.getSource()){
            try{
          m.Limpiar_Tab_Instalacion();
          m.Desbloquear_Tab_Instalacion();
          instalacion.btnIS_guardar.setEnabled(true);
          instalacion.btnIS_anular.setEnabled(false);
          instalacion.btnIS_modificar.setEnabled(false);
          instalacion.btnIS_consultar.setEnabled(false);
          instalacion.txtIS_buscar.setEnabled(false);
          instalacion.txtIS_fecha_instalacion.setEnabled(false);
          instalacion.txtIS_fecha_actualizacion.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
        }else if(instalacion.btnIS_guardar ==e.getSource()){
              try { 
                  m.Tabla_Instalacion_Insertar();
                  m.Limpiar_Tab_Instalacion();
                  instalacion.btnIS_anular.setEnabled(true);
                  instalacion.btnIS_modificar.setEnabled(true);
                  instalacion.btnIS_consultar.setEnabled(true);
                  instalacion.btnIS_guardar.setEnabled(false);
                  m.Bloquear_Tab_Instalacion();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
        }else if(instalacion.btnIS_consultar ==e.getSource()){
              try {
                 m.Tabla_Instalacion_Mostrar();
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         } else if(instalacion.btnIS_modificar ==e.getSource()){
              try {
                 m.Tabla_Instalacion_Modificar();
                 instalacion.txtIS_fecha_instalacion.setEnabled(false);
                 instalacion.txtIS_fecha_actualizacion.setEnabled(false);
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(instalacion.btnIS_actualizar ==e.getSource()){
              try {
                 m.Tabla_Instalacion_Actualizar();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         }else if(instalacion.btnIS_anular ==e.getSource()){
              try {
                boolean result=m.Tabla_Instalacion_Mostrar();
                    if(result){
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular este registro?");
                            if(opt==JOptionPane.OK_OPTION){
                                Modelo m=new Modelo();
           try {
               m.Tabla_Instalacion_Anular();
               m.Limpiar_Tab_Instalacion();
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM011.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }
             m.Limpiar_Tab_Instalacion();         
           } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
                                       }
         } 
/*************** ACTION PERFORMED DE LA TABLA TRANSACCIONES 012 ***************/  
         else if(transaccion.btnT_insertar ==e.getSource()){
            try{
          m.Limpiar_Tab_Transaccion();
          m.Desbloquear_Tab_Transaccion();
          transaccion.btnT_guardar.setEnabled(true);
          transaccion.btnT_anular.setEnabled(false);
          transaccion.btnT_modificar.setEnabled(false);
          transaccion.btnT_consultar.setEnabled(false);
          transaccion.txtT_buscar.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
        }else  if(transaccion.btnT_guardar ==e.getSource()){
              try { 
                  m.Tabla_Transaccion_Insertar();
                  m.Limpiar_Tab_Transaccion();
                  transaccion.btnT_anular.setEnabled(true);
                  transaccion.btnT_modificar.setEnabled(true);
                  transaccion.btnT_consultar.setEnabled(true);
                  transaccion.btnT_guardar.setEnabled(false);
                  m.Bloquear_Tab_Transaccion();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(transaccion.btnT_consultar ==e.getSource()){
              try {
                 m.Tabla_Transaccion_Mostrar();
             } catch (SQLException ex) {
                 Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(transaccion.btnT_modificar ==e.getSource()){
              try {
                 m.Tabla_Transaccion_Modificar();
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(transaccion.btnT_actualizar ==e.getSource()){
              try {
                 m.Tabla_Transaccion_Actualizar();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         }else if(transaccion.btnT_anular ==e.getSource()){
              try {
                boolean result=m.Tabla_Transaccion_Mostrar();
                    if(result){
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular este registro?");
                            if(opt==JOptionPane.OK_OPTION){
                                Modelo m=new Modelo();
           try {
               m.Tabla_Transaccion_Anular();
               m.Limpiar_Tab_Transaccion();
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM012.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }
            m.Limpiar_Tab_Transaccion();        
           } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
                                       }
         }
/*************** ACTION PERFORMED DE LA TABLA PLAN CONTABLE 013 ***************/
         else if(plan_contable.btnPC_insertar ==e.getSource()){
             DefaultTableModel modo = new DefaultTableModel();
             FSOSEGTABFRM013.Tabla_plan_contable.setModel(modo);
             try{
             m.Limpiar_Tab_Plan_Contable();
             m.Desbloquear_Tab_Plan_Contable();
             plan_contable.btnPC_guardar.setEnabled(true);
             plan_contable.btnPC_anular.setEnabled(false);
             plan_contable.btnPC_modificar.setEnabled(false);
             plan_contable.btnPC_consultar.setEnabled(false);
             plan_contable.txtPC_buscar.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar"+ex);
              }
         }else if(plan_contable.btnPC_guardar ==e.getSource()){
              try { 
                 m.Tabla_Plan_Contable_Insertar();
                 m.Limpiar_Tab_Plan_Contable();
                 plan_contable.btnPC_anular.setEnabled(true);
                 plan_contable.btnPC_modificar.setEnabled(true);
                 plan_contable.btnPC_consultar.setEnabled(true);
                 plan_contable.btnPC_guardar.setEnabled(false);
                 m.Bloquear_Tab_Plan_Contable();
                 m.Tabla_Plan_Contable_Mostrar_Modo();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(plan_contable.btnPC_consultar ==e.getSource()){
              try {
                 m.Tabla_Plan_Contable_Mostrar();
                 plan_contable.txtPC_buscar.setText("");
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(plan_contable.btnPC_refrescar ==e.getSource()){
              try {
                 m.Tabla_Plan_Contable_Mostrar_Modo();
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(plan_contable.btnPC_modificar ==e.getSource()){
             try {
                 m.Tabla_Plan_Contable_Modificar();
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(plan_contable.btnPC_actualizar ==e.getSource()){
              try {
                 m.Tabla_Plan_Contable_Actualizar();
                 m.Tabla_Plan_Contable_Mostrar_Modo();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         }else if(plan_contable.btnPC_anular ==e.getSource()){
              try {
                boolean result=m.Tabla_Plan_Contable_Mostrar();
                    if(result){  
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular este registro?");
                             if(opt==JOptionPane.OK_OPTION){
                                   Modelo m=new Modelo();
           try {
               m.Tabla_Plan_Contable_Anular();
               m.Limpiar_Tab_Plan_Contable();
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM013.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           } 
              m.Limpiar_Tab_Plan_Contable();
              m.Tabla_Plan_Contable_Mostrar_Modo();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
             }
         }  
/****************** ACTION PERFORMED DE LA TABLA USUARIO 017 ******************/
         else if(usuario.btnUS_insertar ==e.getSource()){
             try{
             m.Limpiar_Tab_Usuario();
             m.Desbloquear_Tab_Usuario();
             usuario.btnUS_guardar.setEnabled(true);
             usuario.btnUS_anular.setEnabled(false);
             usuario.btnUS_modificar.setEnabled(false);
             usuario.btnUS_consultar.setEnabled(false);
             usuario.txtUS_buscar.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar"+ex);
              }
         }else if(usuario.btnUS_guardar ==e.getSource()){
              try { 
                 m.Tabla_Usuario_Insertar();
                 m.Limpiar_Tab_Usuario();
                 usuario.btnUS_anular.setEnabled(true);
                 usuario.btnUS_modificar.setEnabled(true);
                 usuario.btnUS_consultar.setEnabled(true);
                 usuario.btnUS_guardar.setEnabled(false);
                 m.Bloquear_Tab_Usuario();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(usuario.btnUS_consultar ==e.getSource()){
              try {
                 m.Tabla_Usuario_Mostrar();
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(usuario.btnUS_modificar ==e.getSource()){
             try {
                 m.Tabla_Usuario_Modificar();
                 } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(usuario.btnUS_actualizar ==e.getSource()){
              try {
                 m.Tabla_Usuario_Actualizar();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         }else if(usuario.btnUS_anular ==e.getSource()){
              try {
                boolean result=m.Tabla_Usuario_Mostrar();
                    if(result){  
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular este usuario?");
                             if(opt==JOptionPane.OK_OPTION){
                                   Modelo m=new Modelo();
           try {
               m.Tabla_Usuario_Anular();
               m.Limpiar_Tab_Usuario(); 
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM017.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }  
              m.Limpiar_Tab_Usuario();      
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
             }
         }       
/****************** ACTION PERFORMED DE LA TABLA USUARIO 017 ******************/
         else if(moneda.btnM_insertar ==e.getSource()){
             try{
             m.Limpiar_Tab_Moneda();
             m.Desbloquear_Tab_Moneda();
             moneda.btnM_guardar.setEnabled(true);
             moneda.btnM_anular.setEnabled(false);
             moneda.btnM_modificar.setEnabled(false);
             moneda.btnM_consultar.setEnabled(false);
             moneda.txtM_buscar.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar"+ex);
              }
         }else if(moneda.btnM_guardar ==e.getSource()){
              try { 
                 m.Tabla_Moneda_Insertar();
                 m.Limpiar_Tab_Moneda();
                 moneda.btnM_anular.setEnabled(true);
                 moneda.btnM_modificar.setEnabled(true);
                 moneda.btnM_consultar.setEnabled(true);
                 moneda.btnM_guardar.setEnabled(false);
                 m.Bloquear_Tab_Moneda();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(moneda.btnM_consultar ==e.getSource()){
              try {
                 m.Tabla_Moneda_Mostrar();
                 moneda.txtM_buscar.setText("");
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(moneda.btnM_modificar ==e.getSource()){
             try {
                 m.Tabla_Moneda_Modificar();
                 } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(moneda.btnM_actualizar ==e.getSource()){
              try {
                 m.Tabla_Moneda_Actualizar();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         }else if(moneda.btnM_anular ==e.getSource()){
              try {
                boolean result=m.Tabla_Moneda_Mostrar();
                    if(result){  
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular esta Moneda?");
                             if(opt==JOptionPane.OK_OPTION){
                                   Modelo m=new Modelo();
           try {
               m.Tabla_Moneda_Anular();
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM017.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }  
                m.Limpiar_Tab_Moneda();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
             }
         }  
 /************** ACTION PERFORMED DE LA TABLA DIAS FERIADOS 018 ***************/
         else if(dia_feriado.btnDF_insertar==e.getSource()){
             try{
             m.Limpiar_Tab_dia_feriado(); 
             m.Desbloquear_Tab_dia_feriado(); 
             dia_feriado.btnDF_guardar.setEnabled(true);
             dia_feriado.btnDF_anular.setEnabled(false);
             dia_feriado.btnDF_modificar.setEnabled(false);
             dia_feriado.btnDF_consultar.setEnabled(false);
             dia_feriado.txtDF_buscar.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar"+ex);
              }
         }else if(dia_feriado.btnDF_guardar ==e.getSource()){
              try { 
                 m.Tabla_dia_feriado_Insertar();
                 m.Limpiar_Tab_dia_feriado();
                 dia_feriado.btnDF_anular.setEnabled(true);
                 dia_feriado.btnDF_modificar.setEnabled(true);
                 dia_feriado.btnDF_consultar.setEnabled(true);
                 dia_feriado.btnDF_guardar.setEnabled(false);
                 m.Bloquear_Tab_dia_feriado();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(dia_feriado.btnDF_consultar ==e.getSource()){
              try {
                 m.Tabla_dia_feriado_Mostrar();
                 dia_feriado.txtDF_buscar.setText("");
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(dia_feriado.btnDF_modificar ==e.getSource()){
             try {
                 m.Tabla_dia_feriado_Modificar();
                 } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(dia_feriado.btnDF_actualizar ==e.getSource()){
              try {
                 m.Tabla_dia_feriado_Actualizar();
                 dia_feriado.txtDF_buscar.setText("");
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         }else if(dia_feriado.btnDF_anular ==e.getSource()){
              try {
                boolean result=m.Tabla_dia_feriado_Mostrar();
                    if(result){  
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular esta Fecha?");
                             if(opt==JOptionPane.OK_OPTION){
                                   Modelo m=new Modelo();
           try {
               m.Tabla_dia_feriado_Anular();
               dia_feriado.txtDF_buscar.setText("");
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM017.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }  
                m.Limpiar_Tab_dia_feriado();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
             }
         }      
/************** ACTION PERFORMED DE LA TABLA DIAS FERIADOS 018 ***************/
         else if(localizacion.btnl_insertar==e.getSource()){
             try{
             m.Limpiar_Tab_Localizacion(); 
             m.Desbloquear_Tab_Localizacion(); 
             localizacion.btnl_guardar.setEnabled(true);
             localizacion.btnl_anular.setEnabled(false);
             localizacion.btnl_modificar.setEnabled(false);
             localizacion.btnl_consultar.setEnabled(false);
             localizacion.txtl_buscar.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar"+ex);
              }
         }else if(localizacion.btnl_guardar ==e.getSource()){
              try { 
                 m.Tabla_Localizacion_Insertar();
                 m.Limpiar_Tab_Localizacion();
                 localizacion.btnl_anular.setEnabled(true);
                 localizacion.btnl_modificar.setEnabled(true);
                 localizacion.btnl_consultar.setEnabled(true);
                 localizacion.btnl_guardar.setEnabled(false);
                 m.Bloquear_Tab_Localizacion();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(localizacion.btnl_consultar ==e.getSource()){
              try {
                 m.Tabla_Localizacion_Mostrar();
                 localizacion.txtl_buscar.setText("");
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(localizacion.btnl_modificar ==e.getSource()){
             try {
                 m.Tabla_Localizacion_Modificar();
                 } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(localizacion.btnl_actualizar ==e.getSource()){
              try {
                 m.Tabla_Localizacion_Actualizar();
                 localizacion.txtl_buscar.setText("");
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         }else if(localizacion.btnl_anular ==e.getSource()){
              try {
                boolean result=m.Tabla_Localizacion_Mostrar();
                    if(result){  
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular este registro?");
                             if(opt==JOptionPane.OK_OPTION){
                                   Modelo m=new Modelo();
           try {
               m.Tabla_Localizacion_Anular();
               localizacion.txtl_buscar.setText("");
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM017.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }  
                m.Limpiar_Tab_Localizacion();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
             }
         } 
/************** ACTION PERFORMED DE LA TABLA DIAS FERIADOS 009 ***************/
         else if(calculo.btnCC_insertar==e.getSource()){
             DefaultTableModel modo = new DefaultTableModel();
             FSOSEGTABFRM009.Tabla_calculo_comisiones.setModel(modo);
             try{
             m.Limpiar_Tab_Calculo(); 
             m.Desbloquear_Tab_Calculo(); 
             calculo.btnCC_guardar.setEnabled(true);
             calculo.btnCC_anular.setEnabled(false);
             calculo.btnCC_modificar.setEnabled(false);
             calculo.btnCC_consultar.setEnabled(false);
             calculo.txtCC_buscar.setEnabled(false);
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar"+ex);
              }
         }else if(calculo.btnCC_guardar ==e.getSource()){
              try { 
                 m.Tabla_Calculo_Insertar();
                 m.Limpiar_Tab_Calculo();
                 calculo.btnCC_anular.setEnabled(true);
                 calculo.btnCC_modificar.setEnabled(true);
                 calculo.btnCC_consultar.setEnabled(true);
                 calculo.btnCC_guardar.setEnabled(false);
                 m.Bloquear_Tab_Calculo();
                 m.Tabla_Calculo_Comisiones_Mostrar_Modo();
              } catch (SQLException ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar "+ex);
              }
         }else if(calculo.btnCC_consultar ==e.getSource()){
              try {
                 m.Tabla_Calculo_Mostrar();
                 calculo.txtCC_buscar.setText("");
             } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
              }else if(calculo.btnCC_refrescar ==e.getSource()){
              try {
                 m.Tabla_Calculo_Comisiones_Mostrar_Modo();
             } catch (SQLException ex) {
     Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(calculo.btnCC_modificar ==e.getSource()){
             try {
                 m.Tabla_Calculo_Modificar();
                 } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
              }
         }else if(calculo.btnCC_actualizar ==e.getSource()){
              try {
                 m.Tabla_Calculo_Actualizar();
                 m.Tabla_Calculo_Comisiones_Mostrar_Modo();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar! "+ex);
              }
         }else if(calculo.btnCC_anular ==e.getSource()){
              try {
                boolean result=m.Tabla_Calculo_Mostrar();
                    if(result){  
                        int opt=JOptionPane.showConfirmDialog(null,"¿Está Seguro que desea anular este registro?");
                             if(opt==JOptionPane.OK_OPTION){
                                   Modelo m=new Modelo();
           try {
               m.Tabla_Calculo_Anular();
               m.Limpiar_Tab_Calculo();
           } catch (SQLException ex) {
               Logger.getLogger(FSOSEGTABFRM017.class.getName()).log(Level.SEVERE, null, ex);
           }
           }
           }  
                m.Limpiar_Tab_Calculo();
                m.Tabla_Calculo_Comisiones_Mostrar_Modo();
             } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "No se pudo Anular! "+ex);
             }
         }
          
          /* ACTION PERFORMED TABLA CONTABILIDAD 020*/
          
         else if(contabilidad.btnC_insertar ==e.getSource()){
             try{
            m.Limpiar_Tab_Contabilidad();
            m.Desbloquear_Tab_Contabilidad();
            contabilidad.btnC_guardar.setEnabled(true);
            contabilidad.btnC_anular.setEnabled(false);
            contabilidad.btnC_modificar.setEnabled(false);
            contabilidad.btnC_consultar.setEnabled(false);
            contabilidad.txtC_buscar.setEnabled(false);
            
         }catch (Exception ex) {
                  JOptionPane.showMessageDialog(null, "No se pudo Insertar"+ex);
              }
       }else if(contabilidad.btnC_guardar ==e.getSource()){
                // m.Tabla_Calculo_Insertar();
                m.Limpiar_Tab_Contabilidad();
                contabilidad.btnC_anular.setEnabled(true);
                contabilidad.btnC_modificar.setEnabled(true);
                contabilidad.btnC_consultar.setEnabled(true);
                contabilidad.btnC_guardar.setEnabled(false);
                m.Bloquear_Tab_Contabilidad();
         }
    }

    public boolean isError() {
        return m.esExito();
    }
}
