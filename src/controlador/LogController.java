/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import com.bimodal.app.db.DataModel;
import com.bimodal.app.beans.Principal;

/**
 *
 * @author usuario1
 */
public class LogController{
    private ResultSet bitacoras;
    private DataModel dat;
    private JTable table;
       public LogController(){
           dat=Principal.getModel();
           DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
           Date date=new Date();
           String inicio=dateFormat.format(date);
           String fin=dateFormat.format(date);
           bitacoras=dat.findLog(inicio,fin);
       
       }
       public void LoadRecords(JTable table){
           this.table=table;
          DefaultTableModel model=(DefaultTableModel) table.getModel();
          TableColumnModel tcm = table.getColumnModel();
            table.setFillsViewportHeight(true);
            table.setPreferredSize(new Dimension(1000,1000));
              if(bitacoras==null)
                  return;
        try {
               int columnsCount=bitacoras.getMetaData().getColumnCount();
               
              while(bitacoras.next()){
                   Object[] obj=new Object[columnsCount];
                   int count=1;
                   for(int i=0; i<(columnsCount); i++){
                        
                       obj[i]=bitacoras.getObject(i+1);
                       //tcm.getColumn(i-1).setMinWidth(60);
                       tcm.getColumn(i).setMaxWidth((count++)*100);
                       //tcm.getColumn(i-1).setWidth(1);
                   }
                     tcm.getColumn(6).setMaxWidth(20000);
                 model.addRow(obj);
              }   
              
        } catch (SQLException ex) {
            Logger.getLogger(LogController.class.getName()).log(Level.SEVERE, null, ex);
        }
          table.setModel(model);
          
       }
       public void filter(Date inicio,Date fin){
             DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
             Date date=new Date();
             JFrame fr=new JFrame();
            // String inicioF=dateFormat.format(inicio);
          //   JOptionPane.showMessageDialog(fr, "Fecha:"+inicioF, "lol", 0);
           bitacoras=dat.findLog(dateFormat.format(inicio),dateFormat.format(fin));
           DefaultTableModel model=(DefaultTableModel) table.getModel();
           model.setRowCount(0);
          TableColumnModel tcm = table.getColumnModel();
            table.setFillsViewportHeight(true);
            table.setPreferredSize(new Dimension(1000,1000));
        try {
               int columnsCount=bitacoras.getMetaData().getColumnCount();
               
              while(bitacoras.next()){
                   Object[] obj=new Object[columnsCount];
                   int count=1;
                   for(int i=0; i<(columnsCount); i++){
                        
                       obj[i]=bitacoras.getObject(i+1);
                       //tcm.getColumn(i-1).setMinWidth(60);
                       tcm.getColumn(i).setMaxWidth((count++)*100);
                       //tcm.getColumn(i-1).setWidth(1);
                   }
                     tcm.getColumn(6).setMaxWidth(20000);
                 model.addRow(obj);
              }   
              
        } catch (SQLException ex) {
            Logger.getLogger(LogController.class.getName()).log(Level.SEVERE, null, ex);
        }
          table.setModel(model); 
       
       
       }

    
    
}
