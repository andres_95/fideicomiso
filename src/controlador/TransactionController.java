/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.bimodal.app.conf.GlobalConstants;
import com.bimodal.app.db.TransactionModel;
import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author usuario1
 */
public class TransactionController {
    ResultSet rs;
    TransactionModel modelo;
    private JTable table;
         public TransactionController(JTable table){
              rs=null;
              this.table=table;
            modelo=new TransactionModel(GlobalConstants.getConnector());
         }
         public void LoadRecords(Date inicioF,Date finF){
            DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
           Date date=new Date();
           String inicio=dateFormat.format(inicioF);
           String fin=dateFormat.format(finF);
           ResultSet transacciones=modelo.LoadTransactionList(inicio, fin);
              
                DefaultTableModel model=(DefaultTableModel) table.getModel();
          TableColumnModel tcm = table.getColumnModel();
            table.setFillsViewportHeight(true);
            table.setPreferredSize(new Dimension(1000,1000));
        try {
               int columnsCount=transacciones.getMetaData().getColumnCount();
               
              while(transacciones.next()){
                   Object[] obj=new Object[columnsCount];
                   int count=1;
                   for(int i=0; i<(columnsCount); i++){
                        
                       obj[i]=transacciones.getObject(i+1);
                       //tcm.getColumn(i-1).setMinWidth(60);
                       tcm.getColumn(i).setMaxWidth((count++)*100);
                       //tcm.getColumn(i-1).setWidth(1);
                   }
                     tcm.getColumn(6).setMaxWidth(20000);
                 model.addRow(obj);
              }   
              
        } catch (SQLException ex) {
            Logger.getLogger(LogController.class.getName()).log(Level.SEVERE, null, ex);
        }
          table.setModel(model);
           
         }
       
         
      
    
}
