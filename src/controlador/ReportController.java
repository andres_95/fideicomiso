/*
 * Nombre Objeto: ReportController
 * Autor: Hetsy Rodríguez
 * Fecha Elab: 10/02/2017
 * Descripción: Controlador para generar los reportes
 * Última Modificación:
 * Autor Última Modificación:
 */
package controlador;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.bimodal.app.beans.Participante;
import com.bimodal.app.beans.Participante;
import com.bimodal.app.db.ParticipantesDatasource;
import com.bimodal.app.db.ReportModel;
import com.bimodal.app.db.ReportModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import vista.FSOSEG_MENU_FRAME;

public class ReportController {
    private ReportModel rep;
    private  ParticipantesDatasource datasource;
    public ReportController(ReportModel rep){
        this.rep=rep;
    }
                
    public void BuildReport(){
        datasource = new ParticipantesDatasource();  
        Participante p; 
        float num;
        try {
            ResultSet rs=rep.extract();
            while(rs.next()){
                num = rs.getFloat(6);
                num = (float) (num+100.52);
                p = new Participante(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),String.valueOf(num));
                datasource.addParticipante(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
                   
    }
    
    public void ShowReport(){
        JasperReport reporte= null;
        String ruta = "C:\\Users\\usuario1\\Documents\\NetBeansProjects\\Fideicomiso\\src\\reportes\\report4.jasper";
        File archivo = new File(ruta);    
        try {
            reporte = (JasperReport) JRLoader.loadObject(archivo);
        } catch (JRException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JasperPrint print = null;
        try {
            print = JasperFillManager.fillReport(reporte,null,datasource);
        } catch (JRException ex) {
            Logger.getLogger(FSOSEG_MENU_FRAME.class.getName()).log(Level.SEVERE, null, ex);
        }
     
        JasperViewer ver= new JasperViewer(print,false);
        ver.setTitle("Prueba");
        ver.setVisible(true);         
    }
}
