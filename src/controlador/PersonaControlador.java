/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.bimodal.app.conf.GlobalConstants;
import com.bimodal.app.db.PersonaDatasource;
import com.toedter.calendar.JDateChooser;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author usuario1
 */
public class PersonaControlador extends DataExplorer{
    private JTextField cod_Persona,ci_rif;
    private PersonaDatasource datasource=new PersonaDatasource();
    public PersonaControlador(JFrame fram) {
        super(fram);
    }
        public void setData(JTextField cod_Persona,JTextField ci_rif){
            this.cod_Persona=cod_Persona;
            this.ci_rif=ci_rif;
        }
         
        public void setDataPersona(JTextField field1,JTextField field2){
            field1.setText(this.cod_Persona.getText());
            field2.setText(this.ci_rif.getText());
        }
        
        public void Operar(String accion){
          ArrayList<String> parameters=new ArrayList<>();
          int size=lists.size();
                //Los primeros components son para registrar a la persona
             for(int i=0; i<size; i++){
                 if(lists.get(i) instanceof JTextField){
                  
                     parameters.add(((JTextField)lists.get(i)).getText());
                 }
                else
                if(lists.get(i) instanceof JComboBox){
                    
                     parameters.add((String)(((JComboBox)lists.get(i)).getSelectedItem()));
                }
               else
                if(lists.get(i).getClass().getName().equals("com.toedter.calendar.JDateChooser")){
                         JDateChooser date=(JDateChooser)lists.get(i);
                      SimpleDateFormat formatofecha = new SimpleDateFormat("yyyy-MM-dd"); 
                       if(date.getDate()!=null){
                    
                       parameters.add(formatofecha.format(date.getDate()));}
                 }
               }
             if(accion.equals("I")){
                 datasource.add(parameters);
                //JOptionPane.showMessageDialog(null,"Persona registrado con éxito");
             }
              else
              if(accion.equals("U")){
                 datasource.Update(parameters);
                 JOptionPane.showMessageDialog(null,"Persona actualizada con éxito");
              }
              if(datasource.isSuccess()){
                 GlobalConstants.getAuditoria().setLog(accion);
                 Clean();
                 Enabled(false);
              }
               //  parameters.clear();
                 
           /* for(int i=22; i<size; i++){
                 if(lists.get(i) instanceof JTextField){
                     System.out.println(((JTextField)lists.get(i)).getText());
                     parameters.add(((JTextField)lists.get(i)).getText());
                 }
                else
                if(lists.get(i) instanceof JComboBox){
                     System.out.println(((JComboBox)lists.get(i)).getSelectedItem());
                     parameters.add((String)(((JComboBox)lists.get(i)).getSelectedItem()));
                }
               else
                if(lists.get(i).getClass().getName().equals("com.toedter.calendar.JDateChooser")){
                         JDateChooser date=(JDateChooser)lists.get(i);
                      SimpleDateFormat formatofecha = new SimpleDateFormat("yyyy-MM-dd"); 
                       if(date.getDate()==null){
                        System.out.println(date.getDate());
                       parameters.add(formatofecha.format(date.getDate()));}
                 }
               }*/
              
          }
       //  public void 
         public void Find(String buscar){
            ArrayList<Object> dataArray=new ArrayList<>();
       
        try {
            dataArray=datasource.Find(buscar);
              
                int size=dataArray.size();
              for(int i=0; i<size; i++){
                 if(lists.get(i) instanceof JTextField){
                       ((JTextField)lists.get(i)).setText((String)dataArray.get(i));
                    // parameters.add(((JTextField)lists.get(i)).getText());
                 }
                else
                if(lists.get(i) instanceof JComboBox){
                     ((JComboBox)lists.get(i)).setSelectedItem((String)dataArray.get(i));
                   //  parameters.add((String)(((JComboBox)lists.get(i)).getSelectedItem()));
                }
               else
                if(lists.get(i).getClass().getName().equals("com.toedter.calendar.JDateChooser")){
                         JDateChooser date=(JDateChooser)lists.get(i);
                      SimpleDateFormat formatofecha = new SimpleDateFormat("dd/MM/yyyy"); 
                          date.setDate((Date)dataArray.get(i));
                       //if(date.getDate()==null){
                       // System.out.println(date.getDate());
                      // parameters.add(formatofecha.format(date.getDate()));}
                   }
               }
       
            
             
        } catch (SQLException ex) {
            Logger.getLogger(PersonaControlador.class.getName()).log(Level.SEVERE, null, ex);
        }
            
         }
    }
         

