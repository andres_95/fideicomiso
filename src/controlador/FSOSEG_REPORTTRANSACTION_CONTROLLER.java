package controlador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.bimodal.app.db.FSOSEG_BALCOM_REPORT;
import com.bimodal.app.db.FSOSEG_MOVMENCF_REPORT;
import com.bimodal.app.db.FSOSEG_RESCONGEN_REPORT;
import com.bimodal.app.db.FSOSEG_LIQINT_REPORT;
import com.bimodal.app.*;
import com.bimodal.app.conf.GlobalConstants;
import com.bimodal.app.db.FSOSEG_REPORTTRANSACTION_MODEL;
import com.toedter.calendar.JDateChooser;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
/**
 *
 * @author Becerra
 */
  enum FSOSEG_REPORTTRANSACTION{
       MOVIMIENTOS_MENSUAL("Movimiento Mensual"),BALANCE("Balance de Comprobacion"),
       RESUMEN_CONTABLE("Resumen Contable General"),LIQUIDACION("Liquidación de Intereses");
       
       private final String display;
       private FSOSEG_REPORTTRANSACTION(String str){
         display=str;
       }
    @Override
     public String toString() {
        return display;
      }
      /* MOVIMIENTOS_MENSUAL,
       RESUMEN_CONTABLE,
       BALANCE,
       LIQUIDACION,*/
       
  };
/*   Movimiento Mensual
Balance de Comprobacion
Resumen Contable General
Liquidación de Intereses
Movimiento de Fideicomiso*/

public class FSOSEG_REPORTTRANSACTION_CONTROLLER extends DataExplorer{
    private FSOSEG_BALCOM_REPORT balance;
    private FSOSEG_MOVMENCF_REPORT movimientos;
    private FSOSEG_LIQINT_REPORT liquidacion;
    private FSOSEG_RESCONGEN_REPORT resumen;
    private FSOSEG_REPORTTRANSACTION_MODEL model;
   
    public FSOSEG_REPORTTRANSACTION_CONTROLLER(JFrame fram) {
       super(fram);   
       movimientos=new FSOSEG_MOVMENCF_REPORT(GlobalConstants.getConnector());
       balance=new FSOSEG_BALCOM_REPORT(GlobalConstants.getConnector());
       liquidacion=new FSOSEG_LIQINT_REPORT(GlobalConstants.getConnector());
       resumen=new FSOSEG_RESCONGEN_REPORT(GlobalConstants.getConnector());
       model=new FSOSEG_REPORTTRANSACTION_MODEL();
    
    }
    
    private String getDate(String formato,Date dat){
         SimpleDateFormat format=new SimpleDateFormat(formato);
         return format.format(dat);
     }
    
    private void Load(ResultSet rs,JComboBox cmb1,JComboBox cmb2){
         try {
            while(rs.next()){
                cmb1.addItem(rs.getString(1));
                cmb2.addItem(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FSOSEG_REPORTTRANSACTION_CONTROLLER.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void loadFideicomisos(JComboBox cmb1,JComboBox cmb2){
        ResultSet rs=model.getFideicomisos();
        Load(rs,cmb1,cmb2);
       
    }
    public void loadPersonas(JComboBox cmb1,JComboBox cmb2){
        ResultSet rs=model.getPersona();
        Load(rs,cmb1,cmb2);
    }
     
     public void InitCombo(JComboBox cmb){
        cmb.setModel(new DefaultComboBoxModel(FSOSEG_REPORTTRANSACTION.values()));
     }
    private String[] getArgs(){
          String[] args=new String[6];
        
            for(int i=0; i<args.length; i++){
               if(lists.get(i) instanceof JComboBox){
                  args[i]=(String)((JComboBox)lists.get(i)).getSelectedItem();
               } 
              else
                if(lists.get(i).getClass().getName().indexOf("JDateChooser")>=-1){
                     Date date=((JDateChooser)lists.get(i)).getDate();
                    if(date!=null){
                     args[i]=getDate("yyyy-MM-dd",date);
                    }
                }
             }
          return args;
    }
     
    public void genReport(Object type){
        String[] args=getArgs();
        
        FSOSEG_REPORTTRANSACTION transact=(FSOSEG_REPORTTRANSACTION)type;
               requestReport(transact,args);
          
    }
    private void requestReport(FSOSEG_REPORTTRANSACTION tipo,String[] args)
    {
        switch (tipo) {
            case BALANCE:
                balance.OpenReport("src/reportes/FSOSEG_BALCOM");
                balance.setDate(args[0],args[1]);
                balance.genReport();
                balance.printReport();
                break;
            case MOVIMIENTOS_MENSUAL:
                movimientos.OpenReport("src/reportes/FSOSEG_MOVMENCF");
                movimientos.setDate(args[0], args[1]);
                movimientos.setFideicomiso(args[2], args[3]);
                movimientos.setCuenta(args[4], args[5]);
                movimientos.genReport();
                movimientos.printReport();
                break;
            case LIQUIDACION: 
                liquidacion.OpenReport("src/reportes/FSOSEG_LIQINT");
                liquidacion.setDate(args[0],args[1]);
                liquidacion.setFideicomiso(args[2], args[3]);
                liquidacion.genReport();
                liquidacion.printReport();
                break;
            case RESUMEN_CONTABLE:
                resumen.OpenReport("src/reportes/FSOSEG_RESCONGEN");
                resumen.setDate(args[0],args[1]);
                resumen.setFideicomiso(args[2],args[3]);
                resumen.setCuenta(args[4],args[5]);
               // resumen.setAsiento(args[])
                resumen.genReport();
                break;
        }
    
    }
       
}
