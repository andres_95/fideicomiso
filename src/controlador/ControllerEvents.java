/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.bimodal.util.TextParse;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import com.bimodal.app.db.DataModel;
import com.bimodal.app.beans.Principal;
import java.awt.Point;

/**
 *
 * @author usuario1
 */
public abstract class ControllerEvents extends DataExplorer implements ActionListener, MouseListener, MenuListener {

    protected ResultSet results = null;
    protected DataModel modelo;
    protected static Map<String, JTextField> datas;
    protected static ArrayList<JTextField> textArr;
    protected static ArrayList<String> valores;
    protected String modulo;
    protected JFrame trat;
    protected int count;
    protected String[] columns;
    
      public ControllerEvents(JFrame fram){
          super(fram);
           explorerMenu(fram.getJMenuBar());
            trat=fram;
          
          textArr=new ArrayList<>();
          int sz=lists.size();
          for(int i=0; i<sz; i++){
             if(lists.get(i) instanceof JTextField){
                textArr.add((JTextField)lists.get(i));
             }
            else
             if(lists.get(i).getClass().getName().equals("com.toedter.calendar.JDateChooser")
                     || lists.get(i) instanceof JComboBox){
                TextParse text=new TextParse(lists.get(i));
             }
             
          }
      }
    

 /*   public ControllerEvents(JFrame trat) {
        this.trat = trat;
        textArr = new ArrayList<>();
        valores = new ArrayList<String>();
        explorerMenu(trat.getJMenuBar());
        modulos = new Stack<String>();
        Component[] comp = trat.getContentPane().getComponents();
        datas = new HashMap<String, JTextField>();

        int sz = comp.length;
        count = 0;
        for (int i = 0; i < sz; i++) {
            if (comp[i] instanceof JButton) {
                ((JButton) comp[i]).addActionListener(this);

                if (((JButton) comp[i]).getText().toLowerCase().equals("guardar")) {
                    ((JButton) comp[i]).setActionCommand("I");
                } else if (((JButton) comp[i]).getText().toLowerCase().equals("actualizar")) {
                    ((JButton) comp[i]).setActionCommand("U");
                }
                else if (((JButton) comp[i]).getText().toLowerCase().equals("anular")) {
                     ((JButton) comp[i]).setActionCommand("A");
                }

            } else if (comp[i] instanceof JPanel) {
                ExplorePanel((JPanel) comp[i]);
            } else if (comp[i] instanceof JTabbedPane) {
                JTabbedPane tab = (JTabbedPane) comp[i];
                Component[] subcomp = tab.getComponents();
                for (int j = 0; j < subcomp.length; j++) {
                    if (subcomp[j] instanceof JPanel) {
                        ExplorePanel((JPanel) subcomp[j]);
                    }
                }

            }
        }
    }*/

    public void setModelo(DataModel model) {
        this.modelo = model;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getModulo() {
        return this.modulo;
    }

    protected void parseMenu(JMenu men) {
        int sz = men.getMenuComponentCount();
        int count = 0;
        for (int i = 0; i < sz; i++) {
            if (men.getMenuComponent(i) instanceof JMenu) {
                parseMenu((JMenu) men.getMenuComponent(i));
            } else {
                ((JMenuItem) men.getMenuComponent(i)).addActionListener(this);
                ((JMenuItem) men.getMenuComponent(i)).setActionCommand(String.format("%02d", ++count));
                continue;
            }
            men.addMouseListener(this);
            men.addMenuListener(this);
            men.setActionCommand(String.format("%02d", ++count));
        }
    }

    protected void explorerMenu(JMenuBar Menus) {
        if (Menus == null) {
            return;
        }
        Menus.addMouseListener(this);

        int size = Menus.getMenuCount();
        int count = 0;
        for (int j = 0; j < size; j++) {
            Menus.getMenu(j).addMenuListener(this);
            Menus.getMenu(j).addMouseListener(this);
            Menus.getMenu(j).setActionCommand(String.format("%02d", j + 1));
            count = 0;
            int sz = Menus.getMenu(j).getMenuComponentCount();
            for (int i = 0; i < sz; i++) {
                if (Menus.getMenu(j).getMenuComponent(i) instanceof JMenu) {
                    parseMenu((JMenu) Menus.getMenu(j).getMenuComponent(i));
                    ((JMenu) Menus.getMenu(j).getMenuComponent(i)).addMenuListener(this);
                    ((JMenu) Menus.getMenu(j).getMenuComponent(i)).addMouseListener(this);
                    ((JMenu) Menus.getMenu(j).getMenuComponent(i)).setActionCommand(String.format("%02d", ++count));

                } else if (Menus.getMenu(j).getMenuComponent(i) instanceof JMenuItem) {
                    ((JMenuItem) Menus.getMenu(j).getMenuComponent(i)).addActionListener(this);
                    ((JMenuItem) Menus.getMenu(j).getMenuComponent(i)).setActionCommand(String.format("%02d", ++count));
                    continue;
                  }

            }

        }

    }

  /*  protected void ExplorePanel(JPanel panel) {
        Component[] pan = panel.getComponents();
        int size = pan.length;

        for (int i = 0; i < size; i++) {
            if (pan[i] instanceof JTabbedPane) {
                JTabbedPane tab = (JTabbedPane) pan[i];

                Component[] comp = tab.getComponents();
                for (int j = 0; j < comp.length; j++) {
                    if (comp[j] instanceof JPanel) {
                        ExplorePanel((JPanel) comp[j]);
                        
                    }
                }

            } else if (pan[i] instanceof JTextField) {
                JTextField data = (JTextField) pan[i];
                datas.put(data.getName(), data);
                textArr.add(data);
            } else if (pan[i] instanceof JPanel) {
                if (pan[i].getClass().getName().equals("com.toedter.calendar.JDateChooser")) {
                    TextParse text = new TextParse(pan[i]);
                    String name = text.getName();
                    datas.put(name, text);
                    continue;
                }
                ExplorePanel((JPanel) pan[i]);
            } else if (pan[i] instanceof JComboBox) {
                TextParse text = new TextParse(pan[i]);
                String name = text.getName();
                datas.put(name, text);

            }
        }
    }*/

 /*   public void Spy(JFrame trat) {

        explorerMenu(trat.getJMenuBar());
        datas = new HashMap<String, JTextField>();
        valores = new ArrayList<String>();
        Component[] comp = trat.getContentPane().getComponents();

        int sz = comp.length;
        for (int i = 0; i < sz; i++) {
            if (comp[i] instanceof JTextField) {
                datas.put(((JTextField) comp[i]).getName(), (JTextField) comp[i]);
                textArr.add((JTextField) comp[i]);
            } else if (comp[i] instanceof JButton) {
                ((JButton) comp[i]).addActionListener(this);
                if (((JButton) comp[i]).getText().toLowerCase().equals("guardar")) {
                    ((JButton) comp[i]).setActionCommand("I");
                } else if (((JButton) comp[i]).getText().toLowerCase().equals("actualizar")) {
                    ((JButton) comp[i]).setActionCommand("U");
                }

            } else if (comp[i] instanceof JPanel) {
                ExplorePanel((JPanel) comp[i]);
            } else if (comp[i] instanceof JTabbedPane) {
                JTabbedPane tab = (JTabbedPane) comp[i];
                Component[] subcomp = tab.getComponents();
                for (int j = 0; j < subcomp.length; j++) {
                    if (subcomp[j] instanceof JPanel) {
                        ExplorePanel((JPanel) subcomp[j]);
                    }
                }

            }
        }

    }*/
    protected Stack<String> modulos=new Stack<>();

    

    public boolean exists() {
        boolean flag = true;
        try {
            flag = results.isBeforeFirst();
        } catch (SQLException ex) {
            Logger.getLogger(ControllerEvents.class.getName()).log(Level.SEVERE, null, ex);
        }
        return flag;
    }

    public void setModel(DataModel model) {

        modelo = model;
    }

    public ResultSet getRecords() {
        return results;
    }

    @Override
    public abstract void actionPerformed(ActionEvent e);

    @Override
    public abstract void mouseClicked(MouseEvent e);

    @Override
    public abstract void mousePressed(MouseEvent e);

    @Override
    public abstract void mouseReleased(MouseEvent e);

    @Override
    public abstract void mouseEntered(MouseEvent e);

    @Override
    public abstract void mouseExited(MouseEvent e);

    @Override
    public abstract void menuSelected(MenuEvent e);

    @Override
    public abstract void menuDeselected(MenuEvent e);

    @Override
    public abstract void menuCanceled(MenuEvent e);

}
