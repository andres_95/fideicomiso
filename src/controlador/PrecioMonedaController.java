/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.bimodal.app.conf.GlobalConstants;
import com.bimodal.app.db.PrecioMonedaModel;
import com.toedter.calendar.JDateChooser;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author usuario1
 */
public class PrecioMonedaController implements ActionListener {
     PrecioMonedaModel modelo;
     DefaultTableModel model;
     TableColumnModel tcm;
     JComboBox[] cmb;
     JTextField text,param;
     JDateChooser dates;
     JTable tabl;
           public PrecioMonedaController(JComboBox[] cmb,JTextField text,JDateChooser dates){
              modelo=new PrecioMonedaModel(GlobalConstants.getConnector());
              this.cmb=cmb;
              this.text=text;
              this.dates=dates;
              
                 text.setEditable(false);
                  for(int i=0; i<2; i++){
                    cmb[i].setEnabled(false);
                   
                  }
                   dates.setEnabled(false);
           }
             
         public void LoadData(JComboBox[] cmb){
             this.cmb=cmb;
            ResultSet paises=modelo.LoadPaises();
            ResultSet monedas=modelo.LoadMonedas();
         try {
             while(paises.next()){
               cmb[1].addItem(paises.getString(1)+"-"+paises.getString(2));
             }
             while(monedas.next()){
                 cmb[0].addItem(monedas.getString(1)+"-"+monedas.getString(2));
             }
         } catch (SQLException ex) {
             Logger.getLogger(PrecioMonedaController.class.getName()).log(Level.SEVERE, null, ex);
         }
      }
        public void setParam(JTextField param,JTable tabl){
          this.param=param;
          this.tabl=tabl;
        }
      private void LoadTable(){
          DefaultTableModel model=(DefaultTableModel) tabl.getModel();
          TableColumnModel tcm = tabl.getColumnModel();
            tabl.setFillsViewportHeight(true);
            tabl.setPreferredSize(new Dimension(1000,1000));
            int columnsCount;
            ResultSet precioMoneda=modelo.ShowPrecioMoneda();
        /*try {
            columnsCount = contables.getMetaData().getColumnCount();
        } catch (SQLException ex) {
            Logger.getLogger(TransactionContableController.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        try {
             columnsCount = precioMoneda.getMetaData().getColumnCount();
            while(precioMoneda.next()){
                Object[] obj=new Object[columnsCount];
                int count=1;
                for(int i=0; i<(columnsCount); i++){
                    
                    obj[i]=precioMoneda.getObject(i+1);
                    //tcm.getColumn(i-1).setMinWidth(60);
                    tcm.getColumn(i).setMaxWidth((count++)*100);
                    //tcm.getColumn(i-1).setWidth(1);
                }
                tcm.getColumn(5).setMaxWidth(20000);
                model.addRow(obj);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TransactionContableController.class.getName()).log(Level.SEVERE, null, ex);
        }
          tabl.setModel(model);
           
     }
     
      public void add(){
          SimpleDateFormat formatofecha = new SimpleDateFormat("yyyy-MM-dd");
         ArrayList<String> parameters=new ArrayList<>();
         
             parameters.add(((String)cmb[0].getSelectedItem()).split("-")[0]);
             parameters.add(((String)cmb[1].getSelectedItem()).split("-")[0]);
             parameters.add(text.getText());
             parameters.add(formatofecha.format(dates.getDate()));
            // parameters.add(formatofecha.format(dates[1].getDate()));
             
             modelo.addPrecioMoneda(parameters);
             int columnsCount;
              DefaultTableModel model=(DefaultTableModel) tabl.getModel();
              TableColumnModel tcm = tabl.getColumnModel();
               columnsCount = tcm.getColumnCount();
             Object[] obj=new Object[columnsCount];
             // columnsCount = tcm.getColumnCount();
             
             for(int i=0; i<5; i++){
                obj[i]=parameters.get(i);
                
             }
              model.addRow(obj);
              
      }
      public void clear(){
          text.setText("");
                  for(int i=0; i<2; i++){
                    cmb[i].setSelectedIndex(0);
                   
                  }
                   dates.setDate(null);
      }
     public void Update(String buscar){
                 SimpleDateFormat formatofecha = new SimpleDateFormat("yyyy-MM-dd");
         ArrayList<String> parameters=new ArrayList<>();
         
             parameters.add(((String)cmb[0].getSelectedItem()).split("-")[0]);
             parameters.add(((String)cmb[1].getSelectedItem()).split("-")[0]);
             parameters.add(text.getText());
             parameters.add(formatofecha.format(dates.getDate()));
             parameters.add(buscar);
             modelo.UpdPrecioMoneda(parameters);
     }
     public void Find(String buscar) throws SQLException{
         SimpleDateFormat formatofecha = new SimpleDateFormat("dd/MM/yyyy");
         
         ResultSet rs=modelo.FndPrecioMoneda(buscar);
            if(!rs.isBeforeFirst())
                return;
            
         for(int i=0; i<cmb[0].getItemCount(); i++){
             int index=(((String)cmb[0].getItemAt(i)).split("-")[0].equals(rs.getString(1)))?i:0;
                cmb[0].setSelectedIndex(index);
         }
         for(int i=0; i<cmb[1].getItemCount(); i++){
             int index=(((String)cmb[1].getItemAt(i)).split("-")[0].equals(rs.getString(5)))?i:0;
                cmb[1].setSelectedIndex(index);
         }
         dates.setDate(rs.getDate(2));
         text.setText(rs.getString(3));
       
     }
     
    @Override
    public void actionPerformed(ActionEvent e) {
         JButton button=(JButton)e.getSource();
            if(button.getText().equalsIgnoreCase("Insertar")){
                 text.setEditable(true);
                  for(int i=0; i<2; i++){
                    cmb[i].setEnabled(true);
                    
                  }
                  dates.setEnabled(true);
            }
           else
               if(button.getText().equalsIgnoreCase("consultar")){
                  
               }
           else
              if(button.getText().equalsIgnoreCase("guardar")){
                   add();
                   JOptionPane.showMessageDialog(null, "Registro guardado con éxito");
                   clear();
              }
           else
              if(button.getText().equalsIgnoreCase("modificar")){
                   try {
                       Find(param.getText());
                   } catch (SQLException ex) {
                       Logger.getLogger(PrecioMonedaController.class.getName()).log(Level.SEVERE, null, ex);
                   }
                  
              }
           //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
