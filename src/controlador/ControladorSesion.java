/*
 * Nombre Objeto: ControladorSesion
 * Autor: Josue Becerra
 * Fecha Elab: 24/01/2017
 * Descripción: 
 * Última Modificación:
 * Autor Última Modificación:
 */
package controlador;
import com.bimodal.app.db.SessionModel;

public class ControladorSesion {
    SessionModel sesion;
    
    public ControladorSesion(SessionModel sesion){
        this.sesion=sesion;
    }
        
    public boolean isExistUser(String user,String password){
        return (sesion.find(user, password));
    }
    
    public String getRol(){
        return sesion.getRol();
    }
    
}
