
package com.bimodal.app.modulos.view;

import controlador.DataController;
import controlador.LogController;
import java.util.Date;


public class FSOSEGTABFRM016 extends javax.swing.JFrame {
      LogController log;
      
    
    public FSOSEGTABFRM016() {
        initComponents();
        
        log=new LogController();
        log.LoadRecords(this.jtabLogs);
        Date date=new Date();
        this.inicio.setDate(date);
        this.fin.setDate(date);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jtabLogs = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        inicio = new com.toedter.calendar.JDateChooser();
        btnPS_consultar = new javax.swing.JButton();
        fin = new com.toedter.calendar.JDateChooser();
        jlblinicio = new javax.swing.JLabel();
        jlblinicio1 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        btnI_salir = new javax.swing.JButton();
        btnI_salir1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1141, 766));
        setSize(new java.awt.Dimension(1070, 600));
        getContentPane().setLayout(null);

        jScrollPane2.setPreferredSize(new java.awt.Dimension(1141, 766));

        jtabLogs.setBackground(new java.awt.Color(204, 204, 204));
        jtabLogs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Usuario", "Módulo", "Opción", "Perfil", "Fecha del Evento", "Hora del Evento", "Descripción"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtabLogs.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jtabLogs.setDragEnabled(true);
        jtabLogs.setMinimumSize(new java.awt.Dimension(1141, 766));
        jtabLogs.setPreferredSize(new java.awt.Dimension(600, 600));
        jScrollPane2.setViewportView(jtabLogs);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(30, 50, 1070, 600);

        jLabel1.setFont(new java.awt.Font("Utsaah", 0, 24)); // NOI18N
        jLabel1.setText("Log de Auditoría");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(30, 20, 140, 28);

        inicio.setMinimumSize(new java.awt.Dimension(6, 20));
        getContentPane().add(inicio);
        inicio.setBounds(320, 660, 170, 20);

        btnPS_consultar.setBackground(new java.awt.Color(204, 204, 204));
        btnPS_consultar.setFont(new java.awt.Font("Utsaah", 0, 20)); // NOI18N
        btnPS_consultar.setText("Consultar");
        btnPS_consultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPS_consultarActionPerformed(evt);
            }
        });
        getContentPane().add(btnPS_consultar);
        btnPS_consultar.setBounds(780, 660, 93, 20);

        fin.setMinimumSize(new java.awt.Dimension(6, 20));
        getContentPane().add(fin);
        fin.setBounds(590, 660, 170, 20);

        jlblinicio.setText("Fin:");
        getContentPane().add(jlblinicio);
        jlblinicio.setBounds(540, 660, 34, 14);

        jlblinicio1.setText("Inicio:");
        getContentPane().add(jlblinicio1);
        jlblinicio1.setBounds(240, 660, 29, 14);

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 7)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(102, 102, 102));
        jLabel26.setText("FSOSEGTABFRM009");
        getContentPane().add(jLabel26);
        jLabel26.setBounds(190, 360, 63, 9);

        jLabel27.setFont(new java.awt.Font("Tahoma", 0, 7)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(102, 102, 102));
        jLabel27.setText("FSOSEGTABFRM009");
        getContentPane().add(jLabel27);
        jLabel27.setBounds(730, 270, 63, 9);

        jLabel28.setFont(new java.awt.Font("Tahoma", 0, 7)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(102, 102, 102));
        jLabel28.setText("FSOSEGTABFRM016");
        getContentPane().add(jLabel28);
        jLabel28.setBounds(30, 650, 63, 9);

        btnI_salir.setBackground(new java.awt.Color(204, 204, 204));
        btnI_salir.setFont(new java.awt.Font("Utsaah", 0, 20)); // NOI18N
        btnI_salir.setText("Salir");
        getContentPane().add(btnI_salir);
        btnI_salir.setBounds(760, 380, 61, 31);

        btnI_salir1.setBackground(new java.awt.Color(204, 204, 204));
        btnI_salir1.setFont(new java.awt.Font("Utsaah", 0, 20)); // NOI18N
        btnI_salir1.setText("Salir");
        btnI_salir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnI_salir1ActionPerformed(evt);
            }
        });
        getContentPane().add(btnI_salir1);
        btnI_salir1.setBounds(1010, 660, 90, 20);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPS_consultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPS_consultarActionPerformed
        // TODO add your handling code here:
        
        
        log.filter(inicio.getDate(), fin.getDate());
    }//GEN-LAST:event_btnPS_consultarActionPerformed

    private void btnI_salir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnI_salir1ActionPerformed
      dispose();
    }//GEN-LAST:event_btnI_salir1ActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnI_salir;
    public static javax.swing.JButton btnI_salir1;
    public static javax.swing.JButton btnPS_consultar;
    private com.toedter.calendar.JDateChooser fin;
    private com.toedter.calendar.JDateChooser inicio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel jlblinicio;
    private javax.swing.JLabel jlblinicio1;
    private javax.swing.JTable jtabLogs;
    // End of variables declaration//GEN-END:variables
}
