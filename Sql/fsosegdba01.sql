﻿# Host: localhost  (Version 5.7.18-log)
# Date: 2017-06-30 04:59:04
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "t_auditables"
#

DROP TABLE IF EXISTS `t_auditables`;
CREATE TABLE `t_auditables` (
  `cod_auditoria` varchar(20) NOT NULL,
  `des_auditable` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estatus_auditable` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`cod_auditoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_auditables"
#

INSERT INTO `t_auditables` VALUES ('0403','t_persona','2017-03-16','A');

#
# Structure for table "t_calculo_comisiones"
#

DROP TABLE IF EXISTS `t_calculo_comisiones`;
CREATE TABLE `t_calculo_comisiones` (
  `codigo` varchar(20) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `formula` varchar(100) DEFAULT NULL,
  `cod_status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "t_calculo_comisiones"
#

INSERT INTO `t_calculo_comisiones` VALUES ('CAF','CAPITAL','TOT_HABERES+TOT_ANTICIPOS','A'),('CFA','FIDEICOMITENTES','CAN_FIDEICOM','A'),('CPO','CAPITAL PROMEDIO','NULL','A'),('CTA','CANTIDAD DE TRANSACCIONES APORTE','CAN_APORTES','A'),('CXC','CUENTA POR COBRAR','CONTAB_461001000010','A'),('CXP','CUENTA POR PAGAR','NULL','A'),('FLA','FLAT','','A'),('PBF','PRODUCTO BRUTO','CONTAB_451','A'),('PMC','PRODUCTO MAS CAPITAL','TOT_HABERES+TOT_ANTICIPOS+CONTAB_451+CONTAB_434','A'),('PNT','PROMEDIO NETO','CONTAB_431+CONTAB_451+CONTAB_434+CONTAB_413','A');

#
# Structure for table "t_cierre_diario"
#

DROP TABLE IF EXISTS `t_cierre_diario`;
CREATE TABLE `t_cierre_diario` (
  `Fecha_cierre` date NOT NULL,
  `finalizado` char(1) NOT NULL,
  `ultimo_paso_realizado` varchar(50) NOT NULL,
  PRIMARY KEY (`Fecha_cierre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "t_cierre_diario"
#


#
# Structure for table "t_cierre_mensual"
#

DROP TABLE IF EXISTS `t_cierre_mensual`;
CREATE TABLE `t_cierre_mensual` (
  `Fecha_cierre` date NOT NULL,
  `finalizado` char(1) NOT NULL,
  `ultimo_paso_realizado` varchar(50) NOT NULL,
  PRIMARY KEY (`Fecha_cierre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "t_cierre_mensual"
#


#
# Structure for table "t_contabilidad"
#

DROP TABLE IF EXISTS `t_contabilidad`;
CREATE TABLE `t_contabilidad` (
  `nro_transaccion` int(11) NOT NULL AUTO_INCREMENT,
  `nro_oper_transaccion` int(11) NOT NULL,
  `cod_tipo_oper` varchar(20) NOT NULL,
  `clase_trans` varchar(30) DEFAULT NULL,
  `cod_cuenta_contable` varchar(30) NOT NULL,
  `Debe_Haber` varchar(1) DEFAULT NULL,
  `monto` decimal(17,2) DEFAULT NULL,
  `fecha_transaccion` date DEFAULT NULL,
  `fecha_contable` date DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  PRIMARY KEY (`nro_transaccion`,`cod_tipo_oper`),
  KEY `t_contabilidad_ibfk1_idx` (`cod_status`)
) ENGINE=InnoDB AUTO_INCREMENT=22546 DEFAULT CHARSET=utf8;

#
# Data for table "t_contabilidad"
#


#
# Structure for table "t_perfil"
#

DROP TABLE IF EXISTS `t_perfil`;
CREATE TABLE `t_perfil` (
  `cod_perfil` varchar(2) NOT NULL,
  `condicion` varchar(7) DEFAULT NULL,
  `cod_status_perfil` varchar(1) NOT NULL,
  `descripcion_perfil` varchar(20) NOT NULL,
  `status_condicion` varchar(1) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

#
# Data for table "t_perfil"
#

INSERT INTO `t_perfil` VALUES ('1','0403','1','Administrador','1',1),('1','020401','1','Administrador','1',2),('1','0201','1','Administrador','1',3),('1','0202','1','Administrador','1',4),('1','020301','1','Administrador','1',5),('1','020302','1','Administrador','1',6),('1','020401','1','Administrador','1',7),('1','020402','1','Administrador','1',8),('1','020403','1','Administrador','1',9),('1','020404','1','Administrador','1',10),('1','0205','1','Administrador','1',11),('1','0101','1','Administrador','1',12),('1','040101','1','Administrador','1',13),('1','040201','1','Administrador','1',14),('1','040202','1','Administrador','1',15),('1','040203','1','Administrador','1',16),('1','0403','1','Administrador','1',17),('1','0404','1','Administrador','1',18),('1','020402','1','Administrador','1',19),('2','0101','1','Prueba','1',20),('2','0102','1','Prueba','1',21),('2','0201','1','Prueba','1',22),('2','0202','1','Prueba','1',23),('2','0203','1','Prueba','1',24),('1','0204','1','Administrador','1',25),('2','0205','1','Prueba','1',26),('1','0303','1','Administrador','1',27),('1','040102','1','Administrador','1',28),('1','0302','1','Administrador','1',29),('1','020201','1','Administrador','1',30),('1','020202','1','Administrador','1',31),('1','020303','1','Administrador','1',32),('1','0204','1','Administrador','1',33),('1','0301','1','Administrador','1',35),('1','0304','1','Administrador','1',36);

#
# Structure for table "t_persona"
#

DROP TABLE IF EXISTS `t_persona`;
CREATE TABLE `t_persona` (
  `cod_Persona` varchar(20) NOT NULL,
  `ci_rif` varchar(20) NOT NULL,
  `tipo_persona` varchar(1) DEFAULT NULL,
  `sexo` varchar(1) DEFAULT NULL,
  `nom_Persona1` varchar(45) DEFAULT NULL,
  `nom_Persona2` varchar(45) DEFAULT NULL,
  `ape_Persona1` varchar(45) DEFAULT NULL,
  `ape_Persona2` varchar(45) DEFAULT NULL,
  `razon_social_persona` varchar(100) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `lugar_nacimiento` varchar(45) DEFAULT NULL,
  `estado_civil` varchar(1) DEFAULT NULL,
  `pais_nacimiento` varchar(20) DEFAULT NULL,
  `ciudad_nacimiento` varchar(20) DEFAULT NULL,
  `estado_nacimiento` varchar(20) DEFAULT NULL,
  `fecha_activacion` date DEFAULT NULL,
  `fecha_actualizacion` date DEFAULT NULL,
  `cod_status` varchar(1) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `instagram` varchar(45) DEFAULT NULL,
  `facebook` varchar(45) DEFAULT NULL,
  `twitter` varchar(45) DEFAULT NULL,
  `pagina` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `celular` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ci_rif`,`cod_Persona`),
  UNIQUE KEY `cod_Persona` (`cod_Persona`),
  KEY `cod_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_persona"
#

INSERT INTO `t_persona` VALUES ('1','1','1','M','1','1','1','1','2','2017-03-17','2','S','2','2','2','2017-03-17','2017-03-17','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('3132','20793760','N','M','Josue','None','Becerra','LLamozas','Ninguna','2017-03-23','Altamira','S','Venezuela','Caracas','DC','2017-03-27','2017-03-27','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('G2000015342','G2000015342','J','M','Alcaldia del Dtto','Metropolitano de Caracas',NULL,NULL,'Alcaldia del Dtto Metropolitano de Caracas',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('J11295702','J11295702','J','M','Fundacion','BMA',NULL,NULL,'Fundacion BMA',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('J21518890','J21518890','J','M','Fogade','Alucasa',NULL,NULL,'Fogade - Alucasa',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('J23337583','J23337583','J','M','Inversiones','Rodven CA.',NULL,NULL,'Inversiones Rodven CA',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('J23503181','J23503181','J','M','Sociedad','Venezolana',NULL,NULL,'Sociedad Venezolana',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('J29395593','J29395593','J','M','Fundacion','Banco Mercantil',NULL,NULL,'Fundacion Banco Mercantil II',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('J2957271','J2957271','J','M','Corimon','Pinturas CA.',NULL,NULL,'Corimon Pinturas CA',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('J3047515598','J3047515598','J','M','Imosa','Tuboacero','Fabricacion',NULL,'Imosa Tuboacero Fabricacion',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('J3048569705','J3048569705','J','M','Asociacion','Civil',NULL,NULL,'Asociacion Civil',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('J3457914','J3457914','J','M','Sindicato ','Beda CA.',NULL,NULL,'Sindicato Beda CA.',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('J4862715','J4862715','J','M','G Y B CA.',NULL,NULL,NULL,'G Y B CA.',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('J691908','J691908','J','M','Clinica','Caurimare CA.',NULL,NULL,'Clinica Caurimare CA',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('V15515690','V15515690','N','M','Jorge y Eneida','Marquina','Mendez','Bonilla','Mendez Bonilla Jorge y Eneida Marquina',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('V68188004','V68188004','N','M','Francisco',NULL,'Diaz','Barrera','Francisco Diaz Barrera',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('V69092558','V69092558','N','M','Iraida',NULL,'Prato','De Andrew','Prato de Andrew Iraida/Andrew Daniel G',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-08','2017-05-08','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL);

#
# Structure for table "t_rendimiento"
#

DROP TABLE IF EXISTS `t_rendimiento`;
CREATE TABLE `t_rendimiento` (
  `cod_cuenta_contable` varchar(30) NOT NULL,
  `cod_producto` varchar(10) NOT NULL,
  `monto` decimal(17,2) DEFAULT NULL,
  `fecha` date NOT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`cod_cuenta_contable`,`cod_producto`,`fecha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "t_rendimiento"
#

INSERT INTO `t_rendimiento` VALUES ('414001000050','4010867',2.50,'2011-02-17','Pendiente'),('414001000050','4010867',2.50,'2017-05-15','Pendiente'),('414001000050','4107034',2.50,'2011-02-17','Pendiente'),('414001000050','4107034',2.50,'2017-05-15','Pendiente'),('414001000050','4107060',2.50,'2011-02-17','Pendiente'),('414001000050','4107060',2.50,'2017-05-15','Pendiente'),('414001000050','4107078',2.50,'2011-02-17','Pendiente');

#
# Structure for table "t_rendimiento_diario"
#

DROP TABLE IF EXISTS `t_rendimiento_diario`;
CREATE TABLE `t_rendimiento_diario` (
  `asig_diaria_bs` decimal(17,2) DEFAULT NULL,
  `asig_diaria_usd` decimal(17,2) DEFAULT NULL,
  `rendimiento_bs` decimal(17,2) DEFAULT NULL,
  `rendimiento_usd` decimal(17,2) DEFAULT NULL,
  `fecha` date NOT NULL,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fecha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "t_rendimiento_diario"
#

INSERT INTO `t_rendimiento_diario` VALUES (10.00,10.00,10.00,10.00,'2011-02-17','Pendiente'),(10.00,10.00,10.00,10.00,'2017-05-15','Pendiente');

#
# Structure for table "t_saldo_contable"
#

DROP TABLE IF EXISTS `t_saldo_contable`;
CREATE TABLE `t_saldo_contable` (
  `cod_cuenta_contable` varchar(30) NOT NULL,
  `cod_producto` varchar(10) NOT NULL,
  `saldo` decimal(19,2) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `Debe_Haber` varchar(1) NOT NULL,
  `cod_status` varchar(1) NOT NULL,
  `fecha_last_update` date NOT NULL,
  `cod_moneda` varchar(10) NOT NULL,
  PRIMARY KEY (`cod_producto`,`cod_cuenta_contable`,`Debe_Haber`,`cod_moneda`),
  KEY `cod_producto_UNIQUE` (`cod_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_saldo_contable"
#

INSERT INTO `t_saldo_contable` VALUES ('41400100050','0000000',0.00,'2017-05-15','D','A','2017-05-17','01'),('451001000050','0000000',0.00,'2017-05-15','H','A','0207-05-17','01'),('41102100001','4010046',1279662080.00,'2017-05-14','D','A','2017-05-17','01'),('41102100001','4010046',79939280.00,'2017-05-14','D','A','2017-05-17','02'),('41102100001','4010046',166000.00,'2017-05-15','H','A','2017-05-17','01'),('41200100006','4010046',58000.00,'2017-05-15','D','A','2017-05-17','01'),('41200100006','4010046',46000.00,'2017-05-15','H','A','2017-05-17','01'),('41700100001','4010046',30000.00,'2017-05-15','D','A','2017-05-17','01'),('41700100001','4010046',58000.00,'2017-05-15','H','A','2017-05-17','01'),('41801102001','4010046',38000.00,'2017-05-15','D','A','2017-05-17','01'),('41801102001','4010046',48000.00,'2017-05-15','H','A','2017-05-17','01'),('41801103001','4010046',58000.00,'2017-05-15','D','A','2017-05-17','01'),('41801103001','4010046',64006.00,'2017-05-14','H','A','2017-05-17','01'),('41801104001','4010046',54000.00,'2017-05-15','D','A','2017-05-17','01'),('41801104001','4010046',52000.00,'2017-05-15','H','A','2017-05-17','01'),('42200100001','4010046',12803386.00,'2017-05-14','D','A','2017-05-17','01'),('42200100001','4010046',36000.00,'2017-05-15','H','A','2017-05-17','01'),('42200100016','4010046',38006.00,'2017-05-14','D','A','2017-05-17','01'),('42200100016','4010046',30000.00,'2017-05-15','H','A','2017-05-17','01'),('42200100020','4010046',36000.00,'2017-05-15','D','A','2017-05-17','01'),('42200100020','4010046',18000.00,'2017-05-15','H','A','2017-05-17','01'),('43101100001','4010046',30000.00,'2017-05-15','D','A','2017-05-17','01'),('43101100001','4010046',48000.00,'2017-05-15','H','A','2017-05-17','01'),('43101100001','4010046',79939280.00,'2017-05-14','H','A','2017-05-17','02'),('43102199001','4010046',178000.00,'2017-05-15','D','A','2017-05-17','01'),('43102199001','4010046',1279619070.00,'2017-05-14','H','A','2017-05-17','01'),('43103101001','4010046',52000.00,'2017-05-15','D','A','2017-05-17','01'),('43103101001','4010046',54000.00,'2017-05-15','H','A','2017-05-17','01'),('43103102001','4010046',48000.00,'2017-05-15','D','A','2017-05-17','01'),('43103102001','4010046',54000.00,'2017-05-15','H','A','2017-05-17','01'),('43103103001','4010046',64006.00,'2017-05-14','D','A','2017-05-17','01'),('43103103001','4010046',88000.00,'2017-05-15','H','A','2017-05-17','01'),('43103104001','4010046',132000.00,'2017-05-15','D','A','2017-05-17','01'),('43103104001','4010046',12899386.00,'2017-05-14','H','A','2017-05-17','01'),('43400100001','4010046',80000.00,'2017-05-15','D','A','2017-05-17','01'),('43400100001','4010046',56000.00,'2017-05-15','H','A','2017-05-17','01'),('44300100002','4010046',24000.00,'2017-05-15','H','A','2017-05-17','01');

#
# Structure for table "t_saldo_producto"
#

DROP TABLE IF EXISTS `t_saldo_producto`;
CREATE TABLE `t_saldo_producto` (
  `cod_Producto` varchar(10) NOT NULL,
  `saldo` decimal(17,2) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`cod_Producto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "t_saldo_producto"
#

INSERT INTO `t_saldo_producto` VALUES ('4010046',10.00,'A'),('4010064',10.00,'A'),('4010082',10.00,'A'),('4010180',10.00,'A'),('4010206',10.00,'A'),('4010242',10.00,'A'),('4010322',10.00,'A'),('4010395',10.00,'A'),('4010830',10.00,'A'),('4010867',10.00,'A'),('4106702',10.00,'A'),('4106815',10.00,'A'),('4107034',10.00,'A'),('4107060',10.00,'A'),('4107078',12.50,'A'),('4107359',10.00,'A'),('4107374',10.00,'A');

#
# Structure for table "t_status"
#

DROP TABLE IF EXISTS `t_status`;
CREATE TABLE `t_status` (
  `cod_status` varchar(3) NOT NULL,
  `des_status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_status"
#

INSERT INTO `t_status` VALUES ('A','Activo'),('I','Inactivo');

#
# Structure for table "t_plan_contable"
#

DROP TABLE IF EXISTS `t_plan_contable`;
CREATE TABLE `t_plan_contable` (
  `cod_cuenta_contable` varchar(30) NOT NULL,
  `des_cuenta_contable` varchar(120) DEFAULT NULL,
  `tipo_cuenta_contable` varchar(10) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  `referencia` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`cod_cuenta_contable`),
  KEY `t_plan_contable_ibfk_1_idx` (`cod_status`),
  CONSTRAINT `t_plan_contable_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_plan_contable"
#

INSERT INTO `t_plan_contable` VALUES ('411','Disponibilidades','Efectivo','A','NULL'),('41102','Depósitos en la Institución','Efectivo','A','NULL'),('411021','Depósitos en la Institución-Efectivo','Efectivo','A','NULL'),('41102100','Depósitos en la Institución-Efectivo','Efectivo','A','NULL'),('41102100001','Cuenta Operativa Fideicomiso','Catalogo','A','NULL'),('411021000010','Cuenta Operativa Fideicomiso 1077-47755-4','Efectivo','A','NULL'),('411021000020','Cuenta Inv. Fideicomiso 1699-07614-6','Efectivo','A','NULL'),('411021000030','Cta Inv Fid Epecializado 1009-00416-6','Efectivo','A','NULL'),('411021000040','Cta Inv Fid FAPBM 1699-07620-0','Efectivo','A','NULL'),('411021000050','Cta Inv Fid FAPSM 1699-07621-9','Efectivo','A','NULL'),('411021000060','Cta Inv Fid BOD 1009-00418-2','Efectivo','A','NULL'),('41102200002','Cuenta Inversión Fideicomiso','Catalogo','A','NULL'),('41103','Otras Disponibilidades','Efectivo','A','NULL'),('411031','Otras Disponibilidades','Efectivo','A','NULL'),('41103100','Otras Disponibilidades','Efectivo','A','NULL'),('411031000010','Otras Disponibilidades','Efectivo','A','NULL'),('411031000020','Provincial 0108-0582-18-010037918','Efectivo','A','NULL'),('411031000030','Exterior 0115-0010-24-3000189518','Efectivo','A','NULL'),('411031000040','Banesco 0134-0850-51-5803004509','Efectivo','A','NULL'),('411032','Otras Disponibilidades','Efectivo','A','NULL'),('41103200','Otras Disponibilidades','Efectivo','A','NULL'),('411032000010','Cuenta Operativa Fideicomiso ME 83000383..','Efectivo','A','NULL'),('411032000020','Cta Inv Fid ME MERILL LYNCH 6E2-07245','Efectivo','A','NULL'),('411032000030','Cta Inv COMMERCEBANK 830151212 FEM','Efectivo','A','NULL'),('411032000040','Cta Inv COMMERCEBANK 8301562212 AFR..','Efectivo','A','NULL'),('411032000050','Cta Inv UBS 1105030701/01400','Efectivo','A','NULL'),('411032000060','Cta Inv Standar Charter 1105040601','Efectivo','A','NULL'),('411032000070','6E2-07811 WCMAM-BMF','Efectivo','A','NULL'),('411032000080','6E2-02090 WCMA-FAPSM','Efectivo','A','NULL'),('411032000090','6E2-07077 WCMA-FACEMSF','Efectivo','A','NULL'),('411032000200','916-6E2A7 IIA-FACEMSF','Efectivo','A','NULL'),('411032000210','916-6E2A7 IIA-BMF','Efectivo','A','NULL'),('412','Inversiones en Títulos Valores','Activo','A','NULL'),('41200','Inversiones en Títulos Valores','Activo','A','NULL'),('412001','Inversiones en Títulos Valores','Activo','A','NULL'),('41200100','Inversiones en Títulos Valores','Activo','A','NULL'),('412001000010','Letras del Tesoro','Activo','A','NULL'),('412001000020','Bonos del Tesoro','Activo','A','NULL'),('412001000030','Bonos y Oblig de la D.P.N','Activo','A','NULL'),('412001000040','Bonos y Oblig Emit por el B.C.V','Activo','A','NULL'),('412001000050','Otros Títulos Valores Emitidos por..','Activo','A','NULL'),('41200100006','Acciones','Catalogo','A','NULL'),('412001000060','Oblig Emit por Emp PRiv No Financiera','Activo','A','NULL'),('412001000070','Dep a Plazo en Inst Financ del País','Activo','A','NULL'),('412001000080','Papeles Comerciales Emt p Emp Priv','Activo','A','NULL'),('412001000090','Participación en Inst Financ del País','Activo','A','NULL'),('412001000100','Participación en Emp Priv no Financiera','Activo','A','NULL'),('412001000110','Participación en Otras Instituciones','Activo','A','NULL'),('412002','Inversiones en Títulos Valores','Activo','A','NULL'),('41200200','Inversiones en Títulos Valores','Activo','A','NULL'),('412002000010','Dep a Plazo en Instit Financ del Exterior','Activo','A','NULL'),('412002000020','Oblig Emit por Emp Priv No Financiera','Activo','A','NULL'),('412002000030','Bonos y Oblig de la D.P.N. M/E','Activo','A','NULL'),('412002000040','BDT M/E Emitidos p/Otros Países USD','Activo','A','NULL'),('412002000050','BDT M/E Emitidios p/Gobierno Extran $','Activo ','A','NULL'),('412002000060','Acciones de Inversión ME','Activo','A','NULL'),('412002000070','Particp en Emp PRiv no Financ d/Ext','Activo','A','NULL'),('412002000130','Oblig Emit P/Entid Pub No Finc del País','Activo','A','NULL'),('413','Cartera de Créditos','Activo','A','NULL'),('4130','Prestamos empleados m/e','Activo','A','NULL'),('41300','Cartera de Créditos','Activo','A','NULL'),('413001','Cartera de Créditos','Activo ','A','NULL'),('41300100','Carteras de Créditos','Activo','A','NULL'),('413001000010','Cartera de Crédito Vigentes','Activo','A','NULL'),('413001000020','Cartera de Crédito Vencidos','Activo','A','NULL'),('41301','Créditos Vigentes','Activo','A','NULL'),('413011','Créditos Vigentes','Activo','A','NULL'),('41301100','Cartera de Créditos Vigentes','Activo','A','NULL'),('413011000010','Cartera de Crédito Vigentes','Activo','A','NULL'),('413011000020','Cartera de Crédito Vencidos','Activo','A','NULL'),('413011000030','Préstamos Fondur','Activo','A','NULL'),('413011000040','Cartera de cdto adq de vehiculos','Activo','A','NULL'),('41301101','Prestamos por cobrar vigentes','Activo','A','NULL'),('413011010010','Prestamos por cobrar vigentes','Activo','A','NULL'),('413011010020','Prestamos hipotecarios','Activo','A','NULL'),('413011010040','Cartera de cdto adq de vehiculos','Activo','A','NULL'),('413011010050','Valuaciones','Activo','A','NULL'),('413011010070','Prestamos pyme y peq productores','Activo','A','NULL'),('413011010080','Cartera de credito educativo','Activo','A','NULL'),('413011010090','Construccion de inmueble','Activo','A','NULL'),('41301102','Prestamos a beneficiarios p/cobrar','Activo','A','NULL'),('413011020010','Prestamos a beneficiarios p/cobrar','Activo','A','NULL'),('413012','Cartera de creditos vigentes m/e','Activo','A','NULL'),('41301200','Cartera de creditos vigentes m/e','Activo','A','NULL'),('41301201','Prestamos por cobrar vigentes m/e','Activo','A','NULL'),('413012010020','Prestamos m/e','Activo','A','NULL'),('41301202','Prestamos a beneficiarios p/cobrar','Activo','A','NULL'),('413012020010','Prestamos empleados m/e','Activo','A','NULL'),('41302','Creditos Reestructurados','Activo','A','NULL'),('413021','Creditos Reestructurados','Activo','A','NULL'),('41302100','Prestamos a beneficiarios por cobrar','Activo','A','NULL'),('413021000010','Prestamos empleados','Activo','A','NULL'),('413021000020','Prestamos hipotecarios','Activo','A','NULL'),('413021000030','Prestamos estudiantiles','Activo','A','NULL'),('413021000040','Prestamos en transito','Activo','A','NULL'),('413021000050','valuaciones','Activo','A','NULL'),('413021000060','Prestamos moneda extranjera','Activo','A','NULL'),('413021000070','Prestamos pyme y peq productores','Activo','A','NULL'),('41302101','Prestamos por cobrar reestructurados','Activo','A','NULL'),('413021010010','Prestamos por cobrar reestructurados','Activo','A','NULL'),('41302102','Prestamos a beneficiarios p/cobrar','Activo','A','NULL'),('413021020010','Prest a beneficiarios p/cobrar reestructurados','Activo','A','NULL'),('413022','Prestamos a beneficiarios por cobrar','Activo','A','NULL'),('41302200','Prestamos a beneficiarios por cobrar','Activo','A','NULL'),('413022000010','Prestamos empleados','Activo','A','NULL'),('413022000040','Prestamos en transitos','Activo','A','NULL'),('41302202','Prestamos a beneficiarios por cobrar','Activo','A','NULL'),('413022020010','Prestamos empleados','Activo','A','NULL'),('41303','Creditos vencidos','Activo','A','NULL'),('413031','Creditos vencidos','Activo','A','NULL'),('41303101','Prestamos por cobrar vencidos','Activo','A','NULL'),('413031010010','Prestamos por cobrar vencidos','Activo','A','NULL'),('413031010020','Prestamos hipotecarios','Activo','A','NULL'),('413031010040','Cartera de cdto adq de vehiculos','Activo','A','NULL'),('413031010070','Prestamos pyme y peq productos','Activo','A','NULL'),('41303102','Prestamos a beneficiarios p/cobrar','Activo','A','NULL'),('413031020010','Prest a beneficiarios p/cobrar venc','Activo','A','NULL'),('41304','Creditos en litigio','Activo','A','NULL'),('413041','Creditos en litigio','Activo','A','NULL'),('41304101','Prestamos por cobrar en litigio','Activo','A','NULL'),('413041010010','Prestamos por cobrar en litigio','Activo','A','NULL'),('413041010020','Prestamos hipotecarios','Activo','A','NULL'),('41304102','Prestamos a beneficiarios p/cobrar','Activo','A','NULL'),('413041020010','Prest a beneficiarios p/cobrar litg','Activo','A','NULL'),('414','Intereses y comisiones por cobrar','Activo','A','NULL'),('41400','Intereses y comisiones por cobrar','Activo','A','NULL'),('414001','Intereses y comisiones por cobrar','Activo','A','NULL'),('41400100','Intereses y comisiones por cobrar','Activo','A','NULL'),('414001000010','Rend por cobrar inv en titulos valor','Activo','A','NULL'),('414001000020','Rend por cobrar inv en otros valor','Activo','A','NULL'),('414001000030','Rend por cobrar cartera de credito','Activo','A','NULL'),('414001000050','Iints por cobrar en bdpn','Activo','A','NULL'),('414001000060','Iints p/cobrar en bonos y oblig quir','Activo','A','NULL'),('414001000070','Ints p/cobrar en certif de dep a pla','Activo','A','NULL'),('414001000080','Intereses inverfacil','Activo','A','NULL'),('414002','Intereses y comisiones por cobrar','Activo','A','NULL'),('41400200','Intereses y comisiones por cobrar','Activo','A','NULL'),('414002000010','Rend por cobrar inv en titulos valor','Activo','A','NULL'),('414002000020','Rend por cobrar inv enusd mant al v','Activo','A','NULL'),('414002000030','Rend por cobrar cartera de credito','Activo','A','NULL'),('414002000050','Ints p/cobrar en bdpn en me $','Activo','A','NULL'),('414002000060','Ints p/cobrar en bonos y oblig quir','Activo','A','NULL'),('414002000070','Ints p/cobrar en certif de dep a pla','Activo','A','NULL'),('414002000080','Ints p/cobrar bdt me emit por otro','Activo','A','NULL'),('416','Bienes realizables','Activo','A','NULL'),('41600','Bienes realizables','Activo','A','NULL'),('416001','Bienes realizables','Activo','A','NULL'),('41600100','Bienes realizables','Activo','A','NULL'),('416001000010','Inmuebles','Activo','A','NULL'),('416001000020','Maquinarias y Equipos','Activo','A','NULL'),('417','Bienes recibidos para su admon','Activo','A','NULL'),('41700','Bienes recibidos para su admon','Activo','A','NULL'),('417001','Bienes recibidos para su admon','Activo','A','NULL'),('41700100','Bienes recibidos para su admon','Activo','A','NULL'),('41700100001','Inmuebles','Catalogo','A','NULL'),('417001000010','Inmuebles','Activo','A','NULL'),('417001000020','Maquinarias y Equipos','Activo','A','NULL'),('417001000030','Giros','Activo','A','NULL'),('417001000040','Acciones','Activo','A','NULL'),('417002','Inmuebles m/e','Activo','A','NULL'),('41700200','Inmuebles m/e','Activo','A','NULL'),('417002000010','Inmuebles m/e','Activo','A','NULL'),('417002000020','Maquinarias y Equipos m/e','Activo','A','NULL'),('417002000040','Acciones m/e','Activo','A','NULL'),('417002000050','Instrumentos neg en m/e','Activo','A','NULL'),('418','Otros activos','Activo','A','NULL'),('41800','Otros activos','Activo','A','NULL'),('418001','Otros activos','Activo','A','NULL'),('41800100','Otros activos','Activo','A','NULL'),('418011','Fideicomisos de garantias','Activo','A','NULL'),('418011010010','Garantias inmobiliarias','Activo','A','NULL'),('41801102001','Garantias mobiliarias','Activo','A',NULL),('418011020010','Garantias mobiliarias','Activo','A','NULL'),('41801103001','Garantia de titulos valores','Activo','A',NULL),('418011030010','Garantias de titulos valores','Activo','A','NULL'),('41801104001','Otros fideicomisos de garantias','Activo','A',NULL),('418011040010','Otros fideicomisos de garantias','Activo','A','NULL'),('418012','Otros fideicomisos de garantias m/e','Activo','A','NULL'),('41801202','Garantias mobiliarias','Activo','A','NULL'),('418012020010','Garantias mobiliarias m/e','Activo','A','NULL'),('41801203','Garantias de titulos valores','Activo','A','NULL'),('418012030010','Garantias de titulos valores m/e','Activo','A','NULL'),('41801204','Otros Fideicomisos de garantias m/e','Activo','A','NULL'),('418012040010','Otros Fideicomisos de garantias do','Activo','A','NULL'),('41802','Diferencias d/ajuste p/redondeo en','Activo','A','NULL'),('418021','Diferencias d/ajuste p/redondeo en','Activo','A','NULL'),('41802101','Disponibilidades','Activo','A','NULL'),('418021010100','Diferencias deudoras','Activo','A','NULL'),('418021010200','Diferencias acrredoras','Activo','A','NULL'),('41802102','Inversiones en titulos valores','Activo','A','NULL'),('418021020100','Diferencias deudoras','Activo','A','NULL'),('418021020200','Diferencias acrredoras','Activo','A','NULL'),('41802103','Cartera de creditos','Activo','A','NULL'),('418021030100','Diferencias deudoras','Activo','A','NULL'),('418021030200','Diferencias acrredoras','Activo','A','NULL'),('41802104','Intereses y comisiones por pagar','Activo','A','NULL'),('418021040100','Diferencias deudoras','Activo','A','NULL'),('41802104010001','Diferencias deudoras','Activo','A','NULL'),('418021040200','Diferencias acrredoras','Activo','A','NULL'),('41802105','Bienes realizables','Activo','A','NULL'),('418021050100','Diferencias deudoras','Activo','A','NULL'),('418021050200','Diferencias acrredoras','Activo','A','NULL'),('41802106','Bienes recibidos para su admon','Activo','A','NULL'),('418021060100','Diferencias deudoras','Activo','A','NULL'),('418021060200','Diferencias acrredoras','Activo','A','NULL'),('41802106020001','Diferencias acrredoras','Activo','A','NULL'),('41802107','Otros activos','Activo','A','NULL'),('418021070100','Diferencias deudoras','Activo','A','NULL'),('418021070200','Diferencias acrredoras','Activo','A','NULL'),('41899','Otros','Activo','A','NULL'),('418991','Otros activos','Activo','A','NULL'),('41899100','Otros activos','Activo','A','NULL'),('418991000000','Otras cuentas por cobrar','Activo','A','NULL'),('418991000010','Polizas','Activo','A','NULL'),('418991000020','Otras cuentas por cobrar','Activo','A','NULL'),('418991000030','Intereses pagados por anticipados','Activo','A','NULL'),('418991000040','Anticipo Haberes/prestaciones','Activo','A','NULL'),('418991000050','Otras partidas db p/identificar (la)','Activo','A','NULL'),('418991000060','Otras cuentas deudoras varias','Activo','A','NULL'),('418991000070','Otras diferencias deudoras fideicomiso','Activo','A','NULL'),('418991000080','Comis admon fideicomiso p/cobrar','Activo','A','NULL'),('418991000090','Comis erogacion fideicomiso p/cobrar','Activo','A','NULL'),('418991000130','Comis de admon fideicomisos cobrados','Activo','A','NULL'),('418991000150','Partidas conciliaciones bancarias','Activo','A','NULL'),('418991000160','Comision p/fid pend p/cobrar','Activo','A','NULL'),('418991000170','Cupones vencidos pendientes de co','Activo','A','NULL'),('418991000180','Ctas p/cobrar empresas otros gast','Activo','A','NULL'),('418992','Otros activos','Activo','A','NULL'),('41899200','Otros activos','Activo','A','NULL'),('418992000000','Otras cuentas por cobrar m/e','Activo','A','NULL'),('418992000010','Poliza m/e','Activo','A','NULL'),('418992000020','Cuentas por cobrar m/e','Activo','A','NULL'),('418992000030','Comision admon p/cobrar a empresa','Activo','A','NULL'),('418992000040','Comis erogacion fideicomiso p/cobrar','Activo','A','NULL'),('418992000060','Otras cuentas deudoras varias me','Activo','A','NULL'),('418992000080','Comis admon fideicomiso p/cobrar','Activo','A','NULL'),('418992000130','Comis de admon fideicomiso cobra','Activo','A','NULL'),('418992000140','Comision p/fid pend p/cobrar me','Activo','A','NULL'),('418992000150','Partidas deudoras pend p/conciliar','Activo','A','NULL'),('418992000160','Cupones vencidos pendiente de cobrar','Activo','A','NULL'),('418992000180','Ctas p/cobrar empresas otros gast','Activo','A','NULL'),('422','Otras cuentas por pagar','Pasivo','A','NULL'),('42200','Otras cuentas por pagar','Pasivo','A','NULL'),('422001','Otras cuentas por pagar','Pasivo','A','NULL'),('42200100','Otras cuentas por pagar','Pasivo','A','NULL'),('42200100001','Impuesto por pagar','Pasivo','A',NULL),('422001000010','Impuestos por pagar','Pasivo','A','NULL'),('422001000020','Cuentas por pagar','Pasivo','A','NULL'),('422001000030','Intereses adeudados','Pasivo','A','NULL'),('422001000040','Primas de seguros por pagar','Pasivo','A','NULL'),('422001000050','Comision admon paga rendimiento','Pasivo','A','NULL'),('422001000060','Comision admon p/pagar empresas','Pasivo','A','NULL'),('422001000070','Timbre fiscal por pagar','Pasivo','A','NULL'),('422001000080','Imp municipal/regionales p/pagar','Pasivo','A','NULL'),('422001000090','Iva por pagar','Pasivo','A','NULL'),('422001000100','Cuentas por aplicar plan vivienda','Pasivo','A','NULL'),('422001000110','Cuentas por pagar educativo','Pasivo','A','NULL'),('422001000120','Prima seguro por pagar','Pasivo','A','NULL'),('422001000140','Cuentas por pagar pronto pago','Pasivo','A','NULL'),('422001000150','Poliza por pagar frm','Pasivo','A','NULL'),('42200100016','Retención de ISLR','Catalogo','A','NULL'),('422001000160','Retencion de islr','Pasivo','A','NULL'),('422001000170','Comision admon paga haberes','Pasivo','A','NULL'),('422001000180','Primas de seg por pagar fem falleci','Pasivo','A','NULL'),('422001000190','Primas de seg por pagar fem invalid','Pasivo','A','NULL'),('42200100020','Comisión Erogación Fideicomiso','Catalogo','A','NULL'),('422001000200','Comis erogacion fideicomiso','Pasivo','A','NULL'),('422001000210','Comis erogacion fideicomiso p/pagar','Pasivo','A','NULL'),('42200100022','Comision Cheque de Gerencia','Catalogo','A','NULL'),('422001000220','Comision cheque de gerencia','Pasivo','A','NULL'),('422001000230','Comision penalizacion planes especi','Pasivo','A','NULL'),('422001000240','Impuesto igtf por pagar','Pasivo','A','NULL'),('422001000250','Comis transf otros bcos','Pasivo','A','NULL'),('422002','Otras cuentas por pagar','Pasivo','A','NULL'),('42200200','Otras cuentas por pagar','Pasivo','A','NULL'),('422002000020','Cuentas por pagar me','Pasivo','A','NULL'),('422002000030','Intereses adeudados','Pasivo','A','NULL'),('422002000040','Comision admon paga rendimiento','Pasivo','A','NULL'),('422002000050','Comision admon p/pagar empresas','Pasivo','A','NULL'),('422002000060','Primas de seg por pagar fem falleci','Pasivo','A','NULL'),('422002000070','Primas de seg por pagar fem invalid','Pasivo','A','NULL'),('422002000080','Comis admon paga haberes m/e','Pasivo','A','NULL'),('422002000090','Comis erogacion fideicomiso m/e','Pasivo','A','NULL'),('422002000100','Comis erogacion fideicomiso p/cobrar','Pasivo','A','NULL'),('422002000110','Comision penalizacion planes especi','Pasivo','A','NULL'),('424','Otros Pasivos','Pasivo','A','NULL'),('42400','Otros Pasivos','Pasivo','A','NULL'),('424001','Otros Pasivos','Pasivo','A','NULL'),('42400100','Otros Pasivos','Pasivo','A','NULL'),('424001000010','Intereses cobrados no devengados','Pasivo','A','NULL'),('424001000020','Saldos a/f clientes planes de vivienda','Pasivo','A','NULL'),('424001000030','Ints mensuales cob no devengados','Pasivo','A','NULL'),('424001000040','Otras cuentas acreedoras varias','Pasivo','A','NULL'),('424001000050','Dep recib p/antic del fideicomiente','Pasivo','A','NULL'),('424001000150','Partidas acreedoras pend p/concili','Pasivo','A','NULL'),('424002','Otros Pasivos','Pasivo','A','NULL'),('42400200','Otros Pasivos','Pasivo','A','NULL'),('424002000020','Cuentas acreedoras','Pasivo','A','NULL'),('42401','Otros Pasivos','Pasivo','A','NULL'),('424011','Otros Pasivos','Pasivo','A','NULL'),('42401100','Otros Pasivos','Pasivo','A','NULL'),('424011000010','Intereses cobrados no devengados','Pasivo','A','NULL'),('424011000020','Otras cuentas acreedoras','Pasivo','A','NULL'),('424011000030','Ints mensuales cob no devengados','Pasivo','A','NULL'),('424011000040','Otras cuentas acreedoras varias','Pasivo','A','NULL'),('424011000050','Dep recib p/antic del fideicomiente','Pasivo','A','NULL'),('424011000060','Otras partidas cr p/identificar (la)','Pasivo','A','NULL'),('424011000070','Partidas conciliaciones bancarias','Pasivo','A','NULL'),('424011000080','Com cobradas p/pagar fiduciarias','Pasivo','A','NULL'),('424011000090','Cuenta transito chqs de gcia anul','Pasivo','A','NULL'),('424011000150','Otras diferencias efectivo p/ubicar','Pasivo','A','NULL'),('424012','Otros pasivos m/e','Pasivo','A','NULL'),('42401200','Otros pasivos m/e','Pasivo','A','NULL'),('424012000020','Otras cuentas acreedoras m/e','Pasivo','A','NULL'),('424012000150','Partidas acreedoras pend p/concili','Pasivo','A','NULL'),('42402','Diferencias d/ajuste p/redondeo','Pasivo','A','NULL'),('424021','Diferencias d/ajuste p/redondeo','Pasivo','A','NULL'),('42402101','Obligaciones financieras','Pasivo','A','NULL'),('424021010100','Diferencias deudoras','Pasivo','A','NULL'),('424021010200','Diferencias acreedoras','Pasivo','A','NULL'),('42402102','Otras cuentas por pagar','Pasivo','A','NULL'),('424021020100','Diferencias deudoras','Pasivo','A','NULL'),('424021020200','Diferencias acreedoras','Pasivo','A','NULL'),('42402103','Proviciones','Pasivo','A','NULL'),('424021030100','Diferencias deudoras','Pasivo','A','NULL'),('424021030200','Diferencias acreedoras','Pasivo','A','NULL'),('42402104','Otros pasivos','Pasivo','A','NULL'),('424021040100','Diferencias deudoras','Pasivo','A','NULL'),('424021040200','Diferencias acreedoras','Pasivo','A','NULL'),('42402105','Patrimonio asignado de los fideicomisos','Pasivo','A','NULL'),('424021050100','Diferencias deudoras','Pasivo','A','NULL'),('424021050200','Diferencias acreedoras','Pasivo','A','NULL'),('42402106','Reservas','Pasivo','A','NULL'),('424021060100','Diferencias deudoras','Pasivo','A','NULL'),('424021060200','Diferencias acreedoras','Pasivo','A','NULL'),('42402107','Ajustes al patrimonio','Pasivo','A','NULL'),('424021070100','Diferencias deudoras','Pasivo','A','NULL'),('424021070200','Diferencias acreedoras','Pasivo','A','NULL'),('42402108','Resultados Acumulados','Pasivo','A','NULL'),('424021080100','Diferencias deudoras','Pasivo','A','NULL'),('424021080200','Diferencias acreedoras','Pasivo','A','NULL'),('43','Patrimonio de los fideicomisos','Capital','A','NULL'),('431','Patrimonio asignado de los fideicomisos','Capital','A','NULL'),('43100','Patrimonio asignado de los fideicomisos','Capital','A','NULL'),('431001','Patrimonio asignado de los fideicomisos','Capital','A','NULL'),('43100100','Patrimonio asignado de los fideicomisos','Capital','A','NULL'),('431001000010','Capital en moneda nacional','Capital','A','NULL'),('431002','Patrimonio asignado de los fideicomisos','Capital','A','NULL'),('43100200','Patrimonio asignado de los fideicomisos','Capital','A','NULL'),('431002000020','Capital en me','Capital','A','NULL'),('43101','Fideicomisos de inversion','Capital','A','NULL'),('431011','Fideicomisos de inversion','Capital','A','NULL'),('43101100','Fideicomisos de inversion','Capital','A','NULL'),('43101100001','Fideicomisos de inversion','Capital','A',NULL),('431011000010','Fideicomisos de inversion','Capital','A','NULL'),('431012','Fideicomisos de inversion m/e','Capital','A','NULL'),('43101200','Fideicomisos de inversion m/e','Capital','A','NULL'),('431012000010','Fideicomisos de inversion m/e','Capital','A','NULL'),('43102','Fideicomisos de administracion','Capital','A','NULL'),('431021','Fideicomisos de administracion','Capital','A','NULL'),('43102101','Prestaciones sociales','Capital','A','NULL'),('431021010010','Prestaciones sociales','Capital','A','NULL'),('43102102','Fondos o cajas de ahorros','Capital','A','NULL'),('431021020010','Fondos o cajas de ahorros','Capital','A','NULL'),('43102103','Cooperativas y similares','Capital','A','NULL'),('431021030010','Cooperativas y similares','Capital','A','NULL'),('43102104','Desarrollos inmobiliarios','Capital','A','NULL'),('431021040010','Desarrollos inmobiliarios','Capital','A','NULL'),('43102105','Programas de financiamiento','Capital','A','NULL'),('431021050010','Programas de financiamiento','Capital','A','NULL'),('43102106','Clubes y asociaciones similares','Capital','A','NULL'),('431021060010','Clubes y asociaciones similares','Capital','A','NULL'),('43102107','Sociales y asistencias','Capital','A','NULL'),('431021070010','Sociales y asistencias','Capital','A','NULL'),('43102108','Testamentarios','Capital','A','NULL'),('431021080010','Testamentarios','Capital','A','NULL'),('43102199','Otros de administracion','Capital','A','NULL'),('43102199001','Otros de Administración','Catalogo','A','NULL'),('431021990010','Otros de administracion','Capital','A','NULL'),('431022','Prestaciones sociales m/e','Capital','A','NULL'),('43102201','Prestaciones sociales m/e','Capital','A','NULL'),('431022010010','Prestaciones sociales m/e','Capital','A','NULL'),('43102202','Fondos o cajas de ahorros m/e','Capital','A','NULL'),('431022020010','Fondos o cajas de ahorros m/e','Capital','A','NULL'),('43102205','Programas de financiamiento m/e','Capital','A','NULL'),('431022050010','Programas de financiamiento m/e','Capital','A','NULL'),('43102207','Sociales y asistenciales m/e','Capital','A','NULL'),('431022070010','Sociales y asistenciales m/e','Capital','A','NULL'),('43102208','Testamentarios m/e','Capital','A','NULL'),('431022080010','Testamentarios m/e','Capital','A','NULL'),('43102299','Otros de administracion m/e','Capital','A','NULL'),('431022990010','Otros de administracion m/e','Capital','A','NULL'),('43103','Fideicomisos de garantia','Capital','A','NULL'),('431031','Fideicomisos de garantia','Capital','A','NULL'),('43103101','Garantias inmobiliarias','Capital','A','NULL'),('43103101001','Garantias de creditos inmobiliaria','Capital','A',NULL),('431031010010','Garantias de creditos inmobiliaria','Capital','A','NULL'),('43103102','Garantias mobiliarias','Capital','A','NULL'),('43103102001','Garantias de creditos mobiliaria','Capital','A',NULL),('431031020010','Garantias de creditos mobiliaria','Capital','A','NULL'),('43103103','Garantia de titulos valores','Capital','A','NULL'),('43103103001','Garantia de creditos titulos calores','Capital','A',NULL),('431031030010','Garantia de creditos titulos valores','Capital','A','NULL'),('43103104','Garantia de creditos titulos valores','Capital','A','NULL'),('43103104001','Otros de garantias','Capital','A',NULL),('431031040010','Otros de garantias','Capital','A','NULL'),('431032','Fideicomisos de garantia m/e','Capital','A','NULL'),('431032010010','Inmuebles de garantia me','Capital','A','NULL'),('431032020010','Muebles de garantia me','Capital','A','NULL'),('431032030010','Titulos valores en garantia me','Capital','A','NULL'),('43103204','Otros fideicomisos de garantias m/e','Capital','A','NULL'),('431032040010','Otros fideicomisos de garantias m/e','Capital','A','NULL'),('43104','Fideicomiso caracteristicas mixtas','Capital','A','NULL'),('431041','Fideicomisos caracteristicas mixtas','Capital','A','NULL'),('43104100','Fideicomisos caracteristicas mixtas','Capital','A','NULL'),('431041000010','Fideicomisos caracteristicas mixtas','Capital','A','NULL'),('431042','Fideicomisos caracteristicas mixtas','Capital','A','NULL'),('43104200','Fideicomisos caracteristicas mixtas','Capital','A','NULL'),('431042000010','Fideicomisos caracteristicas mixtas','Capital','A','NULL'),('43105','Otros fideicomisos','Capital','A','NULL'),('431051','Otros fideicomisos','Capital','A','NULL'),('43105100','Otros fideicomisos','Capital','A','NULL'),('431051000010','Otros fideicomisos','Capital','A','NULL'),('432','Reservas de capital','Capital','A','NULL'),('43200','Reservas de capital','Capital','A','NULL'),('432001','Reservas de capital','Capital','A','NULL'),('43200100','Reservas de capital','Capital','A','NULL'),('432001000010','Reservas de capital','Capital','A','NULL'),('432002','Reservas de capital m/e','Capital','A','NULL'),('43200200','Reservas de capital m/e','Capital','A','NULL'),('432002000010','Reservas de capital m/e','Capital','A','NULL'),('433','Ajuste al patrimonio','Capital','A','NULL'),('43300','Ajuste al patrimonio','Capital','A','NULL'),('433001000010','Ajuste al patrimonio por diferencia','Capital','A','NULL'),('434','Resultados acumulados','Capital','A','NULL'),('43400','Resultados acumulados','Capital','A','NULL'),('434001','Resultados acumulados','Capital','A','NULL'),('43400100','Resultados acumulados','Capital','A','NULL'),('43400100001','Resultado Acumulado','Catalogo','A','NULL'),('434001000010','Resultados acumulados','Capital','A','NULL'),('434002000010','Resultados acumulados me','Capital','A','NULL'),('44','Gastos de los fideicomisos','Gastos','A','NULL'),('441','Gastos financieros','Gastos','A','NULL'),('44100','Gastos financieros','Gastos','A','NULL'),('441001','Gastos financieros','Gastos','A','NULL'),('44100100','Gastos financieros','Gastos','A','NULL'),('441001000010','Prod comis bco mercantil','Gastos','A','NULL'),('441001000020','Prod ints pag por adelantado','Gastos','A','NULL'),('44100100005','Perdida Prima en Compra de BDPN','Catalogo','A','NULL'),('441001000050','Perdida prima en compra de bdpn','Gastos','A','NULL'),('441001000060','Perdida prima en compra de bonos','Gastos','A','NULL'),('441001000070','Perdida prima en compra de papel','Gastos','A','NULL'),('441001000080','Perdida cert dep a plazo','Gastos','A','NULL'),('441001000090','Otros gastos por desincorporacion','Gastos','A','NULL'),('441001000100','Otros gastos por desincorporacion','Gastos','A','NULL'),('441002','Gastos financieros','Gastos','A','NULL'),('44100200','Gastos financieros','Gastos','A','NULL'),('441002000010','M/e producto','Gastos','A','NULL'),('44100200005','Perdida en Prima de BDPN Me $','Catalogo','A','NULL'),('441002000050','Perdida en prima de bdpn me $','Gastos','A','NULL'),('441002000060','Perdida el prima de bonos y oblig quir','Gastos','A','NULL'),('441002000070','Perdida en prima cert dep a plazo me $','Gastos','A','NULL'),('441002000080','Perd en prima bdt me emit p/otros ','Gastos','A','NULL'),('441002000090','Perd en prima bdt me emit p/gob eeu','Gastos','A','NULL'),('441002000100','Otros gastos por desincorporacion','Gastos','A','NULL'),('441002000110','Otros gastos por desincorporacion','Gastos','A','NULL'),('44220000','Valores en gtia','Gastos','A','NULL'),('44220100','Bonos entregados por recibir','Gastos','A','NULL'),('443','Otros gastos operativos','Gastos','A','NULL'),('44300','Otros gastos operativos','Gastos','A','NULL'),('443001','Otros gastos operativos','Gastos','A','NULL'),('44300100','Otros gastos operativos','Gastos','A','NULL'),('44300100001','Comisión Sobre Inv TIT Val M/N','Catalogo','A','NULL'),('443001000010','Comision sobre inv tit val m/n','Gastos','A','NULL'),('44300100002','Comisión Administración','Catalogo','A','NULL'),('443001000020','Comision administracion','Gastos','A','NULL'),('443001000030','Gastos comisiones de cobranza','Gastos','A','NULL'),('443001000040','Comision por cheques de gerencia','Gastos','A','NULL'),('443001000050','Aceptaciones comerciales','Gastos','A','NULL'),('443001000060','Perd en vta de bdpn','Gastos','A','NULL'),('443001000070','Perd en vta de bonos y oblig quirg','Gastos','A','NULL'),('443001000080','Perd en vta de papeles comerciles','Gastos','A','NULL'),('443001000090','Perd en vta de certif de dep a plazo','Gastos','A','NULL'),('443001000100','Otros gastos operativos fideicomiso','Gastos','A','NULL'),('443001000110','Comision por custodia mn','Gastos','A','NULL'),('443002','Otros gastos operativos m/e','Gastos','A','NULL'),('44300200','Otros gastos operativos m/e','Gastos','A','NULL'),('443002000010','Comision sobre inv tit val me','Gastos','A','NULL'),('443002000020','Comision administracion m/e','Gastos','A','NULL'),('443002000030','Comision por transferencia en m/e','Gastos','A','NULL'),('443002000040','Mantenimiento cuentas en el extra','Gastos','A','NULL'),('443002000050','Aceptaciones comerciales m/e','Gastos','A','NULL'),('443002000060','Perd en vta de bdpn me $','Gastos','A','NULL'),('443002000070','Perd en vta de bonos y oblig quir','Gastos','A','NULL'),('443002000080','Perd en vta cert de dep a plazo me $','Gastos','A','NULL'),('443002000090','Perd en vta de bdt me emit p/otros paises','Gastos','A','NULL'),('443002000100','Perd en vta de bdt me emit p/gob eeuu','Gastos','A','NULL'),('443002000110','Otros gastos operativos fideicmiso','Gastos','A','NULL'),('443002000120','Comision por custodia me','Gastos','A','NULL'),('443010000010','Comision Bursatil','Gastos','A','NULL'),('444','Gastos generales y administrativos','Gastos','A','NULL'),('44400','Gastos generales y administrativos','Gastos','A','NULL'),('444001','Gastos generales y administrativos','Gastos','A','NULL'),('44400100','Gastos generales y administrativos','Gastos','A','NULL'),('444001000010','Gastos generales','Gastos','A','NULL'),('444001000020','Gastos de notaria','Gastos','A','NULL'),('444001000030','Gastos de registro','Gastos','A','NULL'),('444001000040','Honorarios profesionales mn ','Gastos','A','NULL'),('444002','Gastos generales y administrativos','Gastos','A','NULL'),('44400200','Gastos generales y administrativos','Gastos','A','NULL'),('444002000010','Gastos generales me','Gastos','A','NULL'),('444002000020','Honorarios profesionales me','Gastos','A','NULL'),('447','Impuestos','Gastos','A','NULL'),('44700','Impuestos','Gastos','A','NULL'),('447001','Impuestos','Gastos','A','NULL'),('44700100','Impuestos','Gastos','A','NULL'),('447001000010','Impuesto sobre la renta islr','Gastos','A','NULL'),('447001000020','Impuesto igtf','Gastos','A','NULL'),('451','Ingresos financieros','Ingresos','A','NULL'),('45100','Ingresos financieros','Ingresos','A','NULL'),('451001','Ingresos financieros','Ingresos','A','NULL'),('45100100','Ingresos financieros','Ingresos','A','NULL'),('451001000010','Prod ingreso gravable','Ingresos','A','NULL'),('451001000020','Intereses prest planes de vivienda','Ingresos','A','NULL'),('451001000030','Ingresos intereses','Ingresos','A','NULL'),('451001000040','Ingresos intereses de mora','Ingresos','A','NULL'),('45100100005','Ingreso por Intereses de BDPN','Catalogo','A','NULL'),('451001000050','Ingresos por intereses de bdpn','Ingresos','A','NULL'),('451001000060','Ing p/ints bonos y oblig quirog','Ingresos','A','NULL'),('451001000070','Ing p/ints certif de dep a plazo','Ingresos','A','NULL'),('45100100010','Ganancia Descuento en BDPN','Catalogo','A','NULL'),('451001000100','Ganancia descuento en bdpn','Ingresos','A','NULL'),('451001000110','Gananc desc en bonos y oblig quirog','Ingresos','A','NULL'),('451001000120','Gananc desc en papeles comerciales','Ingresos','A','NULL'),('451001000130','Gananc desc en certif de dep a plazo','Ingresos','A','NULL'),('45100100014','Intereses Cuentas Remuneradas MN','Catalogo','A','NULL'),('451001000140','Intereses cuentas remuneradas mn','Ingresos','A','NULL'),('451002','Ingresos financieros','Ingresos','A','NULL'),('45100200','Ingresos financieros','Ingresos','A','NULL'),('451002000010','Prod ingresos gravables m/e','Ingresos','A','NULL'),('45100200005','Ingresos P/Ints de BDPN Me $','Catalogo','A','NULL'),('451002000050','Ingresos p/ints de bdpn me $','Ingresos','A','NULL'),('451002000060','Ing p/ints bonos y oblig quirog','Ingresos','A','NULL'),('451002000070','Ing p/ints certif de dep a plazo me $','Ingresos','A','NULL'),('451002000080','Ing p/ints bdt me emit p/otros paises','Ingresos','A','NULL'),('45100200010','Ganancia Desc en DPN Me $','Catalogo','A','NULL'),('451002000100','Gananc desc en bdpn me $','Ingresos','A','NULL'),('451002000110','Gananc desc en bonos y oblig quir ','Ingresos','A','NULL'),('451002000120','Gananc desc cert de dep a plazo','Ingresos','A','NULL'),('451002000130','Gananc desc en bdt me emit p/otros paises','Ingresos','A','NULL'),('451002000140','Gananc desc en bdt me emit p/gob','Ingresos','A','NULL'),('451002000150','Intereses cuentas remuneradas me','Ingresos','A','NULL'),('45300100001','Otros Ingresos Operativos','Catalogo','A','NULL');

#
# Structure for table "t_localizacion"
#

DROP TABLE IF EXISTS `t_localizacion`;
CREATE TABLE `t_localizacion` (
  `cod_Persona` varchar(20) NOT NULL,
  `ci_rif` varchar(20) NOT NULL,
  `fecha_proceso` date DEFAULT NULL,
  `fecha_actualizacion` date DEFAULT NULL,
  `avenida` varchar(60) DEFAULT NULL,
  `edificio` varchar(60) DEFAULT NULL,
  `urb` varchar(60) DEFAULT NULL,
  `piso` varchar(60) DEFAULT NULL,
  `pais` varchar(20) NOT NULL,
  `ciudad` varchar(20) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `zona_postal` varchar(30) DEFAULT NULL,
  `telefono1` varchar(30) DEFAULT NULL,
  `telefono2` varchar(30) DEFAULT NULL,
  `apartamento` varchar(30) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  UNIQUE KEY `cod_Persona_UNIQUE` (`cod_Persona`),
  UNIQUE KEY `ci_rif_UNIQUE` (`ci_rif`),
  KEY `cod_status` (`cod_status`),
  CONSTRAINT `t_localizacion_ibfk_2` FOREIGN KEY (`cod_Persona`) REFERENCES `t_persona` (`cod_Persona`),
  CONSTRAINT `t_localizacion_ibfk_3` FOREIGN KEY (`ci_rif`) REFERENCES `t_persona` (`ci_rif`),
  CONSTRAINT `t_localizacion_ibfk_7` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_localizacion"
#


#
# Structure for table "t_pais"
#

DROP TABLE IF EXISTS `t_pais`;
CREATE TABLE `t_pais` (
  `cod_pais` varchar(10) NOT NULL,
  `descripcion_pais` varchar(80) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_pais`),
  KEY `t_pais_ibfk_1` (`cod_status`),
  CONSTRAINT `t_pais_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_pais"
#


#
# Structure for table "t_opciones"
#

DROP TABLE IF EXISTS `t_opciones`;
CREATE TABLE `t_opciones` (
  `cod_opcion` varchar(3) NOT NULL,
  `des_opcion` varchar(100) DEFAULT NULL,
  `cod_status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`cod_opcion`),
  KEY `cod_status` (`cod_status`),
  CONSTRAINT `t_opciones_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_opciones"
#


#
# Structure for table "t_moneda"
#

DROP TABLE IF EXISTS `t_moneda`;
CREATE TABLE `t_moneda` (
  `cod_moneda` varchar(10) NOT NULL,
  `descripcion_moneda` varchar(10) NOT NULL,
  `tasa` double NOT NULL,
  `cod_status` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_moneda`),
  KEY `cod_status_idx` (`cod_status`),
  CONSTRAINT `cod_status` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_moneda"
#

INSERT INTO `t_moneda` VALUES ('01','VEF',0,'A'),('02','USD',947546.4657,'A');

#
# Structure for table "t_configuracion"
#

DROP TABLE IF EXISTS `t_configuracion`;
CREATE TABLE `t_configuracion` (
  `cod_empresa` varchar(20) NOT NULL,
  `ci_rif` varchar(20) NOT NULL,
  `razon_social_persona` varchar(100) DEFAULT NULL,
  `cod_moneda` varchar(10) DEFAULT NULL,
  `cod_pais` varchar(10) DEFAULT NULL,
  `cod_postal` varchar(10) NOT NULL,
  `formato_fecha` decimal(10,0) DEFAULT NULL,
  `formato_hora` decimal(10,0) DEFAULT NULL,
  `idioma` varchar(30) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefonos` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `fecha_cierre_actividades` varchar(10) DEFAULT NULL,
  `frecuencia_cierre_operativo` varchar(10) DEFAULT NULL,
  `comision_administracion01` varchar(10) DEFAULT NULL,
  `comision_administracion02` varchar(10) DEFAULT NULL,
  `comision_administracion03` varchar(10) DEFAULT NULL,
  `comision_operativa01` varchar(10) DEFAULT NULL,
  `comision_operativa02` varchar(10) DEFAULT NULL,
  `comision_operativa03` varchar(10) DEFAULT NULL,
  `mensaje_oficial_reportes` varchar(100) DEFAULT NULL,
  `nombre_Sistema_aplicacion` varchar(80) DEFAULT NULL,
  `fecha_configuracion_sistema` varchar(10) DEFAULT NULL,
  `caracter_separa_campos` varchar(1) DEFAULT NULL,
  `ruta_importar_archivo_plano` varchar(45) DEFAULT NULL,
  `ruta_archivos_entrada` varchar(100) DEFAULT NULL,
  `ruta_archivos_salida` varchar(100) DEFAULT NULL,
  `ruta_pagina_web` varchar(45) DEFAULT NULL,
  `ruta_reporte` varchar(100) DEFAULT NULL,
  `ruta_formato_reportes` varbinary(100) DEFAULT NULL,
  `ruta_instalacion_aplicacion` varchar(100) DEFAULT NULL,
  `ruta_log_auditoria` varchar(100) DEFAULT NULL,
  `ruta_destino_reporte` varchar(100) DEFAULT NULL,
  `ruta_conexion_servidor` varchar(100) DEFAULT NULL,
  `ejecucion_procesos_batch` varchar(100) DEFAULT NULL,
  `persona_contacto` varchar(80) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_empresa`,`ci_rif`),
  UNIQUE KEY `cod_empresa_UNIQUE` (`cod_empresa`),
  UNIQUE KEY `cod_moneda_UNIQUE` (`cod_moneda`),
  UNIQUE KEY `cod_pais_UNIQUE` (`cod_pais`),
  KEY `cod_status` (`cod_status`),
  CONSTRAINT `t_configuracion_ibfk_1` FOREIGN KEY (`cod_pais`) REFERENCES `t_pais` (`cod_pais`),
  CONSTRAINT `t_configuracion_ibfk_2` FOREIGN KEY (`cod_moneda`) REFERENCES `t_moneda` (`cod_moneda`),
  CONSTRAINT `t_configuracion_ibfk_3` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_configuracion"
#


#
# Structure for table "t_producto"
#

DROP TABLE IF EXISTS `t_producto`;
CREATE TABLE `t_producto` (
  `cod_Producto` varchar(10) NOT NULL,
  `Des_Producto` varchar(150) DEFAULT NULL,
  `Caracteristica_Producto` varchar(45) DEFAULT NULL,
  `cod_moneda` varchar(10) DEFAULT NULL,
  `pago_fondo` varchar(1) DEFAULT NULL,
  `paga_empresa` varchar(1) DEFAULT NULL,
  `comisiones_Administracion_Producto` varchar(20) DEFAULT NULL,
  `comisiones_operativa_Producto` decimal(17,2) DEFAULT NULL,
  `impuesto` decimal(17,2) DEFAULT NULL,
  `frecuencia_cierre_producto` varchar(10) DEFAULT NULL,
  `fecuencia_pago_dividendo` varchar(10) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  `portafolio` varchar(1) NOT NULL,
  `num_fideicomitentes` int(11) DEFAULT NULL,
  `pais` varchar(20) DEFAULT NULL,
  `ciudad` varchar(20) DEFAULT NULL,
  `estado` varchar(20) DEFAULT NULL,
  `tipo_cliente` varchar(10) DEFAULT NULL,
  `fecha_constitucion` date DEFAULT NULL,
  `fecha_vencimiento` date DEFAULT NULL,
  `fecha_prox_pago` date DEFAULT NULL,
  `fecha_prox_cierre` date DEFAULT NULL,
  `pago_ganancias` varchar(20) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `distribucion_mensual` varchar(1) DEFAULT NULL,
  `funcionario` varchar(45) DEFAULT NULL,
  `tipo_comision` varchar(20) DEFAULT NULL,
  `monto_comision` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`cod_Producto`),
  KEY `cod_moneda` (`cod_moneda`),
  KEY `t_producto_ibfk_1` (`cod_status`),
  CONSTRAINT `t_producto_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`),
  CONSTRAINT `t_producto_ibfk_2` FOREIGN KEY (`cod_moneda`) REFERENCES `t_moneda` (`cod_moneda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_producto"
#

INSERT INTO `t_producto` VALUES ('0000000','Rendimientos',NULL,'01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'A','N',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('123','Producto',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'A','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('4010046','Inversiones Rodven CA','garantia','01',NULL,NULL,'anual anticipado',NULL,0.00,'anual','anual','A','N',1,'Venezuela','Caracas','Dtto Capital','juridico','1996-12-12','2026-12-31','2017-12-31','2017-12-31','capitalizacion','prados del este','N','Corporaciones Metro','saldo promedio','0,80%'),('4010064','Herben CA - GYB CA','garantia','01',NULL,NULL,'mensual',NULL,0.00,'anual','anual','A','N',1,'Venezuela','Caracas','Dtto Capital','juridico','1996-12-12','2026-12-31','2017-12-31','2017-12-31','capitalizacion','san bernandino, avenida andres bello','N','Empresas Metro II','monto fijo','50.00'),('4010082','Francisco Diaz Barrera- Inversi','garantia','01',NULL,NULL,'anual anticipado',NULL,0.00,'anual','anual','A','N',1,'Venezuela','Caracas','Dtto Capital','natural','1996-12-12','2096-12-31','2096-12-31','2096-12-31','capitalizacion','el marques, av romulo gallegos','N','Personas Metro II','monto fijo','3000.00'),('4010180','Fogade - Alucasa','garantia','01',NULL,NULL,'unica',NULL,0.00,'anual','anual','A','N',1,'Venezuela','Caracas','Dtto Capital','juridico','1996-12-12','2026-12-31','2017-12-31','2017-12-31','capitalizacion','san bernardino, avenida andres bello','N','Corporativa Nacional Metro','monto fijo','2500.00'),('4010206','Fundacion BMA','garantia','01',NULL,NULL,'anual',NULL,NULL,'anual','anual','A','N',1,'Venezuela','Caracas','Dtto Capital','juridico','1997-01-31','2023-06-30','2018-01-31','2018-01-31','capitalizacion','san bernardino, avenida andres bello','N','Instituciones Financ Metro','monto fijo','400.00'),('4010242','Tuboacero Fabricacion, CA. Petrozuata','garantia','01',NULL,NULL,'mensual anticipado',NULL,0.00,'anual','anual','A','N',1,'Venezuela','Caracas','Dtto Capital','juridico','1997-05-13','2027-05-31','2017-05-31','2017-05-31','capitalizacion','san bernardino, avenida andres bello','N','Corporativa Nacional Metro','monto fijo','350.00'),('4010322','Tuboacero Fabricacion, CA. Bariven II','garantia','01',NULL,NULL,'mensual anticipado',NULL,0.00,'anual','anual','A','N',1,'Venezuela','Los Teques','Miranda','juridico','1997-11-28','2027-11-28','2017-11-30','2017-11-30','capitalizacion','san bernardino, avenida andres bello','N','Corporativa Nacional Metro','monto fijo','50.00'),('4010395','Tuboacero Fabricacion, CA. Bariven SA','garantia','01',NULL,NULL,'mensual anticipado',NULL,0.00,'anual','anual','A','N',1,'Venezuela','Caracas','Dtto Capital','juridico','1998-02-04','2028-02-04','2018-02-28','2018-02-28','capitalizacion','san bernardino, avenida andres bello','N','Corporativa Nacional Metro','monto fijo','40.00'),('4010830','Corimon Pinturas, CA.','garantia','01',NULL,NULL,'unica',NULL,0.00,'anual','anual','A','N',1,'Venezuela','Valencia','Carabobo','juridico','1999-03-26','2029-03-31','2017-12-31','2017-12-31','capitalizacion','san bernardino, avenida andres bello','N','Corporativa Nacional Metro','saldo promedio','1%anual'),('4010867','Clinica Caurimare, CA.','garantia','01',NULL,NULL,'anual',NULL,NULL,'anual','anual','A','S',1,'Venezuela','Los Teques','Miranda','juridico','1999-09-20','2020-09-30','2017-12-31','2017-12-31','capitalizacion','colinas de bello monte, avenida caurimare','N','Empresas Metro I','monto fijo','600.00'),('4106702','Sindicato Beda, CA.','garantia','01',NULL,NULL,'anual anticipado',NULL,NULL,'anual','anual','A','N',1,'Venezuela','Caracas','Dtto Capital','juridico','1999-04-13','2029-04-30','2018-04-30','2018-04-30','capitalizacion','san bernardino, avenida andres bello','N','Admin de Credito Metro','monto fijo','2500.00'),('4106815','Prato de Andrew Iraida/Andrew Daniel Guillermo','administracion','02',NULL,NULL,'anual anticipado',NULL,NULL,NULL,NULL,'A','N',1,'Venezuela','Caracas','Dtto Capital','natural','2001-11-13','2050-11-30','2017-10-31','2017-10-31','capitalizacion','los naranjos el cafetal, avenida norte','N','Banca Privada Metro','saldo promedio','0.11%'),('4107034','Fundacion Banco Mercantil II','inversion','01',NULL,NULL,'mensual',NULL,0.00,'mensual','mensual','A','S',1,'Venezuela','Caracas','Dtto Capital','juridico','2003-05-08','2033-05-31','2017-05-31','2017-05-31','capitalizacion','avenida andres bello','N','Empresas Metro I','saldo capital','0.50%'),('4107060','Alcaldia del Dtto Metropolitano de Caracas','administracion','01',NULL,NULL,'mensual',NULL,0.00,'mensual','mensual','A','S',1,'Venezuela','Caracas','Dtto Capital','descentr','2003-08-06','2020-08-08','2017-05-31','2017-05-31','capitalizacion','sec Catedral, esquina ppal','S','Sector Publico Metro II','saldo promedio','0.50%'),('4107078','Fundacion Mercantil (Todos con Vzla)','inversion','01',NULL,NULL,'mensual',NULL,0.00,'mensual','mensual','A','S',2,'Venezuela','Caracas','Dtto Capital','juridico','2005-02-28','2035-02-28','2017-05-15','2017-05-31','capitalizacion','Av el Lago Torre Mercantil','N','Empresas Metro I','saldos administrados','0.50%'),('4107359','Mendez Bonilla Jorge y Eneida Marquina','garantia','01',NULL,NULL,'trimestral',NULL,NULL,'trimestral','trimestral','A','N',1,'Venezuela','Caracas','Dtto Capital','natural','2011-05-18','2030-05-18','2017-06-30','2017-06-30','capitalizacion',NULL,'N','Personas Metro I','saldos administrados','0.50%'),('4107374','Diaz Barrera Francisco','garantia','01',NULL,NULL,NULL,NULL,0.00,'trimestral','trimestral','A','N',1,'Venezuela','Caracas','Dtto Capital','natural','2015-10-05','2027-10-31','2016-06-30','2017-06-30','capitalizacion',NULL,'N','Personas Metro II',NULL,NULL);

#
# Structure for table "t_producto_persona"
#

DROP TABLE IF EXISTS `t_producto_persona`;
CREATE TABLE `t_producto_persona` (
  `cod_Producto` varchar(10) NOT NULL,
  `cod_Persona` varchar(20) NOT NULL,
  `ci_rif` varchar(20) NOT NULL,
  `fecha_proceso` date DEFAULT NULL,
  `fecha_actualizacion` date DEFAULT NULL,
  `saldo_inicial` decimal(19,2) DEFAULT NULL,
  `saldo_final` decimal(19,2) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  UNIQUE KEY `cod_Producto_UNIQUE` (`cod_Producto`),
  KEY `t_producto_persona_ibfk_4` (`cod_status`),
  KEY `t_producto_persona_ibfk_2_idx` (`cod_Persona`),
  KEY `t_producto_persona_ibfk_3_idx` (`ci_rif`),
  CONSTRAINT `t_producto_persona_ibfk_1` FOREIGN KEY (`cod_Producto`) REFERENCES `t_producto` (`cod_Producto`),
  CONSTRAINT `t_producto_persona_ibfk_2` FOREIGN KEY (`cod_Persona`) REFERENCES `t_persona` (`cod_Persona`),
  CONSTRAINT `t_producto_persona_ibfk_3` FOREIGN KEY (`ci_rif`) REFERENCES `t_persona` (`ci_rif`),
  CONSTRAINT `t_producto_persona_ibfk_4` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_producto_persona"
#

INSERT INTO `t_producto_persona` VALUES ('4010046','1','J23337583','2012-12-12','2012-12-12',1032586.05,1032586.05,'A');

#
# Structure for table "t_precios_moneda"
#

DROP TABLE IF EXISTS `t_precios_moneda`;
CREATE TABLE `t_precios_moneda` (
  `cod_moneda` varchar(10) NOT NULL,
  `fecha_precio` varchar(10) DEFAULT NULL,
  `tasa_precio` decimal(17,8) DEFAULT NULL,
  `fecha_hora_actualizacion` datetime DEFAULT NULL,
  `cod_pais` varchar(10) NOT NULL,
  `cod_status` varchar(1) NOT NULL,
  UNIQUE KEY `cod_moneda_UNIQUE` (`cod_moneda`),
  KEY `cod_status` (`cod_status`),
  KEY `cod_pais` (`cod_pais`),
  CONSTRAINT `t_precios_moneda_ibfk_1` FOREIGN KEY (`cod_moneda`) REFERENCES `t_moneda` (`cod_moneda`),
  CONSTRAINT `t_precios_moneda_ibfk_2` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`),
  CONSTRAINT `t_precios_moneda_ibfk_3` FOREIGN KEY (`cod_pais`) REFERENCES `t_pais` (`cod_pais`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_precios_moneda"
#


#
# Structure for table "t_modulo"
#

DROP TABLE IF EXISTS `t_modulo`;
CREATE TABLE `t_modulo` (
  `cod_modulo` varchar(3) NOT NULL,
  `des_modulo` varchar(100) DEFAULT NULL,
  `cod_status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`cod_modulo`),
  KEY `cod_status` (`cod_status`),
  CONSTRAINT `t_modulo_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_modulo"
#


#
# Structure for table "t_log_auditoria"
#

DROP TABLE IF EXISTS `t_log_auditoria`;
CREATE TABLE `t_log_auditoria` (
  `cod_auditoria` varchar(20) NOT NULL,
  `cod_usuario` varchar(20) NOT NULL,
  `cod_modulo` varchar(10) NOT NULL,
  `cod_opcion` varchar(10) NOT NULL,
  `cod_perfil_usuario` varchar(20) NOT NULL,
  `fecha_registro` date NOT NULL,
  `hora` time DEFAULT NULL,
  `descripcion_detalle` longtext,
  `cod_status` varchar(1) NOT NULL,
  KEY `t_log_auditoria_ibfk_2` (`cod_status`),
  KEY `t_log_auditable_fk_idx` (`cod_auditoria`),
  CONSTRAINT `t_log_auditable_fk` FOREIGN KEY (`cod_auditoria`) REFERENCES `t_auditables` (`cod_auditoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `t_log_auditoria_ibfk_2` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_log_auditoria"
#

INSERT INTO `t_log_auditoria` VALUES ('0403','luis','04','03','1','2017-03-17','15:32:54','insert into t_persona values(\'null=\',\'razon_social_persona=0\',\'cod_estado_nacimiento=0\',\'cod_persona=0\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=0\',\'fecha_actualizacion=null\',\'cod_ciudad_nacimiento=0\',\'Id_Persona=0\',\'zona_postal=0\',\'ape_Persona2=0\',\'ape_Persona1=0\',\'persona_contacto=0\',\'cod_localizacion=0\',\'ci_rif=0\',\'nom_Persona2=0\',\'nom_Persona1=0\',\'fecha_activacion=null\',\'tipo_persona=0\',\'sexo=Masculino\',\'cod_pais_nacimiento=0\',\'sitio_referencia=0\')','A'),('0403','luis','04','03','1','2017-03-17','15:35:48','insert into t_persona values(\'null=\',\'razon_social_persona=5\',\'cod_estado_nacimiento=5\',\'cod_persona=4\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=5\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=5\',\'Id_Persona=4\',\'zona_postal=5\',\'ape_Persona2=4\',\'ape_Persona1=4\',\'persona_contacto=5\',\'cod_localizacion=5\',\'ci_rif=4\',\'nom_Persona2=4\',\'nom_Persona1=4\',\'fecha_activacion=2017/03/17\',\'tipo_persona=4\',\'sexo=Masculino\',\'cod_pais_nacimiento=5\',\'sitio_referencia=5\')','A'),('0403','luis','04','03','1','2017-03-17','15:42:10','insert into t_persona values(\'null=\',\'razon_social_persona=i\',\'cod_estado_nacimiento=o\',\'cod_persona=7\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=h\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=i\',\'Id_Persona=3\',\'zona_postal=g\',\'ape_Persona2=h\',\'ape_Persona1=g\',\'persona_contacto=o\',\'cod_localizacion=p\',\'ci_rif=713239\',\'nom_Persona2=t\',\'nom_Persona1=r\',\'fecha_activacion=2017/03/17\',\'tipo_persona=3\',\'sexo=Masculino\',\'cod_pais_nacimiento=u\',\'sitio_referencia=j\')','A'),('0403','luis','04','03','1','2017-03-17','15:42:55','insert into t_persona values(\'null=\',\'razon_social_persona=i\',\'cod_estado_nacimiento=o\',\'cod_persona=7\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=h\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=i\',\'Id_Persona=3\',\'zona_postal=g\',\'ape_Persona2=h\',\'ape_Persona1=g\',\'persona_contacto=o\',\'cod_localizacion=p\',\'ci_rif=713239\',\'nom_Persona2=t\',\'nom_Persona1=r\',\'fecha_activacion=2017/03/17\',\'tipo_persona=3\',\'sexo=Masculino\',\'cod_pais_nacimiento=u\',\'sitio_referencia=j\',\'null=\',\'razon_social_persona=i\',\'cod_estado_nacimiento=k\',\'cod_persona=l\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=l\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=h\',\'Id_Persona=h\',\'zona_postal=k\',\'ape_Persona2=erwe\',\'ape_Persona1=ewr\',\'persona_contacto=u\',\'cod_localizacion=gh\',\'ci_rif=3\',\'nom_Persona2=e\',\'nom_Persona1=ueiew\',\'fecha_activacion=2017/03/17\',\'tipo_persona=j\',\'sexo=Masculino\',\'cod_pais_nacimiento=j\',\'sitio_referencia=g\')','A'),('0403','luis','04','03','1','2017-03-17','15:46:06','insert into t_persona values(\'null=\',\'razon_social_persona=u\',\'cod_estado_nacimiento=g\',\'cod_persona=56\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=f\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=h\',\'Id_Persona=2\',\'zona_postal=d\',\'ape_Persona2=3423\',\'ape_Persona1=fddwe\',\'persona_contacto=i\',\'cod_localizacion=u\',\'ci_rif=55656\',\'nom_Persona2=eqwe\',\'nom_Persona1=qwewe\',\'fecha_activacion=2017/03/17\',\'tipo_persona=2\',\'sexo=Masculino\',\'cod_pais_nacimiento=i\',\'sitio_referencia=r\')','A'),('0403','luis','04','03','1','2017-03-17','15:49:44','insert into t_persona values(\'null=\',\'razon_social_persona=i\',\'cod_estado_nacimiento=i\',\'cod_persona=o\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=i\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=i\',\'Id_Persona=o\',\'zona_postal=i\',\'ape_Persona2=p\',\'ape_Persona1=p\',\'persona_contacto=i\',\'cod_localizacion=i\',\'ci_rif=o\',\'nom_Persona2=p\',\'nom_Persona1=p\',\'fecha_activacion=2017/03/17\',\'tipo_persona=o\',\'sexo=Masculino\',\'cod_pais_nacimiento=i\',\'sitio_referencia=i\')','A'),('0403','luis','04','03','1','2017-03-17','15:58:33','insert into t_persona values(\'null=\',\'razon_social_persona=h\',\'cod_estado_nacimiento=h\',\'cod_persona=u\',\'fecha_nacimiento=2017/03/18\',\'estado_civil=Soltero\',\'lugar_nacimiento=h\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=h\',\'Id_Persona=u\',\'zona_postal=h\',\'ape_Persona2=l\',\'ape_Persona1=l\',\'persona_contacto=h\',\'cod_localizacion=h\',\'ci_rif=u\',\'nom_Persona2=l\',\'nom_Persona1=l\',\'fecha_activacion=2017/03/17\',\'tipo_persona=u\',\'sexo=Masculino\',\'cod_pais_nacimiento=h\',\'sitio_referencia=h\')','A'),('0403','luis','04','03','1','2017-03-17','16:01:29','insert into t_persona values(\'null=\',\'razon_social_persona=9\',\'cod_estado_nacimiento=9\',\'cod_persona=6\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=9\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=9\',\'Id_Persona=6\',\'zona_postal=9\',\'ape_Persona2=8\',\'ape_Persona1=8\',\'persona_contacto=9\',\'cod_localizacion=9\',\'ci_rif=6\',\'nom_Persona2=8\',\'nom_Persona1=8\',\'fecha_activacion=2017/03/17\',\'tipo_persona=6\',\'sexo=Masculino\',\'cod_pais_nacimiento=9\',\'sitio_referencia=9\')','A'),('0403','luis','04','03','1','2017-03-17','16:05:05','insert into t_persona values(\'razon_social_persona=u\',\'cod_estado_nacimiento=o\',\'cod_persona=o\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=y\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=i\',\'Id_Persona=6\',\'zona_postal=h\',\'ape_Persona2=2\',\'ape_Persona1=yuy\',\'persona_contacto=i\',\'cod_localizacion=i\',\'ci_rif=u\',\'nom_Persona2=5fy\',\'nom_Persona1=6\',\'fecha_activacion=2017/03/17\',\'tipo_persona=p\',\'sexo=Masculino\',\'cod_pais_nacimiento=u\',\'sitio_referencia=h\')','A'),('0403','luis','04','03','1','2017-03-17','16:05:47','insert into t_persona values(\'razon_social_persona=u\',\'cod_estado_nacimiento=o\',\'cod_persona=o\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=y\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=i\',\'Id_Persona=6\',\'zona_postal=h\',\'ape_Persona2=2\',\'ape_Persona1=yuy\',\'persona_contacto=i\',\'cod_localizacion=i\',\'ci_rif=u\',\'nom_Persona2=5fy\',\'nom_Persona1=6\',\'fecha_activacion=2017/03/17\',\'tipo_persona=p\',\'sexo=Masculino\',\'cod_pais_nacimiento=u\',\'sitio_referencia=h\',\'razon_social_persona=p\',\'cod_estado_nacimiento=t\',\'cod_persona=o\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=dfu\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=y\',\'Id_Persona=o\',\'zona_postal=uyu\',\'ape_Persona2=g\',\'ape_Persona1=h\',\'persona_contacto=hj\',\'cod_localizacion=g\',\'ci_rif=o\',\'nom_Persona2=h\',\'nom_Persona1=h\',\'fecha_activacion=2017/03/17\',\'tipo_persona=p\',\'sexo=Masculino\',\'cod_pais_nacimiento=g\',\'sitio_referencia=jhgj\')','A'),('0403','luis','04','03','1','2017-03-17','16:06:33','insert into t_persona values(\'razon_social_persona=u\',\'cod_estado_nacimiento=o\',\'cod_persona=o\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=y\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=i\',\'Id_Persona=6\',\'zona_postal=h\',\'ape_Persona2=2\',\'ape_Persona1=yuy\',\'persona_contacto=i\',\'cod_localizacion=i\',\'ci_rif=u\',\'nom_Persona2=5fy\',\'nom_Persona1=6\',\'fecha_activacion=2017/03/17\',\'tipo_persona=p\',\'sexo=Masculino\',\'cod_pais_nacimiento=u\',\'sitio_referencia=h\',\'razon_social_persona=p\',\'cod_estado_nacimiento=t\',\'cod_persona=o\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=dfu\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=y\',\'Id_Persona=o\',\'zona_postal=uyu\',\'ape_Persona2=g\',\'ape_Persona1=h\',\'persona_contacto=hj\',\'cod_localizacion=g\',\'ci_rif=o\',\'nom_Persona2=h\',\'nom_Persona1=h\',\'fecha_activacion=2017/03/17\',\'tipo_persona=p\',\'sexo=Masculino\',\'cod_pais_nacimiento=g\',\'sitio_referencia=jhgj\',\'razon_social_persona=o\',\'cod_estado_nacimiento=uiui\',\'cod_persona=j\',\'fecha_nacimiento=2017/03/18\',\'estado_civil=Soltero\',\'lugar_nacimiento=iooio\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=huiu\',\'Id_Persona=h\',\'zona_postal=ioio\',\'ape_Persona2=klkl\',\'ape_Persona1=uyuy\',\'persona_contacto=o\',\'cod_localizacion=po\',\'ci_rif=j\',\'nom_Persona2=yuy\',\'nom_Persona1=yu\',\'fecha_activacion=2017/03/17\',\'tipo_persona=j\',\'sexo=Masculino\',\'cod_pais_nacimiento=hui\',\'sitio_referencia=tfhj\')','A'),('0403','luis','04','03','1','2017-03-17','16:08:22','insert into t_persona values(\'razon_social_persona=d\',\'cod_estado_nacimiento=d\',\'cod_persona=y\',\'fecha_nacimiento=2017/03/04\',\'estado_civil=Soltero\',\'lugar_nacimiento=d\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=d\',\'Id_Persona=5\',\'zona_postal=d\',\'ape_Persona2=lklk\',\'ape_Persona1=kjkj\',\'persona_contacto=d\',\'cod_localizacion=d\',\'ci_rif=y\',\'nom_Persona2=kjkj\',\'nom_Persona1=jkk\',\'fecha_activacion=2017/03/17\',\'tipo_persona=g\',\'sexo=Masculino\',\'cod_pais_nacimiento=d\',\'sitio_referencia=d\')','A'),('0403','luis','04','03','1','2017-03-17','16:10:01','insert into t_persona values(\'razon_social_persona=i\',\'cod_estado_nacimiento=i\',\'cod_persona=u\',\'fecha_nacimiento=2017/03/11\',\'estado_civil=Soltero\',\'lugar_nacimiento=i\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=i\',\'Id_Persona=u\',\'zona_postal=i\',\'ape_Persona2=ll\',\'ape_Persona1=j\',\'persona_contacto=i\',\'cod_localizacion=i\',\'ci_rif=u\',\'nom_Persona2=j\',\'nom_Persona1=j\',\'fecha_activacion=2017/03/17\',\'tipo_persona=u\',\'sexo=Masculino\',\'cod_pais_nacimiento=i\',\'sitio_referencia=pop\')','A'),('0403','luis','04','03','1','2017-03-17','16:10:25','insert into t_persona values(\'razon_social_persona=i\',\'cod_estado_nacimiento=i\',\'cod_persona=u\',\'fecha_nacimiento=2017/03/11\',\'estado_civil=Soltero\',\'lugar_nacimiento=i\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=i\',\'Id_Persona=u\',\'zona_postal=i\',\'ape_Persona2=ll\',\'ape_Persona1=j\',\'persona_contacto=i\',\'cod_localizacion=i\',\'ci_rif=u\',\'nom_Persona2=j\',\'nom_Persona1=j\',\'fecha_activacion=2017/03/17\',\'tipo_persona=u\',\'sexo=Masculino\',\'cod_pais_nacimiento=i\',\'sitio_referencia=pop\',\'razon_social_persona=i\',\'cod_estado_nacimiento=i\',\'cod_persona=i\',\'fecha_nacimiento=2017/03/17\',\'estado_civil=Soltero\',\'lugar_nacimiento=i\',\'fecha_actualizacion=2017/03/17\',\'cod_ciudad_nacimiento=i\',\'Id_Persona=i\',\'zona_postal=i\',\'ape_Persona2=j\',\'ape_Persona1=j\',\'persona_contacto=i\',\'cod_localizacion=i\',\'ci_rif=i\',\'nom_Persona2=j\',\'nom_Persona1=j\',\'fecha_activacion=2017/03/17\',\'tipo_persona=i\',\'sexo=Masculino\',\'cod_pais_nacimiento=i\',\'sitio_referencia=p\')','A'),('0403','luis','04','03','1','2017-03-27','13:28:08','insert into t_persona values(\'razon_social_persona=Ninguna\',\'cod_estado_nacimiento=DC\',\'cod_persona=3132\',\'fecha_nacimiento=2017/03/23\',\'estado_civil=Soltero\',\'lugar_nacimiento=Altamira\',\'fecha_actualizacion=2017/03/27\',\'cod_ciudad_nacimiento=Caracas\',\'Id_Persona=1\',\'zona_postal=1011\',\'ape_Persona2=LLamozas\',\'ape_Persona1=Becerra\',\'persona_contacto=Josue Becerra\',\'cod_localizacion=1011\',\'ci_rif=20793760\',\'nom_Persona2=None\',\'nom_Persona1=Josue\',\'fecha_activacion=2017/03/27\',\'tipo_persona=N\',\'sexo=Masculino\',\'cod_pais_nacimiento=Venezuela\',\'sitio_referencia=Lidice, La Pastora\')','A'),('0403','luis','04','03','1','2017-03-28','11:15:56','insert into t_persona values(\'razon_social_persona=4\',\'cod_estado_nacimiento=4\',\'cod_persona=4\',\'fecha_nacimiento=2017/03/18\',\'estado_civil=Soltero\',\'lugar_nacimiento=44\',\'fecha_actualizacion=2017/03/28\',\'cod_ciudad_nacimiento=4\',\'Id_Persona=4\',\'zona_postal=44\',\'ape_Persona2=4\',\'ape_Persona1=4\',\'persona_contacto=4\',\'cod_localizacion=4\',\'ci_rif=4\',\'nom_Persona2=4\',\'nom_Persona1=4\',\'fecha_activacion=2017/03/28\',\'tipo_persona=4\',\'sexo=Femenino\',\'cod_pais_nacimiento=44\',\'sitio_referencia=4\')','A'),('0403','luis','04','03','1','2017-03-28','11:16:37','insert into t_persona values(\'razon_social_persona=4\',\'cod_estado_nacimiento=4\',\'cod_persona=4\',\'fecha_nacimiento=2017/03/18\',\'estado_civil=Soltero\',\'lugar_nacimiento=44\',\'fecha_actualizacion=2017/03/28\',\'cod_ciudad_nacimiento=4\',\'Id_Persona=4\',\'zona_postal=44\',\'ape_Persona2=4\',\'ape_Persona1=4\',\'persona_contacto=4\',\'cod_localizacion=4\',\'ci_rif=4\',\'nom_Persona2=4\',\'nom_Persona1=4\',\'fecha_activacion=2017/03/28\',\'tipo_persona=4\',\'sexo=Femenino\',\'cod_pais_nacimiento=44\',\'sitio_referencia=4\',\'razon_social_persona=9\',\'cod_estado_nacimiento=9\',\'cod_persona=9\',\'fecha_nacimiento=2017/03/10\',\'estado_civil=Soltero\',\'lugar_nacimiento=9\',\'fecha_actualizacion=2017/03/28\',\'cod_ciudad_nacimiento=9\',\'Id_Persona=9\',\'zona_postal=9\',\'ape_Persona2=9\',\'ape_Persona1=9\',\'persona_contacto=9\',\'cod_localizacion=9\',\'ci_rif=9\',\'nom_Persona2=99\',\'nom_Persona1=9\',\'fecha_activacion=2017/03/28\',\'tipo_persona=9\',\'sexo=Masculino\',\'cod_pais_nacimiento=9\',\'sitio_referencia=9\')','A'),('0403','jose','04','03','1','2017-04-20','15:00:03','insert into t_persona values(\'razon_social_persona=ssss\',\'cod_estado_nacimiento=ssss\',\'cod_persona=4\',\'fecha_nacimiento=2011/04/22\',\'estado_civil=Soltero\',\'lugar_nacimiento=ssss\',\'fecha_actualizacion=2017/04/20\',\'cod_ciudad_nacimiento=ssss\',\'Id_Persona=sss\',\'zona_postal=ssss\',\'ape_Persona2=ss\',\'ape_Persona1=ss\',\'persona_contacto=ssss\',\'cod_localizacion=ssss\',\'ci_rif=4444\',\'nom_Persona2=ss\',\'nom_Persona1=ss\',\'fecha_activacion=2017/04/20\',\'tipo_persona=n\',\'sexo=Masculino\',\'cod_pais_nacimiento=sss\',\'sitio_referencia=ssss\')','A'),('0403','jose','04','03','1','2017-04-20','15:00:37','insert into t_persona values(\'razon_social_persona=ssss\',\'cod_estado_nacimiento=ssss\',\'cod_persona=4\',\'fecha_nacimiento=2011/04/22\',\'estado_civil=Soltero\',\'lugar_nacimiento=ssss\',\'fecha_actualizacion=2017/04/20\',\'cod_ciudad_nacimiento=ssss\',\'Id_Persona=sss\',\'zona_postal=ssss\',\'ape_Persona2=ss\',\'ape_Persona1=ss\',\'persona_contacto=ssss\',\'cod_localizacion=ssss\',\'ci_rif=4444\',\'nom_Persona2=ss\',\'nom_Persona1=ss\',\'fecha_activacion=2017/04/20\',\'tipo_persona=n\',\'sexo=Masculino\',\'cod_pais_nacimiento=sss\',\'sitio_referencia=ssss\',\'razon_social_persona=a\',\'cod_estado_nacimiento=a\',\'cod_persona=11111\',\'fecha_nacimiento=2017/04/15\',\'estado_civil=Soltero\',\'lugar_nacimiento=a\',\'fecha_actualizacion=2017/04/20\',\'cod_ciudad_nacimiento=a\',\'Id_Persona=a\',\'zona_postal=a\',\'ape_Persona2=a\',\'ape_Persona1=a\',\'persona_contacto=a\',\'cod_localizacion=a\',\'ci_rif=111111111\',\'nom_Persona2=a\',\'nom_Persona1=a\',\'fecha_activacion=2017/04/20\',\'tipo_persona=a\',\'sexo=Masculino\',\'cod_pais_nacimiento=a\',\'sitio_referencia=a\')','A');

#
# Structure for table "t_instalacion_sistema"
#

DROP TABLE IF EXISTS `t_instalacion_sistema`;
CREATE TABLE `t_instalacion_sistema` (
  `codigo_sistema` varchar(20) NOT NULL,
  `descripcion_Sistema` varchar(200) DEFAULT NULL,
  `version_sistema` varchar(20) DEFAULT NULL,
  `fecha_instalacion` date DEFAULT NULL,
  `fecha_actualizacion` date DEFAULT NULL,
  `memoria_RAM_requerida` varchar(45) DEFAULT NULL,
  `manejador_documentos_sistema` varchar(45) DEFAULT NULL,
  `instalar_servidor_ApacheTOMCAT` varchar(45) DEFAULT NULL,
  `tipo_Sistema_Operativo` varchar(45) DEFAULT NULL,
  `ArquitecturaNro_Bits` varchar(3) DEFAULT NULL,
  `tipo_Manejador_BaseDatos` varchar(45) DEFAULT NULL,
  `usuario_Admin_BD` varchar(45) DEFAULT NULL,
  `clave_Admin_BD` varchar(45) DEFAULT NULL,
  `nombre_Base_Datos` varchar(80) DEFAULT NULL,
  `bd_manejo_reportesDocumentos` varchar(45) DEFAULT NULL,
  `nombre_Servidor` varchar(80) DEFAULT NULL,
  `autenticacion_directorioActivo` varchar(1) DEFAULT NULL,
  `manejo_multi_hilos` varchar(1) DEFAULT NULL,
  `manejo_chat_ayuda_online` varchar(1) DEFAULT NULL,
  `requiereInstalar_software_manejador_contenido` varchar(1) DEFAULT NULL,
  `manejo_historicos_Tablas` varchar(1) DEFAULT NULL,
  `cod_perfil_usuario_instalador` varchar(10) DEFAULT NULL,
  `responsable_instalacion` varchar(45) DEFAULT NULL,
  `ruta_log_auditoria_instalacion` varchar(100) DEFAULT NULL,
  `direccion_ip` varchar(100) DEFAULT NULL,
  `puerto_conexion` varchar(45) DEFAULT NULL,
  `espacio_minimo_DiscoDuro_servidor` varchar(45) DEFAULT NULL,
  `espacio_minimo_manejador_BD` varchar(45) DEFAULT NULL,
  `activar_directorioActivo` varchar(1) DEFAULT NULL,
  `servicio_directorioActivo` varchar(100) DEFAULT NULL,
  `servidor_correos` varchar(100) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  PRIMARY KEY (`codigo_sistema`),
  KEY `cod_status` (`cod_status`),
  CONSTRAINT `t_instalacion_sistema_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_instalacion_sistema"
#

INSERT INTO `t_instalacion_sistema` VALUES ('1','1','1','2017-03-28','2017-03-28','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','A');

#
# Structure for table "t_forma_pago"
#

DROP TABLE IF EXISTS `t_forma_pago`;
CREATE TABLE `t_forma_pago` (
  `cod_forma_pago` varchar(8) NOT NULL,
  `des_forma_pago` varchar(100) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_forma_pago`),
  KEY `cod_status` (`cod_status`),
  CONSTRAINT `t_forma_pago_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_forma_pago"
#

INSERT INTO `t_forma_pago` VALUES ('01','efectivo','A'),('02','Transferencia','A');

#
# Structure for table "t_estado"
#

DROP TABLE IF EXISTS `t_estado`;
CREATE TABLE `t_estado` (
  `cod_estado` varchar(10) NOT NULL,
  `cod_pais` varchar(10) NOT NULL,
  `descripcion_estado` varchar(80) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_estado`),
  UNIQUE KEY `cod_pais_UNIQUE` (`cod_pais`),
  KEY `cod_status_idx` (`cod_status`),
  CONSTRAINT `t_estado_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`),
  CONSTRAINT `t_estado_ibfk_2` FOREIGN KEY (`cod_pais`) REFERENCES `t_pais` (`cod_pais`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_estado"
#


#
# Structure for table "t_dias_feriados"
#

DROP TABLE IF EXISTS `t_dias_feriados`;
CREATE TABLE `t_dias_feriados` (
  `fecha_feriado` date NOT NULL,
  `des_feriado` varchar(60) DEFAULT NULL,
  `dia_feriado` varchar(9) DEFAULT NULL,
  `mes_feriado` varchar(12) DEFAULT NULL,
  `año_feriado` year(4) DEFAULT NULL,
  `cod_status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`fecha_feriado`),
  KEY `cod_status` (`cod_status`),
  CONSTRAINT `t_dias_feriados_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_dias_feriados"
#

INSERT INTO `t_dias_feriados` VALUES ('2017-05-01','Dia del trabajador','Lunes','Mayo',2017,'A'),('2017-05-06','prueba','Domingo','Mayo',2017,'A');

#
# Structure for table "t_ciudad"
#

DROP TABLE IF EXISTS `t_ciudad`;
CREATE TABLE `t_ciudad` (
  `cod_ciudad` varchar(10) NOT NULL,
  `cod_estado` varchar(10) NOT NULL,
  `descripcion_ciudad` varchar(80) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_ciudad`,`cod_estado`),
  KEY `cod_estado` (`cod_estado`),
  KEY `t_ciudad_ibfk_1` (`cod_status`),
  CONSTRAINT `t_ciudad_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`),
  CONSTRAINT `t_ciudad_ibfk_2` FOREIGN KEY (`cod_estado`) REFERENCES `t_estado` (`cod_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_ciudad"
#


#
# Structure for table "t_subopciones"
#

DROP TABLE IF EXISTS `t_subopciones`;
CREATE TABLE `t_subopciones` (
  `cod_subopcion` varchar(2) NOT NULL,
  `des_subopcion` varchar(100) DEFAULT NULL,
  `cod_status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`cod_subopcion`),
  KEY `cod_status` (`cod_status`),
  CONSTRAINT `t_subopciones_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_subopciones"
#


#
# Structure for table "t_tipo_inversion"
#

DROP TABLE IF EXISTS `t_tipo_inversion`;
CREATE TABLE `t_tipo_inversion` (
  `cod_tipo_inversion` varchar(10) NOT NULL,
  `des_tipo_inversion` varchar(100) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_tipo_inversion`),
  KEY `t_tipo_inversion_ibfk_1` (`cod_status`),
  CONSTRAINT `t_tipo_inversion_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_tipo_inversion"
#


#
# Structure for table "t_cartera_inversion"
#

DROP TABLE IF EXISTS `t_cartera_inversion`;
CREATE TABLE `t_cartera_inversion` (
  `cod_cartera_inversion` varchar(10) NOT NULL,
  `cod_tipo_inversion` varchar(10) NOT NULL,
  `cod_contrato` varchar(10) NOT NULL,
  `cod_Producto` varchar(10) NOT NULL,
  `ci_rif` varchar(20) NOT NULL,
  `fecha_operacion` date NOT NULL,
  `fecha_valor` date DEFAULT NULL,
  `fecha_registro` date DEFAULT NULL,
  `tipo_operacion` varchar(2) DEFAULT NULL,
  `monto_precio` decimal(17,2) DEFAULT NULL,
  `monto_comision` decimal(17,2) DEFAULT NULL,
  `porcentaje_comision` decimal(17,2) DEFAULT NULL,
  `monto_operacion` decimal(17,2) DEFAULT NULL,
  `rendimiento` decimal(17,2) DEFAULT NULL,
  `Intereses` decimal(17,2) DEFAULT NULL,
  `cod_pais` varchar(10) DEFAULT NULL,
  `cod_moneda` varchar(10) DEFAULT NULL,
  `cod_status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`cod_cartera_inversion`,`cod_contrato`),
  UNIQUE KEY `cod_tipo_inversion` (`cod_tipo_inversion`,`cod_contrato`,`cod_Producto`,`ci_rif`),
  UNIQUE KEY `cod_contrato_UNIQUE` (`cod_contrato`),
  KEY `cod_status` (`cod_status`),
  KEY `cod_Producto` (`cod_Producto`),
  KEY `ci_rif` (`ci_rif`),
  CONSTRAINT `t_cartera_inversion_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`),
  CONSTRAINT `t_cartera_inversion_ibfk_2` FOREIGN KEY (`cod_tipo_inversion`) REFERENCES `t_tipo_inversion` (`cod_tipo_inversion`),
  CONSTRAINT `t_cartera_inversion_ibfk_3` FOREIGN KEY (`cod_Producto`) REFERENCES `t_producto` (`cod_Producto`),
  CONSTRAINT `t_cartera_inversion_ibfk_4` FOREIGN KEY (`ci_rif`) REFERENCES `t_persona` (`ci_rif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_cartera_inversion"
#


#
# Structure for table "t_inversiones"
#

DROP TABLE IF EXISTS `t_inversiones`;
CREATE TABLE `t_inversiones` (
  `cod_inversion` varchar(10) NOT NULL,
  `cod_tipo_inversion` varchar(10) NOT NULL,
  `cod_contrato` varchar(10) NOT NULL,
  `cod_Producto` varchar(10) NOT NULL,
  `ci_rif` varchar(20) NOT NULL,
  `fecha_operacion` date DEFAULT NULL,
  `fecha_valor` date DEFAULT NULL,
  `fecha_registro` date DEFAULT NULL,
  `tipo_operacion` varchar(2) DEFAULT NULL,
  `monto_precio` decimal(17,2) DEFAULT NULL,
  `monto_comision` decimal(17,2) DEFAULT NULL,
  `porcentaje_comision` decimal(17,2) DEFAULT NULL,
  `monto_operacion` decimal(17,2) DEFAULT NULL,
  `cod_cartera_inversion` varchar(10) NOT NULL,
  `cod_moneda` varchar(10) DEFAULT NULL,
  `cod_pais` varchar(10) DEFAULT NULL,
  `Rendimiento` decimal(17,8) DEFAULT NULL,
  `interes` decimal(17,8) DEFAULT NULL,
  `condiciones` varchar(100) DEFAULT NULL,
  `mercado_ps` varchar(1) DEFAULT NULL,
  `base_360_365` varchar(20) DEFAULT NULL,
  `pago_dividendos` varchar(10) DEFAULT NULL,
  `frecuencia_pago_dividendo` varchar(1) DEFAULT NULL,
  `tasa` decimal(17,8) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_inversion`),
  UNIQUE KEY `ci_rif_UNIQUE` (`ci_rif`),
  UNIQUE KEY `cod_Producto_UNIQUE` (`cod_Producto`),
  UNIQUE KEY `cod_tipo_inversion_UNIQUE` (`cod_tipo_inversion`),
  UNIQUE KEY `cod_contrato_UNIQUE` (`cod_contrato`),
  UNIQUE KEY `cod_cartera_inversion_UNIQUE` (`cod_cartera_inversion`),
  KEY `cod_contrato` (`cod_contrato`),
  KEY `cod_moneda` (`cod_moneda`),
  KEY `cod_pais` (`cod_pais`),
  KEY `cod_contrato_2` (`cod_contrato`),
  KEY `t_inversiones_ibfk_1` (`cod_status`),
  CONSTRAINT `t_inversiones_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`),
  CONSTRAINT `t_inversiones_ibfk_2` FOREIGN KEY (`ci_rif`) REFERENCES `t_persona` (`ci_rif`),
  CONSTRAINT `t_inversiones_ibfk_3` FOREIGN KEY (`cod_Producto`) REFERENCES `t_producto` (`cod_Producto`),
  CONSTRAINT `t_inversiones_ibfk_4` FOREIGN KEY (`cod_moneda`) REFERENCES `t_moneda` (`cod_moneda`),
  CONSTRAINT `t_inversiones_ibfk_5` FOREIGN KEY (`cod_pais`) REFERENCES `t_pais` (`cod_pais`),
  CONSTRAINT `t_inversiones_ibfk_6` FOREIGN KEY (`cod_tipo_inversion`) REFERENCES `t_tipo_inversion` (`cod_tipo_inversion`),
  CONSTRAINT `t_inversiones_ibfk_7` FOREIGN KEY (`cod_cartera_inversion`) REFERENCES `t_cartera_inversion` (`cod_cartera_inversion`),
  CONSTRAINT `t_inversiones_ibfk_8` FOREIGN KEY (`cod_contrato`) REFERENCES `t_cartera_inversion` (`cod_contrato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_inversiones"
#


#
# Structure for table "t_tipo_localizacion"
#

DROP TABLE IF EXISTS `t_tipo_localizacion`;
CREATE TABLE `t_tipo_localizacion` (
  `cod_Tipo_localizacion` varchar(10) NOT NULL,
  `des_Tipo_localizacion` varchar(70) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_Tipo_localizacion`),
  KEY `cod_status` (`cod_status`),
  CONSTRAINT `t_tipo_localizacion_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_tipo_localizacion"
#


#
# Structure for table "t_tipo_relacion"
#

DROP TABLE IF EXISTS `t_tipo_relacion`;
CREATE TABLE `t_tipo_relacion` (
  `cod_Tipo_relacion` varchar(10) NOT NULL,
  `des_Tipo_relacion` varchar(70) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_Tipo_relacion`),
  KEY `t_tipo_relacion_ibfk_1` (`cod_status`),
  CONSTRAINT `t_tipo_relacion_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_tipo_relacion"
#


#
# Structure for table "t_relacion_persona"
#

DROP TABLE IF EXISTS `t_relacion_persona`;
CREATE TABLE `t_relacion_persona` (
  `cod_Persona` varchar(20) DEFAULT NULL,
  `cod_producto` varchar(45) DEFAULT NULL,
  `cod_tipo_relacion` varchar(2) DEFAULT NULL,
  `fecha_relacion` varchar(10) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  KEY `t_relacion_persona_ibfk_1_idx` (`cod_Persona`),
  KEY `t_relacion_persona_ibfk_3_idx` (`cod_tipo_relacion`),
  KEY `t_relacion_persona_ibfk_4_idx` (`cod_status`),
  KEY `t_relacion_persona_ibfk_2_idx` (`cod_producto`),
  CONSTRAINT `t_relacion_persona_ibfk_1` FOREIGN KEY (`cod_Persona`) REFERENCES `t_persona` (`cod_status`),
  CONSTRAINT `t_relacion_persona_ibfk_2` FOREIGN KEY (`cod_producto`) REFERENCES `t_producto` (`cod_status`),
  CONSTRAINT `t_relacion_persona_ibfk_3` FOREIGN KEY (`cod_tipo_relacion`) REFERENCES `t_tipo_relacion` (`cod_Tipo_relacion`),
  CONSTRAINT `t_relacion_persona_ibfk_4` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_relacion_persona"
#


#
# Structure for table "t_tipo_transaccion"
#

DROP TABLE IF EXISTS `t_tipo_transaccion`;
CREATE TABLE `t_tipo_transaccion` (
  `cod_tipo_oper` varchar(20) NOT NULL,
  `cuenta` varchar(20) NOT NULL,
  `des_tipo_transaccion` varchar(100) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  `Debe_Haber` varchar(1) DEFAULT NULL,
  `clase_transaccion` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`cod_tipo_oper`,`cuenta`),
  KEY `cod_status_idx` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_tipo_transaccion"
#

INSERT INTO `t_tipo_transaccion` VALUES ('AFC','42200100016','REV. I.S.L.R. PAGO TERC. ADMINISTRACIÓN BS','A','P','Reverso Retiro'),('AGB','43103104001','APORTE GARANTIA BS. (COLECTIVO)','A','P','Aporte'),('AIA','43102199001','AJUSTE I.S.L.R. PROVISIÓN ADMINISTRACIÓN BS.','A','P','Retiro'),('AIB','41102100001','APORTE INVERSION BS. (NO COLECTIVO)','A','P','Aporte'),('AIG','43103104001','AJUSTE I.S.L.R. PROVISION GARANTIA BS.','A','P','Retiro'),('AII','43101100001','AJUSTE I.S.L.R. PROVISION INVERSION BS.','A','P','Retiro'),('AIN','41102100001','APORTE POR TRASPASO DE FONDOS INVERS. BS.','A','P','Aporte'),('AOA','41102100001','APORTE ADMINISTRACIÓN BS (NO COLECTIVO)','A','P','Aporte'),('ARA','43102199001','AJUSTE I.S.L.R. RETENCIÓN ADMINISTRACIÓN BS','A','P','Retro'),('CBI','43400100001','CAPITALIZACIÓN DE INTERESES ADMON BS','A','P','Capitalización'),('CCF','41102100001','REV RETIRO INTER. CAPITALIZ. ADMON BS','A','P','Reverso Retiro'),('CEI','43101100001','COMISION EROGACION INVERSION (MIGRACION)','A','P','Comision Erogacion'),('CFI','43400100001','CAPITALIZACION DE INTERESES INVERSION BS','A','P','Capitalizacion'),('CIP','42200100020','PAGO COMISION POR INVERFACIL','A','P','Cobro Comision Administracion'),('CPP','41102100001','REV. RETIRO INTER. CAPITALIZ. INVERSION BS.','A','P','Reverso Retiro'),('CXA','43102199001','RETIRO INTERESES CAPITALIZADOS ADMON BS','A','P','Retiro'),('DAC','43102199001','DESINCORP. ACCIONES DE INVERSIÓN ADMON BS','A','P','Retiro'),('DGB','43103104001','DESINCORP. GARANTIA BS. (INHABILITADA)','A','P','Desincorporacion'),('DIA','43102199001','DESINCORP. DE INMUEBLES CARACT. MIXTAS BS','A','P','Retiro'),('DIG','43101100001','DESINCORP. DE INMUEBLES GARANTIA BS.','A','P','Retiro'),('DIV','43101100001','DESINCORP. INVERSION BS. (INHABILITADA)','A','P','Desincorporacion'),('DMG','43103102001','DESINCORP. DE MUEBLES GARANTIA BS.','A','P','Retiro'),('DSA','43102199001','DESINCORP. ADMON BS (INHABILITADA)','A','P','Desincorporación'),('DTG','43103103001','DESINC. TITULOS VALOR. GARANTIA BS. (COLECT.)','A','P','Desincorporacion'),('GTV','43103103001','REV. INCORP. TITULOS VALORES GARANTIA BS.','A','P','Reverso Aporte'),('HAG','43103104001','COBRO COM. X ADMON. HABERES (GARANTIA BS.)','A','P','Retiro'),('HAI','43100100001','COBRO COM. X ADMON. HABERES (INVERSION BS','A','P','Retiro'),('IAC','41200100006','INCORPOR. ACCIONES DE INVERSIÓN ADMON BS','A','P','Aporte'),('ICG','43103104001','RETIRO INTERESES CAPITALIZADOS GARANTIA BS','A','P','Retiro'),('IIA','41700100001','INCORPOR. DE INMUEBLES ADMINISTRACIÓN BS','A','P','Aporte'),('IIB','41102100001','INCORPORACION DE FIDEICOMIT. INVERSION BS','A','P','Incorporacion'),('IIG','43101100001','INCORPOR. DE INMUEBLES GARANTIA BS.','A','P','Aporte'),('IMG','41801102001','INCORPOR. DE MUEBLES GARANTIA BS.','A','P','Aporte'),('INC','43101100001','RETIRO INTERESES CAPITALIZADOS INVERSION BS','A','P','Retiro'),('ING','41102100001','INCORPORACION DE FIDEICOMIT. GARANTIA BS. IN','A','P','Incorporacion'),('IOA','41102100001','INCORPORACIÓN DE FIDEICOMIT. ADMON BS','A','P','Incorporación'),('IPT','41102100001','REVERSO PAGO I.S.L.R RETENCIÓN','A','P','Reverso Pago ISLR'),('IRT','43102199001','REV. APORTE POR TRASPASO DE FOND. INVERS B','A','P','Reverso Aporte'),('ISL','42200100016','PAGO DE RETENCIÓN I.S.L.R','A','P','Pago ISLR'),('ITG','43103102001','INCORPOR. DE TITULOS VALORES GARANTIA BS.','A','P','Aporte'),('KCF','43102199001','REV. CAPITALIZACIÓN INTERESES ADMON BS','A','P','Reverso Capitalización'),('KIG','43400100001','CAPITALIZACION DE INTERESES GARANTIA BS.','A','P','Capitalizacion'),('PDH','43102199001','PAGO DE GASTOS A TERCEROS ADMON BS','A','P','Retiro'),('RAB','43102199001','RETIRO DE ADMINISTRACIÓN BS','A','P','Retiro'),('RAC','43102199001','REV. INCORP. ACCIONES DE INVERSIÓN ADMON BS','A','P','Reverso Aporte'),('RAG','41102100001','REVERSO APORTE GARANTIA BS.','A','P','Reverso aporte'),('RAI','43101100001','REVERSO APORTE INVERSION BS.','A','P','Reverso Aporte'),('RCG','41102100001','REV. RETIRO INTER. CAPITALIZ. GARANTIA BS.','A','P','Reverso Retiro'),('RCI','42200100020','REGISTRO COMIS. POR INVERFACIL GARANTIA BS.','A','P','Causacion Comis Adm'),('RDI','41700100001','REV. DESINCORP. DE INMUEBLES ADMON BS','A','P','Reverso Retiro'),('RFI','43101100001','RETIRO DE INVERSION BS.','A','P','Retiro'),('RGB','43103104001','RETIRO DE GARANTIA BS.','A','P','Retiro'),('RGI','42200100001','REVERSO I.S.L.R. POR PAGAR GARANTIA BS.','A','P','Aporte'),('RIA','42200100001','REVERSO I.S.L.R. POR PAGAR ADMINISTRACIÓN BS','A','P','Aporte'),('RIF','43103104001','REV. CAPITALIZACION INTERESES GARANTIA BS.','A','P','Reverso Capitalizacion'),('RIG','43101100001','REV. INCORP. DE INMUEBLES GARANTIA BS.','A','P','Reverso Aporte'),('RII','43102199001','REV. INCORP. DE INMUEBLES ADMON BS','A','P','Reverso Aporte'),('RIO','43101100001','APORTE VOLUNTARIO FONDOS/CAJAS AH. M.E.','A','P','Aporte'),('RMG','41801102001','REV. INCORP. DE MUEBLES GARANTIA BS.','A','P','Reverso Aporte'),('ROA','43102199001','REVERSO APORTE ADMINISTRACIÓN BS','A','P','Retiro'),('RPD','41102100001','REV. PAGO DE GASTOS ADMINISTRACIÓN BS.','A','P','Reverso Retiro'),('RRG','43103104001','REVERSO RETIRO DE GARANTIA BS.','A','P','Reverso Retiro'),('RRI','41102100001','REVERSO RETIRO DE INVERSION BS.','A','P',NULL),('RRP','41102100001','REVERSO RETIRO DE ADMINISTRACIÓN BS.','A','P','Reverso Retiro'),('RSI','42200100001','REVERSO I.S.L.R. POR PAGAR INVERSION BS.','A','P','Aporte'),('RTG','41102100001','REV. APORTE POR TRASPASO FOND. GARANTIA BS','A','P','Reverso Aporte por Transferencia de Fondos'),('RTI','43101100001','RETIRO POR TRASPASO DE FONDOS INVERS. BS.','A','P','Retiro'),('SFG','43400100001','SUSTITUCION FIDUCIARIA GARANTIA','A','P','Desincorporacion'),('SFI','43400100001','SUSTITUCION FIDUCIARIA INVERSION','A','P','Desincorporacion'),('TEG','43400100001','SUST. FID. TITUL. VAL./ EFECTIVO GARANTIA','A','P','Desincorporacion'),('TEI','43400100001','SUST. FID. TITUL. VAL./ EFECTIVO INVERSION','A','P','Desincorporacion'),('TGG','43101100001','REV. RETIRO POR TRASPASO FOND. GARANTIA BS.','A','P','Reverso Retiro'),('TII','41102100001','REV. RETIRO POR TRASPASO DE FOND. INV. BS.','A','P','Reverso Retiro'),('TRG','43101100001','RETIRO POR TRASPASO DE FONDOS GARANTIA BS.','A','P','Retiro'),('VAI','41200100006','REV. DESINCORP. ACCIONES DE INVER. ADMON BS','A','P','Reverso Retiro'),('VIG','43103101001','REV. DESINCORP. DE INMUEBLES GARANTIA BS.','A','P','Reverso Retiro'),('VMG','41801102001','REV. DESINCORP. DE MUEBLES GARANTIA BS.','A','P','Reverso Retiro'),('VTG','41801103001','REV. DESINCORP. TITUL. VALOR. GARANTIA BS.','A','P','Reverso Retiro');

#
# Structure for table "t_asiento_contable"
#

DROP TABLE IF EXISTS `t_asiento_contable`;
CREATE TABLE `t_asiento_contable` (
  `cod_tipo_oper` varchar(20) NOT NULL,
  `clase_transaccion` varchar(30) NOT NULL,
  `cod_cuenta_contable` varchar(30) NOT NULL,
  `Debe_Haber` varchar(1) NOT NULL,
  `tipo_fideicomiso` varchar(50) DEFAULT NULL,
  `calculo` varchar(100) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  KEY `t_asiento_contable_ibfk_3_idx` (`cod_tipo_oper`),
  KEY `t_asiento_contable_ibfk_3_idx1` (`cod_status`),
  KEY `t_asiento_contable_ibfk_1_idx` (`cod_cuenta_contable`),
  CONSTRAINT `t_asiento_contable_ibfk_1` FOREIGN KEY (`cod_cuenta_contable`) REFERENCES `t_plan_contable` (`cod_cuenta_contable`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_asiento_contable_ibfk_2` FOREIGN KEY (`cod_tipo_oper`) REFERENCES `t_tipo_transaccion` (`cod_tipo_oper`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `t_asiento_contable_ibfk_3` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_asiento_contable"
#

INSERT INTO `t_asiento_contable` VALUES ('AOA','Aporte','41102100001','D','administracion',NULL,'A'),('AOA','Aporte','43102199001','H','administracion',NULL,'A'),('ROA','Reverso Aporte','43102199001','D','administracion',NULL,'A'),('ROA','Reverso Aporte','41102100001','H','administracion',NULL,'A'),('RRP','Reverso Retiro','41102100001','D','administracion',NULL,'A'),('RRP','Reverso Retiro','43102199001','H','administracion',NULL,'A'),('RPD','Reverso Retiro','41102100001','D','administracion',NULL,'A'),('RPD','Reverso Retrio','43102199001','H','administracion',NULL,'A'),('IIA','Aporte','41700100001','D','administracion',NULL,'A'),('IIA','Aporte','43102199001','H','administracion',NULL,'A'),('RII','Reverso Aporte','43102199001','D','administracion',NULL,'A'),('RII','Reverso Aporte','41700100001','H','administracion',NULL,'A'),('DIA','Retiro','43102199001','D','administracion',NULL,'A'),('DIA','Retiro','41700100001','H','administracion',NULL,'A'),('RDI','Reverso Retiro','41700100001','D','administracion',NULL,'A'),('RDI','Reverso Retiro','43102199001','H','administracion',NULL,'A'),('IAC','Aporte','41200100006','D','administracion',NULL,'A'),('IAC','Aporte','43102199001','H','administracion',NULL,'A'),('RAC','Reverso Aporte','43102199001','D','administracion',NULL,'A'),('RAC','Reverso Aporte','41200100006','H','administracion',NULL,'A'),('DAC','Retiro','43102199001','D','administracion',NULL,'A'),('DAC','Retiro','41200100006','H','administracion',NULL,'A'),('VAI','Reverso Retiro','41200100006','D','administracion',NULL,'A'),('VAI','Reverso Retiro','43102199001','H','administracion',NULL,'A'),('CBI','Capitalización','43400100001','D','administracion',NULL,'A'),('CBI','Capitalización','43102199001','H','administracion',NULL,'A'),('KCF','Reverso Capitalización','43102199001','D','administracion',NULL,'A'),('KCF','Reverso Capitalización','43400100001','H','administracion',NULL,'A'),('CCF','Reverso Retiro','41102100001','D','administracion',NULL,'A'),('CCF','Reverso Retiro','43102199001','H','administracion',NULL,'A'),('IOA','Incorporación','41102100001','D','administracion',NULL,'A'),('IOA','Incorporación','43102199001','H','administracion',NULL,'A'),('ISL','Pago ISLR','42200100016','D','administracion',NULL,'A'),('ISL','Pago ISLR','41102100001','H','administracion',NULL,'A'),('IPT','Reverso Pago ISLR','41102100001','D','administracion',NULL,'A'),('IPT','Reverso Pago ISLR','42200100016','H','administracion',NULL,'A'),('AFC','Reverso Retiro','42200100016','D','administracion',NULL,'A'),('AFC','Reverso Retiro','43102199001','H','administracion',NULL,'A'),('AGB','Aporte','41102100001','D','garantia',NULL,'A'),('AGB','Aporte','43103104001','H','garantia',NULL,'A'),('RAG','Reverso Aporte','43103104001','D','garantia',NULL,'A'),('RAG','Reverso Aporte','41102100001','H','garantia',NULL,'A'),('RTG','Reverso Aporte','43102199001','D','garantia',NULL,'A'),('RTG','Reverso Aporte','41102100001','H','garantia',NULL,'A'),('RRG','Reverso Retiro','41102100001','D','garantia',NULL,'A'),('RRG','Reverso Retiro','43103104001','H','garantia',NULL,'A'),('TGG','Reverso Retiro','41102100001','D','garantia',NULL,'A'),('TGG','Reverso Retiro','43101100001','H','garantia',NULL,'A'),('IIG','Incorporacion','41801104001','D','garantia',NULL,'A'),('IIG','Incorporacion','43103101001','H','garantia',NULL,'A'),('RIG','Reverso Incorporacion','43103101001','D','garantia',NULL,'A'),('RIG','Reverso Incorporacion','41801104001','H','garantia',NULL,'A'),('DIG','Desincorporacion de Inmuebles','43103101001','D','garantia',NULL,'A'),('DIG','Desincorporacion de Inmuebles','41801104001','H','garantia',NULL,'A'),('VIG','Reverso Desincorporacion','41801104001','D','garantia',NULL,'A'),('VIG','Reverso Desincorporacion','43103101001','H','garantia',NULL,'A'),('IMG','Incorporacion','41801102001','D','garantia',NULL,'A'),('IMG','Incorporacion','43103102001','H','garantia',NULL,'A'),('RMG','Reverso de Incorporacion','43103102001','D','garantia',NULL,'A'),('RMG','Reverso de Incoporacion','41801102001','H','garantia',NULL,'A'),('DMG','Desincorporacion','43103102001','D','garantia',NULL,'A'),('DMG','Desincorporacion','41801102001','H','garantia',NULL,'A'),('VMG','Reverso Desincorporacion','41801102001','D','garantia',NULL,'A'),('VMG','Reverso Desincorporacion','43103102001','H','garantia',NULL,'A'),('ITG','Incorporacion','41801103001','D','garantia',NULL,'A'),('ITG','Incorporacion','43103102001','H','garantia',NULL,'A'),('GTV','Reverso Incorporacion','43103103001','D','garantia',NULL,'A'),('GTV','Reverso Incorporacion','41801103001','H','garantia',NULL,'A'),('DTG','Desincorporacion','43103103001','D','garantia',NULL,'A'),('DTG','Desincorporacion','41801103001','H','garantia',NULL,'A'),('VTG','Reverso Desincorporacion','41801103001','D','garantia',NULL,'A'),('VTG','Reverso Desincorporacion','43103103001','H','garantia',NULL,'A'),('KIG','Capitalizacion','43400100001','D','garantia',NULL,'A'),('KIG','Capitalizacion','43103103001','H','garantia',NULL,'A'),('RIF','Reverso Capitalizacion','43103104001','D','garantia',NULL,'A'),('RIF','Reverso Capitalizacion','43400100001','H','garantia',NULL,'A'),('RCG','Reverso Retiro','41102100001','D','garantia',NULL,'A'),('RCG','Reverso Retiro','43103104001','H','garantia',NULL,'A'),('ING','Incorporacion','41102100001','D','garantia',NULL,'A'),('ING','Incorporacion','43103104001','H','garantia',NULL,'A'),('RCI','Registro Comision','43103104001','D','garantia',NULL,'A'),('RCI','Registro Comision','42200100020','H','garantia',NULL,'A'),('CIP','Pago Comision','42200100020','D','garantia',NULL,'A'),('CIP','Pago Comision','41102100001','H','garantia',NULL,'A'),('RGI','Reverso ISLR','42200100001','D','garantia',NULL,'A'),('RGI','Reverso ISLR','43103104001','H','garantia',NULL,'A'),('AIG','Ajuste ISLR','43103104001','D','garantia',NULL,'A'),('AIG','Ajuste ISLR','42200100001','H','garantia',NULL,'A'),('HAG','Cobro Comision','43103104001','D','garantia',NULL,'A'),('HAG','Cobro Comision','44300100002','H','garantia',NULL,'A'),('AIB','Aporte','41102100001','D','inversion',NULL,'A'),('AIB','Aporte','43101100001','H','inversion',NULL,'A'),('RAI','Reverso Aporte','43101100001','D','inversion',NULL,'A'),('RAI','Reverso Aporte ','41102100001','H','inversion',NULL,'A'),('AIN','Aporte','41102100001','D','inversion',NULL,'A'),('AIN','Aporte','43102199001','H','inversion',NULL,'A');

#
# Structure for table "t_transacciones"
#

DROP TABLE IF EXISTS `t_transacciones`;
CREATE TABLE `t_transacciones` (
  `nro_transaccion` int(11) NOT NULL AUTO_INCREMENT,
  `cod_tipo_transaccion` varchar(20) NOT NULL,
  `cod_Producto` varchar(10) NOT NULL,
  `ci_rif` varchar(20) NOT NULL,
  `cod_moneda` char(10) DEFAULT NULL,
  `fecha_transaccion` date NOT NULL,
  `fecha_registro_transaccion` date NOT NULL,
  `fecha_proceso_transaccion` date NOT NULL,
  `monto_transaccion` decimal(17,2) DEFAULT NULL,
  `nro_cta_banco` varchar(20) DEFAULT NULL,
  `mto_moneda_extranjera_trans` decimal(17,2) DEFAULT NULL,
  `tasa_cambio_trans` decimal(17,2) DEFAULT NULL,
  `nat_trans` varchar(7) DEFAULT NULL,
  `descrip_comis_trans` varchar(30) DEFAULT NULL,
  `monto_comis_aplicada` decimal(17,2) DEFAULT NULL,
  `monto_impuesto_deito_banco` decimal(17,2) DEFAULT NULL,
  `indicador_exoneracion_impuesto_deito_banco` varchar(1) DEFAULT NULL,
  `fecha_ultima_actualizacion` date NOT NULL,
  `cuenta_debito_transferencia` varchar(20) DEFAULT NULL,
  `cuenta_credito_transferencia` varchar(20) DEFAULT NULL,
  `ci_rif_beneficiario_transferencia` varchar(20) DEFAULT NULL,
  `codigo_operador_swift` varchar(20) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  `cod_forma_pago` varchar(8) NOT NULL,
  PRIMARY KEY (`nro_transaccion`),
  KEY `cod_tipo_transaccion` (`cod_tipo_transaccion`)
) ENGINE=InnoDB AUTO_INCREMENT=1021 DEFAULT CHARSET=latin1;

#
# Data for table "t_transacciones"
#

INSERT INTO `t_transacciones` VALUES (4,'AII','4010046','J23337583','01','2017-05-03','2017-06-30','2017-06-30',10000.00,'1515151515615616',0.00,1.00,'1','11',1.00,1.00,'1','2017-05-11','1','1','22047580','2','A','01'),(5,'AIN','4010046','J23337583','01','2017-05-11','2017-06-30','2017-06-30',213213123.00,'123123123123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-14','1','1','22047580','3','A','02'),(6,'IRT','4010046','J23337583','01','2017-05-09','2017-06-30','2017-06-30',123213123.00,'1',1.00,1.00,'1','1',1.00,1.00,'1','2017-05-14','1','1','1','32','A','01'),(7,'AFC','4010046','J23337583','01','2017-05-19','2017-06-30','2017-06-30',1.00,'1',1.00,1.00,'1','1',1.00,1.00,'1','2017-05-14','1','1','1','21','A','01'),(8,'IRT','4010046','J23337583','01','2017-05-25','2017-06-30','2017-06-30',12.00,'212',12.00,12.00,'12','12',12.00,12.00,'2','2017-05-14','12','12','121','21','A','01'),(9,'GTV','4010046','J23337583','01','2017-04-06','2017-06-30','2017-06-30',1.00,'1',1.00,1.00,'1','1',1.00,1.00,'1','2017-05-14','1','1','1','3','A','01'),(10,'HAI','4010046','J23337583','02','2017-01-12','2017-06-30','2017-06-30',1.00,'1',1.00,1.00,'1','1',1.00,1.00,'1','2017-05-14','1','1','1','','A','02'),(11,'RIO','4010046','J23337583','01','2017-02-16','2017-06-30','2017-06-30',213123123.00,'12',121.00,12.00,'12','12',1.00,12.00,'1','2017-05-14','12','12','2','21','A','01'),(12,'AIB','4010046','J23337583','02','2017-04-05','2017-06-30','2017-06-30',13323213.00,'1',1.00,1.00,'1','1',1.00,1.00,'1','2017-05-14','1','1','1','2','A','01'),(13,'RGI','4010046','J23337583','01','2017-02-10','2017-06-30','2017-06-30',2131231.00,'12',12.00,1.00,'1','1',1.00,1.00,'1','2017-05-14','1','1','1','21','A','01'),(14,'AOA','4010046','J23337583','01','2016-11-17','2017-06-30','2017-06-30',12345.00,'12121',1.00,1.00,'1','1',1.00,1.00,'1','2017-05-14','1','1','1','2','A','01'),(15,'KIG','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(21,'ROA','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(22,'DAC','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(23,'RMG','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(24,'ROA','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(25,'RAI','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(26,'CFI','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(27,'IIB','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(28,'RGI','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(29,'RFI','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(30,'CEI','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(31,'RTG','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(32,'IRT','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(33,'RIO','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(34,'RIG','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(35,'DTG','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(36,'IIG','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(37,'ARA','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(38,'TEG','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(39,'RIF','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(40,'DMG','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(41,'DIA','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(42,'AIA','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(43,'VIG','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(44,'RMG','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(45,'RMG','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(46,'IAC','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(47,'VAI','4010046','J23337583','01','2017-05-15','2017-06-30','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(48,'RRI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(49,'AII','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(50,'RTG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(51,'PDH','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(52,'IIA','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(53,'TII','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(54,'VIG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(55,'DSA','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(56,'AII','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(57,'CIP','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(58,'IIG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(59,'RTI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(60,'VTG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(61,'DMG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(62,'RAC','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(63,'TEG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(64,'RAG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(65,'RRI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(66,'CBI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(67,'SFG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(68,'DGB','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(69,'AIG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(70,'KIG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(71,'IMG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(72,'RMG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(73,'VIG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(74,'TEG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(75,'SFG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(76,'RTG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(77,'TEG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(78,'TRG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(79,'VMG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(80,'TEI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(81,'TGG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(82,'ISL','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(83,'RCI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(84,'RIF','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(85,'VMG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(86,'TEI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(87,'RFI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(88,'VMG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(89,'ICG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(90,'SFI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(91,'CXA','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(92,'RII','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(93,'DMG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(94,'ING','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(95,'RRI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(96,'IIB','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(97,'RIO','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(98,'RIO','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(99,'ISL','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(100,'RGB','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(101,'RTG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(102,'RPD','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(103,'RSI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(104,'IPT','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(105,'RRI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(106,'RGI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(107,'RAC','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(108,'RCG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(109,'DSA','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(110,'DIA','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(111,'TEG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(112,'RII','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(113,'CIP','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(114,'VTG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(115,'CCF','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(116,'RFI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(117,'RIF','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(118,'VIG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(119,'AII','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(120,'VIG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(121,'TGG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(122,'IRT','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(123,'DIA','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(124,'AIN','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(125,'GTV','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(126,'CXA','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(127,'RAC','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(128,'AII','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(129,'AFC','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(130,'IIA','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(131,'KIG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(132,'CPP','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(133,'KCF','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(134,'IIB','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(135,'RIO','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(136,'DTG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(137,'RGB','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(138,'IAC','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(139,'RIF','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(140,'KCF','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(141,'IIB','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(142,'IOA','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(143,'TII','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(144,'RCG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(145,'KIG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(146,'DMG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(147,'RGI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(148,'RIA','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(149,'KCF','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(150,'RFI','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(151,'RMG','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(152,'CXA','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(153,'KCF','4010046','J23337583','01','2017-05-15','2017-06-28','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(154,'AIG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(155,'VMG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(156,'RRP','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(157,'AII','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(158,'SFG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(159,'IAC','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(160,'CIP','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(161,'KIG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(162,'RRP','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(163,'CBI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(164,'RCG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(165,'DGB','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(166,'TRG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(167,'ARA','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(168,'KIG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(169,'GTV','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(170,'HAI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(171,'RSI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(172,'RFI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(173,'AIG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(174,'INC','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(175,'VAI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(176,'PDH','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(177,'RTG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(178,'PDH','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(179,'DGB','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(180,'DAC','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(181,'KIG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(182,'CFI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(183,'DTG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(184,'DIV','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(185,'CBI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(186,'RRG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(187,'RRP','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(188,'CPP','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(189,'CPP','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(190,'TRG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(191,'HAI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(192,'CPP','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(193,'DSA','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(194,'TII','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(195,'VIG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(196,'RCI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(197,'INC','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(198,'CFI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(199,'TEI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(200,'CIP','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(201,'RIF','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(202,'RIA','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(203,'RDI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(204,'RIF','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(205,'CPP','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(206,'IPT','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(207,'RAC','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(208,'IIG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(209,'VTG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(210,'RRI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(211,'TRG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(212,'AFC','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(213,'DIG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(214,'DTG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(215,'AIB','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(216,'IPT','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(217,'RTG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(218,'IRT','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(219,'VMG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(220,'IOA','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(221,'RFI','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(222,'ROA','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(223,'RIO','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(224,'ARA','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(225,'DTG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(226,'RRG','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(227,'RAC','4010046','J23337583','01','2017-05-15','2017-06-27','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(228,'DSA','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(229,'ITG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(230,'RRI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(231,'CCF','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(232,'TII','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(233,'AII','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(234,'CIP','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(235,'DIV','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(236,'VIG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(237,'RDI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(238,'RGB','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(239,'TEG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(240,'HAG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(241,'IAC','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(242,'RSI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(243,'RTG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(244,'ISL','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(245,'AIN','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(246,'PDH','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(247,'RTI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(248,'RTI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(249,'RRI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(250,'RII','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(251,'TGG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(252,'CEI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(253,'RFI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(254,'AOA','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(255,'CIP','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(256,'IPT','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(257,'RTI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(258,'RGI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(259,'RAI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(260,'IMG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(261,'VTG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(262,'DAC','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(263,'DGB','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(264,'AIA','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(265,'IIA','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(266,'AIB','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(267,'ING','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(268,'RAI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(269,'RAG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(270,'RII','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(271,'RPD','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(272,'CPP','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(273,'CBI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(274,'RCI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(275,'HAI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(276,'VMG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(277,'RFI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(278,'VTG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(279,'KIG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(280,'TRG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(281,'DIA','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(282,'RIF','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(283,'CFI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(284,'KIG','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(285,'DIA','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(286,'HAI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(287,'CBI','4010046','J23337583','01','2017-05-15','2017-06-26','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(288,'IRT','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(289,'AII','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(290,'RCG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(291,'AIN','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(292,'RAI','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(293,'RIA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(294,'AII','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(295,'HAI','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(296,'HAI','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(297,'CPP','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(298,'DMG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(299,'IRT','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(300,'VTG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(301,'GTV','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(302,'RRP','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(303,'ITG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(304,'RAG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(305,'RAB','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(306,'CCF','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(307,'RIG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(308,'AIA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(309,'SFG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(310,'DSA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(311,'TEI','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(312,'TEG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(313,'RAG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(314,'AIG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(315,'RAC','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(316,'IOA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(317,'RCI','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(318,'ROA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(319,'IIA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(320,'ARA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(321,'RGB','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(322,'ICG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(323,'RTI','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(324,'RIA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(325,'HAG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(326,'AIA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(327,'CIP','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(328,'IIG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(329,'VAI','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(330,'IRT','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(331,'PDH','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(332,'ING','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(333,'CBI','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(334,'TGG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(335,'DMG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(336,'RIG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(337,'DSA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(338,'TRG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(339,'RDI','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(340,'AOA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(341,'RAB','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(342,'RIF','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(343,'AGB','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(344,'IRT','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(345,'IIA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(346,'DMG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(347,'RMG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(348,'RFI','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(349,'IRT','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(350,'VAI','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(351,'RMG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(352,'RIA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(353,'TII','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(354,'DIA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(355,'AGB','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(356,'ISL','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(357,'AGB','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(358,'CPP','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(359,'DGB','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(360,'DTG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(361,'DIG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(362,'RRP','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(363,'AOA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(364,'RIF','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(365,'RAC','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(366,'ISL','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(367,'KIG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(368,'DTG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(369,'RTI','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(370,'AOA','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(371,'RMG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(372,'CFI','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(373,'RMG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(374,'CIP','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(375,'CCF','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(376,'IIB','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(377,'DIV','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(378,'IIG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(379,'RIG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(380,'VIG','4010046','J23337583','01','2017-05-15','2017-06-25','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(381,'CPP','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(382,'DIG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(383,'CPP','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(384,'IMG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(385,'RIA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(386,'AIN','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(387,'VIG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(388,'CXA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(389,'RII','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(390,'GTV','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(391,'HAI','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(392,'RTG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(393,'RIO','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(394,'AIG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(395,'RAG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(396,'ICG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(397,'DIV','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(398,'RRI','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(399,'RII','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(400,'SFG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(401,'VTG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(402,'DIA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(403,'ROA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(404,'RRI','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(405,'IRT','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(406,'IOA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(407,'RAI','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(408,'IMG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(409,'AIN','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(410,'AIN','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(411,'RII','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(412,'IPT','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(413,'IOA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(414,'RIG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(415,'SFI','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(416,'VTG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(417,'RRG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(418,'IIB','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(419,'RTG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(420,'SFG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(421,'RII','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(422,'RCG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(423,'DAC','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(424,'IMG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(425,'DTG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(426,'RPD','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(427,'RII','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(428,'KIG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(429,'RRG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(430,'RAC','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(431,'RAB','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(432,'IIA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(433,'RGB','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(434,'RRG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(435,'RGI','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(436,'HAG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(437,'DIV','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(438,'IOA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(439,'RRP','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(440,'IRT','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(441,'RIF','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(442,'INC','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(443,'ROA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(444,'IAC','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(445,'AII','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(446,'SFI','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(447,'RSI','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(448,'RRI','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(449,'ARA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(450,'ISL','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(451,'DSA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(452,'VTG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(453,'CPP','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(454,'IAC','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(455,'ARA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(456,'VAI','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(457,'VMG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(458,'AGB','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(459,'DTG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(460,'RIG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(461,'DIV','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(462,'DIV','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(463,'RTG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(464,'IIG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(465,'TEI','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(466,'RRI','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(467,'RAC','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(468,'RAB','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(469,'DIA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(470,'DGB','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(471,'DIG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(472,'RII','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(473,'CCF','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(474,'ISL','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(475,'DAC','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(476,'RIG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(477,'AIA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(478,'VMG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(479,'RGB','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(480,'RRG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(481,'RII','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(482,'AIG','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(483,'TII','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(484,'ROA','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(485,'RSI','4010046','J23337583','01','2017-05-15','2017-06-17','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(486,'TEI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(487,'TEG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(488,'DAC','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(489,'RIF','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(490,'KCF','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(491,'VTG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(492,'ING','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(493,'IOA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(494,'RPD','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(495,'KIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(496,'IIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(497,'RPD','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(498,'RTI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(499,'ITG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(500,'AIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(501,'AIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(502,'AIB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(503,'RIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(504,'CXA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(505,'RAC','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(506,'RGB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(507,'RAB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(508,'CIP','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(509,'HAI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(510,'TRG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(511,'RFI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(512,'ISL','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(513,'RSI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(514,'RAG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(515,'HAI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(516,'TGG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(517,'AGB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(518,'RIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(519,'VTG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(520,'DAC','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(521,'TEI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(522,'KCF','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(523,'AIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(524,'ISL','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(525,'RAI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(526,'TRG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(527,'ITG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(528,'CXA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(529,'KIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(530,'CXA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(531,'TEI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(532,'TII','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(533,'HAI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(534,'AIA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(535,'AGB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(536,'HAI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(537,'HAG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(538,'DIV','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(539,'DTG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(540,'CFI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(541,'AOA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(542,'AIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(543,'ICG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(544,'DIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(545,'ROA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(546,'CEI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(547,'IIB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(548,'AIN','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(549,'RAB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(550,'KIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(551,'RAI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(552,'CIP','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(553,'RRI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(554,'AIB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(555,'ISL','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(556,'RIA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(557,'SFG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(558,'SFG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(559,'VAI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(560,'IIB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(561,'PDH','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(562,'RAG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(563,'AIN','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(564,'RRG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(565,'RTI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(566,'SFG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(567,'AFC','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(568,'RAB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(569,'RSI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(570,'RTI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(571,'DSA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(572,'HAG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(573,'RIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(574,'PDH','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(575,'RFI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(576,'RTI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(577,'VIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(578,'GTV','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(579,'DIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(580,'CBI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(581,'IAC','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(582,'AGB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(583,'INC','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(584,'HAI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(585,'VAI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(586,'RIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(587,'RFI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(588,'AIB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(589,'RIF','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(590,'KIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(591,'DGB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(592,'CBI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(593,'IAC','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(594,'RDI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(595,'KIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(596,'AOA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(597,'KCF','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(598,'RTI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(599,'ROA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(600,'KIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(601,'IPT','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(602,'VAI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(603,'DAC','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(604,'GTV','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(605,'RAI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(606,'AOA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(607,'DGB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(608,'SFI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(609,'VTG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(610,'INC','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(611,'RTG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(612,'RIA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(613,'CIP','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(614,'AII','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(615,'TII','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(616,'TRG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(617,'SFI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(618,'ITG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(619,'SFI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(620,'PDH','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(621,'IRT','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01');
INSERT INTO `t_transacciones` VALUES (622,'RAB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(623,'RGB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(624,'CBI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(625,'KIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(626,'DTG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(627,'ARA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(628,'RTI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(629,'IMG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(630,'TRG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(631,'IRT','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(632,'RPD','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(633,'AIG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(634,'RDI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(635,'AIB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(636,'HAG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(637,'DIV','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(638,'VTG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(639,'CPP','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(640,'DIA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(641,'RAB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(642,'IPT','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(643,'RAB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(644,'DIV','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(645,'DIA','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(646,'RCG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(647,'RTI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(648,'RRG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(649,'IAC','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(650,'RAB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(651,'RGB','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(652,'ISL','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(653,'VAI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(654,'VTG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(655,'SFG','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(656,'RTI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(657,'VAI','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(658,'IPT','4010046','J23337583','01','2017-05-15','2017-06-16','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(659,'IOA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(660,'RPD','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(661,'IRT','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(662,'RII','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(663,'IIG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(664,'DGB','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(665,'HAI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(666,'HAI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(667,'RSI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(668,'AIB','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(669,'ING','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(670,'IMG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(671,'KIG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(672,'RTG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(673,'KIG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(674,'TII','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(675,'RAI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(676,'ROA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(677,'RMG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(678,'VIG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(679,'CXA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(680,'DAC','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(681,'CIP','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(682,'CBI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(683,'RCI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(684,'CBI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(685,'RSI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(686,'RCI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(687,'HAI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(688,'DTG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(689,'DTG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(690,'ISL','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(691,'ITG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(692,'TEI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(693,'CIP','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(694,'INC','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(695,'AOA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(696,'RRP','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(697,'DAC','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(698,'PDH','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(699,'TEG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(700,'VTG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(701,'TII','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(702,'RIG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(703,'IRT','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(704,'TGG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(705,'RIO','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(706,'VTG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(707,'KCF','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(708,'RAG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(709,'AIA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(710,'CBI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(711,'TGG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(712,'CIP','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(713,'HAG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(714,'DTG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(715,'RAI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(716,'AIN','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(717,'RRP','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(718,'TEI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(719,'AIB','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(720,'DIA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(721,'RSI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(722,'HAG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(723,'ICG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(724,'CIP','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(725,'RTI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(726,'RIF','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(727,'CCF','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(728,'RCG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(729,'RIO','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(730,'AFC','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(731,'IOA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(732,'AII','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(733,'RAB','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(734,'CFI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(735,'DTG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(736,'KCF','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(737,'DIG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(738,'INC','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(739,'RTI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(740,'VAI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(741,'IPT','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(742,'RIA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(743,'CBI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(744,'HAG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(745,'VAI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(746,'TII','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(747,'ICG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(748,'RCI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(749,'CEI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(750,'RRG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(751,'RIO','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(752,'TEG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(753,'ING','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(754,'ISL','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(755,'CXA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(756,'CFI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(757,'VMG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(758,'AOA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(759,'AIA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(760,'ROA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(761,'VTG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(762,'DGB','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(763,'ARA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(764,'VAI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(765,'IPT','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(766,'RIA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(767,'RGI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(768,'DGB','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(769,'DTG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(770,'RIA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(771,'IMG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(772,'RIO','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(773,'RIF','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(774,'AGB','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(775,'AIA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(776,'AIG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(777,'GTV','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(778,'AOA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(779,'CBI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(780,'TII','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(781,'RMG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(782,'ARA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(783,'RDI','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(784,'DSA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(785,'RAG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(786,'IAC','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(787,'DMG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(788,'TGG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(789,'DGB','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(790,'RII','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(791,'ROA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(792,'RIA','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(793,'IAC','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(794,'ICG','4010046','J23337583','01','2017-05-15','2017-06-14','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(795,'CXA','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(796,'VIG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(797,'RPD','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(798,'RIO','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(799,'AGB','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(800,'DTG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(801,'DMG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(802,'RIO','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(803,'AGB','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(804,'ICG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(805,'IMG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(806,'RRI','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(807,'AIA','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(808,'AII','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(809,'PDH','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(810,'AIA','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(811,'RRI','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(812,'ICG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(813,'RTI','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(814,'TGG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(815,'RGB','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(816,'RMG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(817,'IPT','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(818,'RAI','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(819,'AGB','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(820,'CFI','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(821,'IOA','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(822,'DGB','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(823,'CFI','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(824,'RIA','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(825,'DIA','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(826,'HAG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(827,'RAG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(828,'TEI','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(829,'TEI','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(830,'INC','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(831,'AII','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(832,'RAB','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(833,'KCF','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(834,'IIB','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(835,'RIO','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(836,'VAI','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(837,'RAC','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(838,'RRG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(839,'RAB','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(840,'VIG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(841,'IOA','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(842,'AOA','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(843,'RMG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(844,'DIV','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(845,'VTG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(846,'GTV','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(847,'DIV','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(848,'KIG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(849,'RGB','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(850,'VAI','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(851,'VAI','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(852,'IIB','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(853,'ING','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(854,'SFI','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(855,'DIV','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(856,'RAB','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(857,'CXA','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(858,'ING','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(859,'VIG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(860,'ING','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(861,'DAC','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(862,'IIG','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(863,'RRP','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(864,'IIA','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(865,'RAI','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(866,'TII','4010046','J23337583','01','2017-05-15','2017-06-13','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(867,'RCG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(868,'AIG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(869,'RTI','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(870,'RTI','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(871,'DSA','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(872,'SFI','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(873,'ICG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(874,'RAI','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(875,'IIA','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(876,'INC','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(877,'ITG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(878,'RII','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(879,'RIO','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(880,'ITG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(881,'RRG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(882,'INC','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(883,'RGI','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(884,'ING','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(885,'CFI','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(886,'RAI','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(887,'ICG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(888,'RGB','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(889,'RIO','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(890,'RAC','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(891,'IPT','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(892,'RAI','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(893,'ARA','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(894,'AIB','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(895,'TII','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(896,'AGB','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(897,'DIV','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(898,'DGB','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(899,'RPD','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(900,'TEI','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(901,'RCI','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(902,'TGG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(903,'AGB','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(904,'RAB','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(905,'AIG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(906,'ICG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(907,'DIV','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(908,'GTV','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(909,'CCF','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(910,'HAI','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(911,'RIA','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(912,'AII','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(913,'RIF','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(914,'RCG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(915,'CCF','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(916,'AIB','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(917,'VTG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(918,'AIG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(919,'RGB','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(920,'DIG','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(921,'RTI','4010046','J23337583','01','2017-05-15','2017-06-09','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(922,'ING','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(923,'AIB','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(924,'DIV','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(925,'IPT','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(926,'DIV','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(927,'ROA','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(928,'CBI','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(929,'HAG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(930,'RII','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(931,'RIO','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(932,'RIF','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(933,'VTG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(934,'HAG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(935,'RGB','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(936,'IAC','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(937,'IIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(938,'RRI','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(939,'SFI','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(940,'AIB','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(941,'KIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(942,'SFI','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(943,'RGB','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(944,'ARA','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(945,'CCF','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(946,'AII','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(947,'CPP','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(948,'IAC','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(949,'TII','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(950,'ICG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(951,'RIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(952,'ROA','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(953,'DIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(954,'INC','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(955,'VIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(956,'RIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(957,'CBI','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(958,'RMG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(959,'IPT','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(960,'CBI','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(961,'RII','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(962,'RRP','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(963,'TEG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(964,'RRP','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(965,'CXA','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(966,'AFC','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(967,'VIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(968,'HAI','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(969,'ISL','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(970,'AIA','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(971,'AIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(972,'DGB','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(973,'KCF','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(974,'TEI','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(975,'RCI','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(976,'ING','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(977,'DIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(978,'RIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(979,'SFG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(980,'DIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(981,'KIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(982,'ICG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(983,'AIA','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(984,'RRP','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(985,'ARA','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(986,'IIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(987,'IPT','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(988,'AIG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(989,'RRP','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(990,'RRG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(991,'GTV','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(992,'TGG','4010046','J23337583','01','2017-05-15','2017-06-05','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(993,'ING','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(994,'AOA','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(995,'TEG','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(996,'ICG','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(997,'SFI','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(998,'RGI','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(999,'GTV','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1000,'AGB','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1001,'CIP','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1002,'RII','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1003,'RIO','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1004,'DTG','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1005,'CIP','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1006,'DTG','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1007,'DMG','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1008,'IMG','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1009,'VTG','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1010,'AIG','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1011,'IIA','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1012,'GTV','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1013,'GTV','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1014,'INC','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1015,'CIP','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1016,'AIN','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1017,'RFI','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1018,'RIF','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1019,'AIB','4010046','J23337583','01','2017-05-15','2017-06-02','2017-06-30',1000.00,'23123123',0.00,1.00,'1','1',1.00,1.00,'1','2017-05-15','1','1','1','1','A','01'),(1020,'AIG','4010046','J23337583','01','2017-06-27','2017-06-02','2017-06-30',123412.00,'213123',1.00,1.00,'1','1',1.00,1.00,'1','2017-06-28','1','1','121312','12','A','01');

#
# Structure for table "t_unidad"
#

DROP TABLE IF EXISTS `t_unidad`;
CREATE TABLE `t_unidad` (
  `cod_unidad` varchar(10) NOT NULL,
  `des_unidad` varchar(45) DEFAULT NULL,
  `cod_gerencia` varchar(10) DEFAULT NULL,
  `cod_status` varchar(1) NOT NULL,
  PRIMARY KEY (`cod_unidad`),
  KEY `t_unidad_ibfk_1` (`cod_status`),
  CONSTRAINT `t_unidad_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_unidad"
#

INSERT INTO `t_unidad` VALUES ('1','1','1','A');

#
# Structure for table "t_organizacion"
#

DROP TABLE IF EXISTS `t_organizacion`;
CREATE TABLE `t_organizacion` (
  `cod_unidad` varchar(10) DEFAULT NULL,
  `cod_gerencia` varchar(6) DEFAULT NULL,
  `des_gerencia` varchar(45) DEFAULT NULL,
  `des_cordinacion` varchar(45) DEFAULT NULL,
  `cod_cordinacion` varchar(45) DEFAULT NULL,
  `cod_status` varchar(1) DEFAULT NULL,
  KEY `cod_unidad` (`cod_unidad`),
  KEY `t_organizacion_ibfk_2_idx` (`cod_status`),
  CONSTRAINT `t_organizacion_ibfk_1` FOREIGN KEY (`cod_unidad`) REFERENCES `t_unidad` (`cod_unidad`),
  CONSTRAINT `t_organizacion_ibfk_2` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_organizacion"
#


#
# Structure for table "t_usuario"
#

DROP TABLE IF EXISTS `t_usuario`;
CREATE TABLE `t_usuario` (
  `cod_usuario` varchar(20) NOT NULL,
  `cod_perfil` varchar(20) NOT NULL,
  `nom_usuario` varchar(45) DEFAULT NULL,
  `cod_unidad` varchar(10) DEFAULT NULL,
  `telefono_usuario` varchar(45) DEFAULT NULL,
  `login` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `dias_vigencia_clave` varchar(3) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `fecha_ultimo_ingreso` date DEFAULT NULL,
  `cod_status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`cod_usuario`),
  KEY `cod_status` (`cod_status`),
  KEY `cod_unidad` (`cod_unidad`),
  CONSTRAINT `t_usuario_ibfk_1` FOREIGN KEY (`cod_status`) REFERENCES `t_status` (`cod_status`),
  CONSTRAINT `t_usuario_ibfk_2` FOREIGN KEY (`cod_unidad`) REFERENCES `t_unidad` (`cod_unidad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "t_usuario"
#

INSERT INTO `t_usuario` VALUES ('001','1','jose','1','1212','jose','1234','60','','2017-03-28','2017-03-28','2017-03-28','A');

#
# Procedure "FSOSEGAUDCONS"
#

DROP PROCEDURE IF EXISTS `FSOSEGAUDCONS`;
CREATE PROCEDURE `FSOSEGAUDCONS`()
select cod_auditoria,des_auditable from t_auditables where estatus_auditable='A';

#
# Procedure "FSOSEGAUDINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGAUDINS`;
CREATE PROCEDURE `FSOSEGAUDINS`(in cod_auditablex varchar(6),in des_auditablex varchar(100))
BEGIN
   insert into t_auditables(cod_auditoria,des_auditable,fecha,estatus_auditable) values(cod_auditablex,des_auditablex,curdate(),'A');
END;

#
# Procedure "FSOSEGBALCOMREPORT"
#

DROP PROCEDURE IF EXISTS `FSOSEGBALCOMREPORT`;
CREATE PROCEDURE `FSOSEGBALCOMREPORT`(in inicio date,in fin date)
BEGIN
    select plan.cod_cuenta_contable,plan.des_cuenta_contable,plan.tipo_cuenta_contable,contable.saldo,contable.Debe_Haber
    from t_plan_contable as plan,t_saldo_contable as contable
    where contable.fecha_creacion between inicio and fin and
           plan.tipo_cuenta_contable in ('Activo','pasivo','capital') and plan.cod_cuenta_contable= contable.cod_cuenta_contable
           order by plan.cod_cuenta_contable;
END;

#
# Procedure "FSOSEGCONTCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGCONTCON`;
CREATE PROCEDURE `FSOSEGCONTCON`(in inicio date, in fin date)
BEGIN
   select * from t_contabilidad where fecha_contable between inicio and fin;
END;

#
# Procedure "FSOSEGFPCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGFPCON`;
CREATE PROCEDURE `FSOSEGFPCON`()
BEGIN
   select cod_forma_pago,des_forma_pago from t_forma_pago;
END;

#
# Procedure "FSOSEGLIQINTREPORT"
#

DROP PROCEDURE IF EXISTS `FSOSEGLIQINTREPORT`;
CREATE PROCEDURE `FSOSEGLIQINTREPORT`(in inicio date, in fin date,in fidI varchar(10),in fidF varchar(10),in cuentaI varchar(30),in cuentaF varchar(30))
BEGIN
   select fideicomiso.cod_producto,fideicomiso.Des_producto,contable.saldo_contable
   from t_producto as fideicomiso,t_saldo_contable as contable
   where fideicomiso.cod_producto=contable.cod_producto;
END;

#
# Procedure "FSOSEGLOGCONS"
#

DROP PROCEDURE IF EXISTS `FSOSEGLOGCONS`;
CREATE PROCEDURE `FSOSEGLOGCONS`(in inicio date,in fin date)
BEGIN
	select cod_usuario,cod_modulo,cod_opcion,cod_perfil_usuario,date_format(fecha_registro,"%d/%m/%Y"),hora,descripcion_detalle from t_log_auditoria where cod_status='A' and fecha_registro between inicio and fin;
END;

#
# Procedure "FSOSEGLOGINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGLOGINS`;
CREATE PROCEDURE `FSOSEGLOGINS`(in cod_auditoria varchar(20),in cod_usuario varchar(60),in modulo varchar(20),in opcion varchar(10), in perfil varchar(20),in descripcion longtext)
insert into t_log_auditoria values(cod_auditoria,cod_usuario,modulo,opcion,perfil,curdate(),curtime(),descripcion,'A');

#
# Procedure "FSOSEGMONCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGMONCON`;
CREATE PROCEDURE `FSOSEGMONCON`()
BEGIN
    select *
    from t_moneda;
END;

#
# Procedure "FSOSEGMOVMENREPORT"
#

DROP PROCEDURE IF EXISTS `FSOSEGMOVMENREPORT`;
CREATE PROCEDURE `FSOSEGMOVMENREPORT`(in inicio date, in fin date,in fidI varchar(10),in fidF varchar(10),in cuentaI varchar(30),in cuentaF varchar(30))
BEGIN
    select * 
    from t_saldo_contable
    where fecha_creacion between inicio and fin and cod_producto between fidI and fidF;
END;

#
# Procedure "FSOSEGRESCONGENREPORT"
#

DROP PROCEDURE IF EXISTS `FSOSEGRESCONGENREPORT`;
CREATE PROCEDURE `FSOSEGRESCONGENREPORT`(in inicio date,in fin date,in fidI varchar(10),in fidF varchar(10),in cuentaI varchar(20),in cuentaF varchar(20))
BEGIN
    select contabilidad.fecha_contable,contabilidad.cuenta_contable,contabilidad.cod_producto,tipoTransaccion.des_tipo_transaccion,contabilidad.monto,contabilidad.Debe_Haber 
    from t_contabilidad as contabilidad,t_tipo_transaccion as tipoTransaccion
    where contabilidad.cod_tipo_transaccion=tipoTransaccion.cod_tipo_transaccion and
           contabilidad.fecha_contable between inicio and fin and
           contabilidad.cod_producto between fidI and fidF;
END;

#
# Procedure "FSOSEGSTPASIACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPASIACT`;
CREATE PROCEDURE `FSOSEGSTPASIACT`(in buscar varchar(40),in cod_tipo_cuenta_trans varchar(30),in clase_trans varchar(30),
in cod_cuenta_cont varchar(30), in Deb_Hab varchar(1),in campo_trans varchar(50),
in calc varchar(100),in referencia_trans varchar(20))
BEGIN

update t_asiento_contable set 


cod_tipo_cuenta_transaccion= cod_tipo_cuenta_trans,
clase_transaccion= clase_trans,
cod_cuenta_contable = cod_cuenta_cont,
Debe_Haber= Deb_Hab,
campo_transaccion = campo_trans,
calculo = calc,
referencia_transaccion = referencia_trans

where cod_status = 'A' and cod_cuenta_contable=buscar;

END;

#
# Procedure "FSOSEGSTPASIANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPASIANL`;
CREATE PROCEDURE `FSOSEGSTPASIANL`(in buscar varchar(30))
BEGIN

update t_asiento_contable set cod_status = 'I' where cod_cuenta_contable = buscar and cod_status= 'A' ;

END;

#
# Procedure "FSOSEGSTPASICON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPASICON`;
CREATE PROCEDURE `FSOSEGSTPASICON`(in buscar varchar(30))
BEGIN
  if(buscar="All")then
    select * from t_asiento_contable;
   else
      select * FROM t_asiento_contable where cod_cuenta_contable =buscar and cod_status ='A'; 
   end if;
END;

#
# Procedure "FSOSEGSTPASIINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPASIINS`;
CREATE PROCEDURE `FSOSEGSTPASIINS`(in cod_tipo_cuenta_trans varchar(30),in clase_trans varchar(30),
in cod_cuenta_contable varchar(30), in Debe_Haber varchar(1),in campo_trans varchar(50),
in calculo varchar(100),in referencia_trans varchar(20))
BEGIN

insert into t_asiento_contable
values(cod_tipo_cuenta_trans,clase_trans,cod_cuenta_contable,Debe_Haber,campo_trans,calculo,referencia_trans,'A'); 

END;

#
# Procedure "FSOSEGSTPCARACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCARACT`;
CREATE PROCEDURE `FSOSEGSTPCARACT`(in buscar varchar(40), in cod_cartera_inver varchar(10),in cod_tipo_inver varchar(10),
in cod_cont varchar(10),in cod_Prod varchar(10), in ci_r varchar(20), 
in fecha_val varchar(10), in tipo_oper varchar(2), in monto_prec decimal(17,2),
in monto_comi decimal(17,2), in porcentaje_comi decimal(17,2), 
in monto_oper decimal(17,2), in rend decimal(17,2), in Inte decimal(17,2), 
in cod_p varchar(10))
BEGIN

update t_cartera_inversion set 

cod_cartera_inversion = cod_cartera_inver,
cod_tipo_inversion = cod_tipo_inver,
cod_contrato = cod_cont,
cod_Producto = cod_Prod,
ci_rif = ci_r,
fecha_valor = fecha_val,
tipo_operacion = tipo_oper,
monto_precio = monto_prec,
monto_comision = monto_comi,
porcentaje_comision = porcentaje_comi,
monto_operacion = monto_oper,
rendimiento = rend,
Intereses = Inte,
cod_pais = cod_p

where cod_status = 'A' and ci_rif = buscar;

END;

#
# Procedure "FSOSEGSTPCARANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCARANL`;
CREATE PROCEDURE `FSOSEGSTPCARANL`(in buscar varchar(20))
BEGIN

update t_cartera_inversion set cod_status = 'I' where ci_rif=buscar and cod_status = 'A';


END;

#
# Procedure "FSOSEGSTPCARCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCARCON`;
CREATE PROCEDURE `FSOSEGSTPCARCON`(in buscar varchar(40))
BEGIN

select * FROM t_cartera_inversion where ci_rif = buscar and cod_status ='A'; 

END;

#
# Procedure "FSOSEGSTPCARINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCARINS`;
CREATE PROCEDURE `FSOSEGSTPCARINS`(in cod_cartera_inver varchar(10),in cod_tipo_inver varchar(10),
in cod_cont varchar(10),in cod_Prod varchar(10), in ci_r varchar(20), 
in tipo_oper varchar(2), in monto_prec decimal(17,2),
in monto_comi decimal(17,2), in porcentaje_comi decimal(17,2), 
in monto_oper decimal(17,2), in rend decimal(17,2), in Inte decimal(17,2), 
in cod_p varchar(10), in cod_mon varchar(10))
BEGIN

insert into t_cartera_inversion

values(cod_cartera_inver,cod_tipo_inver,cod_cont,cod_Prod,ci_r,now(),now(),now(),
tipo_oper,monto_prec,monto_comi,porcentaje_comi,monto_oper,rend,Inte,cod_p,cod_mon,'A');


END;

#
# Procedure "FSOSEGSTPCIDASI"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIDASI`;
CREATE PROCEDURE `FSOSEGSTPCIDASI`(in buscar varchar(20))
BEGIN
	select * from t_asiento_contable where cod_tipo_oper = buscar;
END;

#
# Procedure "FSOSEGSTPCIDCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIDCON`;
CREATE PROCEDURE `FSOSEGSTPCIDCON`(IN buscar date)
BEGIN

select * from t_cierre_diario order by Fecha_cierre desc limit 1;
END;

#
# Procedure "FSOSEGSTPCIDREGC"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIDREGC`;
CREATE PROCEDURE `FSOSEGSTPCIDREGC`(IN buscar date)
BEGIN
DELETE FROM `fsosegdba01`.`t_cierre_diario` WHERE `Fecha_cierre`=buscar;
INSERT INTO `fsosegdba01`.`t_cierre_diario` (`Fecha_cierre`, `finalizado`, `ultimo_paso_realizado`) VALUES (buscar, 'N', '1');
END;

#
# Procedure "FSOSEGSTPCIDREGUP"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIDREGUP`;
CREATE PROCEDURE `FSOSEGSTPCIDREGUP`(IN buscar date, in fin char(1), in paso varchar(50) )
BEGIN
UPDATE `fsosegdba01`.`t_cierre_diario` SET `finalizado`=fin, `ultimo_paso_realizado`=paso   WHERE `Fecha_cierre`=buscar;
END;

#
# Procedure "FSOSEGSTPCIDRENDIN"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIDRENDIN`;
CREATE PROCEDURE `FSOSEGSTPCIDRENDIN`(
in cod_ct varchar(30),
in cod_pr varchar(10),
in monto decimal(17,2),
in fecha date
)
BEGIN
	insert into t_rendimiento values (cod_ct,cod_pr,monto,fecha,'Pendiente');
    
END;

#
# Procedure "FSOSEGSTPCIDSALDIN"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIDSALDIN`;
CREATE PROCEDURE `FSOSEGSTPCIDSALDIN`(
in cod_cuenta varchar(30),
in cod_prod varchar(10),
in sald decimal(19,2),
in fecha_c date,
in d_h varchar(1),
in fecha_a date,
in moneda varchar(10))
BEGIN

	insert into t_saldo_contable 
    values(cod_cuenta,cod_prod,sald,fecha_c,d_h,'A',fecha_a,moneda);
END;

#
# Procedure "FSOSEGSTPCIDSALDUP"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIDSALDUP`;
CREATE PROCEDURE `FSOSEGSTPCIDSALDUP`(in cod_cuenta varchar(30),in cod_prod varchar(10),
in sald decimal(19,2),in d_h varchar(1),in moneda varchar(10))
BEGIN
	update t_saldo_contable set saldo=sald, fecha_last_update=now()
    where cod_cuenta_contable=cod_cuenta and cod_producto=cod_prod and Debe_Haber=d_h 
    and cod_moneda=moneda;
END;

#
# Procedure "FSOSEGSTPCIDTRAN"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIDTRAN`;
CREATE PROCEDURE `FSOSEGSTPCIDTRAN`(IN buscar date)
BEGIN
	select nro_transaccion, cod_tipo_transaccion,  monto_transaccion, cod_Producto , cod_moneda from t_transacciones where fecha_registro_transaccion = buscar and cod_status='A'; 

END;

#
# Procedure "FSOSEGSTPCIDUPT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIDUPT`;
CREATE PROCEDURE `FSOSEGSTPCIDUPT`(in fecha date)
BEGIN
	update t_transacciones set cod_status='P', fecha_proceso_transaccion=now() where fecha_registro_transaccion=fecha;
END;

#
# Procedure "FSOSEGSTPCIMCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIMCON`;
CREATE PROCEDURE `FSOSEGSTPCIMCON`(IN buscar date)
BEGIN
select * from t_cierre_mensual order by Fecha_cierre desc limit 1;
END;

#
# Procedure "FSOSEGSTPCIMREGC"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIMREGC`;
CREATE PROCEDURE `FSOSEGSTPCIMREGC`(IN buscar date)
BEGIN
DELETE FROM `fsosegdba01`.`t_cierre_mensual` WHERE `Fecha_cierre`=buscar;
INSERT INTO `fsosegdba01`.`t_cierre_mensual` (`Fecha_cierre`, `finalizado`, `ultimo_paso_realizado`) VALUES (buscar, 'N', '1');

END;

#
# Procedure "FSOSEGSTPCIMREGUP"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIMREGUP`;
CREATE PROCEDURE `FSOSEGSTPCIMREGUP`(IN buscar date, in fin char(1), in paso varchar(50) )
BEGIN
UPDATE `fsosegdba01`.`t_cierre_mensual` SET `finalizado`=fin, `ultimo_paso_realizado`=paso   WHERE `Fecha_cierre`=buscar;

END;

#
# Procedure "FSOSEGSTPCIMTRAN"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIMTRAN`;
CREATE PROCEDURE `FSOSEGSTPCIMTRAN`(IN buscar date)
BEGIN
 select nro_transaccion, cod_tipo_transaccion,  monto_transaccion, cod_Producto , cod_moneda from t_transacciones where fecha_registro_transaccion >= buscar and cod_status='A';
END;

#
# Procedure "FSOSEGSTPCIMUPT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCIMUPT`;
CREATE PROCEDURE `FSOSEGSTPCIMUPT`(in fecha date)
BEGIN
 update t_transacciones set cod_status='P', fecha_proceso_transaccion=now() where fecha_registro_transaccion>=fecha;
END;

#
# Procedure "FSOSEGSTPCLCACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCLCACT`;
CREATE PROCEDURE `FSOSEGSTPCLCACT`(in buscar varchar (20), in cod varchar (20), 
descrip varchar(45),form varchar(100))
BEGIN
update t_calculo_comisiones set 
codigo      = cod,
descripcion = descrip,
formula     = form
where codigo = buscar and cod_status = 'A'; 
END;

#
# Procedure "FSOSEGSTPCLCANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCLCANL`;
CREATE PROCEDURE `FSOSEGSTPCLCANL`(in buscar varchar (20))
BEGIN
update t_calculo_comisiones set cod_status = 'I' where codigo = buscar and cod_status = 'A'; 
END;

#
# Procedure "FSOSEGSTPCLCCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCLCCON`;
CREATE PROCEDURE `FSOSEGSTPCLCCON`(in buscar varchar(20))
BEGIN
select codigo,descripcion,formula from t_calculo_comisiones where codigo = buscar and cod_status = 'A';
END;

#
# Procedure "FSOSEGSTPCLCCON2"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCLCCON2`;
CREATE PROCEDURE `FSOSEGSTPCLCCON2`()
BEGIN
select codigo,descripcion,formula from t_calculo_comisiones where cod_status = 'A';
END;

#
# Procedure "FSOSEGSTPCLCINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCLCINS`;
CREATE PROCEDURE `FSOSEGSTPCLCINS`(in cod varchar(20), in descrip varchar (45), 
in form varchar (100))
BEGIN
insert into t_calculo_comisiones 
values(cod,descrip,form,'A'); 
END;

#
# Procedure "FSOSEGSTPCONACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCONACT`;
CREATE PROCEDURE `FSOSEGSTPCONACT`(in buscar varchar (20),in cod_emp varchar (20), in ci_r varchar (20),
in razon_social_per varchar (100),in cod_mon varchar (10),in cod_p varchar (10),
in cod_pos varchar (10),in formato_f decimal(10,0),in formato_h decimal (10,0),
in idi varchar (30),in dir varchar (100),in tel varchar (45),in email varchar (45),
in fecha_cierre_act varchar (10),in frecuencia_cierre_ope varchar (10),
in comision_admin01 varchar (10), in comision_admin02 varchar (10),
in comision_admin03 varchar (10),in comision_ope01 varchar (10),
in comision_ope02 varchar (10),in comision_ope03 varchar (10), 
in mensaje_oficial_rep varchar (100),in nombre_Sistema_apli varchar (80),
in fecha_configuracion_sis varchar (10),in caracter_separa_cam varchar (1),
in ruta_importar_archivo_pla varchar (45),in ruta_archivos_ent varchar (100),
in ruta_archivos_sal varchar (100), in ruta_pagina_w varchar (45),
in ruta_rep varchar (100),in ruta_formato_rep varbinary(100), 
in ruta_instalacion_apl varchar (100),in ruta_log_aud varchar(100),
in ruta_destino_rep varchar (100), in ruta_conexion_ser varchar (100),
in ejecucion_procesos_b varchar (100),in persona_con varchar (80))
BEGIN

update t_configuracion set 

cod_empresa                 = cod_emp,
ci_rif                      = ci_r,
razon_social_persona        = razon_social_per,
cod_moneda                  = cod_mon,
cod_pais                    = cod_p,
cod_postal                  = cod_pos,
formato_fecha               = formato_f,
formato_hora                = formato_h,
idioma                      = idi,
direccion                   = dir,
telefonos                   = tel,
correo                      = email,
fecha_cierre_actividades    = fecha_cierre_act,
frecuencia_cierre_operativo = frecuencia_cierre_ope,
comision_administracion01   = comision_admin01,
comision_administracion02   = comision_admin02,
comision_administracion03   = comision_admin03,
comision_operativa01        = comision_ope01,
comision_operativa02        = comision_ope02,
comision_operativa03        = comision_ope03,
mensaje_oficial_reportes    = mensaje_oficial_rep,
nombre_Sistema_aplicacion   = nombre_Sistema_apli,
fecha_configuracion_sistema = fecha_configuracion_sis,
caracter_separa_campos      = caracter_separa_cam,
ruta_importar_archivo_plano = ruta_importar_archivo_pla,
ruta_archivos_entrada       = ruta_archivos_ent,
ruta_archivos_salida        = ruta_archivos_sal,
ruta_pagina_web             = ruta_pagina_w,
ruta_reporte                = ruta_rep,
ruta_formato_reportes       = ruta_formato_rep,
ruta_instalacion_aplicacion = ruta_instalacion_apl,
ruta_log_auditoria          = ruta_log_aud,
ruta_destino_reporte        = ruta_destino_rep,
ruta_conexion_servidor      = ruta_conexion_ser,
ejecucion_procesos_batch    = ejecucion_procesos_b,
persona_contacto            = persona_con

where ci_rif = buscar and cod_status = 'A'; 



END;

#
# Procedure "FSOSEGSTPCONANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCONANL`;
CREATE PROCEDURE `FSOSEGSTPCONANL`(in buscar varchar(20))
BEGIN

update t_configuracion set cod_status = 'I' where ci_rif = buscar and cod_status ='A';
END;

#
# Procedure "FSOSEGSTPCONCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCONCON`;
CREATE PROCEDURE `FSOSEGSTPCONCON`(in buscar varchar(20))
BEGIN

select cod_empresa,ci_rif,razon_social_persona,cod_moneda,cod_pais,cod_postal,formato_fecha,formato_hora,
idioma,direccion,telefonos,correo,fecha_cierre_actividades,frecuencia_cierre_operativo,
comision_administracion01,comision_administracion02,comision_administracion03,
comision_operativa01,comision_operativa02,comision_operativa03,mensaje_oficial_reportes,
nombre_Sistema_aplicacion,fecha_configuracion_sistema,caracter_separa_campos,ruta_importar_archivo_plano,
ruta_archivos_entrada,ruta_archivos_salida,ruta_pagina_web,ruta_reporte,ruta_formato_reportes,
ruta_instalacion_aplicacion,ruta_log_auditoria,ruta_destino_reporte,ruta_conexion_servidor,
ejecucion_procesos_batch,persona_contacto

from t_configuracion where ci_rif = buscar and cod_status = 'A'; 

END;

#
# Procedure "FSOSEGSTPCONINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCONINS`;
CREATE PROCEDURE `FSOSEGSTPCONINS`(in cod_empresa varchar (20), in ci_rif varchar (20),
in razon_social_persona varchar (100),in cod_moneda varchar (10),in cod_pais varchar (10),
in cod_postal varchar (10),in formato_fecha decimal(10,0),in formato_hora decimal (10,0),
in idioma varchar (30),in direccion varchar (100),in telefonos varchar (45),in correo varchar (45),
in fecha_cierre_actividades varchar (10),in frecuencia_cierre_operativo varchar (10),
in comision_administracion01 varchar (10), in comision_administracion02 varchar (10),
in comision_administracion03 varchar (10),in comision_operativa01 varchar (10),
in comision_operativa02 varchar (10),in comision_operativa03 varchar (10), 
in mensaje_oficial_reportes varchar (100),in nombre_Sistema_aplicacion varchar (80),
in fecha_configuracion_sistema varchar (10),in caracter_separa_campos varchar (1),
in ruta_importar_archivo_plano varchar (45),in ruta_archivos_entrada varchar (100),
in ruta_archivos_salida varchar (100), in ruta_pagina_web varchar (45),
in ruta_reporte varchar (100),in ruta_formato_reportes varbinary(100), 
in ruta_instalacion_aplicacion varchar (100),in ruta_log_auditoria varchar(100),
in ruta_destino_reporte varchar (100), in ruta_conexion_servidor varchar (100),
in ejecucion_procesos_batch varchar (100),in persona_contacto varchar (80))
BEGIN

insert into t_configuracion

values(cod_empresa,ci_rif,razon_social_persona,cod_moneda,cod_pais,cod_postal,formato_fecha,formato_hora,
idioma,direccion,telefonos,correo,fecha_cierre_actividades,frecuencia_cierre_operativo,
comision_administracion01,comision_administracion02,comision_administracion03,
comision_operativa01,comision_operativa02,comision_operativa03,mensaje_oficial_reportes,
nombre_Sistema_aplicacion,fecha_configuracion_sistema,caracter_separa_campos,ruta_importar_archivo_plano,
ruta_archivos_entrada,ruta_archivos_salida,ruta_pagina_web,ruta_reporte,ruta_formato_reportes,
ruta_instalacion_aplicacion,ruta_log_auditoria,ruta_destino_reporte,ruta_conexion_servidor,
ejecucion_procesos_batch,persona_contacto,'A'); 

END;

#
# Procedure "FSOSEGSTPCONTINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPCONTINS`;
CREATE PROCEDURE `FSOSEGSTPCONTINS`(
in nro_opr_tran int(11),
in cod_tp_op varchar(20),
in clase_trans varchar(30),
in cod_cta_cont varchar(30),
in D_H varchar(1),
in mont decimal(17,2),
in fec_t date
)
BEGIN
	insert into t_contabilidad(nro_oper_transaccion,cod_tipo_oper,clase_trans,
    cod_cuenta_contable,Debe_Haber,monto,fecha_transaccion,fecha_contable,cod_status)
	
    values(nro_opr_tran,cod_tp_op,clase_trans,cod_cta_cont,D_H,mont,fec_t,now(),'A');
END;

#
# Procedure "FSOSEGSTPDIFACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPDIFACT`;
CREATE PROCEDURE `FSOSEGSTPDIFACT`(in buscar date, in fch_feriado date, 
in desc_feriad varchar(60),in d_feriado varchar(9), in m_feriado varchar(12))
BEGIN
update t_dias_feriados set 
fecha_feriado = fch_feriado,
des_feriado   = desc_feriad,
dia_feriado   = d_feriado,
mes_feriado   = m_feriado
where fecha_feriado = buscar and cod_status = 'A';
END;

#
# Procedure "FSOSEGSTPDIFALLCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPDIFALLCON`;
CREATE PROCEDURE `FSOSEGSTPDIFALLCON`()
BEGIN
    select * from t_dias_feriados;
END;

#
# Procedure "FSOSEGSTPDIFANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPDIFANL`;
CREATE PROCEDURE `FSOSEGSTPDIFANL`(in buscar date)
BEGIN
update t_dias_feriados set cod_status = 'I' where fecha_feriado = buscar and cod_status = 'A';
END;

#
# Procedure "FSOSEGSTPDIFCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPDIFCON`;
CREATE PROCEDURE `FSOSEGSTPDIFCON`(in buscar date)
BEGIN
select * from t_dias_feriados where fecha_feriado = buscar and cod_status = 'A'; 

END;

#
# Procedure "FSOSEGSTPDIFINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPDIFINS`;
CREATE PROCEDURE `FSOSEGSTPDIFINS`(in fch_feriado date, in desc_feriad varchar(60),
in d_feriado varchar(9), in m_feriado varchar(12))
BEGIN
insert into t_dias_feriados 
values(fch_feriado,desc_feriad,d_feriado,m_feriado,now(),'A'); 
END;

#
# Procedure "FSOSEGSTPIDSACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPIDSACT`;
CREATE PROCEDURE `FSOSEGSTPIDSACT`(in buscar varchar (20),in codigo_sis varchar(20),
in descripcion_Sis varchar(100),in version_sis varchar(20),
in memoria_RAM_req varchar(45),in manejador_documentos_sis varchar(45), 
in instalar_servidor_ApacheTOM varchar(45), in tipo_Sistema_Ope varchar(45),
in ArquitecturaNro_B varchar(3),in tipo_Manejador_BaseD varchar(45),
in usu_Admin_BD varchar(45),in cla_Admin_BD varchar(45),in nombre_Base_D varchar(80),
in bd_manejo_reportesDoc varchar(45),in nombre_Ser varchar(80),
in autenticacion_directorioAct varchar(1),in manejo_multi_h varchar(1),
in manejo_chat_ayuda_onl varchar(1), in requiereInstalar_software_manejador_cont varchar(1),
in manejo_historicos_Tab varchar(1), in cod_perfil_usuario_ins varchar(10),
in responsable_ins varchar(45), in ruta_log_auditoria_ins varchar(100),
in dir_ip varchar(100),in puerto_con varchar(45),
in espacio_minimo_DiscoDuro_ser varchar(45),in esp_minimo_manejador_BD varchar(45),
in activar_directorioAct varchar(1),in servicio_directorioAct varchar(100),
in servidor_email varchar(100))
BEGIN

update t_instalacion_sistema set 

codigo_sistema                                = codigo_sis,
descripcion_Sistema                           = descripcion_Sis,
version_sistema                               = version_sis,
fecha_actualizacion                           = now(),
memoria_RAM_requerida                         = memoria_RAM_req,
manejador_documentos_sistema                  = manejador_documentos_sis,
instalar_servidor_ApacheTOMCAT                = instalar_servidor_ApacheTOM,
tipo_Sistema_Operativo                        = tipo_Sistema_Ope,
ArquitecturaNro_Bits                          = ArquitecturaNro_B,
tipo_Manejador_BaseDatos                      = tipo_Manejador_BaseD,
usuario_Admin_BD                              = usu_Admin_BD,
clave_Admin_BD                                = cla_Admin_BD,
nombre_Base_Datos                             = nombre_Base_D,
bd_manejo_reportesDocumentos                  = bd_manejo_reportesDoc,
nombre_Servidor                               = nombre_Ser,
autenticacion_directorioActivo                = autenticacion_directorioAct,
manejo_multi_hilos                            = manejo_multi_h,
manejo_chat_ayuda_online                      = manejo_chat_ayuda_onl,
requiereInstalar_software_manejador_contenido = requiereInstalar_software_manejador_cont,
manejo_historicos_Tablas                      = manejo_historicos_Tab,
cod_perfil_usuario_instalador                 = cod_perfil_usuario_ins,
responsable_instalacion                       = responsable_ins,
ruta_log_auditoria_instalacion                = ruta_log_auditoria_ins,
direccion_ip                                  = dir_ip,
puerto_conexion                               = puerto_con,
espacio_minimo_DiscoDuro_servidor             = espacio_minimo_DiscoDuro_ser,
espacio_minimo_manejador_BD                   = esp_minimo_manejador_BD,
activar_directorioActivo                      = activar_directorioAct,
servicio_directorioActivo                     = servicio_directorioAct,
servidor_correos                              = servidor_email

where codigo_sistema = buscar and cod_status= 'A'; 


END;

#
# Procedure "FSOSEGSTPIDSANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPIDSANL`;
CREATE PROCEDURE `FSOSEGSTPIDSANL`(in buscar varchar (20))
BEGIN

update t_instalacion_sistema set cod_status = 'I' where codigo_sistema = buscar and cod_status ='A';

END;

#
# Procedure "FSOSEGSTPIDSCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPIDSCON`;
CREATE PROCEDURE `FSOSEGSTPIDSCON`(in buscar varchar(20))
BEGIN

select codigo_sistema,descripcion_Sistema,version_sistema,fecha_instalacion,
fecha_actualizacion,memoria_RAM_requerida,manejador_documentos_sistema,
instalar_servidor_ApacheTOMCAT,tipo_Sistema_Operativo,ArquitecturaNro_Bits,
tipo_Manejador_BaseDatos,usuario_Admin_BD,clave_Admin_BD,nombre_Base_Datos,
bd_manejo_reportesDocumentos,nombre_Servidor,autenticacion_directorioActivo,
manejo_multi_hilos,manejo_chat_ayuda_online,requiereInstalar_software_manejador_contenido,
manejo_historicos_Tablas,cod_perfil_usuario_instalador,responsable_instalacion,
ruta_log_auditoria_instalacion,direccion_ip,puerto_conexion,espacio_minimo_DiscoDuro_servidor,
espacio_minimo_manejador_BD,activar_directorioActivo,servicio_directorioActivo,
servidor_correos 
from t_instalacion_sistema where codigo_sistema = buscar and cod_status = 'A'; 

END;

#
# Procedure "FSOSEGSTPIDSINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPIDSINS`;
CREATE PROCEDURE `FSOSEGSTPIDSINS`(in codigo_sistema varchar(20),
in descripcion_Sistema varchar(100),
in version_sistema varchar(20),
in memoria_RAM_requerida varchar(45),in manejador_documentos_sistema varchar(45), 
in instalar_servidor_ApacheTOMCAT varchar(45), in tipo_Sistema_Operativo varchar(45),
in ArquitecturaNro_Bits varchar(3),in tipo_Manejador_BaseDatos varchar(45),
in usuario_Admin_BD varchar(45),in clave_Admin_BD varchar(45),in nombre_Base_Datos varchar(80),
in bd_manejo_reportesDocumentos varchar(45),in nombre_Servidor varchar(80),
in autenticacion_directorioActivo varchar(1),in manejo_multi_hilos varchar(1),
in manejo_chat_ayuda_online varchar(1), in requiereInstalar_software_manejador_contenido varchar(1),
in manejo_historicos_Tablas varchar(1), in cod_perfil_usuario_instalador varchar(10),
in responsable_instalacion varchar(45), in ruta_log_auditoria_instalacion varchar(100),
in direccion_ip varchar(100),in puerto_conexion varchar(45),
in espacio_minimo_DiscoDuro_servidor varchar(45),in espacio_minimo_manejador_BD varchar(45),
in activar_directorioActivo varchar(1),in servicio_directorioActivo varchar(100),
in servidor_correos varchar(100))
BEGIN


insert into t_instalacion_sistema

values (codigo_sistema,descripcion_Sistema,version_sistema,now(),
now(),memoria_RAM_requerida,manejador_documentos_sistema,
instalar_servidor_ApacheTOMCAT,tipo_Sistema_Operativo,ArquitecturaNro_Bits,
tipo_Manejador_BaseDatos,usuario_Admin_BD,clave_Admin_BD,nombre_Base_Datos,
bd_manejo_reportesDocumentos,nombre_Servidor,autenticacion_directorioActivo,
manejo_multi_hilos,manejo_chat_ayuda_online,requiereInstalar_software_manejador_contenido,
manejo_historicos_Tablas,cod_perfil_usuario_instalador,responsable_instalacion,
ruta_log_auditoria_instalacion,direccion_ip,puerto_conexion,espacio_minimo_DiscoDuro_servidor,
espacio_minimo_manejador_BD,activar_directorioActivo,servicio_directorioActivo,
servidor_correos,'A'); 
END;

#
# Procedure "FSOSEGSTPINVACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPINVACT`;
CREATE PROCEDURE `FSOSEGSTPINVACT`(in buscar varchar(20),in cod_inv varchar(10),in tipo_inv varchar(10),
in cod_con varchar(10), in cod_Pro varchar(10), in ci_r varchar(20),in fecha_v varchar(10),
in tipo_ope varchar(2),in monto_pre decimal(17,2),in monto_com decimal(17,2),
in porcentaje_com decimal(17,2), in monto_ope decimal(17,2), in cartera_inv varchar(10),
in cod_mon varchar(10),in cod_p varchar(10),in Ren decimal(17,2),in inte decimal(17,2),
in con varchar(100),in mercado_p varchar(1),in b_360_365 varchar(20),
in pago_div varchar(10), in frecuencia_pago_div varchar(1),in tsa decimal(17,2))
BEGIN

update t_inversiones set 

cod_inversion              = cod_inv,
cod_tipo_inversion         = tipo_inv,
cod_contrato               = cod_con,
cod_Producto               = cod_Pro,
ci_rif                     = ci_r,
fecha_valor                = fecha_v,
tipo_operacion             = tipo_ope,
monto_precio               = monto_pre,
monto_comision             = monto_com,  
porcentaje_comision        = porcentaje_com,
monto_operacion            = monto_ope,
cod_cartera_inversion          = cartera_inv,
cod_moneda                 = cod_mon,
cod_pais                   = cod_p,
Rendimiento                = Ren,
interes                    = inte,
condiciones                = con,
mercado_ps                 = mercado_p,
base_360_365               = b_360_365,
pago_dividendos            = pago_div,
frecuencia_pago_dividendo  = frecuencia_pago_div,
tasa                       = tsa

where ci_rif = buscar and cod_status = 'A'; 

END;

#
# Procedure "FSOSEGSTPINVANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPINVANL`;
CREATE PROCEDURE `FSOSEGSTPINVANL`(in buscar varchar (20))
BEGIN

update t_inversiones set cod_status = 'I' where ci_rif = buscar and cod_status ='A';

END;

#
# Procedure "FSOSEGSTPINVCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPINVCON`;
CREATE PROCEDURE `FSOSEGSTPINVCON`(in buscar varchar (20))
BEGIN

select * from t_inversiones where ci_rif = buscar and cod_status = 'A'; 

END;

#
# Procedure "FSOSEGSTPINVINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPINVINS`;
CREATE PROCEDURE `FSOSEGSTPINVINS`(in cod_inversion varchar(10),in tipo_inversion varchar(10),
in cod_contrato varchar(10), in cod_Producto varchar(10), in ci_rif varchar(20),
in tipo_operacion varchar(2),in monto_precio decimal(17,2),in monto_comision decimal(17,2),
in porcentaje_comision decimal(17,2), in monto_operacion decimal(17,2), in cartera_inversion varchar(10),
in cod_moneda varchar(10),in cod_pais varchar(10),in Rendimiento decimal(17,2),in interes decimal(17,2),
in condiciones varchar(100),in mercado_ps varchar(1),in base_360_365 varchar(20),
in pago_dividendos varchar(10), in frecuencia_pago_dividendo varchar(1),in tasa decimal(17,2))
BEGIN

insert into t_inversiones
values(cod_inversion,tipo_inversion,cod_contrato,cod_Producto,ci_rif,
now(),now(),now(),tipo_operacion,monto_precio,monto_comision,
porcentaje_comision,monto_operacion,cartera_inversion,cod_moneda,cod_pais,Rendimiento,interes,
condiciones,mercado_ps,base_360_365,pago_dividendos,frecuencia_pago_dividendo,tasa,'A');

END;

#
# Procedure "FSOSEGSTPLOCACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPLOCACT`;
CREATE PROCEDURE `FSOSEGSTPLOCACT`(in buscar varchar (20),in cod_Pers varchar (20), 
in ci varchar(20),in ave varchar(60),in edif varchar(60),
in urba varchar(60),in piso_p varchar(60),in pais_p varchar(20),
in ciu varchar(20),in edo varchar(20),in zona_post varchar(30),
in telef1 varchar(30),in telef2 varchar(30),in apto varchar(30))
BEGIN

update t_localizacion set 
cod_Persona           = cod_Pers,
ci_rif                = ci,
fecha_actualizacion   = now(),
avenida			      = ave,
edificio              = edif,
urb				      = urba,
piso			      = piso_p,
pais	              = pais_p,
ciudad	              = ciu,
estado            	  = edo,
zona_postal           = zona_post,
telefono1             = telef1,
telefono2             = telef2,
apartamento           = apto
where ci_rif = buscar and cod_status = 'A'; 
END;

#
# Procedure "FSOSEGSTPLOCANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPLOCANL`;
CREATE PROCEDURE `FSOSEGSTPLOCANL`(in buscar varchar(20))
BEGIN
update t_localizacion set cod_status = 'I' where ci_rif = buscar and cod_status = 'A';
END;

#
# Procedure "FSOSEGSTPLOCCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPLOCCON`;
CREATE PROCEDURE `FSOSEGSTPLOCCON`(in buscar varchar(20))
BEGIN
select cod_Persona,ci_rif,fecha_proceso,fecha_actualizacion,
avenida,edificio,urb,piso,pais,ciudad,
estado,zona_postal,telefono1,telefono2,apartamento
from t_localizacion where ci_rif = buscar and cod_status = 'A'; 
END;

#
# Procedure "FSOSEGSTPLOCINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPLOCINS`;
CREATE PROCEDURE `FSOSEGSTPLOCINS`(in cod_Pers varchar (20), 
in ci varchar(20),in ave varchar(60),in edif varchar(60),
in urba varchar(60),in piso varchar(60),in pais varchar(20),
in ciu varchar(20),in edo varchar(20),in zona_post varchar(30),
in telef1 varchar(30),in telef2 varchar(30),in apto varchar(30))
BEGIN
insert into t_localizacion 
values(cod_Pers,ci,now(),now(),ave,edif,urba,piso,pais,
ciu,edo,zona_post,telef1,telef2,apto,'A');
END;

#
# Procedure "FSOSEGSTPMONACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPMONACT`;
CREATE PROCEDURE `FSOSEGSTPMONACT`(in buscar varchar(10), 
in cod_mon varchar(10),in tas double)
BEGIN
update t_moneda set 
cod_moneda = cod_mon,
tasa = tas
where descripcion_moneda = buscar and cod_status = 'A'; 
END;

#
# Procedure "FSOSEGSTPMONANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPMONANL`;
CREATE PROCEDURE `FSOSEGSTPMONANL`(in buscar varchar (10))
BEGIN
update t_moneda set cod_status = 'I' where descripcion_moneda = buscar and cod_status = 'A';
END;

#
# Procedure "FSOSEGSTPMONCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPMONCON`;
CREATE PROCEDURE `FSOSEGSTPMONCON`(in buscar varchar(10))
BEGIN
if(buscar="All")then
   select * from t_moneda;
else
	select cod_moneda,descripcion_moneda,tasa from t_moneda 
	where descripcion_moneda = buscar and cod_status ='A'; 
  end if;
END;

#
# Procedure "FSOSEGSTPMONINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPMONINS`;
CREATE PROCEDURE `FSOSEGSTPMONINS`(in cod_mon varchar(10), 
in desc_mon varchar(10), in tas double)
BEGIN

insert into t_moneda 
values(null,cod_mon,desc_mon,tas,'A');
END;

#
# Procedure "FSOSEGSTPPEFACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPEFACT`;
CREATE PROCEDURE `FSOSEGSTPPEFACT`(in buscar varchar(2))
BEGIN

update  t_perfil set cod_status_perfil  = '1' where cod_perfil = buscar;

END;

#
# Procedure "FSOSEGSTPPEFCNP"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPEFCNP`;
CREATE PROCEDURE `FSOSEGSTPPEFCNP`()
BEGIN

select cod_perfil,cod_status_perfil,descripcion_perfil FROM t_perfil where condicion='0101';

END;

#
# Procedure "FSOSEGSTPPEFCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPEFCON`;
CREATE PROCEDURE `FSOSEGSTPPEFCON`(in buscar varchar(2))
BEGIN

select cod_perfil,cod_status_perfil,descripcion_perfil FROM t_perfil where condicion='0101' and cod_perfil=buscar;

END;

#
# Procedure "FSOSEGSTPPEFDES"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPEFDES`;
CREATE PROCEDURE `FSOSEGSTPPEFDES`(in cond_perfil varchar(7),in buscar varchar(2))
BEGIN
    update t_perfil set status_condicion='0' where condicion=cond_perfil and cod_perfil=buscar;
END;

#
# Procedure "FSOSEGSTPPEFHAB"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPEFHAB`;
CREATE PROCEDURE `FSOSEGSTPPEFHAB`(in cond_perfil varchar(7),in buscar varchar(2))
BEGIN
    update t_perfil set status_condicion='1' where condicion=cond_perfil and cod_perfil=buscar;
END;

#
# Procedure "FSOSEGSTPPEFINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPEFINS`;
CREATE PROCEDURE `FSOSEGSTPPEFINS`(in codigo_perfil varchar(2),in descripcion_p varchar (20))
BEGIN

insert into t_perfil (cod_perfil,condicion,cod_status_perfil,descripcion_perfil,status_condicion)

values
(codigo_perfil,'0101','1',descripcion_p,'0'), 
(codigo_perfil,'0102','1',descripcion_p,'0'), 
(codigo_perfil,'0201','1',descripcion_p,'0'),
(codigo_perfil,'0202','1',descripcion_p,'0'),
(codigo_perfil,'0203','1',descripcion_p,'0'),
(codigo_perfil,'0204','1',descripcion_p,'0'),
(codigo_perfil,'0205','1',descripcion_p,'0');
 
END;

#
# Procedure "FSOSEGSTPPERACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPERACT`;
CREATE PROCEDURE `FSOSEGSTPPERACT`(in buscar varchar(40),in cod_per varchar(20), in ci_r varchar(20), 
in tipo_p varchar(1),in sex varchar(1),in nom1 varchar(45),
in nom2 varchar(45),in ape1 varchar(45),in ape2 varchar(45), in raz_soc varchar(100),
in fch_n date,in lug_n varchar(45), in edo_c varchar(1), in pais_n varchar(20), 
in ciu_n varchar(20),in est_n varchar(20),in fech_act date, in fech_actu date,
in cor varchar(45), in inst varchar(45), in face varchar(45), in twi varchar(45),
in pag varchar(45), in tel varchar(45), in cel varchar(45))
BEGIN

update t_persona set  

cod_Persona = cod_per,
ci_rif = ci_r,
tipo_persona = tipo_p,
sexo = sex,
nom_Persona1 = nom1,
nom_Persona2 = nom2,
ape_Persona1 = ape1,
ape_Persona2 = ape2,
razon_social_persona = raz_soc,
fecha_nacimiento = fch_n,
lugar_nacimiento = lug_n,
estado_civil = edo_c,
pais_nacimiento = pais_n,
ciudad_nacimiento = ciu_n,
estado_nacimiento = est_n,
fecha_activacion = now(),
fecha_actualizacion = now(),
correo = cor,
instagram = inst,
facebook = face,
twitter = twi,
pagina = pag,
telefono = tel,
celular = cel

where cod_status = 'A' and ci_rif=buscar;


END;

#
# Procedure "FSOSEGSTPPERANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPERANL`;
CREATE PROCEDURE `FSOSEGSTPPERANL`( in buscar varchar(20))
BEGIN
update t_persona set cod_status = 'I' where  cod_status= 'A' and ci_rif = buscar;
END;

#
# Procedure "FSOSEGSTPPERCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPERCON`;
CREATE PROCEDURE `FSOSEGSTPPERCON`(in buscar varchar(20))
BEGIN

select * FROM t_persona where ci_rif =buscar; 

END;

#
# Procedure "FSOSEGSTPPERSCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPERSCON`;
CREATE PROCEDURE `FSOSEGSTPPERSCON`(in buscar varchar(20))
BEGIN
   if(buscar='All')then
     select * from t_persona;
   else
	 select * FROM t_persona where ci_rif =buscar;
   end if;
END;

#
# Procedure "FSOSEGSTPPERSINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPERSINS`;
CREATE PROCEDURE `FSOSEGSTPPERSINS`(in cod_per varchar(20), in ci_r varchar(20), 
in tipo_p varchar(1),in sex varchar(1),in nom1 varchar(45),
in nom2 varchar(45),in ape1 varchar(45),in ape2 varchar(45), in raz_soc varchar(100),
in fch_n date,in lug_n varchar(45), in edo_c varchar(1), in pais_n varchar(20), 
in ciu_n varchar(20),in est_n varchar(20),in fech_act date, in fech_actu date,
in cor varchar(45), in inst varchar(45), in face varchar(45), in twi varchar(45),
in pag varchar(45), in tel varchar(45), in cel varchar(45))
BEGIN
INSERT INTO t_persona 
values(cod_per,ci_r,tipo_p,sex,nom1,nom2,ape1,ape2,raz_soc,fch_n,lug_n,edo_c,
pais_n,ciu_n,est_n,now(),now(),'A',cor,inst,face,twi,pag,tel,cel);
END;

#
# Procedure "FSOSEGSTPPLCACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPLCACT`;
CREATE PROCEDURE `FSOSEGSTPPLCACT`(in buscar varchar(30),in cod_cuenta_cont varchar(30),
in des_cuenta_cont varchar(120),in tipo_cuenta_cont varchar(3),in ref varchar(30))
BEGIN

update t_plan_contable set 

cod_cuenta_contable  = cod_cuenta_cont,
des_cuenta_contable  = des_cuenta_cont,
tipo_cuenta_contable = tipo_cuenta_cont,
referencia           = ref

where cod_cuenta_contable =buscar and cod_status = 'A';

END;

#
# Procedure "FSOSEGSTPPLCANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPLCANL`;
CREATE PROCEDURE `FSOSEGSTPPLCANL`(in buscar varchar(30))
BEGIN
update t_plan_contable set cod_status = 'I' where  cod_cuenta_contable = buscar and cod_status= 'A';
END;

#
# Procedure "FSOSEGSTPPLCCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPLCCON`;
CREATE PROCEDURE `FSOSEGSTPPLCCON`(in buscar varchar(30))
BEGIN

select cod_cuenta_contable,des_cuenta_contable,tipo_cuenta_contable,referencia
from t_plan_contable where buscar = cod_cuenta_contable and cod_status = 'A';

END;

#
# Procedure "FSOSEGSTPPLCCON2"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPLCCON2`;
CREATE PROCEDURE `FSOSEGSTPPLCCON2`()
BEGIN
select cod_cuenta_contable,des_cuenta_contable,tipo_cuenta_contable,referencia
from t_plan_contable where cod_status = 'A';
END;

#
# Procedure "FSOSEGSTPPLCINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPLCINS`;
CREATE PROCEDURE `FSOSEGSTPPLCINS`(in cod_cuenta_cont varchar(30),in des_cuenta_cont varchar(120),
in tipo_cuenta_cont varchar(3),in ref varchar(30))
BEGIN

insert into t_plan_contable 
values(cod_cuenta_cont,des_cuenta_cont,tipo_cuenta_cont,'A',ref);

END;

#
# Procedure "FSOSEGSTPPROACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPROACT`;
CREATE PROCEDURE `FSOSEGSTPPROACT`(in buscar varchar(10),in cod_Prod varchar(10), in Des_Prod varchar (150),in Caracteristica_Prod varchar (45),in cod_mon varchar(10),
in Frecuencia_Prod varchar(1),in cod_comisiones_Prod decimal (17,2),in comisiones_Administracion_Prod varchar(20), in comisiones_operativa_Prod decimal (17,2),
in imp decimal (17,2),in frecuencia_cierre_prod varchar (10),in fecuencia_pago_div varchar (10), in porta varchar (1), in num_fidei int (11),
in pais_p varchar(20), in ciu varchar(20), in edo varchar(20),in tipo_cli varchar(10), in fecha_const date, in fecha_venc date, in fecha_prox date,
in fecha_prox_c date, in pago_g varchar(20), in direc varchar(45), in distribucion_men varchar(1), in func varchar(45), in tipo_comis varchar(10),
in monto_c varchar(10))
BEGIN

update t_producto set 


cod_Producto = cod_Prod,
Des_Producto = Des_Prod,
Caracteristica_Producto = Caracteristica_Prod,
cod_moneda = cod_mon,
Frecuencia_Producto = Frecuencia_Prod,
cod_comisiones_Producto = cod_comisiones_Prod,
comisiones_Administracion_Producto = comisiones_Administracion_Prod,
comisiones_operativa_Producto = comisiones_operativa_Prod,
impuesto = imp,
frecuencia_cierre_producto = frecuencia_cierre_prod,
fecuencia_pago_dividendo = fecuencia_pago_div,
portafolio = porta,
num_fideicomitentes = num_fidei,
pais = pais_p,
ciudad = ciu,
estado = edo,
tipo_cliente = tipo_cli,
fecha_constitucion = fecha_const,
fecha_vencimiento = fecha_venc,
fecha_prox_pago = fecha_prox,
fecha_prox_cierre = fecha_prox_c,
pago_ganacias = pago_g,
direccion = direc,
distribucion_mensual = distribucion_men,
funcionario = func,
tipo_comision = tipo_comis,
monto_comision = monto_c


where cod_status = 'A' and cod_Producto=buscar;
END;

#
# Procedure "FSOSEGSTPPROANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPROANL`;
CREATE PROCEDURE `FSOSEGSTPPROANL`(in buscar varchar(10))
BEGIN
update t_producto set cod_status = 'I' where  cod_status= 'A' and cod_producto = buscar;
END;

#
# Procedure "FSOSEGSTPPROCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPROCON`;
CREATE PROCEDURE `FSOSEGSTPPROCON`(in buscar varchar(10))
BEGIN
  if(buscar='All')then
    select * from t_producto;
  else 
    select * FROM t_producto where cod_Producto =buscar and cod_status ='A'; 
  end if;
END;

#
# Procedure "FSOSEGSTPPROINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPROINS`;
CREATE PROCEDURE `FSOSEGSTPPROINS`(in cod_Producto varchar(10), in Des_Producto varchar (150),in Caracteristica_Producto varchar (45),in cod_moneda varchar(10),
in pago_f varchar(1), in paga_e varchar(1),in comisiones_Administracion_Producto varchar(20), in comisiones_operativa_Producto decimal (17,2),
in impuesto decimal (17,2),in frecuencia_cierre_producto varchar (10),in fecuencia_pago_dividendo varchar (10), in portafolio varchar (1), in num_fideicomitentes int (11),
in pais varchar(20), in ciudad varchar(20), in estado varchar(20),in tipo_cliente varchar(10), in fecha_constitucion date, in fecha_vencimiento date, in fecha_prox_pago date,
in fecha_prox_cierre date, in pago_ganancias varchar(20), in direccion varchar(45), in distribucion_mensual varchar(1), in funcionario varchar(45), in tipo_comision varchar(10),
in monto_comision varchar(10))
BEGIN
INSERT INTO t_producto (cod_Producto,Des_Producto,Caracteristica_Producto,cod_moneda,
pago_fondo,paga_empresa,comisiones_Administracion_Producto,comisiones_operativa_Producto,
impuesto,frecuencia_cierre_producto,fecuencia_pago_dividendo,cod_status,portafolio,num_fideicomitentes,pais,ciudad,estado,
tipo_cliente,fecha_constitucion,fecha_vencimiento,fecha_prox_pago,fecha_prox_cierre,pago_ganancias,
direccion,distribucion_mensual,funcionario,tipo_comision,monto_comision)

values(cod_Producto,Des_Producto,Caracteristica_Producto,cod_moneda,pago_f,paga_e,comisiones_Administracion_Producto,
comisiones_operativa_Producto,impuesto,frecuencia_cierre_producto,fecuencia_pago_dividendo,'A',portafolio,
num_fideicomitentes,pais,ciudad,estado,tipo_cliente,fecha_constitucion,fecha_vencimiento,fecha_prox_pago,
fecha_prox_cierre,pago_ganancias,direccion,distribucion_mensual,funcionario,tipo_comision,monto_comision);

END;

#
# Procedure "FSOSEGSTPPRPACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPRPACT`;
CREATE PROCEDURE `FSOSEGSTPPRPACT`(in buscar varchar(20), in cod_Pro varchar (10),in cod_Per varchar (20),
in ci_r varchar (20),in saldo_int decimal(17,2),in saldo_ren decimal(17,2),in saldo_pre decimal(17,2),
in saldo_ant decimal(17,2),in saldo_apo decimal(17,2),in saldo_ret decimal(17,2),
saldo_hab decimal(17,2),in saldo_deb decimal(17,2), in saldo_d decimal(17,2),
in saldo_com decimal(17,2))
BEGIN

update t_producto_persona set

cod_Producto = cod_Pro,
cod_Persona = cod_Per,
ci_rif= ci_r,
fecha_actualizacion=now(),
saldo_intereses = saldo_int,
saldo_rendimiento = saldo_ren,
saldo_prestamos = saldo_pre,
saldo_anticipos = saldo_ant,
saldo_aportes = saldo_apo,
saldo_retiros = saldo_ret,
saldo_haber = saldo_hab,
saldo_debe = saldo_deb,
saldo_dia = saldo_d,
saldo_comisiones = saldo_com

where ci_rif = buscar and cod_status = 'A';

END;

#
# Procedure "FSOSEGSTPPRPANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPRPANL`;
CREATE PROCEDURE `FSOSEGSTPPRPANL`(in buscar varchar(20))
BEGIN
update t_producto_persona set cod_status = 'I' where ci_rif =buscar and cod_status ='A'; 
END;

#
# Procedure "FSOSEGSTPPRPCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPRPCON`;
CREATE PROCEDURE `FSOSEGSTPPRPCON`(in buscar varchar (20))
BEGIN
select * from t_producto_persona where ci_rif = buscar and cod_status = 'A'; 
END;

#
# Procedure "FSOSEGSTPPRPINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPPRPINS`;
CREATE PROCEDURE `FSOSEGSTPPRPINS`(in cod_Producto varchar (10),in cod_Persona varchar (20),
in ci_rif varchar (20),in saldo_intereses decimal(17,2),in saldo_rendimiento decimal(17,2),in saldo_prestamos decimal(17,2),
in saldo_anticipos decimal(17,2),in saldo_aportes decimal(17,2),in saldo_retiros decimal(17,2),
saldo_haber decimal(17,2),in saldo_debe decimal(17,2), in saldo_dia decimal(17,2),
in saldo_comisiones decimal(17,2))
BEGIN

insert into t_producto_persona
values (cod_Producto,cod_Persona,ci_rif,now(),now(),saldo_intereses,
saldo_rendimiento,saldo_prestamos,saldo_anticipos,saldo_aportes,saldo_retiros,saldo_haber,
saldo_debe,saldo_dia,saldo_comisiones,'A'); 

END;

#
# Procedure "FSOSEGSTPRENDIACONS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPRENDIACONS`;
CREATE PROCEDURE `FSOSEGSTPRENDIACONS`(in asigdia_bs decimal(17,2), in asigdia_usd decimal(17,2), 
in ren_bs decimal(17,2), in ren_usd decimal(17,2), in fec date)
BEGIN
 select * from t_rendimiento_diario  where fecha = fec;
END;

#
# Procedure "FSOSEGSTPRENDIAINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPRENDIAINS`;
CREATE PROCEDURE `FSOSEGSTPRENDIAINS`(in asigdia_bs decimal(17,2), in asigdia_usd decimal(17,2), 
in ren_bs decimal(17,2), in ren_usd decimal(17,2), in fec date)
BEGIN
insert into t_rendimiento_diario
values(asigdia_bs,asigdia_usd,ren_bs,ren_usd,fec,'Pendiente');
END;

#
# Procedure "FSOSEGSTPTPFCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPTPFCON`;
CREATE PROCEDURE `FSOSEGSTPTPFCON`(in buscar varchar(10))
BEGIN

select condicion,status_condicion FROM t_perfil WHERE cod_perfil=buscar;

END;

#
# Procedure "FSOSEGSTPTPTACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPTPTACT`;
CREATE PROCEDURE `FSOSEGSTPTPTACT`(in buscar varchar(20), in codigo_uni varchar(20), in cuen varchar(20), 
in des_tipo_tran varchar(100),in Ref varchar(20),in Debe_H varchar(1),
in clase_trans varchar(60))
BEGIN

update t_tipo_transaccion set 

codigo_unico         = codigo_uni,
cuenta               = cuen,
des_tipo_transaccion = des_tipo_tran,
Referencia           = Ref,
Debe_Haber           = Debe_H,
clase_transaccion    = clase_trans 

where cod_tipo_transaccion = buscar and cod_status = 'A';



END;

#
# Procedure "FSOSEGSTPTPTANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPTPTANL`;
CREATE PROCEDURE `FSOSEGSTPTPTANL`(in buscar varchar(20))
BEGIN
update  t_tipo_transaccion set cod_status = 'I' where cuenta = buscar and cod_status ='A';
END;

#
# Procedure "FSOSEGSTPTPTCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPTPTCON`;
CREATE PROCEDURE `FSOSEGSTPTPTCON`(in buscar varchar(20))
BEGIN

select codigo_unico,cuenta,des_tipo_transaccion,Referencia,Debe_Haber,clase_transaccion
from t_tipo_transaccion where cuenta = buscar and cod_status = 'A';

END;

#
# Procedure "FSOSEGSTPTPTCON2"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPTPTCON2`;
CREATE PROCEDURE `FSOSEGSTPTPTCON2`()
BEGIN
select codigo_unico,cuenta,des_tipo_transaccion,Referencia,Debe_Haber,clase_transaccion
from t_tipo_transaccion where cod_status = 'A'; 

END;

#
# Procedure "FSOSEGSTPTPTINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPTPTINS`;
CREATE PROCEDURE `FSOSEGSTPTPTINS`(in codigo_unico varchar(20), in cuen varchar(20), 
in des_tipo_transaccion varchar(100),in Referencia varchar(20),in Debe_Haber varchar(1),
in clase_transaccion varchar(20))
BEGIN

insert into t_tipo_transaccion 
values (codigo_unico,cuenta,des_tipo_transaccion,'A',Referencia,
Debe_Haber,clase_transaccion);

END;

#
# Procedure "FSOSEGSTPTRAACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPTRAACT`;
CREATE PROCEDURE `FSOSEGSTPTRAACT`(in buscar varchar(20),in cod_tipo_trans varchar(20),in cod_Prod varchar(10),
in cod_mon char(10),in cls_trans char(20),in monto_trans decimal (17,2),
in nro_cta_banco char(20),in mto_moneda_extr_trans decimal(17,2),in tasa_camb_trans decimal(17,2),
in nat_tran varchar(7),in descrip_comis_tran varchar(30),
in monto_comis_apli decimal(17,2),in monto_impuesto_deito_ban decimal(17,2),
in indicador_exoneracion_impuesto_deito_ban varchar(1),in cuenta_debito_tran varchar(20),
in cuenta_credito_tran varchar(20),in ci_rif_beneficiario_tran varchar(20),
in cod_oper_swift varchar(20),in cod_forma_p varchar(8))
BEGIN

update t_transacciones set

cod_tipo_transaccion                       = cod_tipo_trans,
cod_Producto                               = cod_Prod,
cod_moneda                                 = cod_mon,
clase_trans                                = cls_trans,
monto_transaccion                          = monto_trans,
nro_cta_banco                              = nro_cta_banco,
mto_moneda_extranjera_trans                = mto_moneda_extr_trans,
tasa_cambio_trans                          = tasa_camb_trans,
nat_trans                                  = nat_tran,
descrip_comis_trans                        = descrip_comis_tran,
monto_comis_aplicada                       = monto_comis_apli,
monto_impuesto_deito_banco                 = monto_impuesto_deito_ban,
indicador_exoneracion_impuesto_deito_banco = indicador_exoneracion_impuesto_deito_ban,
fecha_ultima_actualizacion                  = now(),
cuenta_debito_transferencia                = cuenta_debito_tran,
cuenta_credito_transferencia               = cuenta_credito_tran,
ci_rif_beneficiario_transferencia          = ci_rif_beneficiario_tran,
codigo_operador_swift                      = cod_oper_swift,
cod_forma_pago                             = cod_forma_p

where ci_rif = buscar and cod_status = 'A'; 

END;

#
# Procedure "FSOSEGSTPTRAANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPTRAANL`;
CREATE PROCEDURE `FSOSEGSTPTRAANL`(in buscar varchar(20))
BEGIN

update t_transacciones set cod_status = 'I' where ci_rif = buscar and cod_status = 'A';

END;

#
# Procedure "FSOSEGSTPTRACON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPTRACON`;
CREATE PROCEDURE `FSOSEGSTPTRACON`(in buscar varchar(20))
BEGIN

select nro_transaccion,cod_tipo_transaccion,cod_Producto,A.ci_rif,cod_moneda,
clase_trans,fecha_transaccion,fecha_registro_transaccion,fecha_proceso_transaccion,monto_transaccion,
nro_cta_banco,mto_moneda_extranjera_trans,tasa_cambio_trans,nat_trans,
descrip_comis_trans,monto_comis_aplicada,monto_impuesto_deito_banco,
indicador_exoneracion_impuesto_deito_banco,fecha_ultima_actualizacion,
cuenta_debito_transferencia,cuenta_credito_transferencia,
ci_rif_beneficiario_transferencia,codigo_operador_swift,
cod_forma_pago,B.nom_Persona1,B.nom_Persona2,B.ape_Persona1,B.ape_Persona2

from t_transacciones A, t_persona B 

where A.ci_rif = buscar and A.ci_rif = B.ci_rif  and A.cod_status = 'A';

END;

#
# Procedure "FSOSEGSTPTRAINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPTRAINS`;
CREATE PROCEDURE `FSOSEGSTPTRAINS`(
in cod_tipo_trans varchar(20),
in cod_Prod varchar(10),
in ci_r varchar(20),
in cod_mon char(10),
in fech_t date,
in monto_trans decimal (17,2),
in NRO_CTA_B char(20),
in mto_moneda_extr_trans decimal(17,2),
in tasa_camb_trans decimal(17,2),
in nat_tran varchar(7),
in descrip_comis_tran varchar(30),
in monto_comis_apli decimal(17,2),
in monto_impuesto_deito_ban decimal(17,2),
in indicador_exoneracion_impuesto_deito_ban varchar(1),
in cuenta_debito_tran varchar(20),
in cuenta_credito_tran varchar(20),
in ci_rif_beneficiario_tran varchar(20),
in cod_oper_swift varchar(20),
in cod_forma_p varchar(8))
BEGIN

insert into t_transacciones (cod_tipo_transaccion,cod_Producto,ci_rif,cod_moneda,
fecha_transaccion,fecha_registro_transaccion,fecha_proceso_transaccion,monto_transaccion,nro_cta_banco,
mto_moneda_extranjera_trans,tasa_cambio_trans,nat_trans,
descrip_comis_trans,monto_comis_aplicada,monto_impuesto_deito_banco,indicador_exoneracion_impuesto_deito_banco,
fecha_ultima_actualizacion,
cuenta_debito_transferencia,cuenta_credito_transferencia,ci_rif_beneficiario_transferencia,
codigo_operador_swift,cod_status,cod_forma_pago)

values(cod_tipo_trans,cod_Prod,ci_r,cod_mon,fech_t,now(),now(),monto_trans,
NRO_CTA_B,mto_moneda_extr_trans,tasa_camb_trans,nat_tran,descrip_comis_tran,
monto_comis_apli,monto_impuesto_deito_ban,indicador_exoneracion_impuesto_deito_ban,
now(),cuenta_debito_tran,cuenta_credito_tran,ci_rif_beneficiario_tran,
cod_oper_swift,'A',cod_forma_p); 

END;

#
# Procedure "FSOSEGSTPUSUACT"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPUSUACT`;
CREATE PROCEDURE `FSOSEGSTPUSUACT`(in buscar varchar(20),
in cod_user varchar(20),in cod_profile varchar(20),in nam_user varchar(45),
in cod_unity varchar(10),phone_user varchar(45),in username varchar(20),
in clave varchar(20),in dias_clave varchar(3),in correo varchar(70), in cod_estatus varchar(1))
BEGIN

update t_usuario set

cod_usuario          = cod_user,
cod_perfil           = cod_profile,
nom_usuario          = nam_user,
cod_unidad           = cod_unity,
telefono_usuario     = phone_user,
login                = username,
password             = clave,
dias_vigencia_clave  = dias_clave,
email                = correo,
fecha_ultimo_ingreso = now(),
cod_status           = cod_estatus

where cod_usuario = buscar; 

END;

#
# Procedure "FSOSEGSTPUSUANL"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPUSUANL`;
CREATE PROCEDURE `FSOSEGSTPUSUANL`(in buscar varchar(20))
BEGIN
update t_usuario set cod_status = 'I' where cod_usuario = buscar; 
END;

#
# Procedure "FSOSEGSTPUSUCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPUSUCON`;
CREATE PROCEDURE `FSOSEGSTPUSUCON`(in buscar varchar(20))
BEGIN
select * from t_usuario where cod_usuario=buscar;
END;

#
# Procedure "FSOSEGSTPUSUINS"
#

DROP PROCEDURE IF EXISTS `FSOSEGSTPUSUINS`;
CREATE PROCEDURE `FSOSEGSTPUSUINS`(in cod_user varchar(20),in cod_profile varchar(20),
in nam_user varchar(45),in cod_unity varchar(10),phone_user varchar(45),in username varchar(20),
in clave varchar(20),in dias_clave varchar(3),in correo varchar(70), in cod_estatus varchar(1))
BEGIN

insert into t_usuario
values(cod_user,cod_profile,nam_user,cod_unity,phone_user,username,clave,dias_clave,correo,
now(),now(),now(),cod_estatus);

END;

#
# Procedure "FSOSEGTIPTRANSCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGTIPTRANSCON`;
CREATE PROCEDURE `FSOSEGTIPTRANSCON`()
BEGIN
     select cod_tipo_oper,clase_transaccion from t_tipo_transaccion;
END;

#
# Procedure "FSOSEGTRANSPERCON"
#

DROP PROCEDURE IF EXISTS `FSOSEGTRANSPERCON`;
CREATE PROCEDURE `FSOSEGTRANSPERCON`(in fechaInicio date,in fechaFin date)
BEGIN
   select ci_rif,cod_producto,cod_moneda,nro_transaccion,clase_trans,fecha_transaccion,fecha_proceso_transaccion,fecha_registro_transaccion,cod_forma_pago,monto_transaccion,nro_cta_banco,cuenta_debito_transferencia,cuenta_credito_transferencia,cod_status
   from t_transacciones 
   where fecha_registro_transaccion between fechaInicio and fechaFin;
END;

#
# Procedure "new_procedure"
#

DROP PROCEDURE IF EXISTS `new_procedure`;
CREATE PROCEDURE `new_procedure`(
in cod_ct varchar(10),
in cod_pr varchar(10),
in monto decimal(17,2),
in fecha date
)
BEGIN
	insert into t_rendimiento values (cod_ct,cod_pr,monto,fecha,'Pendiente');
END;
