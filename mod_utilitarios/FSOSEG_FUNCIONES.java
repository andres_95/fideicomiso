package vista;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import com.bimodal.app.beans.Principal;
import com.bimodal.app.conf.GlobalConstants;
import com.bimodal.app.db.FSOSEG_CONNECT_MODULE;

public class FSOSEG_FUNCIONES {
    public static int getPermission(String cond) throws SQLException{
       String roll = GlobalConstants.getRol();
      
        FSOSEG_CONNECT_MODULE connection=GlobalConstants.getConnector();
       String SQL = "SELECT * FROM `t_perfil` WHERE cod_perfil='"+roll+"' AND condicion='"+cond+"'";
          
        ResultSet rs = connection.executeQuery(SQL);
        if (!rs.next() ) {
            return 0;
        }else{
            return 1;
        } 
    }
    
    public static int getPermissionPrueba(String cond) throws SQLException{
        return GlobalConstants.getPERFIL_USER();
    }
    
    public static void setMessagePerm(){
        String msg = "Estimado usuario. No posees permiso para realizar esta operación";
        JOptionPane.showMessageDialog(null, msg, "Error", JOptionPane.ERROR_MESSAGE);
    }
    
    public static void iniciarFrame(JFrame frame, String tit){
       frame=new JFrame();
       frame.setVisible(true);
       frame.setTitle(tit);
    }
}
